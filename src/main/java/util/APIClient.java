package util;

import base.BaseTest;
import enums.HttpMethod;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.config.LogConfig;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.swagger.annotations.Authorization;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import static enums.HttpMethod.*;


//Base client for all the API actions
public class APIClient extends BaseTest {

    private static final Logger logger= LogManager.getLogger(APIClient.class);
    private static final ToLoggerPrintStream loggerPrintStream = new ToLoggerPrintStream(logger);

    public static Response getResponse(HttpMethod requestType, Authorization authorization, String route){
        return getResponse(requestType,authorization,route,null,null);
    }

    public static Response  getResponseForPathParams(HttpMethod requestType, Authorization authorization, String route, Map<String,Object> pParams){
        return getResponse(requestType,authorization,route,pParams,null);
    }

    public static Response getResponseForQParams(HttpMethod requestType, Authorization authorization, String route, Map<String,Object> qParams){
        return getResponse(requestType,authorization,route,null,qParams);
    }

    public static Response getResponse(HttpMethod requestType, Authorization authorization, String route, Map<String,Object> pParams, Map<String,Object> qParams){
        MandirRequestSpecification requestSpecification = MandirRequestSpecification.builder()
                .baseUrl(route)
                .pathParams(pParams)
                .queryParams(qParams)
                .authorization(authorization)
                .build();
        return executeRequest(requestType,requestSpecification);
    }

    //The responsibility of the Impl is to pass the header type  as a value in the request specifaction
    public static io.restassured.response.Response executeRequest(HttpMethod requestType, MandirRequestSpecification requestSpecification){
        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
        //defaulting merchant type to merchant fee bearer in case of merchant type not passed in request specification
//         merchantType = requestSpecification.getMerchantType() != null  ? requestSpecification.getMerchantType() : MerchantType.MERCHANT_FEE_BEARER;
        //default values
        String userName = "";
        String password = "";
        Map<String ,String> headers = new HashMap<String, String>();
        if(requestSpecification.getHeaders()!=null && !requestSpecification.getHeaders().isEmpty()){
            headers.putAll(requestSpecification.getHeaders());
        }
        if(requestSpecification.getQueryParams()!=null && !requestSpecification.getQueryParams().isEmpty()){
            requestSpecBuilder.addQueryParams(requestSpecification.getQueryParams());
        }
        if(requestSpecification.getContentType()!=null){
            requestSpecBuilder.setContentType(requestSpecification.getContentType());
        }
        if(requestSpecification.getContent()!=null){
            requestSpecBuilder.setContentType(requestSpecification.getContent());
        }
        if(requestSpecification.getPathParams()!=null &&!requestSpecification.getPathParams().isEmpty()){
            requestSpecBuilder.addPathParams(requestSpecification.getPathParams());
        }

        String defaultVersion = "2";
        if(requestSpecification.getVersion()!=null){
            defaultVersion = requestSpecification.getVersion();
        }
//        System.out.println(Constants.PASSWORD);
//        System.out.println(Constants.USER);
        if(requestSpecification.getAuthorization()!=null){
            //defaulting to test mode if the mode is not passed by impl

            headers.put("App-Type",Constants.APP_TYPE);
            headers.put("Content-Type","application/json");
            headers.put("App-Version",Constants.APP_VERSION);
            headers.put("Device-ID",Constants.DEVICE_ID);
            headers.put("X-AFB-R-UID",Constants.USER_ID);
            headers.put("Accept-Language",Constants.DEFAULT_LANGUAGE);

        }
        if(requestSpecification.getUserName() != null){
            userName = requestSpecification.getUserName();
        }
        if(requestSpecification.getPassword() != null){
            password = requestSpecification.getPassword();
        }
        if(!headers.isEmpty()) {
            requestSpecBuilder.addHeaders(headers);
        }

        if(requestSpecification.getBody() != null){
            requestSpecBuilder.setBody(requestSpecification.getBody());
        }

        if(requestSpecification.getCookies()!=null){
            requestSpecBuilder.addCookies(requestSpecification.getCookies());
        }
        if(requestSpecification.getAccept()!=null){
            requestSpecBuilder.setAccept(requestSpecification.getAccept());
        }
        if(requestSpecification.getMultiPartSpecs()!= null){
            MandirMultiPartSpecBuilder multiPart = requestSpecification.getMultiPartSpecs();
            requestSpecBuilder.addMultiPart(multiPart.getControlName(),new File(multiPart.getFileName()),multiPart.getMimeType().toString());
        }
        if(requestSpecification.getFormParams() != null){
            requestSpecBuilder.addFormParams(requestSpecification.getFormParams());
        }
        RequestSpecification requestSpec;
        if(userName == null  || userName.equals("")){
            requestSpec = requestSpecBuilder.build().log().all();
        } else {
            requestSpec = requestSpecBuilder.build().log().all().auth().preemptive().basic(userName, password);
        }
        //Build Specification with auth
//            RequestSpecification requestSpec = requestSpecBuilder.build().log().all().auth().preemptive().basic(userName,password);
        System.out.println(headers);
        return execute(requestSpecification.getBaseUrl(),requestType,requestSpec);
    }

    private static Response execute(String baseUrl, HttpMethod requestType , RequestSpecification specification){
        Response response = null;
        RestAssured.config = RestAssured.config().logConfig(new LogConfig( loggerPrintStream.getPrintStream(), true ) );
        try {
            switch (requestType) {
                case GET:
                    response = RestAssured.given().spec(specification).get(baseUrl).then().log().all().extract().response();
                    break;
                case POST:
                    response = RestAssured.given().spec(specification).post(baseUrl).then().log().all().extract().response();
                    break;
                case PUT:
                    response = RestAssured.given().spec(specification).put(baseUrl).then().log().all().extract().response();
                    break;
                case DELETE:
                    response = RestAssured.given().spec(specification).delete(baseUrl).then().log().all().extract().response();
                    break;
                case PATCH:
                    response = RestAssured.given().spec(specification).patch(baseUrl).then().log().all().extract().response();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        //logger.info("Response Time : "+response.getTime() + " Response : "+response.asString());
        return response;
    }
}

