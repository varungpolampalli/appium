package util;

import io.restassured.response.Response;

public class APIAssertionException extends Exception{
    private static final long serialVersionUID = 1L;

    private Response response ;
    /**
     * Constructs a APIAssertionException with no detail message. A detail
     * message is a String that describes this particular exception.
     */
    public APIAssertionException() {
        super();
    }

    /**
     * Constructs a APIAssertionException with the specified detail message.
     * A detail message is a String that describes this particular
     * exception.
     *
     * @param msg the detail message.
     */
    public APIAssertionException(String msg) {
        super(msg);
    }
    /**
     * Constructs a APIAssertionException with the specified detail message and the  response.
     * A detail message is a String that describes this particular
     * exception
     * @param msg the detail message.
     * @param response the failing response to be stored
     */
    public APIAssertionException(String msg, Response response){
        super(msg);
        this.response = response;
    }

    public Response getResponse() {
        return response;
    }

}



