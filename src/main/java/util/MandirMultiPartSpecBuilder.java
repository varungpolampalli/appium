package util;

import enums.ContentType;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MandirMultiPartSpecBuilder {
    //the body of the payload
    private Object content;
    //name of the field
    private String controlName;
    private ContentType mimeType;
    private String charset;
    private String fileName;

}
