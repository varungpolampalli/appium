package util;

import enums.ContentType;
import enums.HttpMethod;

import com.jayway.jsonpath.JsonPath;

import io.restassured.response.Response;
import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;



public class APIImpl extends APIClient{

    public static String WORD_CLOUD_FILENAME = "wordCloud.png";
    private static final Logger logger = Logger.getLogger(APIImpl.class.getName());
    //Move to URL Mapper if more use cases found
    private static final String GET_LATEST_LAUNCH = "/launch/latest" ;
    private static final String LAUNCH = "/launch";
    private static final String API_VERSION = "/api/v1/";
    private static String baseURL;
    private static String  project;
    private static  String UUID;
    private static Map<String,String> headers;

    public static final String BASE_API = "https://slack.com/api";

    public static final String SLACK_FILE_UPLOAD = "/files.upload";

    public static String uploadFileToSlack(String fileName) throws APIAssertionException, FileNotFoundException {
        Map<String,String> formParams = new HashMap<>();
        formParams.put("token", Constants.SLACK_HOOK);
        formParams.put("channels","#tech_qa_reports");
        MandirRequestSpecification requestSpecification = MandirRequestSpecification.builder()
                .baseUrl(BASE_API+SLACK_FILE_UPLOAD)
                .contentType(ContentType.MULTIPART_FORM_DATA.toString())
                .formParams(formParams)
                .multiPartSpecs(MandirMultiPartSpecBuilder.builder().controlName("file").fileName(fileName).mimeType(ContentType.IMAGE_PNG).build())
                .build();
        Response response = APIClient.executeRequest(HttpMethod.POST,requestSpecification);
        if(response!= null)
            return response.path("file.url_private");
        throw new APIAssertionException("The response is null");
    }

    static {
        try {
            Properties props = new Properties();
            FileInputStream propFile = new FileInputStream("src/main/java/resources/reportportal.properties");
            props.load(propFile);
            baseURL = props.getProperty("rp.endpoint");
            project= props.getProperty("rp.project");
            UUID= props.getProperty("rp.uuid");
            headers = new HashMap<>();
            headers.put("Authorization","bearer "+UUID);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    //latest launch id can give worng ids if 2 jobs are running ,add a fix to this later
    public static String getLatestLaunchId() throws IOException, APIAssertionException {
        Map<String,Object> qParams = new HashMap<>();
        qParams.put("page.sort","startTime,DESC");
        MandirRequestSpecification requestSpecification = MandirRequestSpecification.builder()
                .queryParams(qParams)
                .baseUrl(baseURL+API_VERSION+project+GET_LATEST_LAUNCH)
                .headers(headers)
                .contentType("application/json")
                .build();
        Response response = executeRequest(HttpMethod.GET,requestSpecification);
        logger.info("Report portal response :"+response.body().print());
        assertApiResponse(response,200);
        return JsonPath.read(response.getBody().print(),"$.content.[0].id");
    }

    public static String getLatestLaunchForFilter(String launchName) throws APIAssertionException {
        Map<String,Object> qParams = new HashMap<>();
        qParams.put("page.sort","startTime,DESC");
        qParams.put("filter.cnt.name",launchName);
        MandirRequestSpecification requestSpecification = MandirRequestSpecification.builder()
                .queryParams(qParams)
                .baseUrl(baseURL+API_VERSION+project+LAUNCH)
                .headers(headers)
                .contentType("application/json")
                .build();
        Response response = executeRequest(HttpMethod.GET,requestSpecification);
        logger.info("Report portal response :"+response.body().print());
        assertApiResponse(response,200);
        Integer post_id = JsonPath.read(response.getBody().print(),"$.content.[0].id");
        return Integer.toString(post_id);
    }

    public static void assertApiResponse(Response response , int expectedStatus) throws APIAssertionException {
        assertApiResponse(response,expectedStatus,null);
    }

    public static void assertApiResponse(Response response , int expectedStatus , String failureMsg) throws APIAssertionException {
        if(response.getStatusCode() != expectedStatus){
            String errMsg = "API Response Failed, Status_Code: "+response.getStatusCode() + response.getBody().prettyPrint();
            if(failureMsg != null)
                errMsg = errMsg + " ,Error: " + failureMsg;
            throw new APIAssertionException(errMsg);

        }
    }
}
