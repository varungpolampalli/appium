package util;

import io.swagger.annotations.Authorization;
import lombok.Builder;
import lombok.Data;

import java.util.Map;

@Data
@Builder
public class MandirRequestSpecification {
    private String contentType;
    private String baseUrl;
    private String accept;
    private String content;
    private Map<String,String> headers;
    private Map<String,Object> queryParams;
    private Map<String,Object> pathParams;
    private Authorization authorization;
    private Object body;
    private Map<String,Object> cookies;
    //require these values for OAuth as the user name and password changes per oauth regeneration and cant be taken care by Authorization
    private String userName;
    private String password;
    private String filePath;
    private MandirMultiPartSpecBuilder multiPartSpecs;
    private Map<String,String> formParams;

    private String version;

}

