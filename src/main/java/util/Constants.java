package util;

public class Constants {
    public static String API_URL = "https://api-sbox.a4b.io";
    public static  String USER_ID = "7001495";
    public static  String APP_TYPE = "Android";
    public static  String APP_VERSION = "160";
    public static  String  DEFAULT_LANGUAGE = "hi";
    public static  String DEVICE_ID = "ff0d08a1-68bc-4199-b9c9-10ca1ad1abe1";
    public static String REPORT_PORTAL_URL = "http://localhost:8080/ui/#superadmin_personal/launches/all/";
    public static  String SLACK_HOOK = "T01LEES1M7T/B03TBS7FJVB/zjpdPnqwEgOt93FGoaKKOJy5";
}
