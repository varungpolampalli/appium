package util;

import com.epam.reportportal.testng.ReportPortalTestNGListener;
import org.testng.ISuite;
import org.testng.ITestResult;


public class ReportPortalListener extends ReportPortalTestNGListener {

    //blank methods indicate no action to be taken
    public ReportPortalListener(){
        super();
    }

    //thread local listener is initialized for suites to run in parallel for testng.xml as Testng creates seperate thread for each suite
    private final ThreadLocal<ReportPortalTestNGListener> testListener = new ThreadLocal<>();
    @Override
    public void beforeConfiguration(ITestResult testResult) {

    }

    @Override
    public void onConfigurationFailure(ITestResult testResult) {

    }

    @Override
    public void onConfigurationSuccess(ITestResult testResult) {

    }

    @Override
    public void onConfigurationSkip(ITestResult testResult) {

    }

    //handling error messages to continue ,failure would be
    @Override
    public void onTestSuccess(ITestResult testResult) {
        try {
            super.onTestSuccess(testResult);
        }catch (Exception e){
            System.out.println("Error on test success continuing"+e.getMessage());
        }
    }

    @Override
    public void onTestFailure(ITestResult testResult) {
        try {
            super.onTestFailure(testResult);
        } catch (Exception e){
            System.out.println("Error on test failure , continuing"+e.getMessage());
        }
    }

    @Override
    public void onTestSkipped(ITestResult testResult) {
        try {
            super.onTestSkipped(testResult);
        }catch (Exception e){
            System.out.println("Error on test skipped ,continuing"+e.getMessage());
        }
    }

    //overriding and calling the parent class explicitly as testng will not initialize listener class when suites are run parallely
    @Override
    public void onStart(ISuite suite){
        try{
            testListener.set(new ReportPortalTestNGListener());
            testListener.get().onStart(suite);
        }catch (Exception e){
            System.out.println("Error on suite start"+e.getMessage());
        }
    }

    @Override
    public void onFinish(ISuite suite){
        try {
            testListener.get().onFinish(suite);
        }catch (Exception e){
            System.out.println("Error on suite stop"+e.getMessage());
        }
    }

}

