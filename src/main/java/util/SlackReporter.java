package util;

import com.jayway.jsonpath.JsonPath;
import enums.HttpMethod;
import io.restassured.response.Response;
import net.gpedro.integrations.slack.SlackApi;
import net.gpedro.integrations.slack.SlackAttachment;
import net.gpedro.integrations.slack.SlackMessage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.IReporter;
import org.testng.ISuite;
import org.testng.ITestResult;
import org.testng.collections.Lists;
import org.testng.xml.XmlSuite;
import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import static util.Constants.REPORT_PORTAL_URL;


public class SlackReporter implements IReporter {
    static Map<String,String> envMode= new HashMap<>();
    static Map<String,String> testMode = new HashMap<>();
    private static final Logger logger = LogManager.getLogger(SlackReporter.class);


    static {
        envMode.put("Sandbox","Func QA ");
    }
    private final SlackApi slackApi ;
    private String suiteName;
    private final List<EmailableReport.SuiteResult> suiteResults = Lists.newArrayList();
    String reportChannel = "sm-api-test-report";

    public SlackReporter() {
        slackApi = new SlackApi("https://hooks.slack.com/services/"+ Constants.SLACK_HOOK);
    }

    public SlackAttachment getSlackAttachment(String fileName) {
        SlackAttachment attachment = new SlackAttachment();
        try{
            String url = APIImpl.uploadFileToSlack("/app/"+fileName);
            attachment.setImageUrl(url) ;
//        }catch (APIAssertionException ){
//            logger.info("API exception in uploading the attachment");
        } catch (FileNotFoundException | APIAssertionException e) {
            logger.info("Unable to upload file to slack ,file not found error");
        }
        attachment.setFallback("Error Screen shot");
        return attachment;
    }
    public static Integer getRandomNoInRange(int origin,int bound){
        return ThreadLocalRandom.current().nextInt(origin,bound);
    }


    @Override
    public void generateReport(List<XmlSuite> xmlSuites , List<ISuite> suites, String s) {
        for ( ISuite suite : suites ) {
            suiteResults.add( new EmailableReport.SuiteResult( suite ) );
        }
        try {
            postToSlack();
        } catch (Exception e) {
            logger.info("Unable to post results to slack ,continuing the execution");
            e.printStackTrace();
            logger.info("Stack Trace: "+e.getMessage());
        }
    }

    private String rightPad(String text, int leng) {
        for (int i = text.length(); i <= leng; i++)
            text += " ";
        return text;
    }

    private void postToSlack() throws Exception {
        String reportName = "Sri_Mandir_UI_Automation";
        String channel = reportChannel;
        int passedTests = 0;
        int skippedTests =0;
        int failedTests = 0;
        StringBuilder failedTestNames= new StringBuilder();
        //removed usage as of now
        failedTestNames.append(" * Failed Tests :*\n");
        float totalDuration=0;
        for ( EmailableReport.SuiteResult suiteResult : suiteResults ) {
            for ( EmailableReport.TestResult testResult : suiteResult.getTestResults() ) {
                totalDuration = totalDuration + testResult.getDuration();
                List<EmailableReport.ClassResult> failures = testResult.getFailedTestResults();
                for ( EmailableReport.ClassResult classResult : failures ) {
                    for ( EmailableReport.MethodResult methodResult : classResult.getMethodResults() ){
                        List<ITestResult> results = methodResult.getResults();
                        ITestResult aResult = results.iterator().next();
                        failedTestNames.append(rightPad("*Name ",15)+":* "+aResult.getMethod().getMethodName()+"\n");
                        //failedTestNames.append(rightPad("*Description",15)+":* "+aResult.getMethod().getDescription()+"\n");
                    }
                }
                passedTests = passedTests + testResult.getPassedTestCount();
                skippedTests = skippedTests + testResult.getSkippedTestCount();
                failedTests = failedTests + testResult.getFailedTestCount();
                if (failedTests==0){
                    failedTestNames=new StringBuilder();
                }
            }
        }
        String titleName = reportName != null ? reportName : envMode.get(System.getProperty("ENV"))+" "+testMode.get(System.getProperty("MODE"));
        StringBuilder messageBuilder  = new StringBuilder();
        messageBuilder.append("*Automation Run Result - "+titleName+"* \n Duration :"+totalDuration/1000+" secs \n Passed : "+passedTests+"\n Failed: "+failedTests +"\n Skipped:"+skippedTests+"\n");
        try {
            messageBuilder.append("\n *Report Portal URL *: \n"+ REPORT_PORTAL_URL+ APIImpl.getLatestLaunchForFilter(reportName));
        } catch (Exception e) {
            messageBuilder.append("\n Unable to generate Report Portal URL");
            e.printStackTrace();
        }
        SlackAttachment attachment = new SlackAttachment();
        if(failedTests == 0 && skippedTests == 0 && passedTests !=0 ) {
            try {
                attachment.setImageUrl(generateGIF());
            }catch (Exception e){
                System.out.println("Exception in GIF continuing");
            }
            attachment.setTitle("YIPEE ,All test Passed");
        }else{
//            String testMode = System.getProperty("MODE");
            //currently handled in indiviual test code
            //check to post all prod related failures to tech_alerts
//            if(prodSuites.contains(testMode))
//                channel = prodChannel;
            attachment.setImageUrl(generateGChart(passedTests,failedTests,skippedTests));
            attachment.setTitle("Automation Results Chart : ");
//            attachment.setTitle("Automation Results Chart : "+System.getProperty("ENV")+"_"+System.getProperty("MODE"));
        }
        attachment.setFallback("Results for the automation run <Run Details>");
        slackBot(messageBuilder.toString(), Collections.singletonList(attachment),channel);
    }

    private static String generateGIF() {
        String gifURL = "https://api.tenor.com/v1/search?key=LIVDSRZULELA&q=minions dance&limit=15";
        Response response =  APIClient.getResponse(HttpMethod.GET,null,gifURL);
        return JsonPath.read(response.getBody().print(),"$.results["+ getRandomNoInRange(0, 9) +"].media[0].tinygif.url");
    }

    public void slackBot(String messageTxt,List<SlackAttachment> slackAttachments,String channel) {
        SlackMessage message = new SlackMessage();
        message.setChannel(channel);
        message.setAttachments(slackAttachments);
        message.setText(messageTxt);
        slackApi.call(message);
    }

    private String generateGChart(int passed, int failed, int skipped) {
        DecimalFormat df2 = new DecimalFormat();
        df2.setMaximumFractionDigits(0);
        int total=passed+failed+skipped;
        if(total==0){
            total=-1;
        }
        float passedP=(passed*100)/total;
        float failedP=(failed*100)/total;
        float skippedP=(skipped*100)/total;
        String urlBegin = "https://chart.apis.google.com/chart?cht=p&chs=300x150&chdl=Passed|Failed|Skipped&chl=%s|%s|%s&chco=2eb82e|ff4d4d|ffd27f&chts=000000,24&chd=t:%s,%s,%s&chdls=20&chds=a";
        String chartUrl = String.format(urlBegin,df2.format(passedP)+"%",df2.format(failedP)+"%",df2.format(skippedP)+"%",passed,failed,skipped);
        chartUrl = chartUrl.replaceAll(" ", "%20");
        return chartUrl;
    }
}

