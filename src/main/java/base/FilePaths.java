package base;

public interface FilePaths {

	String ABSOLUTEPATH=System.getProperty("user.dir");
	String APPDATA=System.getenv("APPDATA");
	String EXCELDATA_ENGLISH="./src/test/TestData/SriMandirTestData_English.xlsx";
	String EXCELDATA_HINDI="./src/test/TestData/SriMandirTestData_Hindi.xlsx";
	String PROPERTYFILE="./src/test/TestData/TestData.properties";
	String LOG4J_PROPERTIES="./src/main/java/resources/log4j.properties";
	String APPIUM_SERVER_IP="127.0.0.1";

//	String APPIUM_JS_FILE="/opt/homebrew/bin/appium";
//	String APPIUM_NODEJS="/opt/homebrew/bin/node";
//	String SRIMANDIR_APK="./src/main/java/resources/app-debug-4.1.0.apk";

	String APPIUM_JS_FILE=APPDATA+"\\npm\\node_modules\\appium\\build\\lib\\main.js";
	String APPIUM_NODEJS=ABSOLUTEPATH+"/resourse/node.exe";
	String SRIMANDIR_APK="./src/main/java/resources/app-debug-4.1.0.apk";

	String DEVICEFILEPATH="/sdcard/DCIM/screenshot.png";
	String PROFILEIMAGEPATH="./src/test/Images/ProfileImage.png";
}
