package base;

import java.util.Arrays;
import java.util.List;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

import TestAnnotations.TestInfo;
/**
 * 
 * @author TestYantra
 *
 */
public class ListenerImplimentationclass implements ITestListener {

	private static ExtentTest test;
	public static ExtentTest testLog;
	private static ExtentReports report;
	public static String newCycleKey;

	@Override
	public void onTestStart(ITestResult result) {
		test=report.createTest(result.getMethod().getMethodName());
		testLog=test;

		test.assignAuthor("TestYantra");
		test.assignCategory("SriMandir Automation");
	}
	@Override
	public void onTestSuccess(ITestResult result) {
		test.pass(result.getMethod().getMethodName()+"passed");
		test.pass(result.getThrowable());
		String testId = result.getMethod().getConstructorOrMethod().getMethod().getAnnotation(TestInfo.class).testcaseID();
		List<String> Id=Arrays.asList(testId.split(","));
		AIOAppium.markCaseStatus(AIOAppium.projectKey, newCycleKey, Id, "Passed");

	}
	@Override
	public void onTestFailure(ITestResult result) {
		test.fail(result.getMethod().getMethodName()+"test failed");
		test.fail(result.getThrowable());
		AppGenericLib lib=new AppGenericLib();
		String path =lib.TakesScreenShotInBase64(BaseTest.listnerDriver);
		test.addScreenCaptureFromBase64String(path, result.getMethod().getMethodName());
		String testId = result.getMethod().getConstructorOrMethod().getMethod().getAnnotation(TestInfo.class).testcaseID();
		List<String> Id=Arrays.asList(testId.split(","));
		AIOAppium.markCaseStatus(AIOAppium.projectKey, newCycleKey, Id, "Failed");
	}
	@Override
	public void onTestSkipped(ITestResult result) {
		test.skip(result.getMethod().getMethodName()+"test skipped");
		test.pass(result.getMethod().getConstructorOrMethod().getMethod().getAnnotation(TestInfo.class).testcaseID());
		String testId = result.getMethod().getConstructorOrMethod().getMethod().getAnnotation(TestInfo.class).testcaseID();
		List<String> Id=Arrays.asList(testId.split(","));
		AIOAppium.markCaseStatus(AIOAppium.projectKey, newCycleKey, Id, "Not Run");
	}
	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
	}
	@Override
	public void onTestFailedWithTimeout(ITestResult result) {
	}
	@Override
	public void onStart(ITestContext context) {
		ExtentSparkReporter spark=new ExtentSparkReporter("./emailable-report/extent-reports"+context.getClass().getSimpleName());
		spark.config().setDocumentTitle("SRI_MANDIR");
		spark.config().setTheme(Theme.DARK);
		spark.config().setReportName("Sri Mandir Test Report");

		report=new ExtentReports();
		report.attachReporter(spark);
		report.setSystemInfo("Device Name", "Nexus5X");
		report.setSystemInfo("Platform", "Android");
		report.setSystemInfo("Platform Version", "12.0.0");

		AIOAppium.setupAIORestAssuredConfig();
		newCycleKey = AIOAppium.createCycle(AIOAppium.projectKey);
	}
	@Override
	public void onFinish(ITestContext context) {
		report.flush();
	}

}
