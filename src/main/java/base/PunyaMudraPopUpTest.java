package base;

import org.testng.annotations.Test;

import Pages.PunyamudraPopUpPage;
import io.appium.java_client.AppiumDriver;

public class PunyaMudraPopUpTest extends AppGenericLib{

	@Test
	public static void handlePunyaMudraPopUp(AppiumDriver driver)  {
		PunyamudraPopUpPage punyaMudraPopUpPage=new PunyamudraPopUpPage(driver);
		try {
//			ListenerImplimentationclass.testLog.info("checking if punya mudra pop up is displayed");
			while(punyaMudraPopUpPage.isPunyaMudraBottomSheetDisplayed()) {
				punyaMudraPopUpPage.clickOnPraptKareBtn();
			}
		}catch(Exception e) {
//			ListenerImplimentationclass.testLog.info("punya mudra pop up is not displayed");
			System.out.println("punya mudra pop up not found");
		}
	}

}
