package base;

public interface UITestData {
	String pholkiladhiFlowerText="फूल की लड़ी";
	String ghondaFlowerText="गेंदा";
	String bhelapatriFlowerText="बेलपत्र";
	String mograFlowerText="मोगरा";
	String gulhalFlowerText="गुड़हल";
	String kamalFlowerText="कमल";
	String champaFlowerText="चम्पा";
	String gulabhaFlowerText="गुलाब";
	String dhaturaFlowerText="धतूरा";
	String PalaashFlowerText="पलाश";
	String ashokFlowerText="अशोक";
    String paarijaatFlowerText="पारिजात";
    String kovidaarFlowerText="कोविदार";
    String NilakamalFlowerText="नीलकमल";
}
