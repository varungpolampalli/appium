
package base;

import java.io.IOException;
import java.net.MalformedURLException;

import org.openqa.selenium.Platform;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;

import Pages.GodSelectionPage;
import Pages.LanguageSelectionPage;
import Pages.MandirPage;
import Pages.PunyamudraPopUpPage;
import io.appium.java_client.AppiumDriver;
/**
 * 
 * @author TestYantra
 *
 */
public class BaseTest extends AppGenericLib 
{

	public static  AppiumDriver listnerDriver;
	@BeforeSuite(alwaysRun = true)
	public void configBS() 
	{
		logger();
	}

	@BeforeClass
	public void configBC()
	{
		//		String language = System.getProperty("language");
		//		ExcelUtility.initializeExcelFile(language);
		//		System.out.println(language);
//		ExcelUtility.initializeExcelFile(FileUtility.getPropertyValue("language"));
	}
	
	@Parameters("udid")
	@BeforeMethod(alwaysRun = true)
	public void launchApp(ITestResult result,String udid) throws MalformedURLException
	{
		ExcelUtility.initializeExcelFile(FileUtility.getPropertyValue("language"));
		startAppiumServer(setupAppiumServer( result.getMethod().getMethodName()));
		driver = launchDriver(AppiumService.getUrl(), Platform.ANDROID,udid);
		

		listnerDriver=driver;
//		ListenerImplimentationclass.testLog.info("Application Opened");
		GodSelectionPage godSelectionPage=new GodSelectionPage(driver);
		MandirPage mandirPage=new MandirPage(driver);
		LanguageSelectionPage languageSelectionPage = new LanguageSelectionPage(driver);
		PunyamudraPopUpPage punyamudraPopUpPage=new PunyamudraPopUpPage(driver);
		
		try{
			if(languageSelectionPage.isLanguagescreenIsDisplayed()){
				languageSelectionPage.selectHindi();
			}
		} catch (Exception e) {
			System.err.println("Langauge selection page is not found");
			logger.info("Langauge selection page is not found");
		}
		
//		try {
//			if(godSelectionPage.isMandirStapithKareBtnDisplayed()) 
//			{
//				godSelectionPage.clickOnGodSelectionSaveBtn();
//				for (int i = 0; i < 3; i++) 
//				{
//					mandirPage.clickOnNextBtn();
////					mandirPage.tapAnywhereOnScreen();
//				}
//			}
//		}catch(Exception e) {
//			System.err.println("on boarding tutorial not found");
//			logger.info("on boarding tutorial not found");
//		}
		
		try {
			for (int i = 0; i < 3; i++) 
				{
					mandirPage.clickOnNextBtn();
					mandirPage.tapAnywhereOnScreen();
				}
		}catch (Exception e) {
			System.err.println("on boarding tutorial not found");
			logger.info("on boarding tutorial not found");
		}
		
		PunyaMudraPopUpTest.handlePunyaMudraPopUp(driver);

		try {
			if(mandirPage.isSecondTimeOnboardingTutorialDisplayed()) 
			{
				mandirPage.tapAnywhereOnScreen();
				if(mandirPage.isSecondTimeOnboardingTutorialDisplayed())
				{
					mandirPage.tapAnywhereOnScreen();
				}
			}
		} catch (Exception e) 
		{
			System.err.println("second time on boarding tutorial not found");
			logger.info("second time on boarding tutorial not found");
		}
		try {
			if(mandirPage.isDiaToolTipDisplayedForFirstAppOpenForDay()) {
				mandirPage.clickOnDiaThali();
			}
			
		} catch (Exception e) 
		{
			System.err.println("Dia tool tip for first app open for the day not found");
			logger.info("second time on boarding tutorial not found");
		}
		PunyaMudraPopUpTest.handlePunyaMudraPopUp(driver);
	}
	
		
	
	
	@AfterMethod(alwaysRun = true)
	public void closeApp(ITestResult result) throws IOException
	{

		if (result.getStatus() == ITestResult.SUCCESS)
		{
			if (driver != null) 
			{
				driver.closeApp();
				ListenerImplimentationclass.testLog.info("Application closed");
			}

		} 
		else if (result.getStatus() == ITestResult.FAILURE) 
		{
			if (driver != null) 
			{

				driver.closeApp();
				ListenerImplimentationclass.testLog.info("Application closed");
			}

		} 
		else if (result.getStatus() == ITestResult.SKIP)
		{
			if (driver != null) 
			{
				driver.closeApp();
				ListenerImplimentationclass.testLog.info("Application closed");
			}

		}
		stopAppiumServer();
	}

	@AfterClass(alwaysRun = true)
	public void configAC() {

	}

	@AfterSuite(alwaysRun = true)
	public void configAS() {

	}

}
