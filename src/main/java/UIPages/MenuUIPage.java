package UIPages;

import Pages.LoginBottomSheetPage;
import Pages.MenuPage;
import TestAnnotations.StepInfo;
import base.*;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MenuUIPage extends AppGenericLib {
    public ElementUtil elementUtil;
    public MenuUIPage(AppiumDriver driver)
    {
        this.driver=driver;
        elementUtil=new ElementUtil(driver);
        PageFactory.initElements(driver, this);
    }
    @FindBy(id="com.mandir.debug:id/iv_user")
    private WebElement profileImg;

    @FindBy(id="com.mandir.debug:id/loginSubtitle")
    private WebElement logInSubTitle;

    @FindBy(id="com.mandir.debug:id/tv_login")
    private WebElement logInBtn;

    @FindBy(id="com.mandir.debug:id/iv_chevron")
    private WebElement cheveron;

    @FindBy(id="com.mandir.debug:id/card_cancel")
    private WebElement cancelBtn;

    @FindBy(id="com.mandir.debug:id/yes_logout")
    private WebElement yesBtnFromLogoutPopUp;

    private WebElement getGeethShlokTab(String geethShlokText) {
        String GeethShlokTextxpathValue="//android.widget.TextView[contains(@text,'"+geethShlokText+"')]";
        return elementUtil.getElement("xpath", GeethShlokTextxpathValue);
    }

    private WebElement getAppUpdateKareTab(String appUpdateKareText) {
        String appUpdateKareTextxpathValue="//android.widget.TextView[contains(@text,'"+appUpdateKareText+"')]";
        return elementUtil.getElement("xpath", appUpdateKareTextxpathValue);
    }

    private WebElement getAppShareKareTab(String appShareKareText) {
        String appShareKareTextxpathValue="//android.widget.TextView[contains(@text,'"+appShareKareText+"')]";
        return elementUtil.getElement("xpath", appShareKareTextxpathValue);
    }

    private WebElement getAppRateKareTab(String appRateKareText) {
        String appRateKareTextxpathValue="//android.widget.TextView[contains(@text,'"+appRateKareText+"')]";
        return elementUtil.getElement("xpath", appRateKareTextxpathValue);
    }

    private WebElement getTermsAndConditionsTab(String termsAndConditionsText) {
        String termsAndConditionsTextxpathValue="//android.widget.TextView[contains(@text,'"+termsAndConditionsText+"')]";
        return elementUtil.getElement("xpath", termsAndConditionsTextxpathValue);
    }

    private WebElement getPrivacyPolicyTab(String privacyPolicyText) {
        String privacyPolicyTextxpathValue="//android.widget.TextView[contains(@text,'"+privacyPolicyText+"')]";
        return elementUtil.getElement("xpath", privacyPolicyTextxpathValue);
    }

    private WebElement getYourSuggestionsTab(String apneSujavBhejeText) {
        String apneSujavBhejeTextxpathValue="//android.widget.TextView[contains(@text,'"+apneSujavBhejeText+"')]";
        return elementUtil.getElement("xpath", apneSujavBhejeTextxpathValue);
    }

    private WebElement getSahityaBhandarTab(String sahityaBhandarText) {
        String sahityaBhandarTextxpathValue="//android.widget.TextView[contains(@text,'"+sahityaBhandarText+"')]";
        return elementUtil.getElement("xpath", sahityaBhandarTextxpathValue);
    }


    private WebElement getpanchangTab(String panchangText) {
        String panchangTextxpathValue="//android.widget.TextView[contains(@text,'"+panchangText+"')]";
        return elementUtil.getElement("xpath", panchangTextxpathValue);
    }

    private WebElement getkundliTab(String kundliText) {
        String kundliTextxpathValue="//android.widget.TextView[@text='"+kundliText+"']";
        return elementUtil.getElement("xpath", kundliTextxpathValue);
    }

    private WebElement getLogoutTab(String logOutText) {
        String logOutTextxpathValue="//android.widget.TextView[contains(@text,'"+logOutText+"')]";
        return elementUtil.getElement("xpath", logOutTextxpathValue);
    }

    private WebElement getNishulkKundliBanayeTab(String nishulkKundliBanayeText) {
        String NishulkKundliBanayeTextxpathValue="//android.widget.TextView[contains(@text,'"+nishulkKundliBanayeText+"')]";
        return elementUtil.getElement("xpath", NishulkKundliBanayeTextxpathValue);
    }

    private WebElement getWhatsAppParSamparkKareTab(String whatsAppParSamparkKareText) {
        String getWhatsAppParSamparkKareTabTextxpathValue="//android.widget.TextView[contains(@text,'"+whatsAppParSamparkKareText+"')]";
        return elementUtil.getElement("xpath", getWhatsAppParSamparkKareTabTextxpathValue);
    }

    private WebElement getSriMandirKeBareMeinTab(String sriMandirKeBareMeinText) {
        String sriMandirKeBareMeinTabTextxpathValue="//android.widget.TextView[@text,'"+sriMandirKeBareMeinText+"']";
        return elementUtil.getElement("xpath", sriMandirKeBareMeinTabTextxpathValue);
    }

    //business libraries
    @StepInfo(info="checking if nishulk kundli banaye tab displayed in Menu page")
    public boolean isNishulkKundliBanayeTabDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if nishulk kundli banaye tab displayed in Menu page");
            awaitForElement(driver, getNishulkKundliBanayeTab(ExcelUtility.getExcelData("locators", "nishulkKundliBanayeText", "value")));
            return getNishulkKundliBanayeTab(ExcelUtility.getExcelData("locators", "nishulkKundliBanayeText", "value")).isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isNishulkKundliBanayeTabDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info="checking if cancel buttoon displayed in menu page")
    public boolean isCancelBtnDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if cancel buttoon displayed in menu page");
            awaitForElement(driver, cancelBtn);
            return cancelBtn.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isCancelBtnDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info="checking if panchang tab displayed")
    public boolean isPanchangTabDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if panchang tab displayed");
            awaitForElement(driver, getpanchangTab(ExcelUtility.getExcelData("locators", "panchangText","value")));
            return getpanchangTab(ExcelUtility.getExcelData("locators", "panchangText", "value")).isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isPanchangTabDisplayed() "+e.getMessage());
        }
    }

    @StepInfo(info="checking if geetha shloka tab displayed")
    public boolean isGeethShlokTabDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if geetha shloka tab displayed");
            awaitForElement(driver, getGeethShlokTab(ExcelUtility.getExcelData("locators", "geethaShlokTabText","value")));
            return getGeethShlokTab(ExcelUtility.getExcelData("locators", "geethaShlokTabText", "value")).isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isGeethShlokTabDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info="checking if sahitya bhandar tab displayed")
    public boolean isSahityaBhandarTabDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if sahitya bhandar tab displayed");
            awaitForElement(driver, getSahityaBhandarTab(ExcelUtility.getExcelData("locators", "sahithyaBhandarText","value")));
            return getSahityaBhandarTab(ExcelUtility.getExcelData("locators", "sahithyaBhandarText", "value")).isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in clickOnSahityaBhandarTab()"+e.getMessage());
        }
    }

    @StepInfo(info="checking if kundli tab displayed")
    public boolean iskundliTabDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if kundli tab displayed");
            awaitForElement(driver, getkundliTab(ExcelUtility.getExcelData("locators", "kundliText","value")));
            return getkundliTab(ExcelUtility.getExcelData("locators", "kundliText", "value")).isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in iskundliTabDisplayed() "+e.getMessage());
        }

    }

    @StepInfo(info="checking if whatsapp par sampark kare tab displayed")
    public boolean isWhatsAppparSamparkKareTabDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if Whatsapp par sampark kare tab displayed");
            awaitForElement(driver, getkundliTab(ExcelUtility.getExcelData("locators", "kundliText","value")));
            return getWhatsAppParSamparkKareTab(ExcelUtility.getExcelData("locators", "whatsAppParSamparkKareText", "value")).isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isWhatsAppparSamparkKareTabDisplayed() "+e.getMessage());
        }

    }

    @StepInfo(info="checking if App update kare tab displayed")
    public boolean isAppUpdateKareTabDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if App update kare tab displayed");
            awaitForElement(driver, getAppUpdateKareTab(ExcelUtility.getExcelData("locators", "appUpdateKareText","value")));
            return getAppUpdateKareTab(ExcelUtility.getExcelData("locators", "appUpdateKareText", "value")).isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isAppUpdateKareTabDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info="checking if App rate kare Tab Displayed")
    public boolean isAppRateKareTabDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if App rate kare Tab Displayed");
            awaitForElement(driver, getAppRateKareTab(ExcelUtility.getExcelData("locators", "appRateKareText","value")));
            return getAppRateKareTab(ExcelUtility.getExcelData("locators", "appRateKareText", "value")).isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isAppRateKareTabDisplayed()"+e.getMessage());
        }

    }

    @StepInfo(info="checking if App Share kare tab displayed")
    public boolean isAppShareKareTabDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if App Share kare tab displayed");
            awaitForElement(driver, getAppShareKareTab(ExcelUtility.getExcelData("locators", "appShareKareText","value")));
            return getAppShareKareTab(ExcelUtility.getExcelData("locators", "appShareKareText", "value")).isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isAppShareKareTabDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info="checking if apne Sujaav Bheje Tab displayed")
    public boolean isApneSujavBhejeTabDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if apne Sujaav Bheje Tab displayed");
            awaitForElement(driver, getYourSuggestionsTab(ExcelUtility.getExcelData("locators", "apneSujaavBhejeText","value")));
            return getYourSuggestionsTab(ExcelUtility.getExcelData("locators", "apneSujaavBhejeText", "value")).isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isApneSujavBhejeTabDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info="checking if privacy policy Tab Displayed")
    public boolean isPrivacypolicyTabDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if privacy policy Tab Displayed");
            awaitForElement(driver, getPrivacyPolicyTab(ExcelUtility.getExcelData("locators", "privacyPolicyText","value")));
            return getPrivacyPolicyTab(ExcelUtility.getExcelData("locators", "privacyPolicyText", "value")).isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isPrivacypolicyTabDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info="checking if Terms And Conditions Tab displayed")
    public boolean isTermsAndConditionsTabDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if Terms And Conditions Tab displayed");
            awaitForElement(driver, getTermsAndConditionsTab(ExcelUtility.getExcelData("locators", "termsAndConditionsText","value")));
            return getTermsAndConditionsTab(ExcelUtility.getExcelData("locators", "termsAndConditionsText", "value")).isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isTermsAndConditionsTabDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info="checking if sri mandir ke bare mein tab displayed")
    public boolean isSriMandirKeBareMeinTabDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if sri mandir ke bare mein tab displayed");
            awaitForElement(driver, getTermsAndConditionsTab(ExcelUtility.getExcelData("locators", "sriMandirKeBareMeinText","value")));
            return getSriMandirKeBareMeinTab(ExcelUtility.getExcelData("locators", "sriMandirKeBareMeinText", "value")).isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isSriMandirKeBareMeinTabDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info="checking if logout tab displayed")
    public boolean isLogoutTabDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if logout tab displayed");
            awaitForElement(driver,getLogoutTab(ExcelUtility.getExcelData("locators", "logOutText", "value")));
            return getLogoutTab(ExcelUtility.getExcelData("locators", "logOutText", "value")).isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in  isLogoutTabDisplayed()"+e.getMessage());
        }

    }

    @StepInfo(info="clicking on logout Tab")
    public MenuUIPage clickOnLogoutTab() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("clicking on logout Tab");
            awaitForElement(driver,getLogoutTab(ExcelUtility.getExcelData("locators", "logOutText", "value")));
            clickOnElement(getLogoutTab(ExcelUtility.getExcelData("locators", "logOutText", "value")));
            waitOrPause(3);
            return this;
        }catch (Exception e){
            throw new Exception("Error in  clickOnLogoutTab()"+e.getMessage());
        }
    }

    @StepInfo(info="clicking on yes button from logout pop up")
    public MenuUIPage clickOnYesBtnFromLogoutPopup() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("clicking on yes button from logout pop up");
            awaitForElement(driver,yesBtnFromLogoutPopUp);
            clickOnElement(yesBtnFromLogoutPopUp);
            waitOrPause(3);
            return this;
        }catch (Exception e){
            throw new Exception("Error in  clickOnYesBtnFromLogoutPopup()"+e.getMessage());
        }

    }

    @StepInfo(info="checking if cheveron displayed")
    public boolean isCheveronDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if cheveron displayed");
            awaitForElement(driver,cheveron);
            return cheveron.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in  isCheveronDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info="checking if profile Image displayed in menu page")
    public boolean isProfileImageDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if profile Image displayed in menu page");
            awaitForElement(driver, profileImg);
            return profileImg.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isProfileImageDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info="Clicking on loginKare/userslot button from the menu page")
    public LoginBottomSheetUIPage clickOnLoginBtn() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("Clicking on loginKare button from the menu page");
            awaitForElement(driver, logInBtn);
            clickOnElement(logInBtn);
        }catch (Exception e){
            throw new Exception("Error in clickOnLoginBtn()"+e.getMessage());
        }
        return new LoginBottomSheetUIPage(driver);
    }

    @StepInfo(info="checking if phoneNumber displayed")
    public boolean isPhoneNumberDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if phoneNumber displayed");
            awaitForElement(driver, logInSubTitle);
            return logInSubTitle.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isPhoneNumberDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info="Checking if user name is displayed")
    public boolean isUserNameDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("Checking if user name is displayed");
            awaitForElement(driver, logInBtn);
           return logInBtn.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isUserNameDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info="swiping to beginning of the page")
    public MenuUIPage scrollToBeginning() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("swiping to beginning of the page");
            waitOrPause(3);
            swipeByCoordinates(driver, 502, 471, 502, 1330);
            return this;
        }catch (Exception e){
            throw new Exception("Error in  swipeToBeginning()"+e.getMessage());
        }
    }
    
    @StepInfo(info="clicking on cancel button")
	public void clickOnCancelBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on cancel button");
			awaitForElement(driver, cancelBtn);
			clickOnElement(cancelBtn);
		}catch (Exception e){
			throw new Exception("Error in clickOnCancelBtn()"+e.getMessage());
		}
	}
    
   
}
