package UIPages;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ListenerImplimentationclass;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ChowgadiyaUIPage extends AppGenericLib {

    ElementUtil elementUtil;
    public ChowgadiyaUIPage(AppiumDriver driver) {
        this.driver=driver;
        elementUtil=new ElementUtil(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(id="com.mandir.debug:id/tv_title_chogadhiya")
    private WebElement chowgadiyaTitle;

    @FindBy(id="com.mandir.debug:id/tv_location_selector")
    private WebElement locationTab;

    @FindBy(id="com.mandir.debug:id/radio_today")
    private WebElement aajBtn;

    @FindBy(id="com.mandir.debug:id/radio_tomorrow")
    private WebElement kalBtn;

    @FindBy(id="com.mandir.debug:id/tv_hora_title")
    private WebElement dateHighlightTab;

    @FindBy(id="com.mandir.debug:id/iv_help_text")
    private WebElement helpIcon;

    @FindBy(xpath = "//android.widget.LinearLayout[@resource-id='com.mandir.debug:id/linear_header']/following-sibling::android.widget.FrameLayout")
    private WebElement chowgadiyaCard;

    @FindBy(xpath="//android.widget.LinearLayout[@resource-id='com.mandir.debug:id/linear_header']/following-sibling::\t\n" +
            "android.widget.FrameLayout/android.view.ViewGroup/android.widget.TextView[@resource-id='com.mandir.debug:id/tv_chogadhiya_title']")
    private WebElement chowgadiyaCardTitle;

    @FindBy(xpath="//android.widget.LinearLayout[@resource-id='com.mandir.debug:id/linear_header']/following-sibling::\t\n" +
            "android.widget.FrameLayout/android.view.ViewGroup/android.widget.TextView[@resource-id='com.mandir.debug:id/tv_planet_desc']")
    private WebElement chowgadiyaCardTimeDetails;

    @FindBy(xpath="//android.widget.LinearLayout[@resource-id='com.mandir.debug:id/linear_header']/following-sibling::\t\n" +
            "android.widget.FrameLayout/android.view.ViewGroup/android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_expand']")
    private WebElement downCheveron;

    @FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_chogadhiya_desc']")
    private WebElement chowgadiyaCardDescription;

    @FindBy(id="com.mandir.debug:id/fab_share")
    private WebElement floatingShareIcon;

    @FindBy(id="com.mandir.debug:id/tv_chogadhiya_title2")
    private WebElement aageAneWalaChowgaidyaTitle;

    @FindBy(xpath = "//android.widget.LinearLayout[@resource-id='com.mandir.debug:id/linear_header2']/following-sibling::androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout[1]")
    private WebElement aageAaneWalaChowgadiyaCard;

    @FindBy(xpath = "//android.widget.LinearLayout[@resource-id='com.mandir.debug:id/linear_header2']/following-sibling::androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout[1]/\tandroid.view.ViewGroup/android.widget.TextView[@resource-id='com.mandir.debug:id/tv_chogadhiya_title']")
    private WebElement aageAanewalachowgadiyaCardTitle;

    @FindBy(xpath = "//android.widget.LinearLayout[@resource-id='com.mandir.debug:id/linear_header2']/following-sibling::androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout[1]/\tandroid.view.ViewGroup/android.widget.TextView[@resource-id='com.mandir.debug:id/tv_planet_desc']")
    private WebElement aageAanewalaChowgadiyaTimeDetails;

    @FindBy(xpath="//android.widget.LinearLayout[@resource-id='com.mandir.debug:id/linear_header2']/following-sibling::androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout[1]/\tandroid.view.ViewGroup/android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_expand']")
    private WebElement aageAaneWalaChowgadiyaDownCheveron;

    @FindBy(xpath="//android.widget.LinearLayout[@resource-id='com.mandir.debug:id/linear_header2']/following-sibling::androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout[1]/\tandroid.view.ViewGroup/android.widget.TextView[@resource-id='com.mandir.debug:id/tv_chogadhiya_desc']")
    private WebElement aageAaneWalaChowgadiyaCardDescription;

    @FindBy(xpath = "//android.widget.FrameLayout[@resource-id='com.mandir.debug:id/card_app_share']")
    private WebElement whatsAppShareIcon;

    @FindBy(xpath = "//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_title_toolbar']")
    private WebElement dateHighLightBanner;

    @FindBy(id="com.mandir.debug:id/tv_hora_title2")
    private WebElement dateHighLightTabInKalPage;

    @FindBy(xpath = "//android.widget.LinearLayout[@resource-id='com.mandir.debug:id/linear_header']/following-sibling::androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout[1]")
    private WebElement chowgadiyaCardInKalTab;

    @FindBy(xpath = "//android.widget.LinearLayout[@resource-id='com.mandir.debug:id/linear_header']/following-sibling::androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout[1]/android.view.ViewGroup/android.widget.TextView[@resource-id='com.mandir.debug:id/tv_chogadhiya_title']")
    private WebElement chowgadiyaCardTitleInKalTab;

    @FindBy(xpath = "//android.widget.LinearLayout[@resource-id='com.mandir.debug:id/linear_header']/following-sibling::androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout[1]/android.view.ViewGroup/android.widget.TextView[@resource-id='com.mandir.debug:id/tv_planet_desc']")
    private WebElement chowgadiyaCardTimeDetailsInKalTab;

    @FindBy(xpath="//android.widget.LinearLayout[@resource-id='com.mandir.debug:id/linear_header']/following-sibling::androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout[1]/android.view.ViewGroup/android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_expand']")
    private WebElement downCheveronInKalTab;

    @StepInfo(info = "checking if chowgadiya title is displayed")
    public boolean isChowgadiyaTitleDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if chowgadiya title is displayed");
            awaitForElement(driver, chowgadiyaTitle);
            return chowgadiyaTitle.isDisplayed();
        } catch (Exception e) {
            throw new Exception("Error in  isChowgadiyaTabDisplayed()"+e.getMessage());
        }

    }

    @StepInfo(info = "checking if location tab is displayed")
    public boolean isLocationTabDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if location tab is displayed in chowgadiya page");
            awaitForElement(driver, locationTab);
            return locationTab.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in  isLocationTabDisplayed"+e.getMessage());
        }
    }
    @StepInfo(info = "checking if aajBtn is displayed in chowgadiya page")
    public boolean isAajBtnDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if aajBtn is displayed in chowgadiya page");
            awaitForElement(driver, aajBtn);
            return aajBtn.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in  isAajBtnDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "clicking on aaj btn")
    public void clickOnAajTab() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("clicking on aaj btn");
            awaitForElement(driver, aajBtn);
            clickOnElement(aajBtn);
        }catch (Exception e) {
            throw new Exception("Error in  isAajBtnDisplayed()"+e.getMessage());
        }
    }


    @StepInfo(info = "checking if kalBtn is displayed in chowgadiya page")
    public boolean isKalBtnDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if kalBtn is displayed in chowgadiya page");
            awaitForElement(driver, kalBtn);
            return kalBtn.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in  isKalBtnDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "clicking on kal btn")
    public void clickOnKalTab() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("clicking on kal btn");
            awaitForElement(driver, kalBtn);
            clickOnElement(kalBtn);
        }catch (Exception e) {
            throw new Exception("Error in  clickOnKalTab()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if Date Tab is displayed in chowgadiya tab")
    public boolean isDateHighLightTabDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if date tab is displayed in chowgadiya tab");
            awaitForElement(driver, dateHighlightTab);
            return dateHighlightTab.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in  isDateHighLightTabDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if Help icon is displayed in chowgadiya tab")
    public boolean isHelpIconDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if Help icon is displayed in chowgadiya tab");
            awaitForElement(driver, helpIcon);
            return helpIcon.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in  isHelpIconDisplayed()"+e.getMessage());
        }
    }

    
    @StepInfo(info = "checking if Todays chowgadiya  card is displayed")
    public boolean isTodaysChowgadiyaCardIsDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if Todays Chowgadiya card is displayed");
            awaitForElement(driver, chowgadiyaCard);
            return chowgadiyaCard.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in  isTodaysChowgadiyaCardIsDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if chowgadiya card title is displayed")
    public boolean isChowgadiyaCardTitleDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if chowgadiya card title is displayed");
            awaitForElement(driver, chowgadiyaCardTitle);
            return chowgadiyaCardTitle.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in  isChowgadiyaCardTitleDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if chowgadiya card time slot details displayed")
    public boolean isChowgadiyaCardTimeDetailsDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if chowgadiya card time slot details displayed");
            awaitForElement(driver, chowgadiyaCardTimeDetails);
            return chowgadiyaCardTimeDetails.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in  isChowgadiyaCardTimeDetailsDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if down cheveron displayed in chowgadiya card")
    public boolean isDownCheveronDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if down cheveron displayed in chowgadiya card");
            awaitForElement(driver, downCheveron);
            return downCheveron.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in  isDownCheveronDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if chowgadiya card description is displayed")
    public boolean isChowgadiyaCardDescriptionDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if chowgadiya card description is displayed");
            awaitForElement(driver, chowgadiyaCardDescription);
            return chowgadiyaCardDescription.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in  isChowgadiyaCardDescriptionDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if floating share icon is displayed in chowgadiya page")
    public boolean isFloatingShareIconIsDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if floating share icon is displayed in chowgadiya page");
            awaitForElement(driver, floatingShareIcon);
            return floatingShareIcon.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in  isFloatingShareIconIsDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if aage aane wala chowgadiya title is displayed")
    public boolean isAageAaneWaleChowgadiyaTitleDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if aage aane wala chowgadiya title is displayed");
            awaitForElement(driver, aageAneWalaChowgaidyaTitle);
            return aageAneWalaChowgaidyaTitle.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in  isAageAaneWaleChowgadiyaTitleDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "swiping till aage aane wala chowgadiya")
    public void swipeTillAageAaneWalaChowgadiya(){
        ListenerImplimentationclass.testLog.info("swiping till aage aane wala chowgadiya");
        waitOrPause(2);
        swipeByCoordinates(driver,435,1230,435,840);
    }

    @StepInfo(info = "checking if aage aane wala chowgadiya card is displayed")
    public boolean isAageAaneWaleChowgadiyaCardIsDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if aage aane wala chowgadiya card is displayed");
            awaitForElement(driver, aageAaneWalaChowgadiyaCard);
            return aageAaneWalaChowgadiyaCard.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in  isAageAaneWaleChowgadiyaCardIsDisplayed() "+e.getMessage());
        }
    }

    @StepInfo(info = "checking if aage aanewala chowgadiya card title is displayed")
    public boolean isAageAanewaleChowgadiyaCardTitleIsDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if aage aanewala chowgadiya card title is displayed");
            awaitForElement(driver, aageAanewalachowgadiyaCardTitle);
            return aageAanewalachowgadiyaCardTitle.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in   isAageAanewaleChowgadiyaCardTitleIsDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if aage aanewala chowgadiya card time details is displayed")
    public boolean isAageAanewaleChowgadiyaCardTimeDetailsDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if aage aanewala chowgadiya card time details is displayed");
            awaitForElement(driver, aageAanewalaChowgadiyaTimeDetails);
            return aageAanewalaChowgadiyaTimeDetails.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in   isAageAanewaleChowgadiyaCardTimeDetailsDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if aage aanewala chowgadiya card down cheveron displayed")
    public boolean isAageAanewaleChowgadiyaCardDownCheveronDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if aage aanewala chowgadiya card down cheveron displayed");
            awaitForElement(driver, aageAaneWalaChowgadiyaDownCheveron);
            return aageAaneWalaChowgadiyaDownCheveron.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in   isAageAanewaleChowgadiyaCardDownCheveronDisplayed()"+e.getMessage());
        }
    }
    @StepInfo(info = "clicking on aageAaneWala chowgadiya card down cheveron")
    public void clickOnAageAanewalaChowgadiyaCardDownCheveron() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("clicking on aageAaneWala chowgadiya card down cheveron");
            awaitForElement(driver, aageAaneWalaChowgadiyaDownCheveron);
            clickOnElement(aageAaneWalaChowgadiyaDownCheveron);
        }catch (Exception e) {
            throw new Exception("Error in  clickOnAageAanewalaChowgadiyaCardDownCheveron()"+e.getMessage());
        }
    }
    @StepInfo(info = "checking if aageAanewalaChowgadiyaCardDescription is displayed")
    public boolean isAageAaneWalaChowgadiyaCardDescriptionIsDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if aageAanewalaChowgadiyaCardDescription is displayed");
            awaitForElement(driver, aageAaneWalaChowgadiyaCardDescription);
            return aageAaneWalaChowgadiyaCardDescription.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in  clickOnAageAanewalaChowgadiyaCardDownCheveron()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if WhatsAppShareIcon is displayed in chowgadiya page")
    public boolean isWhatsAppShareIconDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if WhatsAppShareIcon is displayed in chowgadiya page");
            awaitForElement(driver, whatsAppShareIcon);
            return whatsAppShareIcon.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in  isWhatsAppShareIconDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if DateHighlight banner is displayed while scrolling in chowgadiya page")
    public boolean isDateHighlightBannerDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if DateHighlight banner is displayed while scrolling in chowgadiya page");
            awaitForElement(driver, dateHighLightBanner);
            return dateHighLightBanner.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in  isDateHighlightBannerDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if date highlight tab is displayed in kal tab")
    public boolean isDateHighlightTabisDisplayedInKalTab() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if date highlight tab is displayed in kal tab");
            awaitForElement(driver, dateHighLightTabInKalPage);
            return dateHighLightTabInKalPage.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in  isDateHighlightTabisDisplayedInKalTab()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if chowgadiya card is displayed in kal tab")
    public boolean isChowgadiyaCardDisplayedInKalTab() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if chowgadiya card is displayed in kal tab");
            awaitForElement(driver, chowgadiyaCardInKalTab);
            return chowgadiyaCardInKalTab.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in  isChowgadiyaCardDisplayedInKalTab()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if chowgadiya card title is displayed in kal tab")
    public boolean isChowgadiyaCardTitleDisplayedInKalTab() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if chowgadiya card title is displayed in kal tab");
            awaitForElement(driver, chowgadiyaCardTitleInKalTab);
            return chowgadiyaCardTitleInKalTab.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in  isChowgadiyaCardTitleDisplayedInKalTab()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if Time details displayed in chowgadiya card in kal tab")
    public boolean isChowgadiyaCardTimeDetailsDisplayedInKalTab() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if Time details displayed in chowgadiya card in kal tab");
            awaitForElement(driver, chowgadiyaCardTimeDetailsInKalTab);
            return chowgadiyaCardTimeDetailsInKalTab.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in  isChowgadiyaCardTitleDisplayedInKalTab()"+e.getMessage());
        }
    }
    @StepInfo(info = "checking if down  cheveron displayed in chowgadiya card in  kal tab")
    public boolean isDownCheveronDisplayedInChowgadiyaCardInKalTab() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if down  cheveron displayed in chowgadiya card in  kal tab");
            awaitForElement(driver, downCheveronInKalTab);
            return downCheveronInKalTab.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in  isDownCheveronDisplayedInChowgadiyaCardInKalTab()"+e.getMessage());
        }
    }

    @StepInfo(info = "clicking on down cheveron from chowgadiya card in kal tab")
    public void clickOnDownCheveron() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if down  cheveron displayed in chowgadiya card in  kal tab");
            awaitForElement(driver, downCheveronInKalTab);
            clickOnElement(downCheveronInKalTab);
        }catch (Exception e) {
            throw new Exception("Error in  clickOnDownCheveron()"+e.getMessage());
        }
    }
}
