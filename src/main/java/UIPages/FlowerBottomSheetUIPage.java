package UIPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import Pages.FlowerBottomSheetPage;
import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ExcelUtility;
import base.ListenerImplimentationclass;
import io.appium.java_client.AppiumDriver;

public class FlowerBottomSheetUIPage extends AppGenericLib {

	public ElementUtil elementUtil;
	public FlowerBottomSheetUIPage(AppiumDriver driver) 
	{
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id="com.mandir.debug:id/tv_garland_name")
	private WebElement pholkiladhiText;

	private WebElement getpholkiladhiFlowerIcon(String pholkiladhiFlowerText) {
		String pholkiladhiFlowerTextxpathValue="//android.widget.TextView[@text='"+pholkiladhiFlowerText+"']/preceding-sibling::android.widget.FrameLayout//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_flower']";
		return elementUtil.getElement("xpath", pholkiladhiFlowerTextxpathValue);
	}

	private WebElement getGhondaFlowerText(String ghondaFlowerText) {
		String ghondaFlowerTextxpathValue="//android.widget.TextView[@text='"+ghondaFlowerText+"']";
		return elementUtil.getElement("xpath", ghondaFlowerTextxpathValue);
	}

	private WebElement getGhondaFlowerIcon(String ghondaFlowerText) {
		String ghondaFlowerTextxpathValue="//android.widget.TextView[@text='"+ghondaFlowerText+"']/preceding-sibling::android.widget.FrameLayout//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_flower']";
		return elementUtil.getElement("xpath", ghondaFlowerTextxpathValue);
	}

	private WebElement getBhelapatriFlowerText(String bhelapatriFlowerText) {
		String bhelpatriFlowerTextxpathValue="//android.widget.TextView[@text='"+bhelapatriFlowerText+"']";
		return elementUtil.getElement("xpath", bhelpatriFlowerTextxpathValue);
	}

	private WebElement getBhelapatriIcon(String bhelapatriFlowerText) {
		String bhelapatriFlowerTextxpathValue="//android.widget.TextView[@text='"+bhelapatriFlowerText+"']/preceding-sibling::android.widget.FrameLayout//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_flower']";
		return elementUtil.getElement("xpath", bhelapatriFlowerTextxpathValue);
	}

	private WebElement getMograFlowerText(String mograFlowerText) {
		String mograFlowerTextxpathValue="//android.widget.TextView[contains(@text,'"+mograFlowerText+"')]";
		return elementUtil.getElement("xpath", mograFlowerTextxpathValue);
	}

	private WebElement getMograFlowerIcon(String mograFlowerText) {
		String mograFlowerTextxpathValue="//android.widget.TextView[contains(@text,'"+mograFlowerText+"')]/preceding-sibling::android.widget.FrameLayout//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_flower']";
		return elementUtil.getElement("xpath", mograFlowerTextxpathValue);
	}

	private WebElement getGulhalFlowerText(String gulhalFlowerText) {
		String gulhalFlowerTextxpathValue="//android.widget.TextView[@text='"+gulhalFlowerText+"']";
		return elementUtil.getElement("xpath", gulhalFlowerTextxpathValue);
	}

	private WebElement getGulhalFlowerIcon(String gulhalFlowerText) {
		String gulhalFlowerTextxpathValue="//android.widget.TextView[@text='"+gulhalFlowerText+"']/preceding-sibling::android.widget.FrameLayout//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_flower']";
		return elementUtil.getElement("xpath", gulhalFlowerTextxpathValue);
	}

	private WebElement getKamalFlowerText(String kamalFlowerText) {
		String kamalFlowerTextxpathValue="//android.widget.TextView[@text='"+kamalFlowerText+"']";
		return elementUtil.getElement("xpath", kamalFlowerTextxpathValue);
	}

	private WebElement getKamalFlowerIcon(String kamalFlowerText) {
		String kamalFlowerTextxpathValue="//android.widget.TextView[@text='"+kamalFlowerText+"']/preceding-sibling::android.widget.FrameLayout//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_flower']";
		return elementUtil.getElement("xpath", kamalFlowerTextxpathValue);
	}

	private WebElement getChampaFlowerText(String champaFlowerText)
	{
		String ChampaTextxpathValue="//android.widget.TextView[@text='"+champaFlowerText+"']";
		return elementUtil.getElement("xpath", ChampaTextxpathValue);
	}

	private WebElement getChampaFlowerIcon(String champaFlowerText)
	{
		String ChampaTextxpathValue="//android.widget.TextView[@text='"+champaFlowerText+"']/preceding-sibling::android.widget.FrameLayout//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_flower']";
		return elementUtil.getElement("xpath", ChampaTextxpathValue);
	}

	private WebElement getGulabhaFlowerText(String gulabhaFlowerText)
	{
		String gulabhaTextxpathValue="//android.widget.TextView[@text='"+gulabhaFlowerText+"']";
		return elementUtil.getElement("xpath", gulabhaTextxpathValue);
	}

	private WebElement getGulabhaFlowerIcon(String gulabhaFlowerText)
	{
		String gulabhaTextxpathValue="//android.widget.TextView[@text='"+gulabhaFlowerText+"']/preceding-sibling::android.widget.FrameLayout//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_flower']";
		return elementUtil.getElement("xpath", gulabhaTextxpathValue);
	}

	private WebElement getDhaturaFlowerIcon(String dhaturaFlowerText)
	{
		String dhaturaTextxpathValue="//android.widget.TextView[@text='"+dhaturaFlowerText+"']/preceding-sibling::android.widget.FrameLayout//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_flower']";
		return elementUtil.getElement("xpath", dhaturaTextxpathValue);
	}

	private WebElement getDhaturaFlowerText(String dhaturaFlowerText)
	{
		String dhaturaTextxpathValue="//android.widget.TextView[@text='"+dhaturaFlowerText+"']";
		return elementUtil.getElement("xpath", dhaturaTextxpathValue);
	}

	private WebElement getPalashFlowerIcon(String palaashFlowerText)
	{
		String palaashTextxpathValue="//android.widget.TextView[@text='"+palaashFlowerText+"']/preceding-sibling::android.widget.FrameLayout//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_flower']";
		return elementUtil.getElement("xpath", palaashTextxpathValue);
	}
	
	private WebElement getPalashFlowerText(String palaashFlowerText)
	{
		String palaashTextxpathValue="//android.widget.TextView[@text='"+palaashFlowerText+"']";
		return elementUtil.getElement("xpath", palaashTextxpathValue);
	}
	
	private WebElement getAshokFlowerIcon(String ashokFlowerText)
	{
		String ashokTextxpathValue="//android.widget.TextView[@text='"+ashokFlowerText+"']/preceding-sibling::android.widget.FrameLayout//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_flower']";
		return elementUtil.getElement("xpath", ashokTextxpathValue);
	}
	
	private WebElement getAshokFlowerText(String ashokFlowerText)
	{
		String ashokTextxpathValue="//android.widget.TextView[@text='"+ashokFlowerText+"']";
		return elementUtil.getElement("xpath", ashokTextxpathValue);
	}
	
	private WebElement getPaarijaatFlowerIcon(String paarijaatFlowerText)
	{
		String paarijaatTextxpathValue="//android.widget.TextView[@text='"+paarijaatFlowerText+"']/preceding-sibling::android.widget.FrameLayout//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_flower']";
		return elementUtil.getElement("xpath", paarijaatTextxpathValue);
	}
	
	private WebElement getPaarijaatFlowerText(String paarijaatFlowerText)
	{
		String paarijaatTextxpathValue="//android.widget.TextView[@text='"+paarijaatFlowerText+"']";
		return elementUtil.getElement("xpath", paarijaatTextxpathValue);
	}
			
	private WebElement getKovidaarFlowerIcon(String kovidaarFlowerText)
	{
		String kovidaarTextxpathValue="//android.widget.TextView[@text='"+kovidaarFlowerText+"']/preceding-sibling::android.widget.FrameLayout//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_flower']";
		return elementUtil.getElement("xpath", kovidaarTextxpathValue);
	}
	
	private WebElement getKovidaarFlowerText(String kovidaarFlowerText)
	{
		String kovidaarTextxpathValue="//android.widget.TextView[@text='"+kovidaarFlowerText+"']";
		return elementUtil.getElement("xpath", kovidaarTextxpathValue);
	}	
	
	private WebElement getNilakamlFlowerIcon(String NilkamalFlowerText)
	{
		String NilkamalTextxpathValue="//android.widget.TextView[@text='"+NilkamalFlowerText+"']/preceding-sibling::android.widget.FrameLayout//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_flower']";
		return elementUtil.getElement("xpath", NilkamalTextxpathValue);
	}
	
	private WebElement getNilakamalFlowerText(String NilkamalFlowerText)
	{
		String NilkamalTextxpathValue="//android.widget.TextView[@text='"+NilkamalFlowerText+"']";
		return elementUtil.getElement("xpath", NilkamalTextxpathValue);
	}
	
	@FindBy(id="com.mandir.debug:id/bt_purchase_flower")
	private WebElement tutorialToUnlockFlower;

	@StepInfo(info="checking  phol ki ladhi Flower is displayed")
	public boolean isPholkiLadhiDisplayed() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("checking  phol ki ladhi Flower is displayed");
			awaitForElement(driver, getpholkiladhiFlowerIcon(ExcelUtility.getExcelData("UILocators", "pholkiladhiFlowerText", "value")));
			return getpholkiladhiFlowerIcon(ExcelUtility.getExcelData("UILocators", "pholkiladhiFlowerText", "value")).isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isPholkiLadhiDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking  Ghonda Flower is displayed")
	public boolean isGhondaFlowerDisplayed() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("checking Ghonda Flower is displayed");
			awaitForElement(driver, getGhondaFlowerIcon(ExcelUtility.getExcelData("UILocators", "ghondaFlowerText", "value")));
		}catch (Exception e){
			throw new Exception("Error in isGhondaFlowerDisplayed()"+e.getMessage());
		}
		return getGhondaFlowerIcon(ExcelUtility.getExcelData("UILocators", "ghondaFlowerText", "value")).isDisplayed();
	}

	@StepInfo(info="getting phol ki ladhi flower text")
	public String getPholkiLadhiFlowerText() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("getting phol ki ladhi flower text");
			awaitForElement(driver, pholkiladhiText);
			return pholkiladhiText.getText();
		}catch (Exception e){
			throw new Exception("Error in getPholkiLadhiFlowerText()"+e.getMessage());
		}
	}

	@StepInfo(info="getting Ghonda flower text ")
	public String getGhondaFlowerText() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("getting Ghonda flower text");
			awaitForElement(driver, getGhondaFlowerText(ExcelUtility.getExcelData("UILocators", "ghondaFlowerText", "value")));
			return getGhondaFlowerText(ExcelUtility.getExcelData("UILocators", "ghondaFlowerText", "value")).getText();
		}catch (Exception e)
		{
			throw new Exception("Error in getGhondaFlowerText()"+e.getMessage());
		}
	}

	@StepInfo(info="checking  Bhelapatri Flower is displayed")
	public boolean isBhelapatriFlowerDisplayed() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("checking Bhelapatri Flower is displayed");
			awaitForElement(driver, getBhelapatriIcon(ExcelUtility.getExcelData("UILocators", "bhelapatriFlowerText", "value")));
			return getBhelapatriIcon(ExcelUtility.getExcelData("UILocators", "bhelapatriFlowerText", "value")).isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isBhelpatriFlowerDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="getting Bhelapatri flower text ")
	public String getBhelapatriFlowerText() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("getting Bhelapatri flower text");
			awaitForElement(driver, getBhelapatriFlowerText(ExcelUtility.getExcelData("UILocators", "bhelapatriFlowerText", "value")));
			return getBhelapatriFlowerText(ExcelUtility.getExcelData("UILocators", "bhelapatriFlowerText", "value")).getText();
		}
		catch (Exception e){
			throw new Exception("Error in getbhelpatriFlowerText()"+e.getMessage());
		}
	}

	@StepInfo(info="checking  Mogra Flower is displayed")
	public boolean isMograFlowerDisplayed() throws Exception
	{
		try{
			ListenerImplimentationclass.testLog.info("checking Mogra Flower is displayed");
			awaitForElement(driver, getMograFlowerIcon(ExcelUtility.getExcelData("UILocators", "mograFlowerText", "value")));
		}catch (Exception e){
			throw new Exception("Error in isMograFlowerDisplayed()"+e.getMessage());
		}
		return getMograFlowerIcon(ExcelUtility.getExcelData("UILocators", "mograFlowerText", "value")).isDisplayed();
	}

	@StepInfo(info="getting mogra flower text ")
	public String getMograFlowerText() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("getting mogra flower text");
			awaitForElement(driver, getMograFlowerText(ExcelUtility.getExcelData("UILocators", "mograFlowerText", "value")));
			return getMograFlowerText(ExcelUtility.getExcelData("UILocators", "mograFlowerText", "value")).getText();
		}catch (Exception e)
		{
			throw new Exception("Error in getmograFlowerText()"+e.getMessage());
		}
	}

	@StepInfo(info="slide over  flower corousel ")
	public void slideOverFlowerCorousel() 
	{
		swipeByCoordinates(driver, 866, 1422,98, 1422);
	}

	@StepInfo(info="checking  Gulhal Flower is displayed")
	public boolean isGulhalFlowerDisplayed() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("checking Gulhal Flower is displayed");
			awaitForElement(driver, getGulhalFlowerIcon(ExcelUtility.getExcelData("UILocators", "gulhalFlowerText", "value")));
		}catch (Exception e){
			throw new Exception("Error in isGulhalFlowerDisplayed()"+e.getMessage());
		}
		return getGulhalFlowerIcon(ExcelUtility.getExcelData("UILocators", "gulhalFlowerText", "value")).isDisplayed();
	}

	@StepInfo(info="getting gulhal flower text ")
	public String getGulhalFlowerText() throws Exception {
		try{
			ListenerImplimentationclass.testLog.info("getting mogra flower text");
			awaitForElement(driver, getGulhalFlowerText(ExcelUtility.getExcelData("UILocators", "gulhalFlowerText", "value")));
			return getGulhalFlowerText(ExcelUtility.getExcelData("UILocators", "gulhalFlowerText", "value")).getText();
		}catch (Exception e)
		{
		 throw new Exception("Error in getGulhalFlowerText()"+e.getMessage());
		}
	}

	@StepInfo(info="checking  kamal Flower is displayed")
	public boolean isKamalFlowerDisplayed() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("checking kamal Flower is displayed");
			awaitForElement(driver, getKamalFlowerIcon(ExcelUtility.getExcelData("UILocators", "kamalFlowerText", "value")));
		}catch (Exception e)
		{
			throw new Exception("Error in isKamalFlowerDisplayed()"+e.getMessage());
		}
		return getKamalFlowerIcon(ExcelUtility.getExcelData("UILocators", "kamalFlowerText", "value")).isDisplayed();
	}


	@StepInfo(info="getting kamal flower text ")
	public String getKamalFlowerText() throws Exception {
		try{
			ListenerImplimentationclass.testLog.info("getting kamal flower text");
			awaitForElement(driver, getKamalFlowerText(ExcelUtility.getExcelData("UILocators", "kamalFlowerText", "value")));
			return getKamalFlowerText(ExcelUtility.getExcelData("UILocators", "kamalFlowerText", "value")).getText();
		}catch (Exception e)
		{
			throw new Exception("Error in getKamalFlowerText()"+e.getMessage());
		}
	}

	@StepInfo(info="checking  Champa Flower is displayed")
	public boolean isChampaFlowerDisplayed() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("checking Champa Flower is displayed");
			awaitForElement(driver, getChampaFlowerIcon(ExcelUtility.getExcelData("UILocators", "champaFlowerText", "value")));
		}catch (Exception e)
		{
			throw new Exception("Error in isChampaFlowerDisplayed()"+e.getMessage());
		}
		return getChampaFlowerIcon(ExcelUtility.getExcelData("UILocators", "champaFlowerText", "value")).isDisplayed();
	}

	@StepInfo(info="getting Champa flower text ")
	public String getChampaFlowerText() throws Exception {
		try{
			ListenerImplimentationclass.testLog.info("getting Champa flower text");
			awaitForElement(driver, getChampaFlowerText(ExcelUtility.getExcelData("UILocators", "champaFlowerText", "value")));
			return getChampaFlowerText(ExcelUtility.getExcelData("UILocators", "champaFlowerText", "value")).getText();
		}catch (Exception e)
		{
			throw new Exception("Error in getChampaFlowerText()"+e.getMessage());
		}
	}

	@StepInfo(info="checking  Gulabha Flower is displayed")
	public boolean isGulabhaFlowerDisplayed() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("checking Gulabha Flower is displayed");
			awaitForElement(driver, getGulabhaFlowerIcon(ExcelUtility.getExcelData("UILocators", "gulabhaFlowerText", "value")));
		}catch (Exception e)
		{
			throw new Exception("Error in isGulabhaFlowerDisplayed()"+e.getMessage());
		}
		return getGulabhaFlowerIcon(ExcelUtility.getExcelData("UILocators", "gulabhaFlowerText", "value")).isDisplayed();
	}

	@StepInfo(info="getting Gulabha flower text ")
	public String getGulabhaFlowerText() throws Exception {
		try{
			ListenerImplimentationclass.testLog.info("getting Gulabha flower text");
			awaitForElement(driver, getGulabhaFlowerText(ExcelUtility.getExcelData("UILocators", "gulabhaFlowerText", "value")));
			return getGulabhaFlowerText(ExcelUtility.getExcelData("UILocators", "gulabhaFlowerText", "value")).getText();
		}catch (Exception e)
		{
			throw new Exception("Error in getGulabhaFlowerText()"+e.getMessage());
		}
	}


	@StepInfo(info="checking  Dhatura Flower is displayed")
	public boolean isDhaturaFlowerDisplayed() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("checking  Dhatura Flower is displayed");
			awaitForElement(driver, getDhaturaFlowerIcon(ExcelUtility.getExcelData("UILocators", "dhaturaFlowerText", "value")));
		}catch (Exception e)
		{
			throw new Exception("Error in isDhaturaFlowerDisplayed()"+e.getMessage());
		}
		return getDhaturaFlowerIcon(ExcelUtility.getExcelData("UILocators", "dhaturaFlowerText", "value")).isDisplayed();
	}

	@StepInfo(info="getting Dhatura flower text ")
	public String getDhaturaFlowerText() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("getting Dhatura flower text");
			awaitForElement(driver, getDhaturaFlowerText(ExcelUtility.getExcelData("UILocators", "dhaturaFlowerText", "value")));
			return getDhaturaFlowerText(ExcelUtility.getExcelData("UILocators", "dhaturaFlowerText", "value")).getText();
		}catch (Exception e)
		{
			throw new Exception("Error in getDhaturaFlowerText()"+e.getMessage());
		}
	}
	
	
	@StepInfo(info="checking  Palaash Flower is displayed")
	public boolean isPalaashFlowerDisplayed() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("checking  Palaash Flower is displayed");
			awaitForElement(driver, getPalashFlowerIcon(ExcelUtility.getExcelData("UILocators", "palaashFlowerText", "value")));
		}catch (Exception e)
		{
			throw new Exception("Error in isPalaashFlowerDisplayed()"+e.getMessage());
		}
		return getPalashFlowerIcon(ExcelUtility.getExcelData("UILocators", "palaashFlowerText", "value")).isDisplayed();
	}

	@StepInfo(info="getting Palaash flower text ")
	public String getPalaashFlowerText() throws Exception {
		try{
			ListenerImplimentationclass.testLog.info("getting Palaash flower text");
			awaitForElement(driver, getPalashFlowerText(ExcelUtility.getExcelData("UILocators", "palaashFlowerText", "value")));
			return getPalashFlowerText(ExcelUtility.getExcelData("UILocators", "palaashFlowerText", "value")).getText();
		}catch (Exception e)
		{
			throw new Exception("Error in getDhaturaFlowerText()"+e.getMessage());
		}
	}

	
	@StepInfo(info="checking  Ashok Flower is displayed")
	public boolean isAshokFlowerDisplayed() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("checking  Ashok Flower is displayed");
			awaitForElement(driver, getAshokFlowerIcon(ExcelUtility.getExcelData("UILocators", "ashokFlowerText", "value")));
			return getAshokFlowerIcon(ExcelUtility.getExcelData("UILocators", "ashokFlowerText", "value")).isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in isAshokFlowerDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="getting Ashok flower text ")
	public String getAshokFlowerText() throws Exception {
		try{
			ListenerImplimentationclass.testLog.info("getting Ashok flower text");
			awaitForElement(driver, getAshokFlowerText(ExcelUtility.getExcelData("UILocators", "ashokFlowerText", "value")));
			return getAshokFlowerText(ExcelUtility.getExcelData("UILocators", "ashokFlowerText", "value")).getText();
		}catch (Exception e)
		{
			throw new Exception("Error in getAshokFlowerText()"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking  Paarijaat Flower is displayed")
	public boolean isPaarijaatFlowerDisplayed() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("checking  Paarijaat Flower is displayed");
			awaitForElement(driver, getPaarijaatFlowerIcon(ExcelUtility.getExcelData("UILocators", "paarijaatFlowerText", "value")));
		}catch (Exception e)
		{
			throw new Exception("Error in isPaarijaatFlowerDisplayed()"+e.getMessage());
		}
		return getPaarijaatFlowerIcon(ExcelUtility.getExcelData("UILocators", "paarijaatFlowerText", "value")).isDisplayed();
	}

	@StepInfo(info="getting Paarijaat flower text ")
	public String getPaarijaatFlowerText() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("getting Paarijaat flower text");
			awaitForElement(driver, getPaarijaatFlowerText(ExcelUtility.getExcelData("UILocators", "paarijaatFlowerText", "value")));
			return getPaarijaatFlowerText(ExcelUtility.getExcelData("UILocators", "paarijaatFlowerText", "value")).getText();
		}catch (Exception e)
		{
			throw new Exception("Error in getPaarijaatFlowerText()"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking  Kovidaar Flower is displayed")
	public boolean isKovidaarFlowerDisplayed() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("checking  Kovidaar Flower is displayed");
			awaitForElement(driver, getKovidaarFlowerIcon(ExcelUtility.getExcelData("UILocators", "kovidaarFlowerText", "value")));
		}catch (Exception e)
		{
			throw new Exception("Error in isKovidaarFlowerDisplayed()"+e.getMessage());
		}
		return getKovidaarFlowerIcon(ExcelUtility.getExcelData("UILocators", "kovidaarFlowerText", "value")).isDisplayed();
	}

	@StepInfo(info="getting Kovidaar flower text ")
	public String getKovidaarFlowerText() throws Exception {
		try 
		{
			ListenerImplimentationclass.testLog.info("getting Kovidaar flower text");
			awaitForElement(driver, getKovidaarFlowerText(ExcelUtility.getExcelData("UILocators", "kovidaarFlowerText", "value")));
			return getKovidaarFlowerText(ExcelUtility.getExcelData("UILocators", "kovidaarFlowerText", "value")).getText();
		}
		catch (Exception e)
		{
			throw new Exception("Error in getKovidaarFlowerText()"+e.getMessage());
		}
	}
		
	@StepInfo(info="checking  Nilakamal Flower is displayed")
	public boolean isNilakamalFlowerDisplayed() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("checking  Nilakamal Flower is displayed");
			awaitForElement(driver, getNilakamlFlowerIcon(ExcelUtility.getExcelData("UILocators", "NilakamalFlowerText", "value")));
		}catch (Exception e){
			throw new Exception("Error in isNilkamalFlowerDisplayed()"+e.getMessage());
		}
		return getNilakamlFlowerIcon(ExcelUtility.getExcelData("UILocators", "NilakamalFlowerText", "value")).isDisplayed();
	}

	@StepInfo(info="getting Nilkamal flower text ")
	public String getNilakamalFlowerText() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("getting Nilkamal flower text");
			awaitForElement(driver, getNilakamalFlowerText(ExcelUtility.getExcelData("UILocators", "NilakamalFlowerText", "value")));
			return getNilakamalFlowerText(ExcelUtility.getExcelData("UILocators", "NilakamalFlowerText", "value")).getText();
		}
		catch (Exception e)
		{
			throw new Exception("Error in getNilkamalFlowerText()"+e.getMessage());
		}
	}	
	
	@StepInfo(info="clicking on Locked flower from flower bottom sheet")
	public FlowerBottomSheetUIPage clickOnLockedFlower() throws Exception
	{

		try {
			ListenerImplimentationclass.testLog.info("Clicking on LockedFlower from flower bottom sheet");
			awaitForElement(driver, getNilakamalFlowerText(ExcelUtility.getExcelData("UILocators", "NilakamalFlowerText", "value")));
			clickOnElement(getNilakamlFlowerIcon(ExcelUtility.getExcelData("UILocators", "NilakamalFlowerText", "value")));
		}catch (Exception e){
			throw new Exception("Error in clickOnLockedFlower()"+e.getMessage());
		}
		return this;
	}
	
	@StepInfo(info="clicking the locked flower user is getting a tutorial to unlock a flower")
	public boolean isTutorialDisplayedForLockedFlower() throws Exception
	{
		try {
			awaitForElement(driver, tutorialToUnlockFlower);
			ListenerImplimentationclass.testLog.info("checking if the flower bottom sheet is displayed");
		}catch (Exception e)
		{
			throw new Exception("Error in isTutorialDisplayedForLockedFlower()"+e.getMessage());
		}
		return tutorialToUnlockFlower.isDisplayed();

	}

}