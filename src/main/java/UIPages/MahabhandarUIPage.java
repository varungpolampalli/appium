package UIPages;

import Pages.ChowgadiyaPage;
import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ExcelUtility;
import base.ListenerImplimentationclass;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class MahabhandarUIPage extends AppGenericLib {
    ElementUtil elementUtil;

    public MahabhandarUIPage(AppiumDriver driver) {
        this.driver=driver;
        elementUtil=new ElementUtil(driver);
        PageFactory.initElements(driver, this);
    }

    private WebElement chowgadiyaTab(String chowgadiyaText) {
        String chowgadiyaTextxpathValue="//android.widget.TextView[@text='"+chowgadiyaText+"']";
        return elementUtil.getElement("xpath", chowgadiyaTextxpathValue);
    }

    @StepInfo(info="clicking On chowgadiya Tab")
    public ChowgadiyaPage clickOnChowgadiyaTab() throws Exception {
        try {
            awaitForElement(driver, chowgadiyaTab(ExcelUtility.getExcelData("locators", "chowgadiyaText", "value")));
            ListenerImplimentationclass.testLog.info("clicking on chowgadiya Tab");
            clickOnElement(chowgadiyaTab(ExcelUtility.getExcelData("locators", "chowgadiyaText", "value")));
        }catch (Exception e)
        {
            throw new Exception("Error in clickOnChowgadiyaTab()"+e.getMessage());
        }
        return new ChowgadiyaPage(driver);
    }
}
