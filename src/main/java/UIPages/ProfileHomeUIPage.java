package UIPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import Pages.EditProfilePage;
import Pages.LoginBottomSheetPage;
import Pages.ProfilePage;
import Pages.PunyaMudraPage;
import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ExcelUtility;
import base.ListenerImplimentationclass;
import io.appium.java_client.AppiumDriver;

public class ProfileHomeUIPage extends AppGenericLib {
	public ElementUtil elementUtil;
    public ProfileHomeUIPage(AppiumDriver driver)
    {
        this.driver=driver;
        elementUtil=new ElementUtil(driver);
        PageFactory.initElements(driver, this);
    }
    
    @FindBy(id="com.mandir.debug:id/back_button_kundli_form")
	private WebElement backBtn;

	@FindBy(id="com.mandir.debug:id/share_kundli")
	private WebElement editBtn;
	
	@FindBy(id="com.mandir.debug:id/punya_mudra_text")
	private WebElement punyaMudraTab;

	@FindBy(id="com.mandir.debug:id/bhakti_chakra_text")
	private WebElement bhaktiChakraTab;
	
	@FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/punya_mudra_text']/following-sibling::android.widget.TextView")
	private WebElement punyaMudraCountText;
	
	@FindBy(id="com.mandir.debug:id/snack_bar")
	private WebElement snackBarMsg;
	
	@FindBy(id="com.mandir.debug:id/chakra_progress")
	private WebElement chakraProgressIcon;
	
	@FindBy(id="com.mandir.debug:id/username")
	private WebElement userName;
	
	@FindBy(id="com.mandir.debug:id/birth_city")
	private WebElement phoneNumber;
	
	@FindBy(id="//android.widget.FrameLayout[@resource-id='com.mandir.debug:id/profile_photo']")
	private WebElement profilePhoto;
	
	private WebElement getprofilePageTitle(String profilePageText) {
		String profilePageTextxpathValue="//android.widget.TextView[@text='"+profilePageText+"']";
		return elementUtil.getElement("xpath", profilePageTextxpathValue);
	}

	private WebElement getKundliTab(String kundliReportText) {
		String kundliReportTextxpathValue="//android.widget.TextView[@text='"+kundliReportText+"']";
		return elementUtil.getElement("xpath", kundliReportTextxpathValue);
	}

	private WebElement getAapkeAlarmTab(String aapkeAlarmText) {
		String aapkeAlarmTextxpathValue="//android.widget.TextView[@text='"+aapkeAlarmText+"']";
		return elementUtil.getElement("xpath", aapkeAlarmTextxpathValue);
	}

	private WebElement getChadavSevaTab(String chadavSevaText) {
		String chadavSevaTextxpathValue="//android.widget.TextView[@text='"+chadavSevaText+"']";
		return elementUtil.getElement("xpath", chadavSevaTextxpathValue);
	}

	private WebElement getLoginTab(String loginKareText) {
		String loginKareTextxpathValue="//android.widget.TextView[@text='"+loginKareText+"']";
		return elementUtil.getElement("xpath", loginKareTextxpathValue);
	}
	
	@StepInfo(info="checking if profile page title displayed")
	public boolean isProfilePageTitleDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if profile page title displayed");
			awaitForElement(driver, getprofilePageTitle(ExcelUtility.getExcelData("locators", "profilePageTitleText", "value")));
			return  getprofilePageTitle(ExcelUtility.getExcelData("locators", "profilePageTitleText", "value")).isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isProfilePageTitleDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if back button displayed in profile home page")
	public boolean isBackBtnDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if back button displayed in profile home page");
			awaitForElement(driver, backBtn);
			return backBtn.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isBackBtnDisplayed()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on back button from my profile page")
	public void clickOnBackBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking back button from my profile page");
			awaitForElement(driver, backBtn);
			clickOnElement(backBtn);
			waitOrPause(2);
		}catch (Exception e){
			throw new Exception("Error in clickOnBackBtn()"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if punya mudra tab displayed")
	public boolean isPunyamudraTabDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if punya mudra tab displayed");
			awaitForElement(driver, punyaMudraTab);
			return punyaMudraTab.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isPunyamudraTabDisplayed()"+e.getMessage()); 
		}

	}

	@StepInfo(info="checking if bhakti chakra tab displayed")
	public boolean isBhaktiChakraTabDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if bhakti chakra tab displayed");
			awaitForElement(driver, bhaktiChakraTab);
			return bhaktiChakraTab.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isBhaktiChakraTabDisplayed()"+e.getMessage()); 
		}

	}

	@StepInfo(info="checking if kundli report tab displayed")
	public boolean isKundliReportTabDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if kundli report tab displayed");
			awaitForElement(driver, getKundliTab(ExcelUtility.getExcelData("locators", "kundliReportText", "value")));
			return getKundliTab(ExcelUtility.getExcelData("locators","kundliReportText", "value")).isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isKundliReportTabDisplayed()"+e.getMessage()); 
		}

	}

	@StepInfo(info="checking if alarm tab displayed in profile home page")
	public boolean isAapkeAlarmTabDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if alarm tab displayed in profile home page");
			awaitForElement(driver, getAapkeAlarmTab(ExcelUtility.getExcelData("locators", "alarmPageTitle", "value")));
			return getAapkeAlarmTab(ExcelUtility.getExcelData("locators","alarmPageTitle", "value")).isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isAapkeAlarmTabDisplayed()"+e.getMessage()); 
		}

	}
	
	@StepInfo(info="checking if chadav seva tab displayed")
	public boolean isChadavSevaTabDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if chadav seva tab displayed");
			awaitForElement(driver, getChadavSevaTab(ExcelUtility.getExcelData("locators", "chadavSevaTabTitle", "value")));
			return getChadavSevaTab(ExcelUtility.getExcelData("locators","chadavSevaTabTitle", "value")).isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in  isChadavSevaTabDisplayed()"+e.getMessage()); 
		}

	}
	
	@StepInfo(info="checking if snack bar message displayed")
	public boolean isSnakBarMessageDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if snack bar message displayed");
		awaitForElement(driver, snackBarMsg);
		return snackBarMsg.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isSnakBarMessageDisplayed()"+e.getMessage()); 
		}
	}
	
	@StepInfo(info="checking if punya mudra count displayed in profile Page")
	public boolean isPunyaMudraCountDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if punya mudra count displayed in profile Page");
			awaitForElement(driver, punyaMudraCountText);
			return punyaMudraCountText.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in  isPunyaMudraCountDisplayed()"+e.getMessage()); 
		}
	}

	@StepInfo(info="checking if edit profile button is displayed")
	public boolean isEditProfileButtonDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if edit profile button is displayed");
			awaitForElement(driver, editBtn);
			return editBtn.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isEditProfileButtonDisplayed()"+e.getMessage()); 
		}

	}
	
	@StepInfo(info="clicking on login tab from profile page")
	public LoginBottomSheetUIPage clickOnLoginTabFromProfilePage() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on login tab from profile page");
			awaitForElement(driver, punyaMudraTab);
			clickOnElement(getLoginTab(ExcelUtility.getExcelData("locators", "loginKareText", "value")));
			return new LoginBottomSheetUIPage(driver);
		}catch (Exception e){
			throw new Exception("Error in clickOnLoginTabFromProfilePage()"+e.getMessage()); 
		}
	}
	
	@StepInfo(info="checking if login tab displayed in profile home")
	public boolean isLoginTabFromProfilePageDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if login tab displayed in profile home page");
			awaitForElement(driver, punyaMudraTab);
			return getLoginTab(ExcelUtility.getExcelData("locators", "loginKareText", "value")).isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isLoginTabFromProfilePageDisplayed()"+e.getMessage()); 
		}
	}
	
	@StepInfo(info="checking if chakra progress displayed")
	public boolean isChakraProgressDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if chakra progress displayed");
			awaitForElement(driver, chakraProgressIcon);
			return chakraProgressIcon.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isChakraProgressDisplayed()"+e.getMessage()); 
		}

	}
	
	@StepInfo(info="checking if userNameDisplayed")
	public boolean isUserNameDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if userNameDisplayed");
			awaitForElement(driver, userName);
			return userName.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isUserNameDisplayed()"+e.getMessage()); 
		}

	}
	
	@StepInfo(info="checking if phoneNumber displayed")
	public boolean isPhoneNumberDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if phoneNumber displayed");
			awaitForElement(driver, phoneNumber);
			return phoneNumber.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isPhoneNumberDisplayed()"+e.getMessage()); 
		}
	}
	
	@StepInfo(info="checking if profile photo displayed")
	public boolean isProfilePhotoDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if profile photo displayed");
			awaitForElement(driver, profilePhoto);
			return profilePhoto.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isPhoneNumberDisplayed()"+e.getMessage()); 
		}
	}
	
	@StepInfo(info="clicking on edit profile button")
	public EditProfileUIPage clickOnEditBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on edit profile button");
			awaitForElement(driver, editBtn);
			clickOnElement(editBtn);
			return new EditProfileUIPage(driver);
		}catch (Exception e){
			throw new Exception("Error in clickOnEditBtn()"+e.getMessage()); 
		}
	}


}
