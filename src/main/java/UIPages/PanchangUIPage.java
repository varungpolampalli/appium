package UIPages;

import Pages.PanchangPage;
import Pages.RashifalPage;
import TestAnnotations.StepInfo;
import base.*;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PanchangUIPage extends AppGenericLib {
    ElementUtil elementUtil;
    public PanchangUIPage(AppiumDriver driver) {
        this.driver=driver;
        elementUtil=new ElementUtil(driver);
        PageFactory.initElements(driver, this);
    }
    @FindBy(id="com.mandir.debug:id/tv_header_title")
    private WebElement dailyPanchangTitle;

    @StepInfo(info = "checking if daily panchang title is displayed")
    public boolean isDailyPanchangTitleDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if daily panchang title is displayed");
            awaitForElement(driver, dailyPanchangTitle);
            return dailyPanchangTitle.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in  isDailyPanchangTitleDisplayed()"+e.getMessage());
        }
    }

    @FindBy(id="com.mandir.debug:id/tv_location_selector")
    private WebElement locationTab;

    @StepInfo(info = "checking if Location tab displayed in daily panchang page")
    public boolean isLocationTabDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if Location tab displayed in daily panchang page");
            awaitForElement(driver, locationTab);
            return locationTab.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in  isLocationTabDisplayed()"+e.getMessage());
        }
    }

    @FindBy(id="com.mandir.debug:id/radio_daily")
    private WebElement dailyPanchangTab;

    @StepInfo(info = "checking if daily panchang tab is displayed")
    public boolean isdailyPanchangTabDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if daily panchang tab is displayed");
            awaitForElement(driver, dailyPanchangTab);
            return dailyPanchangTab.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in  isdailyPanchangTabDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "clicking on Daily Panchang tab")
    public void clickOnDailyPanchangTab() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("clicking on Daily Panchang tab");
            awaitForElement(driver, dailyPanchangTab);
            clickOnElement(dailyPanchangTab);
        }catch (Exception e) {
            throw new Exception("Error in  clickOnDailyPanchangTab()"+e.getMessage());
        }
    }

    @FindBy(id="com.mandir.debug:id/radio_monthly")
    private WebElement monthlyPanchangTab;

    @StepInfo(info = "checking if monthly panchang tab is displayed")
    public boolean isMonthlyPanchangTabDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if Monthly panchang tab is displayed");
            awaitForElement(driver, monthlyPanchangTab);
            return monthlyPanchangTab.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in  isMonthlyPanchangTabDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "clicking on monthly Panchang tab")
    public void clickOnMonthlyPanchangTab() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("clicking on monthly Panchang tab");
            awaitForElement(driver, monthlyPanchangTab);
            clickOnElement(monthlyPanchangTab);
        }catch (Exception e) {
            throw new Exception("Error in  clickOnMonthlyPanchangTab()"+e.getMessage());
        }
    }

    @FindBy(id="com.mandir.debug:id/tv_dayInfo_title")
    private WebElement dayInfo;

    @StepInfo(info = "checking if day info is displayed in panchang page")
    public boolean isDayInfoDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if day info is displayed in panchang page");
            awaitForElement(driver, dayInfo);
            return dayInfo.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in  isDayInfoDisplayed()"+e.getMessage());
        }
    }

    @FindBy(id="com.mandir.debug:id/iv_image_moon")
    private WebElement moonImage;

    @StepInfo(info = "checking if moon image displayed in panchang card")
    public boolean isMoonImageDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if moon image displayed in panchang card");
            awaitForElement(driver, moonImage);
            return moonImage.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in  isMoonImageDisplayed()"+e.getMessage());
        }
    }

    @FindBy(id="com.mandir.debug:id/tv_day")
    private WebElement moonCardTitle;

    @StepInfo(info = "checking if moon card title is displayed in panchang page")
    public boolean isMoonCardTitleDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if moon card title is displayed in panchang page");
            awaitForElement(driver, moonCardTitle);
            return moonCardTitle.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in  isMoonCardTitleDisplayed()"+e.getMessage());
        }

    }

    @FindBy(id="com.mandir.debug:id/tv_time")
    private WebElement moonCardTimeSlot;

    @StepInfo(info = "checking if moon card time details  displayed in panchang page")
    public boolean isMoonCardTimeSlotTimeDetailsDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if moon card time details  displayed in panchang page");
            awaitForElement(driver, moonCardTimeSlot);
            return moonCardTimeSlot.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in  isMoonCardTimeSlotTimeDetailsDisplayed()"+e.getMessage());
        }
    }

    @FindBy(id="com.mandir.debug:id/tv_month")
    private WebElement monthDetailsInMoonCard;

    @StepInfo(info = "checking if month details displayed in moon card")
    public boolean isMonthDetailsDisplayedInMoonCard() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if month details displayed in moon card");
            awaitForElement(driver, monthDetailsInMoonCard);
            return monthDetailsInMoonCard.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in  isMonthDetailsDisplayedInMoonCard()"+e.getMessage());
        }
    }

    @FindBy(id="com.mandir.debug:id/tv_season")
    private WebElement seasonDetailsInMoonCard;

    @StepInfo(info = "checking if season details displayed in moon card")
    public boolean isSeasonDetailsDisplayedInMoonCard() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if season details displayed in moon card");
            awaitForElement(driver, seasonDetailsInMoonCard);
            return seasonDetailsInMoonCard.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in  isSeasonDetailsDisplayedInMoonCard()"+e.getMessage());
        }
    }

    @FindBy(id="com.mandir.debug:id/fab_share")
    private WebElement floatingShareIcon;

    @StepInfo(info = "checking if floating share icon is displayed in panchang page")
    public boolean isFloatingShareIconIsDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if floating share icon is displayed in panchang page");
            awaitForElement(driver, floatingShareIcon);
            return floatingShareIcon.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in  isFloatingShareIconIsDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "scrolling till shubha ashuba samaya")
    public void scrollTillShubhaAshbhaSamay() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("scrolling till shubha ashuba samaya");
            waitOrPause(2);
            swipeByCoordinates(driver,435,1230,435,500);
        }catch (Exception e) {
            throw new Exception("Error in  scrollTillShubhaAshbhaSamay()"+e.getMessage());
        }
    }

    @StepInfo(info = "scrolling till suryoday suryastha ")
    public void scrollTillSuryodaySuryastha() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("scrolling till shubha ashuba samaya");
            waitOrPause(2);
            swipeByCoordinates(driver,435,1230,435,500);
        }catch (Exception e) {
            throw new Exception("Error in  scrollTillShubhaAshbhaSamay()"+e.getMessage());
        }
    }

    @FindBy(id="com.mandir.debug:id/tv_goodTimes_title")
    private WebElement shubhaAshubaSamayaTitle;

    @StepInfo(info = "checking if shubha ashubha samaya title is displayed")
    public boolean isShubhaAshubhaSamayTitleDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if shubha ashubha samaya title is displayed");
            awaitForElement(driver, shubhaAshubaSamayaTitle);
            return shubhaAshubaSamayaTitle.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in  isShubhaAshubhaSamayTitleDisplayed()"+e.getMessage());
        }
    }
    @FindBy(xpath = "//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_goodTimes_title']/following-sibling::android.widget.FrameLayout")
    private WebElement shubhaAshubhaSamayCards;

    @StepInfo(info = "checking if shubha ashubha samaya card  displayed")
    public boolean isShubhaAshubhaSamayCardsDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if shubha ashubha samaya card  displayed");
            awaitForElement(driver, shubhaAshubhaSamayCards);
            return shubhaAshubhaSamayCards.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in  isShubhaAshubhaSamayCardsDisplayed()"+e.getMessage());
        }
    }

    @FindBy(xpath = "//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_title1']")
    private  WebElement shubhaAshubhaSamayCardTitle;

    @StepInfo(info = "checking if shubha ashubha samaya card  title is displayed")
    public boolean isShubhaAshubhaSamayCardTitleDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if shubha ashubha samaya card  title is displayed");
            awaitForElement(driver, shubhaAshubhaSamayCardTitle);
            return shubhaAshubhaSamayCardTitle.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in  shubhaAshubhaSamayCardTitle()"+e.getMessage());
        }
    }

    @FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_desc1']")
    private WebElement shubhaAshubhaSamayCardDescription;

    @StepInfo(info = "checking if shubha ashubha samaya card  description is displayed")
    public boolean isShubhaAshubhaSamayCardDescriptionDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if shubha ashubha samaya card  description is displayed");
            awaitForElement(driver, shubhaAshubhaSamayCardDescription);
            return shubhaAshubhaSamayCardDescription.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in  isShubhaAshubhaSamayCardDescriptionDisplayed()"+e.getMessage());
        }
    }

    @FindBy(id="com.mandir.debug:id/tv_sun_title")
    private WebElement suryodaySuryasthaTitle;

    @StepInfo(info = "checking if suryoday suryastha title is displayed")
    public boolean isSuryodaySuryasthaTitleDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if suryoday suryastha title is displayed");
            scrollToElement(driver,TestData.suryodaySuryasthaText);
            awaitForElement(driver, suryodaySuryasthaTitle);
            return suryodaySuryasthaTitle.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in  isSuryoDaySuryasthaTitleDisplayed()"+e.getMessage());
        }
    }

    @FindBy(xpath = "//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_sun_title']/following-sibling::android.widget.FrameLayout")
    private WebElement suryodaySuryasthaCards;

    @StepInfo(info = "checking if suryoday suryastha cards is displayed")
    public boolean isSuryodaySuryasthaCardsDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if suryoday suryastha cards is displayed");
            awaitForElement(driver, suryodaySuryasthaCards);
            return suryodaySuryasthaCards.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in isSuryodaySuryasthaCardsDisplayed()"+e.getMessage());
        }
    }

    @FindBy(xpath = "//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_icon_1']")
    private WebElement suryodaySuryasthaCardIcon;

    @StepInfo(info = "checking if suryoday suryastha cards icon displayed")
    public boolean isSuryodaySuryasthaCardIconIsDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if suryoday suryastha cards icon displayed");
            awaitForElement(driver, suryodaySuryasthaCardIcon);
            return suryodaySuryasthaCardIcon.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in isSuryodaySuryasthaCardIconIsDisplayed()"+e.getMessage());
        }
    }

    @FindBy(xpath = "//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_title_1']")
    private WebElement suryodaySuryasthaCardTitle;

    @StepInfo(info = "checking if suryoday suryastha cards title displayed")
    public boolean isSuryodaySuryasthaCardTitleDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if suryoday suryastha cards title displayed");
            awaitForElement(driver, suryodaySuryasthaCardTitle);
            return suryodaySuryasthaCardTitle.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in isSuryodaySuryasthaCardTitleDisplayed()"+e.getMessage());
        }
    }

    @FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_desc_1']")
    private WebElement suryodaySuryasthaCardDescription;

    @StepInfo(info = "checking if suryoday suryastha cards description displayed")
    public boolean isSuryodaySuryasthaCardDescriptionDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if suryoday suryastha cards description displayed");
            awaitForElement(driver, suryodaySuryasthaCardDescription);
            return suryodaySuryasthaCardDescription.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in isSuryodaySuryasthaCardDisplayed()"+e.getMessage());
        }
    }

    @FindBy(xpath = "//android.widget.FrameLayout[@resource-id='com.mandir.debug:id/card1']")
    private WebElement sampoornaPanchangCard;

    @StepInfo(info = "checking if sampoorna Panchnag card title displyed")
    public boolean isSampoornaPanchangCardDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if sampoorna Panchnag card title displyed");
            scrollToElement(driver,TestData.sampoornaPanchangText);
            awaitForElement(driver, sampoornaPanchangCard);
            return sampoornaPanchangCard.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in isSampoornaPanchangCardDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "scrolling till sampoorna panchang card")
    public void scrollTillSampoornaPanchangCard() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("scrolling till sampoorna panchang card");
            scrollToElement(driver, TestData.sampoornaPanchangText);
        }catch (Exception e) {
            throw new Exception("Error in scrollTillSampoornaPanchangCard()"+e.getMessage());
        }
    }

    private WebElement getTakurPrasiddhPanchangTab(String takurPrasiddPanchangTabText)
    {
        String takurPrasiddhPanchangTextXpathValue="//android.widget.TextView[@text='"+takurPrasiddPanchangTabText+"']";
        return elementUtil.getElement("xpath",takurPrasiddhPanchangTextXpathValue);
    }

    private WebElement getMasikPanchangTab(String masikPanchangTabText)
    {
        String masikPanchangTabTextXpathValue="//android.widget.TextView[@text='"+masikPanchangTabText+"']";
        return elementUtil.getElement("xpath",masikPanchangTabTextXpathValue);
    }

    @StepInfo(info = "checking if takur prasiddh panchang tab displayed")
    public boolean isTakurPrasiddhPanchangTabDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if takur prasiddh panchang tab displayed");
            scrollToElement(driver,TestData.thakurPrasiddPanchangText);
            awaitForElement(driver, getTakurPrasiddhPanchangTab(ExcelUtility.getExcelData("locators", "takurPrasiddhPanchangText", "value")));
            return getTakurPrasiddhPanchangTab( ExcelUtility.getExcelData("locators", "takurPrasiddhPanchangText", "value")).isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in isTakurPrasiddhPanchangTabDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if Masik panchang tab displayed")
    public boolean isMasikPanchnagTabDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if Masik panchang tab displayed");
            scrollToElement(driver,TestData.masikPanchangText);
            awaitForElement(driver, getMasikPanchangTab(ExcelUtility.getExcelData("locators", "masikPanchangText", "value")));
            return getMasikPanchangTab( ExcelUtility.getExcelData("locators", "masikPanchangText", "value")).isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in isMasikPanchnagTabDisplayed()"+e.getMessage());
        }
    }


    @StepInfo(info="Performing Swipe action ")
    public PanchangUIPage swipeHorizontalBar() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("Performing Swipe action");
            Horizontalscroll(driver, 230, 919, 960,922,4);
            PunyaMudraPopUpTest.handlePunyaMudraPopUp(driver);
            new RashifalPage(driver).handlingRashiBottomSheet();
            return this;
        }catch (Exception e)
        {
            throw new Exception("Error in swipeHorizontalBar() "+e.getMessage());
        }
    }

    private WebElement panchangTab(String panchangText) {
        String panchangTabxpathValue="//android.widget.HorizontalScrollView[@resource-id='com.mandir.debug:id/tab_bar']//android.widget.TextView[@text='"+panchangText+"']";
        return elementUtil.getElement("xpath",panchangTabxpathValue );
    }

    @StepInfo(info="clicking on panchang Tab")
    public PanchangPage clickOnPanchangTab() throws Exception {
        try {
            awaitForElement(driver, panchangTab(ExcelUtility.getExcelData("locators","panchangText", "value")));
            clickOnElement(panchangTab(ExcelUtility.getExcelData("locators", "panchangText", "value")));
            PunyaMudraPopUpTest.handlePunyaMudraPopUp(driver);
            ListenerImplimentationclass.testLog.info("clicking on panchang Tab");
            return new PanchangPage(driver);
        }catch (Exception e)
        {
            throw new Exception("Error in clickOnPanchangTab()"+e.getMessage());
        }
    }
}