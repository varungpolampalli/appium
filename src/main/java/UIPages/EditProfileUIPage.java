package UIPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import Pages.EditProfilePage;
import Pages.ProfilePage;
import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ExcelUtility;
import base.ListenerImplimentationclass;
import io.appium.java_client.AppiumDriver;

public class EditProfileUIPage extends AppGenericLib{
	
	public ElementUtil elementUtil;
    public EditProfileUIPage(AppiumDriver driver)
    {
        this.driver=driver;
        elementUtil=new ElementUtil(driver);
        PageFactory.initElements(driver, this);
    }
    
    @FindBy(id="com.mandir.debug:id/back_button_kundli_form")
	private WebElement backBtn;

	@FindBy(id="com.mandir.debug:id/edit_name")
	private WebElement nameTextFeild;

	@FindBy(id="com.mandir.debug:id/save_karein_ep")
	private WebElement saveBtn;

	@FindBy(id="com.mandir.debug:id/male")
	private WebElement maleGenderTab;

	@FindBy(id="com.mandir.debug:id/other")
	private WebElement otherGenderTab;

	@FindBy(xpath="//android.widget.FrameLayout[@resource-id='com.mandir.debug:id/male']/following-sibling::android.widget.FrameLayout[1]")
	private WebElement femaleGenderTab;

	@FindBy(id="com.mandir.debug:id/ep_birth_date")
	private WebElement birthDateSelectionTab;
	
	@FindBy(id="com.mandir.debug:id/ep_birth_time")
	private WebElement birthTimeSelectionTab;
	
	@FindBy(xpath="//android.widget.FrameLayout[@resource-id='com.mandir.debug:id/profile_photo']")
	private WebElement profilePhoto;
	
	@FindBy(id="com.mandir.debug:id/tv_changePhoto")
	private WebElement changePhotoBtn;
	
	@FindBy(id="com.mandir.debug:id/location_edit_profile")
	private WebElement locationTab;
	
	@FindBy(id="com.mandir.debug:id/ep_occupation")
	private WebElement occupationTab;
	
	private WebElement editProfileTitle(String editProfilePageTitle) {
		String editProfilePageTitlexpathValue="//android.widget.TextView[@text='"+editProfilePageTitle+"']";
		return elementUtil.getElement("xpath", editProfilePageTitlexpathValue);
	}
	
	@StepInfo(info="checking if profile page title is displayed")
	public boolean isEditProfilePageTitle() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if profile page title is displayed");
			awaitForElement(driver,  editProfileTitle(ExcelUtility.getExcelData("locators", "editProfilePageTitleText", "value")));
			return  editProfileTitle(ExcelUtility.getExcelData("locators", "editProfilePageTitleText", "value")).isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isEditProfilePageTitle()"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if profile photo is displayed")
	public boolean isProfilePhotDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if profile photo is displayed");
			awaitForElement(driver, profilePhoto);
			return profilePhoto.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isProfilePhotDisplayed()"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if photo badale button is displayed")
	public boolean isChangePhotoBtnDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if photo badale button is displayed");
			awaitForElement(driver, changePhotoBtn);
			return changePhotoBtn.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in  isChangePhotoBtnDisplayed() "+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if userName Text feild is displayed")
	public boolean isNameTextFeildDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if userName Text feild is displayed");
			awaitForElement(driver, nameTextFeild);
			return nameTextFeild.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in  isNameTextFeildDisplayed()"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if male gender tab displayed and selected")
	public boolean isMaleGenderTabDisplayedAndSelected() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if male gender tab displayed and selected");
			awaitForElement(driver, maleGenderTab);
			clickOnElement(maleGenderTab);
			return maleGenderTab.isEnabled();
		}catch (Exception e){
			throw new Exception("Error in isMaleGenderTabSelected()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if female gender tab displayed and selected")
	public boolean isFemaleGenderTabDisplayedAndSelected() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if female gender tab displayed and selected");
			awaitForElement(driver, femaleGenderTab);
			clickOnElement(femaleGenderTab);
			return femaleGenderTab.isEnabled();
		}catch (Exception e){
			throw new Exception("Error in isFemaleGenderTabSelected()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if other gender tab displayed and selected")
	public boolean isOtherGenderTabDisplayedAndSelected() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if other gender tab displayed and selected");
			awaitForElement(driver, otherGenderTab);
			clickOnElement(otherGenderTab);
			return otherGenderTab.isEnabled();
		}catch (Exception e){
			throw new Exception("Error in isOtherGenderTabDisplayedAndSelected()"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if birth date selection tab displayed")
	public boolean isBirthDateSelectionTabDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if birth date selection tab displayed");
			awaitForElement(driver, birthDateSelectionTab);
			return birthDateSelectionTab.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isBirthDateSelectionTabDisplayed()"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if birth time selection tab displayed")
	public boolean isBirthTimeSelectionTabDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if birth time selection tab displayed");
			awaitForElement(driver, birthTimeSelectionTab);
			return birthTimeSelectionTab.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isBirthTimeSelectionTabDisplayed()"+e.getMessage());
		}

	}
	
	@StepInfo(info="checking if location tab displayed")
	public boolean isLocationTabDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if location tab displayed");
			awaitForElement(driver, locationTab);
			return locationTab.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in   isLocationTabDisplayed() "+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if occupation tab displayed")
	public boolean isOccupationTabDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if occupation tab displayed");
			awaitForElement(driver, occupationTab);
			return occupationTab.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isOccupationTabDisplayed() "+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on back button from my Edit Profile page")
	public ProfileHomeUIPage clickOnBackBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on back button from my Edit Profile page");
			awaitForElement(driver, backBtn);
			clickOnElement(backBtn);
			return new ProfileHomeUIPage(driver);
		}catch (Exception e){
			throw new Exception("Error in clickOnBackBtn()"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if back button displayed in edit profile page")
	public boolean isBackBtnDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if back button displayed in edit profile page");
			awaitForElement(driver, backBtn);
			return backBtn.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isBackBtnDisplayed())"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if save button is displayed")
	public boolean isSaveButtonDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if save button is displayed");
			awaitForElement(driver, saveBtn);
			return saveBtn.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isSaveButtonDisplayed()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on save button")
	public ProfileHomeUIPage clickOnSaveButton() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on save button");
			awaitForElement(driver, saveBtn);
			clickOnElement(saveBtn);
			return new ProfileHomeUIPage(driver);
		}catch (Exception e){
			throw new Exception("Error in clickOnSaveButton()"+e.getMessage());
		}
	}
}
