package UIPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import Pages.MahabhandarPage;
import Pages.MenuPage;
import Pages.ProfilePage;
import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ExcelUtility;
import base.ListenerImplimentationclass;
import base.PunyaMudraPopUpTest;
import io.appium.java_client.AppiumDriver;

public class MandirUIPage extends AppGenericLib{
	
	public ElementUtil elementUtil;
	public MandirUIPage(AppiumDriver driver) 
	{
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="com.mandir.debug:id/iv_flower")
	private WebElement FlowerIcon;	
	
	@FindBy(id="com.mandir.debug:id/hamburger_menu")
	private WebElement hamurgerMenu;
	
	@FindBy(id="com.mandir.debug:id/profile_main_mandir")
	private WebElement punyaMudraprofileIcon;
	
	@StepInfo(info="checking if flower icon is displayed in mandir home screen")
	public boolean isFlowerIconDisplayed() throws Exception
	{		
		try {
			ListenerImplimentationclass.testLog.info("checking if flower icon is displayed in mandir home screen");
			awaitForElement(driver, FlowerIcon);
			}catch (Exception e){
				throw new Exception("Error in clickOnFlowerIcon()"+e.getMessage());
			}
		return FlowerIcon.isDisplayed();			
	}
	
	@StepInfo(info="clicking on flower icon from mandir home screen")
	public FlowerBottomSheetUIPage clickOnFlowerIcon() throws Exception
	{
		
		try {
			ListenerImplimentationclass.testLog.info("clicking on flower icon from mandir home screen");
			awaitForElement(driver, FlowerIcon);
			clickOnElement(FlowerIcon);
			}catch (Exception e){
				throw new Exception("Error in clickOnFlowerIcon()"+e.getMessage());
			}
		return new FlowerBottomSheetUIPage(driver);
		
	}
	

    private WebElement getmahabhandarTab(String mahabhandarText) {
        String mahabhandarTextxpathValue="//android.widget.TextView[@text='"+mahabhandarText+"']";
        return elementUtil.getElement("xpath", mahabhandarTextxpathValue);
    }

    @StepInfo(info="clicking on mahabhandar Tab from the bottom navigation bar")
    public MahabhandarPage clickOnMahabhandarTab() throws Exception
    {
        try {
            ListenerImplimentationclass.testLog.info("clicking on mahabhandar Tab from the bottom navigation bar");
            awaitForElement(driver,getmahabhandarTab(ExcelUtility.getExcelData("locators", "mahabhandarText", "value")));
            clickOnElement(getmahabhandarTab(ExcelUtility.getExcelData("locators", "mahabhandarText", "value")));
            PunyaMudraPopUpTest.handlePunyaMudraPopUp(driver);
        }catch (Exception e){
            throw new Exception("Error in clickOnMahabhandarTab()"+e.getMessage());
        }
        return new MahabhandarPage(driver);
    }
    
    @StepInfo(info="Clicking on Hamburger Menu")
	public MenuUIPage clickOnHamburgerMenu() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("Clicking on HamburgerMenu");
		awaitForElement(driver, hamurgerMenu);
		clickOnElement(hamurgerMenu);
		return new MenuUIPage(driver);
		}catch (Exception e){
			throw new Exception("Error in clickOnHamburgerMenu()"+e.getMessage());
		}
	}
    
    @StepInfo(info="clicking on punya mudra profile icon from mandir page")
	public ProfilePage clickOnPunyamudraProfileIcon() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on punya mudra profile icon from mandir page");
		awaitForElement(driver, punyaMudraprofileIcon);
		clickOnElement(punyaMudraprofileIcon);
		return new ProfilePage(driver);
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnPunyamudraProfileIcon()"+e.getMessage());
		}
	}
}
