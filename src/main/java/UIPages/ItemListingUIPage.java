package UIPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ListenerImplimentationclass;
import io.appium.java_client.AppiumDriver;

public class ItemListingUIPage extends AppGenericLib{
	ElementUtil elementUtil;
	public ItemListingUIPage(AppiumDriver driver) {
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_whatsapp']/preceding-sibling::android.widget.TextView[@resource-id='com.mandir.debug:id/tv_toolbar_title']")
	private WebElement itemListingPageTitle;

	@FindBy(xpath = "//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_whatsapp']/following-sibling::android.widget.TextView[@resource-id='com.mandir.debug:id/tv_toolbar_location']")
	private WebElement templeLocation;

	@FindBy(xpath = "//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_store_video']")
	private WebElement templeVideo;

	@FindBy(xpath = "//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_store_image']")
	private WebElement productInfoImage;

	@FindBy(xpath = "//android.widget.LinearLayout[@resource-id='com.mandir.debug:id/indicator']/following-sibling::android.widget.TextView")
	private WebElement templeDescription;

	@FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_metric_text']")
	private WebElement offeringsDoneText;

	@FindBy(xpath = "//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_info_slide_header']/following-sibling::androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.FrameLayout[1]")
	private WebElement offeringItemImage;

	@FindBy(xpath = "//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_info_slide_header']/following-sibling::androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.TextView[@resource-id='com.mandir.debug:id/tv_product_name']")
	private WebElement offeringItemName;

	@FindBy(xpath = "//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_info_slide_header']/following-sibling::androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.TextView[@resource-id='com.mandir.debug:id/tv_product_desc']")
	private WebElement offeringItemDescription;

	@FindBy(xpath = "//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_info_slide_header']/following-sibling::androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.TextView[@resource-id='com.mandir.debug:id/tv_product_price']")
	private WebElement offeringItemPrice;

	@FindBy(xpath = "//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_info_slide_header']/following-sibling::androidx.recyclerview.widget.RecyclerView/\t\n" +
			"android.view.ViewGroup[1]/child::android.widget.FrameLayout[@resource-id='com.mandir.debug:id/fl_add_remove_item']/android.widget.Button")
	private WebElement addToCartBtn;

	@FindBy(xpath = "//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_info_slide_header']/following-sibling::androidx.recyclerview.widget.RecyclerView/\t\n" +
			"android.view.ViewGroup[1]/child::android.widget.TextView[@resource-id='com.mandir.debug:id/tv_info_text']")
	private WebElement offeringItemBalanceDetails;

	@FindBy(xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_info_slide']/android.view.ViewGroup[1]")
	private WebElement userReviewCard;

	@FindBy(xpath = "//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_user_name']")
	private WebElement ratingCardName;

	@FindBy(xpath = "//android.widget.RatingBar[@resource-id='com.mandir.debug:id/rating_bar']")
	private WebElement ratingIcon;

	@FindBy(xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_faq']")
	private WebElement faqSection;

	@FindBy(xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_faq']/android.view.ViewGroup[1]")
	private WebElement faqList;

	@FindBy(xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_faq']/android.view.ViewGroup[1]/android.widget.ImageView")
	private WebElement downCheveronInFAQSection;

	@FindBy(xpath = "//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_whatsapp']")
	private WebElement whatsAppShareIcon;

	@FindBy(xpath = "//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_whatsapp']/preceding-sibling::android.widget.ImageView")
	private WebElement backBtnFrmItemListingpage;

	@FindBy(xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_offerings_details']//androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.FrameLayout[@resource-id='com.mandir.debug:id/fl_add_remove_item']/android.widget.Button")
	private WebElement firstProductAddToCartBtn;

	@StepInfo(info = "clicking on whatsApp share icon from item listing page")
	public boolean isWhatsAppShareIconDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on whatsApp share icon from item listing page");
			awaitForElement(driver, whatsAppShareIcon);
			return whatsAppShareIcon.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isWhatsAppShareIcon()"+e.getMessage());
		}
	}

	@StepInfo(info = "checking if item listing page title is displayed")
	public boolean isItemListingPageTitleDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if item listing page title is displayed");
			awaitForElement(driver, itemListingPageTitle);
			return itemListingPageTitle.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isItemListingPageTitleDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info = "checking if temple location displayed in item listing page")
	public boolean isTempleLocationDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if temple location displayed in item listing page");
			awaitForElement(driver, templeLocation);
			return templeLocation.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isTempleLocationDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info = "checking if temple video is displayed in temple listing page")
	public boolean isTempleVideoDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on temple video from temple listing page");
			awaitForElement(driver, templeVideo);
			return templeVideo.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in clickOnTempleVideo()"+e.getMessage());
		}
	}

	@StepInfo(info = "scrolling temple info images")
	public void scrollTempleInfoImages() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("scrolling temple info images");
			waitOrPause(3);
			swipeByCoordinates(driver, 890, 495, 240, 495);
		}catch (Exception e){
			throw new Exception("Error in scrollTempleInfoImages()"+e.getMessage());
		}
	}

	@StepInfo(info = "checking if product info image is displayed")
	public boolean isProductInfoImageDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if product info image is displayed");
			awaitForElement(driver, productInfoImage);
			return productInfoImage.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isProductInfoImageDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info = "checking if temple description is displayed")
	public boolean isTempleDescriptionDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if temple description is displayed");
			awaitForElement(driver, templeDescription);
			return templeDescription.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isTempleDescriptionDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info = "checking if respective count of offerings done is displayed in item listing page")
	public boolean isCountOfOfferingsDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if respective count of offerings done is displayed in item listing page");
			awaitForElement(driver, offeringsDoneText);
			return offeringsDoneText.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isTempleSlotsDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info = "scrolling till offering list")
	public void scrollToOfferingItem() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("scrolling till offering list");
			swipeByCoordinates(driver, 473, 1542, 473, 453);
		}catch (Exception e){
			throw new Exception("Error in scrollToOfferingItem()"+e.getMessage());
		}
	}

	@StepInfo(info = "checking if offering item image displayed")
	public boolean isOfferingItemImageDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if offering item image displayed");
			awaitForElement(driver, offeringItemImage);
			return offeringItemImage.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isOfferingItemImageDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info = "checking if offering item name displayed")
	public boolean isOfferingItemNameDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if offering item name displayed");
			awaitForElement(driver, offeringItemName);
			return offeringItemName.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isOfferingItemNameDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info = "checking if offering item description displayed")
	public boolean isOfferingItemDescriptionDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if offering item description displayed");
			awaitForElement(driver, offeringItemDescription);
			return offeringItemDescription.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isOfferingItemDescriptionDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info = "checking if offering item price displayed")
	public boolean isOfferingItemPriceDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if offering item price displayed");
			awaitForElement(driver, offeringItemPrice);
			return offeringItemPrice.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isOfferingItemPriceDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info = "checking if offering item balance details displayed")
	public boolean isOfferingItemBalanceDetailsDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if offering item balance details displayed ");
			awaitForElement(driver, offeringItemBalanceDetails);
			return offeringItemBalanceDetails.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isOfferingItemBalanceDetailsDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info = "checking if first product add to cart button displayed")
	public boolean clickOnFirstProductAddToCartBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if first product add to cart button displayed");
			awaitForElement(driver, firstProductAddToCartBtn);
			return firstProductAddToCartBtn.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in clickOnFirstProductAddToCartBtn()"+e.getMessage());
		}
	}

	@StepInfo(info = "checking if user review card displayed in offering listing page")
	public boolean isUserReviewCardDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if user review card displayed in offering listing page");
			awaitForElement(driver, userReviewCard);
			return userReviewCard.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isUserReviewCardDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info = "checking if user name is displayed in rating card")
	public boolean isUserNameDisplayedInRatingCard() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if user name is displayed in rating card");
			awaitForElement(driver, ratingCardName);
			return ratingCardName.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isUserNameDisplayedInRatingCard()"+e.getMessage());
		}
	}

	@StepInfo(info = "checking if rating is displayed in review card")
	public boolean isRatingsDisplayedInReviewCard() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if rating is displayed in review card");
			awaitForElement(driver, ratingIcon);
			return ratingIcon.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isRatingsDisplayedInReviewCard()"+e.getMessage());
		}
	}

	@StepInfo(info = "checking if FAQ section displayed")
	public boolean isFAQSectionDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if FAQ section displayed");
			awaitForElement(driver, faqSection);
			return faqSection.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isFAQSectionDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info = "checking if FAQ list displayed in FAQ section")
	public boolean isRespectiveFAQListDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if FAQ list displayed in FAQ section");
			awaitForElement(driver, faqList);
			return faqList.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isRespectiveFAQListDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info = "checking if down cheveron displayed")
	public boolean clickOnDownCheveron() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if down cheveron displayed");
			awaitForElement(driver, downCheveronInFAQSection);
			return downCheveronInFAQSection.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in clickOnDownCheveron()"+e.getMessage());
		}
	}

	@StepInfo(info = "scrolling till user review card")
	public void scrollTillUserReviewCard(){
		ListenerImplimentationclass.testLog.info("scrolling till user review card");
		waitOrPause(3);
		swipeByCoordinates(driver,543,379,543,927);
	}


}
