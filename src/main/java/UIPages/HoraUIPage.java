package UIPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import Pages.HoraPage;
import TestAnnotations.StepInfo;
import base.BaseTest;
import base.ElementUtil;
import base.ExcelUtility;
import base.ListenerImplimentationclass;
import base.TestData;
import io.appium.java_client.AppiumDriver;

public class HoraUIPage extends BaseTest {
	public ElementUtil elementUtil;
	public HoraUIPage(AppiumDriver driver) 
	{
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id="com.mandir.debug:id/tv_title_hora")
	private WebElement horaTitle;

	@FindBy(id="com.mandir.debug:id/tv_location_selector")
	private WebElement locationSearchModal;

	@FindBy(id="com.mandir.debug:id/tv_hora_title")
	private WebElement dateInformationTab;

	@FindBy(id="com.mandir.debug:id/iv_planet")
	private WebElement planetImage;

	@FindBy(id="com.mandir.debug:id/iv_planet_name")
	private WebElement planetName;

	@FindBy(id="com.mandir.debug:id/iv_planet_desc")
	private WebElement planetTimingSlot;

	@FindBy(id="com.mandir.debug:id/layout_hora_today")
	private WebElement planetCardDetails;
	
	@FindBy(id="com.mandir.debug:id/iv_help_text")
	private WebElement helpButtton;

	@FindBy(id="com.mandir.debug:id/tv_favorable_title")
	private WebElement anukulText;

	@FindBy(id="com.mandir.debug:id/tv_adverse_title")
	private WebElement pratikulText;

	@FindBy(id="com.mandir.debug:id/tv_hora_desc")
	private WebElement horaDescription;

	@FindBy(id="com.mandir.debug:id/tv_hora_title2")
	private WebElement aageaneWalaHora;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_hora']/android.widget.FrameLayout[1]/android.view.ViewGroup[1]//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_expand']")
	private WebElement aageanewalaDownChevron;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_hora']/android.widget.FrameLayout[1]/android.view.ViewGroup[1]//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_planet']")
	private WebElement aageaneWalaPlanetImage;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_hora']/android.widget.FrameLayout[1]/android.view.ViewGroup[1]//android.widget.TextView[@resource-id='com.mandir.debug:id/iv_planet_name']")
	private WebElement aageaneWalaPlanetName;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_hora']/android.widget.FrameLayout[1]/android.view.ViewGroup[1]//android.widget.TextView[@resource-id='com.mandir.debug:id/iv_planet_desc']")
	private WebElement aageaneWalaPlanetTimingSlot;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_hora']/android.widget.FrameLayout[1]/android.view.ViewGroup[1]")
	private WebElement aageanewalaPlanetCardDetails;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_hora']/android.widget.FrameLayout[1]/android.view.ViewGroup[1]//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_hora_desc']")
	private WebElement aageanewalaHoraDescription;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_hora_tomorrow']/android.widget.FrameLayout[1]/android.view.ViewGroup[1]//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_expand']")
	private WebElement kalPlanetDownChevron;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_hora_tomorrow']/android.widget.FrameLayout[1]/android.view.ViewGroup[1]//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_planet']")
	private WebElement kalPlanetImage;
	
	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_hora_tomorrow']/android.widget.FrameLayout[1]/android.view.ViewGroup[1]//android.widget.TextView[@resource-id='com.mandir.debug:id/iv_planet_name']")
	private WebElement kalPlanetName;
	
	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_hora_tomorrow']/android.widget.FrameLayout[1]/android.view.ViewGroup[1]//android.widget.TextView[@resource-id='com.mandir.debug:id/iv_planet_desc']")
	private WebElement kalPlanetTimingSlot;
	
	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_hora_tomorrow']/android.widget.FrameLayout[1]/android.view.ViewGroup[1]")
	private WebElement kalPlanetCardDetails;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_hora_tomorrow']/android.widget.FrameLayout[1]/android.view.ViewGroup[1]//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_hora_desc']")
	private WebElement kalHoraDescription;
	
	@FindBy(id="com.mandir.debug:id/radio_tomorrow")
	private WebElement kalBtn;
	
	@FindBy(id="com.mandir.debug:id/fab_share")
	private WebElement floatingWhatsappIcon;

	
	private WebElement HoraTab(String  horaTabTitle) {
		String HoraTabTitlexpathValue="//android.widget.TextView[@text='"+horaTabTitle+"']";
		return elementUtil.getElement("xpath", HoraTabTitlexpathValue);
	}

	@StepInfo(info="checking Hora title is displayed")
	public boolean isHoraTitleDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking Hora title is displayed");
			awaitForElement(driver, horaTitle);
			return horaTitle.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in getting isHoraTitleDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking location search modal is displayed")
	public boolean isLocationSearchModalDisplayed() throws Exception
	{

		try {
			ListenerImplimentationclass.testLog.info("checking location search modal is displayed");
			awaitForElement(driver, locationSearchModal);
			return locationSearchModal.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in isLocationSearchModalIsDisplayed()"+e.getMessage());
		}
	}


	@StepInfo(info="checking Date information tab is displayed")
	public boolean isDateInformationTabDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking Date information tab is displayed");
			awaitForElement(driver, dateInformationTab);
			return dateInformationTab.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in getting isDateInformationTabDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking planet image is displayed")
	public boolean isPlanetImageDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking planet image is displayed");
			awaitForElement(driver, planetImage);
			return planetImage.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in getting isPlanetImageDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="swipe down until AnukulPratikul")
	public void swipedownUntilPratikul() 
	{
		waitOrPause(2);
		swipeByCoordinates(driver, 505, 1457, 505, 518);
	}

	
	@StepInfo(info="swipe uo till kal Btn")
	public void swipeupTillKal() 
	{
		ListenerImplimentationclass.testLog.info("swipe uo till kal Btn");
		Verticalscroll(driver, 481, 815, 481, 1480, 3);
	}

	@StepInfo(info="checking planet Name is displayed")
	public boolean isPlanetNameDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking planet Name is displayed");
			awaitForElement(driver, planetName);
			return planetName.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in getting isPlanetNameDisplayed()"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking anukulText is displayed")
	public boolean isPlanetAnukulDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking anukulText is displayed");
			awaitForElement(driver, anukulText);
			return anukulText.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in getting isPlanetAnukulDisplayed()"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking pratikul is displayed")
	public boolean isPlanetPratikulDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking pratikul is displayed");
			awaitForElement(driver, pratikulText);
			return pratikulText.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in getting isPlanetPratikulDisplayed()"+e.getMessage());
		}
	}
	
	

	@StepInfo(info="checking planet timing slot is displayed")
	public boolean isPlanetTimingSlotDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking planet timing slot is displayed");
			awaitForElement(driver, planetTimingSlot);
			return planetTimingSlot.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in getting isPlanetTimingSlotDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking planet Card Details is displayed")
	public boolean isPlanetCardDetailsDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking planet Card Details is displayed");
			awaitForElement(driver, planetCardDetails);
			return planetCardDetails.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in getting isPlanetCardDetailsDisplayed()"+e.getMessage());
		}
	}


	//	@StepInfo(info="checking planet downchevaron is displayed")
	//	public boolean isPlanetDownchevaronDisplayed() throws Exception 
	//	{
	//		try {
	//			ListenerImplimentationclass.testLog.info("checking planet downchevaron is displayed");
	//			awaitForElement(driver, downChevron);
	//			return downChevron.isDisplayed();
	//		}catch (Exception e)
	//		{
	//			throw new Exception("Error in getting isPlanetCardDetailsDisplayed()"+e.getMessage());
	//		}
	//	}

	@StepInfo(info="checking Hora description is displayed")
	public boolean isHoraDescriptionDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking Hora description is displayed");
			awaitForElement(driver, horaDescription);
			return horaDescription.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in getting isHoraDescriptionDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking AageanewalaHora is displayed")
	public boolean isAageanewalaHoraDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking AageanewalaHora is displayed");
			awaitForElement(driver, aageaneWalaHora);
			return aageaneWalaHora.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in getting isAageanewalaHoraDisplayed()"+e.getMessage());
		}
	}
	
	
	@StepInfo(info="clicking on Aageanewala downChevaron")
	public HoraUIPage clickOnDownChevaron() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("clicking on Aageanewala downChevaron");
			awaitForElement(driver, aageanewalaDownChevron);
			clickOnElement(aageanewalaDownChevron);
		}catch (Exception e)
		{
			throw new Exception("Error in getting clickOnDownChevaron()"+e.getMessage());
		}
		return this;
	}
	
	

	@StepInfo(info="checking Aageanewala PlanetImage is displayed")
	public boolean isAageanewalaDPlanetImageDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking Aageanewala PlanetImage is displayed");
			awaitForElement(driver, aageaneWalaPlanetImage);
			return aageaneWalaPlanetImage.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in getting isAageanewalaDPlanetImageDisplayed()"+e.getMessage());
		}
	}


	@StepInfo(info="checking Aageanewala PlanetName is displayed")
	public boolean isAageanewalaPlanetNameDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking Aageanewala PlanetName is displayed");
			awaitForElement(driver, aageaneWalaPlanetName);
			return aageaneWalaPlanetName.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in getting isAageanewalaPlanetNameDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking Aageanewala PlanetTimingSlot is displayed")
	public boolean isAageanewalaDPlanetTimingSlotDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking Aageanewala PlanetTimingSlot is displayed");
			awaitForElement(driver, aageaneWalaPlanetTimingSlot);
			return aageaneWalaPlanetTimingSlot.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in getting isAageanewalaDPlanetTimingSlotDisplayed()"+e.getMessage());
		}
	}


	@StepInfo(info="checking Aageanewala PlanetCardDetails is displayed")
	public boolean isAageanewalaPlanetCardDetailstDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking Aageanewala PlanetCardDetails is displayed");
			awaitForElement(driver, aageanewalaPlanetCardDetails);
			return aageanewalaPlanetCardDetails.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in getting isAageanewalaPlanetCardDetailstDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking Aageanewala HoraDescription is displayed")
	public boolean isAageanewalaHoraDescriptionDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking Aageanewala HoraDescription is displayed");
			awaitForElement(driver,aageanewalaHoraDescription);
			return aageanewalaHoraDescription.isDisplayed();
		}catch(Exception e)
		{
			throw new Exception("Error in getting isAageanewalaHoraDescriptionDisplayed()"+e.getMessage());

		}
	}

	@StepInfo(info="clicking on kal button")
	public HoraUIPage clickOnKalBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on kal button");
			awaitForElement(driver, kalBtn);
			clickOnElement(kalBtn);
		}catch (Exception e){
			throw new Exception("Error in  clickOnKalBtn()"+e.getMessage());
		}
		return this;
}
		
	@StepInfo(info="clicking on kal planet down chevron")
	public HoraUIPage clickOnKalPlanetDownChevaron() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on kal planet down chevron");
			awaitForElement(driver, kalPlanetDownChevron);
			clickOnElement(kalPlanetDownChevron);
		}catch (Exception e){
			throw new Exception("Error in  clickOnKalPlanetDownChevaron()"+e.getMessage());
		}
		return this;
   }
	
	@StepInfo(info="checking kal planet image is displayed")
	public boolean isKalPlanetImageDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking kal planet image is displayed");
			awaitForElement(driver,kalPlanetImage);
			return kalPlanetImage.isDisplayed();
		}catch(Exception e)
		{
			throw new Exception("Error in getting isKalPlanetImageDisplayed()"+e.getMessage());

		}
	}
	
	
	@StepInfo(info="checking kal planet name is displayed")
	public boolean isKalPlanetNameDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking kal planet name is displayed");
			awaitForElement(driver,kalPlanetName);
			return kalPlanetName.isDisplayed();
		}catch(Exception e)
		{
			throw new Exception("Error in getting isKalPlanetNameDisplayed()"+e.getMessage());

		}
	}
	
	
	@StepInfo(info="checking kal planet timing slot is displayed")
	public boolean isKalPlanetTimingSlotDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking kal planet timing slot is displayed");
			awaitForElement(driver,kalPlanetTimingSlot);
			return kalPlanetTimingSlot.isDisplayed();
		}catch(Exception e)
		{
			throw new Exception("Error in getting isKalPlanetTimingSlotDisplayed()"+e.getMessage());

		}
	}
	
	
	@StepInfo(info="checking kal planet card details is displayed")
	public boolean isKalPlanetCardDetailsDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking kal planet card details is displayed");
			awaitForElement(driver,kalPlanetCardDetails);
			return kalPlanetCardDetails.isDisplayed();
		}catch(Exception e)
		{
			throw new Exception("Error in getting isKalPlanetCardDetailsDisplayed()"+e.getMessage());

		}
	}
	
	
	@StepInfo(info="checking kal Hora description is displayed")
	public boolean isKalHoraDescriptionDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking kal Hora description is displayed");
			awaitForElement(driver,kalHoraDescription);
			return kalHoraDescription.isDisplayed();
		}catch(Exception e)
		{
			throw new Exception("Error in getting isKalHoraDescriptionDisplayed()"+e.getMessage());

		}
	}
	
	@StepInfo(info="checking floating whatsapp icon is displayed")
	public boolean isFloatingWhatsAppIconDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking floating whatsapp icon is displayed");
			awaitForElement(driver,floatingWhatsappIcon);
			return floatingWhatsappIcon.isDisplayed();
		}catch(Exception e)
		{
			throw new Exception("Error in getting isFloatingWhatsAppIconDisplayed()"+e.getMessage());

		}
	}
	
	@StepInfo(info="clicking on Hora Tab ")
	public HoraUIPage clickOnHoraTab() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("clicking on Hora Ta");
			awaitForElement(driver,HoraTab(ExcelUtility.getExcelData("locators", "horaTabTitle", "value")));
			clickOnElement(HoraTab(ExcelUtility.getExcelData("locators", "horaTabTitle", "value")));
		}catch (Exception e){
			throw new Exception("Error in clickOnHoraTab()"+e.getMessage());
		}
		return this;
	}
	

	@StepInfo(info="swipe down until AageAnewalehora ")
	public void verticalscrollTillAageAnewaleHora() 
	{
		ListenerImplimentationclass.testLog.info("swipe down until AageAnewalehora");
		waitOrPause(2);
		Verticalscroll(driver, 461, 1524, 461,1016, 2);
	}
	
	
	
	@StepInfo(info="checking help icon is displayed")
	public boolean isHelpIconDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking help icon is displayed");
			awaitForElement(driver,helpButtton);
			return helpButtton.isDisplayed();
		}catch(Exception e)
		{
			throw new Exception("Error in getting isHelpIconDisplayed()"+e.getMessage());

		}
	}
}
