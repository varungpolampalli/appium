package UIPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import Pages.LoginBottomSheetPage;
import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.FileUtility;
import base.ListenerImplimentationclass;
import base.PunyaMudraPopUpTest;
import io.appium.java_client.AppiumDriver;

public class LoginBottomSheetUIPage extends AppGenericLib {
	
	ElementUtil elementUtil;
	public LoginBottomSheetUIPage(AppiumDriver driver) {
		this.driver=driver;
		 elementUtil=new ElementUtil(driver);
		 PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="com.mandir.debug:id/btn_otp_login")
	private WebElement otpLogInBtn;
	
	@FindBy(id="com.mandir.debug:id/mobile_number")
	private WebElement mobileNumberTextFeild;
	
	@FindBy(id="com.mandir.debug:id/bt_login")
	private WebElement logInBtn;

	@FindBy(id="com.mandir.debug:id/otp_view")
	private WebElement otpTextFeildTextFeild;
	
	@FindBy(id="com.mandir.debug:id/bt_verify_otp")
	private WebElement continueBtn;
	
	@FindBy(id="com.mandir.debug:id/design_bottom_sheet")
	private WebElement loginBottomSheet;
	
	
	
	//business library
	@StepInfo(info="Clicking on LogIn button")
	public LoginBottomSheetUIPage clickOnPhoneLogInBtn() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("Clicking on LogIn button");
		awaitForElement(driver, otpLogInBtn);
		clickOnElement(otpLogInBtn);
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnPhoneLogInBtn()"+e.getMessage());
		}
		return this;
		
	}
	
	@StepInfo(info="Typing Mobile Number to Mobile Number Text feild")
	public LoginBottomSheetUIPage enterMobileNumber() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("Typing Mobile Number to Mobile Number Text feild");
		awaitForElement(driver, mobileNumberTextFeild);
		type(mobileNumberTextFeild, FileUtility.getPropertyValue("phoneNumber"));
		}catch (Exception e){
			throw new Exception("Error in EnteringmobileNumber()"+e.getMessage());
		}
		return this;
	}
	
	@StepInfo(info="clicking on Login Button")
	public LoginBottomSheetUIPage clickOnLogInBtn() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on Login Button");
		awaitForElement(driver, logInBtn);
		clickOnElement(logInBtn);
		}catch (Exception e){
			throw new Exception("Error in clickingOnLogInBtn()"+e.getMessage());
		}
		return this;
	}
	
	@StepInfo(info="Typing Otp to Otp textfeild")
	public LoginBottomSheetUIPage enterOtp() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("Typing Otp to Otp textfeild");
		awaitForElement(driver, otpTextFeildTextFeild);
		type(otpTextFeildTextFeild, FileUtility.getPropertyValue("otp"));
		}catch (Exception e){
			throw new Exception("Error in EnteringOTp()"+e.getMessage());
		}
		return this;
	}
	
	@StepInfo(info="Clicking on continue Btn")
	public LoginBottomSheetUIPage clickOnContinueBtn() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("Clicking on continue Btn");
		awaitForElement(driver, continueBtn);
		clickOnElement(continueBtn);
		PunyaMudraPopUpTest.handlePunyaMudraPopUp(driver);
		}catch (Exception e){
			throw new Exception("Error in clickingOnContinueBtn"+e.getMessage());
		}
		return this;
		
	}
	
	@StepInfo(info="checking if login Bottom sheet is displayed")
	public boolean isLoginBottomSheetDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if login Bottom sheet is displayed");
		awaitForElement(driver, loginBottomSheet);
		return loginBottomSheet.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isLoginBottomSheetDisplayed()"+e.getMessage());
		}
	}
	

}
