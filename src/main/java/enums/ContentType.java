package enums;

public enum ContentType {

        JSON("application/json"),
        FORM_URL_ENCODED("application/x-www-form-urlencoded"),
        MULTIPART_FORM_DATA("multipart/form-data"),
        OPEN_XML_FORMATS("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"),
        IMAGE_JPEG("image/jpeg"),
        IMAGE_PNG("image/png"),
        TEXT_HTML("text/html"),
        TEXT_PLAIN("text/plain");

        private final String contentType;

        ContentType(String contentType){
            this.contentType = contentType;
        }

        public  String toString(){
            return contentType;
        }

}

