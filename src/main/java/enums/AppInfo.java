package enums;
/**
 * 
 * @author TestYantra
 *
 */
public enum AppInfo {

    ANDROID_AUTOMATION_NAME("UiAutomator2"),
    ANDROID_PLATFORM_NAME("android"),
    ANDROID_APP_PACKAGE("com.mandir.debug"),
    ANDROID_APP_ACTIVITY(".v2.main.MainActivity"),
    PLATFORM("android"),
    SERVER_URL("http://localhost:4723"),
    EMULATOR_UDDI(""),
    DEVICE_UDID("")
    ;




    String label;
    AppInfo(String label)
    {
        this.label=label;
    }

    public String getLabel()
    {
        return  label;
    }
}
