package Pages;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ExcelUtility;
import base.ListenerImplimentationclass;
import base.PunyaMudraPopUpTest;
import io.appium.java_client.AppiumDriver;

public class MandirPage extends AppGenericLib
{
	public ElementUtil elementUtil;
	public MandirPage(AppiumDriver driver) 
	{
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}

	private WebElement getaajTab(String aajText) {
		String aajTextxpathValue="//android.widget.TextView[@text='"+aajText+"']";
		return elementUtil.getElement("xpath", aajTextxpathValue);
	}

	private WebElement getsangeethTab(String sangeethText) {
		String sangeethTextxpathValue="//android.widget.TextView[@text='"+sangeethText+"']";
		return elementUtil.getElement("xpath", sangeethTextxpathValue);
	}

	private WebElement getmahabhandarTab(String mahabhandarText) {
		String mahabhandarTextxpathValue="//android.widget.TextView[@text='"+mahabhandarText+"']";
		return elementUtil.getElement("xpath", mahabhandarTextxpathValue);
	}

	private WebElement getsamudayaTab(String samudayaText) {
		String samudayaTextxpathValue="//android.widget.TextView[@text='"+samudayaText+"']";
		return elementUtil.getElement("xpath", samudayaTextxpathValue);
	}

	@FindBy(id="com.mandir.debug:id/iv_gods")
	private WebElement godImage;

	@FindBy(id="com.mandir.debug:id/iv_diya_aarthi")
	private WebElement aarthiThaliImage;

	@FindBy(id="com.mandir.debug:id/hamburger_menu")
	private WebElement hamurgerMenu;

	@FindBy(id="com.mandir.debug:id/nxtBtn")
	private WebElement onBoardingTutorialNextBtn;
	
	@FindBy(id="com.mandir.debug:id/iv_aartiMode")
	private WebElement aarthiCTAicon;
	
	@FindBy(id="com.mandir.debug:id/fl_play")
	private WebElement songIcon;
	
	@FindBy(id="com.mandir.debug:id/iv_header")
	private WebElement mandirPageHeader;
	
	@FindBy(id="com.mandir.debug:id/tv_player")
	private WebElement songTitle;
	
	@FindBy(id="com.mandir.debug:id/navigation_main_mandir")
	private WebElement mandirIcon;
	
	@FindBy(id="com.mandir.debug:id/iv_flower")
	private WebElement FlowerIcon;	
	
	@FindBy(id="com.mandir.debug:id/tv_avatar_onboarding")
	private WebElement secondTimeOnBoardingTooltip;
	
	@FindBy(id="com.mandir.debug:id/headerTitle")
	private WebElement onBoardingTutorialTitle;
	
	@FindBy(id="com.mandir.debug:id/ic_add_god_icon")
	private WebElement editGodBtn;
	
	@FindBy(id="com.mandir.debug:id/nav_view")
	private WebElement mandirBottomNavigationBar;
	
	@FindBy(id="com.mandir.debug:id/tooltip_diya")
	private WebElement diaToolTipForFirstAppOpen;
	
	@FindBy(id="com.mandir.debug:id/iv_aarthi")
	private WebElement diaAarthiThali;
	
	@FindBy(id="com.mandir.debug:id/profile_main_mandir")
	private WebElement punyaMudraprofileIcon;
	
	@FindBy(id="com.mandir.debug:id/empty_temple_fram")
	private WebElement UYOGIcon;
	
	@FindBy(id="com.mandir.debug:id/snackbar_text")
	private WebElement noInternetConnectionSnackBarText;
	
	@FindBy(id="com.mandir.debug:id/iv_calendar")
    private WebElement panchangIcon;
	
	@FindBy(id="android:id/parentPanel")
	private WebElement appExitDialogBox;
	
	@FindBy(id="android:id/button2")
	private WebElement nahiButton;
	
	@FindBy(id="android:id/button2")
	private WebElement okButton;
	
	
	//business libraries

	
	@StepInfo(info="clicking on flower icon from mandir home screen")
	public FlowerBottomSheetPage clickOnFlowerIcon() throws Exception
	{
		
		try {
			ListenerImplimentationclass.testLog.info("Clicking on Flower icon");
			awaitForElement(driver, FlowerIcon);
			clickOnElement(FlowerIcon);
			}catch (Exception e){
				throw new Exception("Error in clickOnFlowerIcon()"+e.getMessage());
			}
		return new FlowerBottomSheetPage(driver);
		
	
	}
	
	

	@StepInfo(info="clicking on aaj Tab from the bottom navigation bar")
	public AajPage clickOnAajTab() throws Exception
	{
		try {
		ListenerImplimentationclass.testLog.info("clicking on aaj Tab from the bottom navigation bar");
		awaitForElement(driver,getaajTab(ExcelUtility.getExcelData("locators", "aajText","value")));
		clickOnElement(getaajTab(ExcelUtility.getExcelData("locators", "aajText","value")));	
		}catch (Exception e){
			throw new Exception("Error in clickOnAajTab()"+e.getMessage());
		}
		return new AajPage(driver);
	}

	@StepInfo(info="clicking on sangeeth Tab from the bottom navigation bar")
	public SangeethPage clickOnSangeethTab() throws Exception
	{
		try {
		ListenerImplimentationclass.testLog.info("clicking on sangeeth Tab from the bottom navigation bar");
		awaitForElement(driver,getsangeethTab(ExcelUtility.getExcelData("locators", "sangeethText","value")));
		clickOnElement(getsangeethTab(ExcelUtility.getExcelData("locators", "sangeethText", "value")));
		waitOrPause(3);
		}catch (Exception e){
			throw new Exception("Error in clickOnGodSangeethTab()"+e.getMessage());
		}
		return new SangeethPage(driver);
		
	}
	@StepInfo(info="clicking on mahabhandar Tab from the bottom navigation bar")
	public MahabhandarPage clickOnMahabhandarTab() throws Exception
	{
		try {
		ListenerImplimentationclass.testLog.info("clicking on mahabhandar Tab from the bottom navigation bar");
		awaitForElement(driver,getmahabhandarTab(ExcelUtility.getExcelData("locators", "mahabhandarText", "value")));
		clickOnElement(getmahabhandarTab(ExcelUtility.getExcelData("locators", "mahabhandarText", "value")));
		PunyaMudraPopUpTest.handlePunyaMudraPopUp(driver);
		}catch (Exception e){
			throw new Exception("Error in clickOnMahabhandarTab()"+e.getMessage());
		}
		return new MahabhandarPage(driver);
	}
	
	
	@StepInfo(info="clicking on samudaya Tab from the bottom navigation bar")
	public SamudayPage clickOnSamudayaTab() throws Exception
	{
		try {
		ListenerImplimentationclass.testLog.info("clicking on samudaya Tab from the bottom navigation bar");
		awaitForElement(driver,getsamudayaTab(ExcelUtility.getExcelData("locators", "samudayText", "value")));
		clickOnElement(getsamudayaTab(ExcelUtility.getExcelData("locators", "samudayText", "value")));
		}catch (Exception e){
			throw new Exception("Error in clickOnSamudayaTab()"+e.getMessage());
		}
		return new SamudayPage(driver);
		
	}

	@StepInfo(info="Getting the screenshot of Mandir page God")
	public File getScreenshotOfGod() throws Exception {
		try {
		//		awaitForElement(driver,getgodImage());
		ListenerImplimentationclass.testLog.info("Getting the screenshot of Mandir page God");
		Thread.sleep(6000);
		try {
			customWait(driver, godImage);
		}catch(Exception e) 
		{
			System.err.println("stale element reference Exception");
		}
		}catch (Exception e){
			throw new Exception("Error in getScreenShotofGod()"+e.getMessage());
		}
		return takeScreenshot(driver, "actualGodImg"); 
	}

	@StepInfo(info="Getting the screenshot of Aarthi Thali")
	public File getaarthiThaliImage() throws Exception {
		try {
		//		awaitForElement(driver, getgodImage());
		awaitForElement(driver, aarthiThaliImage);
		ListenerImplimentationclass.testLog.info("Getting the screenshot of Aarthi Thali");
		try {
			customWait(driver, aarthiThaliImage);
		}catch(Exception e) 
		{

		}
		}catch (Exception e){
			throw new Exception("Error in getaarthiThaliImage()"+e.getMessage());
		}
		return takeScreenshot(aarthiThaliImage, "aarthiThali");
	}

	@StepInfo(info="Clicking on Hamburger Menu")
	public MenuPage clickOnHamburgerMenu() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("Clicking on HamburgerMenu");
		awaitForElement(driver, hamurgerMenu);
		clickOnElement(hamurgerMenu);
		}catch (Exception e){
			throw new Exception("Error in clickOnHamburgerMenu()"+e.getMessage());
		}
		return new MenuPage(driver);
	}

	@StepInfo(info="Clicking on Next button")
	public void clickOnNextBtn() throws Exception {
		try {
//		ListenerImplimentationclass.testLog.info("Clicking on Next button");
//		awaitForElement(driver,onBoardingTutorialNextBtn);
		clickOnElement(onBoardingTutorialNextBtn);
		}catch (Exception e){
			throw new Exception("Error in clickOnNextBtn()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if the god is displayed in the mandir page")
	public boolean isGodDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if the god is displayed in the mandir page");
		awaitForElement(driver, godImage);
		}catch (Exception e){
			throw new Exception("Error in isGodDisplayed()"+e.getMessage());
		}
		return godImage.isDisplayed();
	}
	
	@StepInfo(info="checking if aarthi Thali is displayed on landing Mandir Page")
	public boolean isAarthiThaliDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if aarthi Thali is displayed on landing Mandir Page");
		}catch (Exception e){
			throw new Exception("Error in isAarthiThaliDisplayed"+e.getMessage());
		}
		return aarthiThaliImage.isDisplayed();
	}
	
	@StepInfo(info="Clicking on Aarthi CTA icon")
	public MandirPage clickOnAarthiCTAicon() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("Clicking on aarthiCTA icon");
		awaitForElement(driver, aarthiCTAicon);
		clickOnElement(aarthiCTAicon);
		}catch (Exception e){
			throw new Exception("Error in clickOnAarthiThaliCTAicon()"+e.getMessage());
		}
		return this;
	}
	
	@StepInfo(info="Dragging aarthi Thali")
	public void dragAarthiThali() throws Exception {
		try {
		awaitForElement(driver, aarthiThaliImage);
		ListenerImplimentationclass.testLog.info("Dragging The Aarthi Thali");
		dragAndDrop(559, 1386,545, 770);
		waitOrPause(5);
		}catch (Exception e)
		{
			throw new Exception("Error in dragAarthiThali()"+e.getMessage());
		}
	}
	

	
	@StepInfo(info="Clicking on Song Icon")
	public MusicBottomSheetPage clickOnSongIcon() throws Exception {
		try {
		awaitForElement(driver, songIcon);
		ListenerImplimentationclass.testLog.info("Clicking on Song Icon");
		clickOnElement(songIcon);
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnSongIcon()"+e.getMessage());
		}
		
		return new MusicBottomSheetPage(driver);
		
	}
	
	
	@StepInfo(info="checking if the song title is displayed below the song icon")
	public String getsongTitleBelowSongIcon() {
		ListenerImplimentationclass.testLog.info("checking if the song title is displayed below the song icon");
		return songTitle.getText();
	}
	
	@StepInfo(info="clicking on mandir icon from the bottom navigation bar")
	public MandirPage clickOnMandirIcon() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on mandir icon from the bottom navigation bar");
		awaitForElement(driver, mandirIcon);
		clickOnElement(mandirIcon);
		return this;
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnMandirIcon()"+e.getMessage());
		}
		
	}
	
	@StepInfo(info="checking if second time onboarding tutorial displayed")
	public boolean isSecondTimeOnboardingTutorialDisplayed() throws Exception {
		try {
		return secondTimeOnBoardingTooltip.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in isSecondTimeOnboardingTutorialDisplayed()"+e.getMessage());
		}
		
	}
	
	
	@StepInfo(info="tapping on the screen to ignore the tutorial")
	public MandirPage tapAnywhereOnScreen() throws Exception {
		try {
//		ListenerImplimentationclass.testLog.info("tapping on the screen to ignore the tutorial");
		tapByCoordinates(535, 871);
		return this;
		}catch (Exception e)
		{
			throw new Exception("Error in  tapAnyWhereOnScreen()"+e.getMessage());
		}
		
	}
	
	@StepInfo(info="checking if sangeeth onboarding tutorial displayed")
	public String getOnboardingTutorialTitle() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if sangeeth onboarding tutorial displayed");
		return onBoardingTutorialTitle.getText();
		}catch (Exception e)
		{
			throw new Exception("Error in  getOnboardingTutorialTitle()"+e.getMessage());
		}
		
	}
	
	@StepInfo(info="clicking on edit god flow button in mandir page")
	public void clickOnEditGodBtn() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on edit god flow button in mandir page");
		clickOnElement(editGodBtn);
		}catch (Exception e)
		{
			throw new Exception("Error in  clickOnEditGodBtn()"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if bottom navigation Bar is displayed")
	public boolean isBottomNavigationDisplayed() throws Exception {
		try {
			awaitForElement(driver, mandirBottomNavigationBar);
			ListenerImplimentationclass.testLog.info("checking  bottom navigation Bar is displayed");
		}catch (Exception e)
		{
			throw new Exception("Error in isMandirBottomSheetDisplayed()"+e.getMessage());
		}
		return mandirBottomNavigationBar.isDisplayed();
	}
	
	@StepInfo(info="checking if dia tool tip is displayed for first app open for the day")
	public boolean isDiaToolTipDisplayedForFirstAppOpenForDay() throws Exception {
		try {
//		ListenerImplimentationclass.testLog.info("checking if dia tool tip is displayed for first app open for the day");
		return diaToolTipForFirstAppOpen.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in isMandirBottomSheetDisplayed()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on  Aarthi Thali to light Dia")
	public void clickOnDiaThali() throws Exception {
		try {
//		ListenerImplimentationclass.testLog.info("clicking on  Aarthi Thali to light Dia");
		awaitForElement(driver, diaAarthiThali);
		clickOnElement(diaAarthiThali);
		}catch (Exception e)
		{
			throw new Exception("Error in isMandirBottomSheetDisplayed()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on punya mudra profile icon from mandir page")
	public ProfilePage clickOnPunyamudraProfileIcon() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on punya mudra profile icon from mandir page");
		awaitForElement(driver, punyaMudraprofileIcon);
		clickOnElement(punyaMudraprofileIcon);
		return new ProfilePage(driver);
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnPunyamudraProfileIcon()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on UYOG frame from mandir page")
	public void swipingTillUYOGIconAndClick() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on UYOG frame from mandir page");
		scrollToHorizontalElementAndClick(driver, 165, 858, UYOGIcon,6);
		}catch (Exception e)
		{
			throw new Exception("Error in swipingTillUYOGIconAndClick()"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if no internet connection message is displayed in snackbar")
	public boolean isNoInternetConnectionSnakBarMessageDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if no internet connection message is displayed ");
			awaitForElement(driver, noInternetConnectionSnackBarText);
			return noInternetConnectionSnackBarText.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isNoInternetConnectionSnakBarMessageDisplayed() "+e.getMessage()); 
		}
	}
	
	@StepInfo(info="clicking on panchang  icon from mandir page")
    public PanchangPage clickOnPanchangIcon() throws Exception {
        try {
        ListenerImplimentationclass.testLog.info("clicking on panchang icon from mandir page");
        awaitForElement(driver, panchangIcon);
        clickOnElement(panchangIcon);
        return new PanchangPage(driver);
        }catch (Exception e)
        {
            throw new Exception("Error in clickOnPanchangIcon()"+e.getMessage());
        }
    }
	
	@StepInfo(info="getting on boarding page")
	public void handleOnBoardingTutorial() throws Exception
	{
		ListenerImplimentationclass.testLog.info("getting on boarding page");
		MandirPage mandirPage=new MandirPage(driver);
		awaitForElement(driver, onBoardingTutorialTitle);
		for(int i=0;i<3;i++)
		{
			mandirPage.tapAnywhereOnScreen();
		}
		PunyaMudraPopUpTest.handlePunyaMudraPopUp(driver);
	}
	
	@StepInfo(info="checking if App Exit Dialog Box is displayed")
	public boolean isAppExitDialogBoxDisplayed() throws Exception {
		try {
			awaitForElement(driver, appExitDialogBox);
			ListenerImplimentationclass.testLog.info("checking if App Exit Dialog Box is displayed");
		}catch (Exception e)
		{
			throw new Exception("Error in isAppExitDialogBoxDisplayed()"+e.getMessage());
		}
		return appExitDialogBox.isDisplayed();
	}
	
	@StepInfo(info="clicking on Nahi Button on app exit dialog box")
	public void clickOnNahiButton() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on Nahi Button on app exit dialog box");
		awaitForElement(driver, nahiButton);
		clickOnElement(nahiButton);
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnNahiButton()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on Ok Button on app exit dialog box")
	public void clickOnOkButton() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on Ok Button on app exit dialog box");
		awaitForElement(driver, okButton);
		clickOnElement(okButton);
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnOkButton()"+e.getMessage());
		}
	}
}
