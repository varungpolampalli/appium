package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ExcelUtility;
import base.ListenerImplimentationclass;
import base.TestData;
import io.appium.java_client.AppiumDriver;

public class MoreCommunitiesPage extends AppGenericLib{
	
	ElementUtil elementUtil;
	
	public MoreCommunitiesPage(AppiumDriver driver) {
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}	
	
	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir:id/rv_community_list']//android.view.ViewGroup[1]//android.widget.TextView[@resource-id='com.mandir:id/header_title']")
	private WebElement  moreCommunitiesHeader;
	
	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir:id/rv_community_list']/android.view.ViewGroup[1]//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir:id/social_list_vertical_cards_rv']/android.view.ViewGroup[1]//android.widget.TextView[@resource-id='com.mandir:id/tv_description']")
	private WebElement communityDescriptionInLokPriyaSection;
	
	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir:id/rv_community_list']/android.view.ViewGroup[1]//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir:id/social_list_vertical_cards_rv']")
	private WebElement  listOfCommunitiesInLokPriyaSection;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir:id/rv_community_list']/android.view.ViewGroup[2]//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir:id/social_list_vertical_cards_rv']")
	private WebElement  listOfCommunitiesInAnyaSamudaySection;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir:id/rv_community_list']/android.view.ViewGroup[1]//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir:id/social_list_vertical_cards_rv']/android.view.ViewGroup[1]")
	private WebElement  firstCardInMoreCommunites;

	@FindBy(xpath="com.mandir:id/tab_community_title")
	private WebElement communityHeaderInCommunityFeed;
	
	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir:id/rv_community_list']/android.view.ViewGroup[1]//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir:id/social_list_vertical_cards_rv']/android.view.ViewGroup[1]//android.widget.TextView[@resource-id='com.mandir:id/bt_add_community']")
	private WebElement joinBtnInMoreCommunities;
		
	@FindBy(xpath="//android.widget.ImageView[@resource-id='com.mandir:id/bt_add_community_tick']")
	private WebElement tickMark;
	
	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir:id/rv_community_list']/android.view.ViewGroup[1]//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir:id/social_list_vertical_cards_rv']/android.view.ViewGroup[1]//android.widget.ImageView[@resource-id='com.mandir:id/iv_community']")
	private WebElement profileImageInMoreCommunities;
	
	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir:id/rv_community_list']/android.view.ViewGroup[1]//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir:id/social_list_vertical_cards_rv']/android.view.ViewGroup[1]//android.widget.TextView[@resource-id='com.mandir:id/subtitle']")
	private WebElement memberCountInCommunities;
	
	private WebElement getSamudayaText(String samudayText)
	{
		String SamudayaTextxpathValue="//android.widget.TextView[@text='"+samudayText+"']";
		return elementUtil.getElement("xpath", SamudayaTextxpathValue);
	}
	
	
	@StepInfo(info="checking if more communities header displayed")
	public boolean isMoreCommunityHeaderDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if more communities header displayed");
			awaitForElement(driver, moreCommunitiesHeader);
			return moreCommunitiesHeader.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isMoreCommunityHeaderDisplayed() "+e.getMessage());
		}
	}	
	
	@StepInfo(info="checking if list of communities displayed in lokpriya section")
	public boolean isListOfCommunitiesInLokPriyaSectionDisplayed()  throws Exception{
		try {
			ListenerImplimentationclass.testLog.info("checking if list of communities displayed in lokpriya section");
			awaitForElement(driver,listOfCommunitiesInLokPriyaSection);
			return listOfCommunitiesInLokPriyaSection.isDisplayed();
		}catch(Exception e) {
			throw new Exception("Error in  isListofCommunitiesInLokPriyaSectionDisplayed() "+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if community Description displayed in lokpriya section")
	public boolean isCommunityDescriptionInLokpriyaSectionDisplayed()  throws Exception{
		try {
			ListenerImplimentationclass.testLog.info("checking if community Description displayed in lokpriya section");
			awaitForElement(driver,communityDescriptionInLokPriyaSection);
			return communityDescriptionInLokPriyaSection.isDisplayed();
		}catch(Exception e) {
			throw new Exception("Error in  isCommunityDescriptionInLokpriyaSectionDisplayed() "+e.getMessage());
		}
	}
	
	@StepInfo(info="scrolling till Anya samudaya")
	public MoreCommunitiesPage scrollTillAnyaSamudaya()
	{
    scrollToElement(driver,TestData.anyaSamudayText);
	ListenerImplimentationclass.testLog.info("scrolling till Anya samudaya");
	return this;
	}	
	
	@StepInfo(info="scrolling till Anya samudaya cards ")
	public MoreCommunitiesPage scrollingTillAnyaSamudayCards() throws Exception {
		   ListenerImplimentationclass.testLog.info("scrolling till Anya samudaya cards");
	       waitOrPause(3);
           swipeByCoordinates(driver, 447, 1625, 488, 212);
           return this;
	}
	
	@StepInfo(info="checking if List of Communities In Anya Samudaya Section Displayed in lokpriya section")
	public boolean isListofCommunitiesInAnyaSamudayaSectionDisplayed()  throws Exception{
		try {
			ListenerImplimentationclass.testLog.info("checking if List of Communities In Anya Samudaya Section Displayed in lokpriya section");
			awaitForElement(driver,listOfCommunitiesInLokPriyaSection);
			return listOfCommunitiesInLokPriyaSection.isDisplayed();
		}
		catch(Exception e) {
			throw new Exception("Error in  isListofCommunitiesInAnyaSamudayaSectionDisplayed() "+e.getMessage());
		}
	}	
	
	@StepInfo(info="clicking on first card in more community page")
	public void clickOnFirstCardInMoreCommunities() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on first card in more community page");
		awaitForElement(driver, firstCardInMoreCommunites);
		clickOnElement(firstCardInMoreCommunites);
		}catch (Exception e) {
			throw new Exception("Error in clickOnFirstCardInMoreCommunities()"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if community Header In community Feed Displayed ")
	public boolean isCommunityHeaderInCommunityFeedDisplayed()  throws Exception{
		try {
			ListenerImplimentationclass.testLog.info("checking if community Header In community Feed Displayed");
			awaitForElement(driver,communityHeaderInCommunityFeed);
			return communityHeaderInCommunityFeed.isDisplayed();
		}
		catch(Exception e) {
			throw new Exception("Error in  isCommunityHeaderInCommunityFeedDisplayed() "+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on join btn in more community page")
	public void clickOnJoinBtnInMoreCommunities() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on join btn in more community page");
		awaitForElement(driver, joinBtnInMoreCommunities);
		clickOnElement(joinBtnInMoreCommunities);
		}catch (Exception e) {
			throw new Exception("Error in clickOnJoinBtnInMoreCommunities()"+e.getMessage());
		}
	}
		
	@StepInfo(info="checking if tick btn Displayed ")
	public boolean isTickBtnDisplayed()  throws Exception{
		try {
			ListenerImplimentationclass.testLog.info("checking if tick btn Displayed ");
			awaitForElement(driver,tickMark);
			return tickMark.isDisplayed();
		}
		catch(Exception e) {
			throw new Exception("Error in  isTickBtnDisplayed() "+e.getMessage());
		}
	}
		
	@StepInfo(info="checking if profile image Displayed ")
	public boolean isProfileImageDisplayed()  throws Exception{
		try {
			ListenerImplimentationclass.testLog.info("checking if profile image Displayed");
			awaitForElement(driver,profileImageInMoreCommunities);
			return profileImageInMoreCommunities.isDisplayed();
		}
		catch(Exception e) {
			throw new Exception("Error in  isProfileImageDisplayed() "+e.getMessage());
		}
	}	
	
	@StepInfo(info="checking if member count Displayed ")
	public boolean isMemberCountDisplayed()  throws Exception{
		try {
			ListenerImplimentationclass.testLog.info("checking if member count Displayed");
			awaitForElement(driver,memberCountInCommunities);
			return memberCountInCommunities.isDisplayed();
		}
		catch(Exception e) {
			throw new Exception("Error in  isMemberCountDisplayed() "+e.getMessage());
		}
	}	
	
	@StepInfo(info="Fetching more communities  title")
	public String getMoreCommunitiesPageTitle() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("Fetching more communities  title");
			awaitForElement(driver, getSamudayaText(ExcelUtility.getExcelData("locators", "samudayText","value")));
			return getSamudayaText(ExcelUtility.getExcelData("locators", "samudayText","value")).getText();
		}catch (Exception e){
			throw new Exception("Error in getMoreCommunitiesPageTitle"+e.getMessage());
		}
	}
}





