package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ExcelUtility;
import base.ListenerImplimentationclass;
import base.PunyaMudraPopUpTest;
import io.appium.java_client.AppiumDriver;

public class MukhyaPage extends AppGenericLib {

	ElementUtil elementUtil;
	public MukhyaPage(AppiumDriver driver)
	{
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	} 

	
	@FindBy(xpath="//android.widget.FrameLayout[@resource-id='com.mandir.debug:id/card_header']/android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_header']")
	private WebElement kundliCard;
	
	@FindBy(id="com.mandir.debug:id/rv_mb_cards")
	private WebElement mukhyaCards;

	private WebElement mukhyaTabTitle(String  mukhyaTabTitle)
	{
		String mukhyaTabTitlexpathValue="//android.widget.TextView[@text='"+mukhyaTabTitle+"']";
		return elementUtil.getElement("xpath", mukhyaTabTitlexpathValue);
	}

	private WebElement panchangCard(String panchangText)
	{
		String panchangCardxpathValue="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_mb_cards']//android.widget.TextView[@text='"+panchangText+"']";
		return elementUtil.getElement("xpath",panchangCardxpathValue);

	}

	private WebElement rashifalCard(String rashifalText )
	{
		String rashifalCardxpathValue="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_mb_cards']//android.widget.TextView[@text='"+rashifalText+"']";
		return elementUtil.getElement("xpath",rashifalCardxpathValue);

	}
	
	private WebElement horaCard(String horaTabTitle)
	{
		String horaCardxpathValue="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_mb_cards']//android.widget.TextView[@text='"+horaTabTitle+ "']";
		return elementUtil.getElement("xpath", horaCardxpathValue);
	}

	
	private WebElement chowgadiyaCard(String chowgadiyaText )
	
	{
		String chowgadiyaCardxpathValue="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_mb_cards']//android.widget.TextView[@text='"+chowgadiyaText+ "']";
		return elementUtil.getElement("xpath", chowgadiyaCardxpathValue);
	}
	
	
	private WebElement sahithyaCard(String sahithyaText )
	
	{
		String sahithyaCardxpathValue="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_mb_cards']//android.widget.TextView[@text='"+sahithyaText+"']";
		return elementUtil.getElement("xpath", sahithyaCardxpathValue);

	}
	
	private WebElement suvicharCard(String suvichaarText )
	
	{
		String suvicharCardxpathValue="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_mb_cards']//android.widget.TextView[@text='"+suvichaarText+"']";
		return elementUtil.getElement("xpath", suvicharCardxpathValue);

	}

    private WebElement geethaShlokCard(String geethaShlokTabText)
	
	{
		String geethaShlokCardxpathValue="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_mb_cards']//android.widget.TextView[@text='"+geethaShlokTabText+"']";
		return elementUtil.getElement("xpath", geethaShlokCardxpathValue);

	}
	  
    private WebElement tvCard(String tvText)
	
	{
		String tvCardxpathValue="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_mb_cards']//android.widget.TextView[@text='"+tvText+"']";
		return elementUtil.getElement("xpath", tvCardxpathValue);

	}
     
    private WebElement sujavCard(String sujavText)
	
	{
		String sujavCardxpathValue="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_mb_cards']//android.widget.TextView[@text='"+sujavText+"']";
		return elementUtil.getElement("xpath", sujavCardxpathValue);

	}
    
	@StepInfo(info="clicking on Mukhya tab")
	public MukhyaPage clickOnMukyatab() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("clicking on Mukhya tab");
			awaitForElement(driver,mukhyaTabTitle(ExcelUtility.getExcelData("locators", "mukhyaTabTitle", "value")));
			clickOnElement(mukhyaTabTitle(ExcelUtility.getExcelData("locators", "mukhyaTabTitle", "value")));
			waitOrPause(3);
		}
		catch (Exception e)
		{
			throw new Exception("Error in clickOnMukyatab()"+e.getMessage());
		}
		return this;
	}

	@StepInfo(info = "clicking on panchang card")
	public PanchangPage clickOnPanchangCard() throws Exception{
		try
		{
			ListenerImplimentationclass.testLog.info("clicking on panchang card");
			awaitForElement(driver, panchangCard(ExcelUtility.getExcelData("locators", "panchangText", "value")));
			clickOnElement(panchangCard(ExcelUtility.getExcelData("locators", "panchangText", "value")));
			PunyaMudraPopUpTest.handlePunyaMudraPopUp(driver);
			return new PanchangPage(driver);
		}catch(Exception e)
		{
			throw new Exception("Error in clickOnPanchangCard()"+e.getMessage());
		}
		
	}

	@StepInfo(info = "clicking on rashifal card")
	public RashifalPage clickOnRashifalCard() throws Exception
	{
		try
		{
			ListenerImplimentationclass.testLog.info("clicking on rashifal card");
			awaitForElement(driver, rashifalCard(ExcelUtility.getExcelData("locators", "rashifalText", "value")));
			clickOnElement(rashifalCard(ExcelUtility.getExcelData("locators", "rashifalText", "value")));
			PunyaMudraPopUpTest.handlePunyaMudraPopUp(driver);
			return new RashifalPage(driver);
		}
		catch(Exception e)
		{
			throw new Exception("Error in clickOnRashifalCard()"+e.getMessage());
		}
		
	}
		
	@StepInfo(info = "clicking on Hora card")
	public HoraPage clickOnHoraCard() throws Exception
	{
		try
		{
			ListenerImplimentationclass.testLog.info("clicking on Hora card");
			awaitForElement(driver, horaCard(ExcelUtility.getExcelData("locators", "horaTabTitle", "value")));
			clickOnElement(horaCard(ExcelUtility.getExcelData("locators", "horaTabTitle", "value")));
		}
		catch(Exception e)
		{

			throw new Exception("Error in clickOnHoraCard()"+e.getMessage());
		}
		return new HoraPage(driver);
	}
		
	@StepInfo(info = "clicking on chowgadiya card")
	public ChowgadiyaPage clickOnChowgadiyaCard() throws Exception
	{
		try
		{
			ListenerImplimentationclass.testLog.info("clicking on chowgadiya card");
			awaitForElement(driver, chowgadiyaCard(ExcelUtility.getExcelData("locators", "chowgadiyaText", "value")));
			clickOnElement(chowgadiyaCard(ExcelUtility.getExcelData("locators", "chowgadiyaText", "value")));
			return new ChowgadiyaPage(driver);
		}
		catch(Exception e)
		{
			throw new Exception("Error in clickOnChowgadiyaCard()"+e.getMessage());
		}
		
	}
		
	@StepInfo(info = "clicking on Sahithya card")
	public SahithyaPage clickOnSahithyaCard() throws Exception
	{
		try
		{
			ListenerImplimentationclass.testLog.info("clicking on Sahithya card");
			awaitForElement(driver, sahithyaCard(ExcelUtility.getExcelData("locators", "sahithyaText", "value")));
			clickOnElement(sahithyaCard(ExcelUtility.getExcelData("locators", "sahithyaText", "value")));
		}
		catch(Exception e)
		{
			throw new Exception("Error in clickOnSahithyaCard()"+e.getMessage());
		}
		return new SahithyaPage(driver);
	}
	
	@StepInfo(info = "clicking on suvichaar card")
	public SuvicharPage clickOnSuvichaarCard() throws Exception
	{
		try
		{
			ListenerImplimentationclass.testLog.info("clicking on suvichaar card");
			awaitForElement(driver, suvicharCard(ExcelUtility.getExcelData("locators", "suvicharTabText", "value")));
			clickOnElement(suvicharCard(ExcelUtility.getExcelData("locators", "suvicharTabText", "value")));
			return new SuvicharPage(driver);
		}
		catch(Exception e)
		{

			throw new Exception("Error in clickOnSuvichaarCard()"+e.getMessage());
		}
		
	}
	
	@StepInfo(info = "clicking on geethashlokcard card")
	public GeethaShlokPage clickOnGeethashlokCard() throws Exception
	{
		try
		{
			ListenerImplimentationclass.testLog.info("clicking on geethashlokcard card");
			awaitForElement(driver, geethaShlokCard(ExcelUtility.getExcelData("locators", "geethaShlokTabText", "value")));
			clickOnElement(geethaShlokCard(ExcelUtility.getExcelData("locators", "geethaShlokTabText", "value")));
			return new GeethaShlokPage(driver);
		}
		catch(Exception e)
		{
			throw new Exception("Error in clickOnGeethashlokCard()"+e.getMessage());
		}
		
	}
	
	
	@StepInfo(info = "clicking on tv card")
	public TvPage clickOnTvCard() throws Exception
	{
		try
		{
			ListenerImplimentationclass.testLog.info("clicking on geethashlokcard card");
			awaitForElement(driver, tvCard(ExcelUtility.getExcelData("locators", "tvText", "value")));
			clickOnElement(tvCard(ExcelUtility.getExcelData("locators", "tvText", "value")));
			return new TvPage(driver);
		}
		catch(Exception e)
		{
			throw new Exception("Error in clickOnTvCard()"+e.getMessage());
		}
		
	}
	
	@StepInfo(info="Performing Swipe action")
    public MukhyaPage swipeHorizontalBarTillMukhya() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("Performing Swipe action");
            Horizontalscroll(driver, 230, 919, 960,922,8);
            return this;
        }
        catch (Exception e)
        {
            throw new Exception("Error in swipeHorizontalBarTillMukhya() "+e.getMessage());
        }
    }
		
	@StepInfo(info = "clicking on sujav card")
	public SujavPage clickOnSujavCard() throws Exception
	{
		try
		{
			ListenerImplimentationclass.testLog.info("clicking on sujav card");
			awaitForElement(driver, sujavCard(ExcelUtility.getExcelData("locators", "sujavText", "value")));
			clickOnElement(sujavCard(ExcelUtility.getExcelData("locators", "sujavText", "value")));
			return new SujavPage(driver);
		}
		catch(Exception e)
		{
			throw new Exception("Error in clickOnSujavCard()"+e.getMessage());
		}
		
	}	
	@StepInfo(info = "clicking on kundli card")
	public KundliPage clickOnKundliCard() throws Exception
	{
		try
		{
			ListenerImplimentationclass.testLog.info("clicking on kundli card");
			awaitForElement(driver, kundliCard);
			clickOnElement(kundliCard);
			return new KundliPage(driver);
		}
		catch(Exception e)
		{
			throw new Exception("Error in clickOnKundliCard()"+e.getMessage());
		}
		
	}
	
	@StepInfo(info = "checking if mukhya cards displayed offline mode")
	public boolean isMukhyaCardsDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if mukhya cards displayed in offline mode");
		awaitForElement(driver, mukhyaCards);
		return mukhyaCards.isDisplayed();
		}catch(Exception e)
		{
			throw new Exception("Error in isMukhyaCardsDisplayed()"+e.getMessage());
		}
	}
	
   }




