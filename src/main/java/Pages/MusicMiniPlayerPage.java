package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ListenerImplimentationclass;
import io.appium.java_client.AppiumDriver;

public class MusicMiniPlayerPage extends AppGenericLib {
	
ElementUtil elementUtil;
	
	public MusicMiniPlayerPage(AppiumDriver driver) {
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="com.mandir.debug:id/player_ll")
	private WebElement miniPlayerTab;
	
	@FindBy(id="com.mandir.debug:id/song_play_pause_button")
	private WebElement playPauseBtn;
	
	@FindBy(id="com.mandir.debug:id/song_fullscreen_button")
	private WebElement expandBtn;
	
	@FindBy(id="com.mandir.debug:id/ic_mini_player_close")
	private WebElement closeBtn;
	
	
	@StepInfo(info="checking if mini player is displayed in mahabhandar page")
	public boolean isMiniPlayerDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if mini player is displayed");
		awaitForElement(driver, miniPlayerTab);
		}catch (Exception e){
			throw new Exception("Error in isMiniPlayerIsDisplayed()"+e.getMessage());
		}
		return miniPlayerTab.isDisplayed();
	}
	
	@StepInfo(info="clicking on play or pause Btn")
	public MusicMiniPlayerPage clickOnPlayPauseBtn() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on Play or Pause Button");
		clickOnElement(playPauseBtn);
		waitOrPause(3);
		return this;
		}catch (Exception e){
			throw new Exception("Error in clickOnPlayOrPauseBtn()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on expand button from the mini player")
	public void clickOnExpandBtn() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on Maximize button from the mini player");
		clickOnElement(expandBtn);
		}catch (Exception e){
			throw new Exception("Error in clickOnExpandBtn()"+e.getMessage());
		}
		
	}
	
	
	
	@StepInfo(info="clicking on close button from the mini player")
	public void clickOnCloseBtn() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on close button from the mini player");
		clickOnElement(closeBtn);
		}catch (Exception e){
			throw new Exception("Error in clickOnCloseBtn()"+e.getMessage());
		}
		
	}
	

}
