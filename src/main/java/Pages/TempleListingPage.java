package Pages;

import base.TestData;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ExcelUtility;
import base.ListenerImplimentationclass;
import io.appium.java_client.AppiumDriver;

public class TempleListingPage extends AppGenericLib{
	ElementUtil elementUtil;
	public TempleListingPage(AppiumDriver driver) {
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id="com.mandir.debug:id/tv_toolbar_title")
	private WebElement chadavPageTitle;

	@FindBy(id="com.mandir.debug:id/iv_back")
	private WebElement backBtn;

	@FindBy(xpath = "//android.widget.TextView[@text='"+ TestData.openTempleName +"']")
	private WebElement chandiDeviTempleCard;

	@FindBy(xpath = "//android.widget.HorizontalScrollView[@resource-id='com.mandir.debug:id/tabLayout']//android.widget.LinearLayout[3]")
	private WebElement sampannaSevayeTab;

	@FindBy(xpath="//*[@resource-id='com.mandir.debug:id/rv_stores']/descendant::*[@resource-id='com.mandir.debug:id/card_root'][2]")
	private WebElement listOfTemples;

	@FindBy(id="com.mandir.debug:id/tv_title")
	private WebElement templeTitle;

	@FindBy(xpath="//*[@resource-id='com.mandir.debug:id/rv_stores']/descendant::*[@resource-id='com.mandir.debug:id/card_root'][2]/descendant::android.widget.ImageView[@resource-id='com.mandir.debug:id/lottie_image']")
	private WebElement templeImage;

	private WebElement subTitle(String templeNameTextText) {
		String subtitleTextxpathValue="//android.widget.TextView[@text='"+templeNameTextText+"']/following-sibling::android.widget.TextView[@resource-id='com.mandir.debug:id/tv_sub_title']";
		return elementUtil.getElement("xpath", subtitleTextxpathValue);
	}
	
	private WebElement templeDescription(String templeNameTextText) {
		String templeDescriptionTextxpathValue="//android.widget.TextView[@text='"+templeNameTextText+"']/following-sibling::android.widget.TextView[@resource-id='com.mandir.debug:id/tv_description']";
		return elementUtil.getElement("xpath", templeDescriptionTextxpathValue);
	}
	
	private WebElement chadavaOfferingsImage(String templeNameTextText) {
		String chadavaOfferingsImagexpathValue="//android.widget.TextView[@text='"+templeNameTextText+"']/following-sibling::android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_items']";
		return elementUtil.getElement("xpath", chadavaOfferingsImagexpathValue);
	}
	
	private WebElement offeringsButton(String templeNameTextText) {
		String offeringsButtonxpathValue="//android.widget.TextView[@text='"+templeNameTextText+"']/following-sibling::android.widget.Button[@resource-id='com.mandir.debug:id/btn_offer_now']";
		return elementUtil.getElement("xpath", offeringsButtonxpathValue);
	}

	private WebElement offeringsAvailablityTab(String templeNameTextText) {
		String offeringsAvailablityTabxpathValue="//android.widget.TextView[@text='"+templeNameTextText+"']/parent::android.view.ViewGroup/android.view.ViewGroup";
		return elementUtil.getElement("xpath", offeringsAvailablityTabxpathValue);
	}
	
	private WebElement offeringsAvailablityImage(String templeNameTextText) {
		String offeringsAvailablityImagexpathValue="//android.widget.TextView[@text='"+templeNameTextText+"']/parent::android.view.ViewGroup/android.view.ViewGroup/android.widget.ImageView";
		return elementUtil.getElement("xpath", offeringsAvailablityImagexpathValue);
	}
	
	@FindBy(id="com.mandir.debug:id/cta")
	private WebElement watsAppShareIcon;

	@FindBy(id="com.mandir.debug:id/design_bottom_sheet")
	private WebElement watsAppShareBottomSheet;

	private WebElement mandirKaSuchiTab(String mandirKaSuchiText) {
		String mandirKaSuchiTabTextxpathValue="//android.widget.LinearLayout[@content-desc='"+mandirKaSuchiText+"']";
		return elementUtil.getElement("xpath", mandirKaSuchiTabTextxpathValue);
	}

	@StepInfo(info="fetching Chadav Seva page title")
	public String getChadavSevaPageTitle() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("fetching Chadav Seva page title");
			awaitForElement(driver, chadavPageTitle);
			return chadavPageTitle.getText();
		}catch (Exception e){
			throw new Exception("Error in getChadavSevaPageTitle()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking on back button from chadav Seva Page")
	public void clickOnBackBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on back button from chadav Seva Page");
			awaitForElement(driver, backBtn);
			clickOnElement(backBtn);
			waitOrPause(2);
		}catch (Exception e){
			throw new Exception("Error in clickOnBackBtn()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking on temple card")
	public ItemListingPage clickOnChandiDeviTempleCard() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on temple card");
			awaitForElement(driver, chandiDeviTempleCard);
			clickOnElement(chandiDeviTempleCard);
			return new ItemListingPage(driver);
		}catch (Exception e){
			throw new Exception("Error in clickOnChandiDeviTempleCard()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking on sampanna sevaye tab")
	public void clickOnSampannaSevayeTab() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on sampanna sevaye tab");
			awaitForElement(driver, sampannaSevayeTab);
			clickOnElement(sampannaSevayeTab);
		}catch (Exception e){
			throw new Exception("Error in clickOnSampannaSevayeTab()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking on mandir ka suchi tab")
	public void clickOnMandirKaSuchiTab() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on mandir ka suchi tab");
			awaitForElement(driver, mandirKaSuchiTab(ExcelUtility.getExcelData("locators", "mandirKaSuchiText", "value")));
			clickOnElement(mandirKaSuchiTab(ExcelUtility.getExcelData("locators", "mandirKaSuchiText", "value")));
		}catch (Exception e){
			throw new Exception("Error in clickOnMandirKaSuchiTab()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if list of temples displayed in temple listing page")
	public boolean isTempleListDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if list of temples displayed in temple listing page");
			awaitForElement(driver, listOfTemples);
			return listOfTemples.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isTempleListDisplayed()"+e.getMessage());
		}
	}


	@StepInfo(info="checking if Temple Title displayed on temple Card")
	public boolean isTempleTitleDisplayed() throws Exception{
		try {
			ListenerImplimentationclass.testLog.info("verifying Temple Title displayed on temple Card");
			awaitForElement(driver,templeTitle);
			return templeTitle.isDisplayed();
		} catch (Exception e) {
			throw new Exception("Error in isTempleTitleDisplayed()"+e.getMessage());		}
	}

	@StepInfo(info="fetching Temple Title text from temple Card")
	public String getTempleTitle() throws Exception{
		try {
			ListenerImplimentationclass.testLog.info("fetching Temple Title text from temple Card");
			awaitForElement(driver,templeTitle);
			return templeTitle.getText();
		} catch (Exception e) {
			throw new Exception("Error in getTempleTitle()"+e.getMessage());		}
	}

	@StepInfo(info="checking if Temple image displayed on temple Card")
	public boolean isTempleImageDisplayed() throws Exception{
		try {
			ListenerImplimentationclass.testLog.info("verifying Temple image displayed on temple Card");
			awaitForElement(driver,templeImage );
			return templeImage.isDisplayed();
		} catch (Exception e) {
			throw new Exception("Error in isTempleImageDisplayed()"+e.getMessage());		}
	}

	@StepInfo(info="checking if Sub-Title displayed on temple Card")
	public boolean isSubTitleDisplayed() throws Exception{
		try {
			ListenerImplimentationclass.testLog.info("verifying Sub-Title displayed on temple Card");
			awaitForElement(driver, subTitle(ExcelUtility.getExcelData("locators", "templeNameText", "value")));
			return subTitle(ExcelUtility.getExcelData("locators", "templeNameText", "value")).isDisplayed();
		} catch (Exception e) {
			throw new Exception("Error in isSubTitleDisplayed()"+e.getMessage());		}
	}

	@StepInfo(info="checking if Temple Description displayed on temple Card")
	public boolean isTempleDescriptionDisplayed() throws Exception{
		try {
			ListenerImplimentationclass.testLog.info("verifying Temple Description displayed on temple Card");
			awaitForElement(driver, templeDescription(ExcelUtility.getExcelData("locators", "templeNameText", "value")));
			return templeDescription(ExcelUtility.getExcelData("locators", "templeNameText", "value")).isDisplayed();
		} catch (Exception e) {
			throw new Exception("Error in isTempleDescriptionDisplayed()"+e.getMessage());		}
	}

	@StepInfo(info="checking if chadava offerings image displayed")
	public boolean isChadavaOfferingsImageDisplayed() throws Exception{
		try {
			ListenerImplimentationclass.testLog.info("checking if chadava offerings image displayed");
			awaitForElement(driver, chadavaOfferingsImage(ExcelUtility.getExcelData("locators", "templeNameText", "value")));
			return chadavaOfferingsImage(ExcelUtility.getExcelData("locators", "templeNameText", "value")).isDisplayed();
		} catch (Exception e) {
			throw new Exception("Error in isChadavaOfferingsImageDisplayed()"+e.getMessage());		}
	}

	@StepInfo(info="checking if  Offerings Button displayed on temple Card")
	public boolean isOfferingsButtonDisplayed() throws Exception{
		try {
			ListenerImplimentationclass.testLog.info("verifying Offerings Button displayed on temple Card");
			awaitForElement(driver, offeringsButton(ExcelUtility.getExcelData("locators", "templeNameText", "value")));
			return offeringsButton(ExcelUtility.getExcelData("locators", "templeNameText", "value")).isDisplayed();
		} catch (Exception e) {
			throw new Exception("Error in isofferingsButtonDisplayed()"+e.getMessage());		}
	}

	@StepInfo(info="checking if offerings Availablity tab displayed on temple card")
	public boolean isOfferingsAvailablityTabDisplayed() throws Exception{
		try {
			ListenerImplimentationclass.testLog.info("verifying offerings Availablity tab displayed on temple");
			awaitForElement(driver, offeringsAvailablityTab(ExcelUtility.getExcelData("locators", "templeNameText", "value")));
			return offeringsAvailablityTab(ExcelUtility.getExcelData("locators", "templeNameText", "value")).isDisplayed();
		} catch (Exception e) {
			throw new Exception("Error in isOfferingsAvailablityTabDisplayed()"+e.getMessage());		}
	}

	@StepInfo(info="checking if offerings Availablity tab displayed on temple card")
	public boolean isOfferingsAvailablityImageDisplayed() throws Exception{
		try {
			ListenerImplimentationclass.testLog.info("verifying offerings Availablity tab displayed on temple");
			awaitForElement(driver, offeringsAvailablityImage(ExcelUtility.getExcelData("locators", "templeNameText", "value")));
			return offeringsAvailablityImage(ExcelUtility.getExcelData("locators", "templeNameText", "value")).isDisplayed();
		} catch (Exception e) {
			throw new Exception("Error in isOfferingsAvailablityImageDisplayed()"+e.getMessage());		}
	}

	@StepInfo(info="scrolling to temple card")
	public void scrollToTempleCard() throws Exception{
		try {
			ListenerImplimentationclass.testLog.info("scrolling to temple card");
			waitOrPause(3);
			swipeByCoordinates(driver,505,1564,505,767);
		} catch (Exception e) {
			throw new Exception("Error in scrollToTempleCard()"+e.getMessage());		}
	}

	@StepInfo(info="clicking on whatsApp share kare button")
	public void clickOnWhatsAppShareButton() throws Exception{
		try {
			ListenerImplimentationclass.testLog.info("clicking on whatsApp share kare button");
			awaitForElement(driver, watsAppShareIcon);
			clickOnElement(watsAppShareIcon);
		} catch (Exception e) {
			throw new Exception("Error in clickOnWatsAppShareButton()"+e.getMessage());		}
	}

	@StepInfo(info="checking if whatsapp bottom sheet is displayed")
	public boolean isWhatsAppShareBottomSheetDisplayed() throws Exception{
		try {
			ListenerImplimentationclass.testLog.info("checking if whatsapp bottom sheet is displayed");
			awaitForElement(driver, watsAppShareBottomSheet);
			return watsAppShareBottomSheet.isDisplayed();
		} catch (Exception e) {
			throw new Exception("Error in isWatsAppShareBottomSheetDisplayed()"+e.getMessage());		}
	}

}
