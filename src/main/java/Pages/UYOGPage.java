package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ExcelUtility;
import base.ListenerImplimentationclass;
import base.TestData;
import io.appium.java_client.AppiumDriver;

public class UYOGPage extends AppGenericLib{
	
	public ElementUtil elementUtil;
	public UYOGPage(AppiumDriver driver) 
	{
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="com.mandir.debug:id/empty_temple_fram")
	private WebElement UYOGIcon;
	
	@FindBy(id="com.mandir.debug:id/img_tutorial")
	private WebElement firstOnBoardingTutorial;
	
	@FindBy(id="com.mandir.debug:id/img_close")
	private WebElement closeUYOGBtn;
	
	@FindBy(id="com.mandir.debug:id/img_next")
	private WebElement UYOGTutorialNextBtn;
	
	@FindBy(id="com.mandir.debug:id/tv_uyog")
	private WebElement ApneMandirBanayeBtn;
	
	@FindBy(id="com.android.permissioncontroller:id/permission_allow_one_time_button")
	private WebElement allowOnceBtn;
	
	@FindBy(id="com.android.permissioncontroller:id/permission_deny_button")
	private WebElement denyBtn;
	
	@FindBy(id="com.android.permissioncontroller:id/permission_allow_button")
	private WebElement appAllowbtn;
	
	@FindBy(id="com.mandir.debug:id/design_bottom_sheet")
	private WebElement photoChuneBottomSheet;
	
	@FindBy(id="com.mandir.debug:id/select_from_gallery_ll")
	private WebElement choosePhotoFromGalleryTab;
	
	@FindBy(id="com.google.android.documentsui:id/icon_thumb")
	private WebElement ImageForUYOG;
	
	@FindBy(id="com.mandir.debug:id/cropImageView")
	private WebElement cropImageTab;
	
	@FindBy(id="com.mandir.debug:id/btn_rotate")
	private WebElement rotateImageBtn;
	
	@FindBy(id="com.mandir.debug:id/btn_crop")
	private WebElement aageBadeBtnFromCropImage; 
	
	@FindBy(id="com.mandir.debug:id/iv_back")
	private WebElement backBtnFromCropImage;
	
	@FindBy(id="com.mandir.debug:id/iv_warning")
	private WebElement warningPopUpFromcropImageScreen;
	
	@FindBy(id="com.mandir.debug:id/tv_delete")
	private WebElement nahiJaariRakeBtn;
	
	@FindBy(id="com.mandir.debug:id/tv_deny")
	private WebElement radhKareBtn;
	
	@FindBy(id="com.mandir.debug:id/tv_title")
	private WebElement chooseFramePageTitle;
	
	@FindBy(id="com.mandir.debug:id/back_button")
	private WebElement backBtnFromFrameSelectionScreen;
	
	@FindBy(id="com.mandir.debug:id/rv_own_god_frames")
	private WebElement godFramesTab;
	
	@FindBy(id="com.mandir.debug:id/tv_title")
	private WebElement chooseNamePageTitle;
	
	@FindBy(id="com.mandir.debug:id/et_enter_name")
	private WebElement mandirGodNameFeild;
	
	@FindBy(id="com.mandir.debug:id/btn_set_up_god")
	private WebElement mandirStapithKareBtn;
	
	@FindBy(id="com.mandir.debug:id/back_button")
	private WebElement backBtnFromChooseNamePage;
	
	@FindBy(id="com.mandir.debug:id/design_bottom_sheet")
	private WebElement badaiHoPopUp;
	
	@FindBy(id="com.mandir.debug:id/see_mandir_btn")
	private WebElement mandirDhekeBtn;
	
	@FindBy(id="com.mandir.debug:id/own_god_iv")
	private WebElement ownGodImg;
	
	@FindBy(xpath="//android.widget.ImageView[@resource-id='com.mandir.debug:id/edit_my_god_iv']")
	private WebElement ownGodEditTab;
	
	@FindBy(id="com.mandir.debug:id/share_button")
	private WebElement shareBtn;
	
	@FindBy(id="com.mandir.debug:id/back_iv")
	private WebElement backBtnFromEditOwnGodPage;
	
	@FindBy(id="com.mandir.debug:id/edit_own_god_title_tv")
	private WebElement editOwnGodPageTitle;
	
	@FindBy(id="com.mandir.debug:id/edit_own_god_delete_ll")
	private WebElement deleteOwnGodTab;
	
	@FindBy(id="android:id/profile_tabhost")
	private WebElement shareBottomSheet;
	
	@FindBy(id="com.mandir.debug:id/tv_deny")
	private WebElement removeOwnGodBtnFromWarningPopUp;
	
	@FindBy(id="com.mandir.debug:id/add_own_god_button")
	private WebElement addOwnGodIconInGodListingPage;
	
	private WebElement getlastFrameTab(String frameTabText) {
		String frameTabTextxpathValue="//android.widget.TextView[@text='"+frameTabText+"']/preceding-sibling::androidx.cardview.widget.CardView";
		return elementUtil.getElement("xpath", frameTabTextxpathValue);
	}
	
	//android.widget.TextView[@text='शाइनी सिल्वर']/preceding-sibling::androidx.cardview.widget.CardView
	
	@StepInfo(info="checking if onboarding tutorial displayed")
	public boolean isFirstUYOGTutorialDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if onboarding tutorial displayed");
		awaitForElement(driver,firstOnBoardingTutorial );
		return firstOnBoardingTutorial.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in isOnBoardingTutorialDisplayed()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on close UYOG button")
	public void clickOnCloseBtnFromUYOGTutorial() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicing on close UYOG Btn");
		awaitForElement(driver, closeUYOGBtn);
		clickOnElement(closeUYOGBtn);
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnCloseUYOGBtn()"+e.getMessage());
		}
		
	}
	
	@StepInfo(info="clicking on UYOG frame")
	public UYOGPage clickOnUYOGFrame() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on UYOG frame");
		awaitForElement(driver, UYOGIcon);
		clickOnElement(UYOGIcon);
		return this;
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnUYOGFrame()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking in next button")
	public UYOGPage clickOnNextBtnFromUYOGTutorial() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on Next button");
		awaitForElement(driver, UYOGTutorialNextBtn);
		clickOnElement(UYOGTutorialNextBtn);
		return this;
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnNextBtnFromUYOGTutorial()"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if Apne Mandir Banaye CTA Button is displayed on the last tutorial")
	public boolean isApnemandirBanayeCTABtnDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if Apne Mandir Banaye CTA Button is displayed on the last tutorial");
		awaitForElement(driver, ApneMandirBanayeBtn);
		return ApneMandirBanayeBtn.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in isApnemandirBanayeCTABtnDisplayed()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on apne mandir Banaye CTA Button")
	public UYOGPage clickOnApneMandirBanayeCTABtn() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on apne mandir Banaye CTA Button");
		awaitForElement(driver, ApneMandirBanayeBtn);
		clickOnElement(ApneMandirBanayeBtn);
		return this;
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnApneMandirBanayeCTABtn()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on deny button to access camera")
	public void clickOnDenyBtn() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on deny button to access camera");
		awaitForElement(driver, denyBtn);
		clickOnElement(denyBtn);
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnDenyBtn()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clikcing allow once button")
	public UYOGPage clickOnAllowOnceBtnFromBottomSheet() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clikcing allow once button");
		clickOnElement(allowOnceBtn);
		return this;
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnAllowFromBottomSheet()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clikcing allow button from bottom sheet")
	public UYOGPage clickOnAllowFromBottomSheet() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on allow button from bottom sheet");
		clickOnElement(appAllowbtn);
		return this;
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnAllowFromBottomSheet()"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if photo chune bottom sheet is displayed")
	public boolean isPhotoChuneBottomSheetDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if photo chune bottom sheet is displayed");
		awaitForElement(driver, photoChuneBottomSheet);
		return photoChuneBottomSheet.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in isPhotoChuneBottomSheetDisplayed()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on choose photo from gallery tab")
	public UYOGPage clickOnChoosePhotoFromGalleryTab() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info(" clicking on choose photo from gallery tab");
		awaitForElement(driver, choosePhotoFromGalleryTab);
		clickOnElement(choosePhotoFromGalleryTab);
		return this;
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnChoosePhotoFromGalleryTab()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clikcing on image for UYOG")
	public UYOGPage clickOnImageForUYOG() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clikcing on image for UYOG");
		awaitForElement(driver, ImageForUYOG);
		clickOnElement(ImageForUYOG);
		return this;
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnImageForUYOG()"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if crop image page  is displayed")
	public boolean isCropImageTabDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if crop image page is displayed");
		awaitForElement(driver, cropImageTab);
		return cropImageTab.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in isCropImageTabDisplayed()"+e.getMessage());
		}
	}
	
	@StepInfo(info="cliking on rotate button")
	public void clickOnRotateBtn() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("cliking on rotate button");
		awaitForElement(driver, rotateImageBtn);
		for (int i = 0; i < 4; i++) 
		{
			clickOnElement(rotateImageBtn);
		}
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnRotateBtn()"+e.getMessage());
		}
		
	}
	
	@StepInfo(info="clikcing on back button from crop image page")
	public UYOGPage clickOnBackBtnFromCropImagePage() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clikcing on back button from crop image page");
		awaitForElement(driver, backBtnFromCropImage);
		clickOnElement(backBtnFromCropImage);
		return this;
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnBackBtnFromCropImagePage()"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if warning pop up is displayed on clicking back button from crop image screen")
	public boolean isWarningPopupDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if warning pop up is displayed on clicking back button from crop image screen");
		awaitForElement(driver, warningPopUpFromcropImageScreen);
		return warningPopUpFromcropImageScreen.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in  isWarningPopupDisplayed()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on Nahi jaari kare button")
	public void clickOnNahiJaariRakeBtn() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on nahi jaari kare button");
		awaitForElement(driver, nahiJaariRakeBtn);
		clickOnElement(nahiJaariRakeBtn);
		}catch (Exception e)
		{
			throw new Exception("Error in  clickOnNahiJaariRakeBtn()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on radh kare button")
	public void clickOnRadhKareBtn() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on radh kare button");
		awaitForElement(driver, radhKareBtn);
		clickOnElement(radhKareBtn);
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnRadhKareBtn()"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if UYOG frame is displayed in Mandir Page")
	public boolean isUYOGframeDisplayedInMandirPage() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if UYOG frame is displayed in Mandir Page");
		awaitForElement(driver, UYOGIcon);
		return UYOGIcon.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in isUYOGframeDisplayed()"+e.getMessage());
		}
	}
	
	@StepInfo(info="fetching choose frame Page Title")
	public String getChooseFrameTitle() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("fetching choose frame Page Title");
		awaitForElement(driver, chooseFramePageTitle);
		return chooseFramePageTitle.getText();
		}catch (Exception e)
		{
			throw new Exception("Error in getChooseFrameTitle()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on Aage bade button")
	public UYOGPage clickOnAageBadeBtn() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on Aage bade button");
		awaitForElement(driver, aageBadeBtnFromCropImage);
		clickOnElement(aageBadeBtnFromCropImage);
		return this;
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnAageBadeBtn()"+e.getMessage());
		}
		
	}
	
	@StepInfo(info="clikcing on back button from frame selection screen")
	public UYOGPage clickOnBackBtnFromFrameSelectionScreen() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clikcing on back button from frame selection screen");
		awaitForElement(driver, backBtnFromFrameSelectionScreen);
		clickOnElement(backBtnFromFrameSelectionScreen);
		return this;
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnBackBtnFromCropImagePage()"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if god frame displayed")
	public boolean isGodFrameTabDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if god frame displayed");
		awaitForElement(driver, godFramesTab);
		return godFramesTab.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in isGodFrameTabDisplayed()"+e.getMessage());
		}
	}
	
	@StepInfo(info="scrolling the frame corousel")
	public UYOGPage swipeFrameCorousel() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("scrolling the frame corousel");
		awaitForElement(driver,  godFramesTab);
		swipeByCoordinates(driver, 863, 1400, 275, 1400);
		return this;
		}catch (Exception e)
		{
			throw new Exception("Error in swipeFrameCorousel()"+e.getMessage());
		}
	}
	
	@StepInfo(info="scrolling the frame corousel")
	public UYOGPage swipeFrameCorouselBack() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("scrolling the frame corousel");
		awaitForElement(driver,  godFramesTab);
		swipeByCoordinates(driver, 275, 1400, 863, 1400);
		return this;
		}catch (Exception e)
		{
			throw new Exception("Error in swipeFrameCorouselBack()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on shining silver frame")
	public void clickOnShiningSilverFrame() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on shining silver frame");
		awaitForElement(driver, getlastFrameTab(ExcelUtility.getExcelData("locators", "shiningSilverText", "value")));
		clickOnElement(getlastFrameTab(ExcelUtility.getExcelData("locators", "shiningSilverText", "value")));
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnShiningSilverFrame()"+e.getMessage());
		}
	}
	
	@StepInfo(info="fetching choose Name Page Title")
	public String getChooseNamePageTitle() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("fetching choose Name Page Title");
		awaitForElement(driver, chooseNamePageTitle);
		return chooseNamePageTitle.getText();
		}catch (Exception e)
		{
			throw new Exception("Error in getChooseNamePageTitle()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clearing own god text feild")
	public UYOGPage clearOwnGodTextFeild() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clearing own god text feild");
		awaitForElement(driver,mandirGodNameFeild);
		mandirGodNameFeild.clear();
		return this;
		}catch (Exception e)
		{
			throw new Exception("Error in clearOwnGodTextFeild()"+e.getMessage());
		}
	}
	
	@StepInfo(info="Typing name to own god text feild")
	public UYOGPage typeNameToOwnGodTextFeild() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("Typing name to own god text feild");
		awaitForElement(driver, mandirGodNameFeild);
		mandirGodNameFeild.sendKeys(TestData.ownGodName);
		return this;
		}catch (Exception e)
		{
			throw new Exception("Error in typeNameToOwnGodTextFeild()"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if mandir stapith kare button is displayed in choose name screen")
	public boolean isMandirStapithKareButtonDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if mandir stapith kare button is displayed in choose name screen");
		awaitForElement(driver, mandirStapithKareBtn);
		return mandirStapithKareBtn.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in isMandirStapithKareButtonDisplayed()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on mandir stapith kare button")
	public void clickOnMandirStapithKareButton() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on mandir stapith kare button");
		awaitForElement(driver, mandirStapithKareBtn);
		clickOnElement(mandirStapithKareBtn);
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnMandirStapithKareButton()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clikcing on back button from frame selection screen")
	public UYOGPage clickOnBackBtnFromNameSelectionScreen() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clikcing on back button from frame selection screen");
		awaitForElement(driver, backBtnFromChooseNamePage);
		clickOnElement(backBtnFromChooseNamePage);
		return this;
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnBackBtnFromNameSelectionScreen()"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if badai ho pop up is displayed")
	public boolean isBadaiHoPopup() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if badai ho pop up is displayed");
		awaitForElement(driver, badaiHoPopUp);
		return badaiHoPopUp.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in isBadaiHoPopup()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on mandir dheke button")
	public void clickOnMandirDhekeButton() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on mandir dheke button");
		awaitForElement(driver, mandirDhekeBtn);
		clickOnElement(mandirDhekeBtn);
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnMandirDhekeButton()"+e.getMessage());
		}
		
	}
	
	@StepInfo(info="chceking if own god is displayed in mandir page")
	public boolean isOwnGodDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("chceking if own god is displayed");
		awaitForElement(driver, ownGodImg);
		return ownGodImg.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in isOwnGodDisplayed()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on own god edit button")
	public UYOGPage clickOnEditOwnGodBtn() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("cloicking on own god edit button");
		awaitForElement(driver, ownGodEditTab);
		clickOnElement(ownGodEditTab);
		return this;
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnEditGodBtn()"+e.getMessage());
		}
	}
	
	
	
	@StepInfo(info="clicking on own god share button")
	public void clickOnShareBtn() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on own god share button");
		awaitForElement(driver, shareBtn);
		clickOnElement(shareBtn);
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnShareBtn()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on back button from edit own god page")
	public void clickOnbackBtnfromEditOwnGod() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on back button from edit own god page");
		awaitForElement(driver, backBtnFromEditOwnGodPage);
		clickOnElement(backBtnFromEditOwnGodPage);
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnbackBtnfromEditOwnGod()"+e.getMessage());
		}
	}
	
	@StepInfo(info="fetching edit own god page title")
	public String getEditOwnGodPageTitle() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("fetching edit own god page title");
		awaitForElement(driver, editOwnGodPageTitle);
		return editOwnGodPageTitle.getText();
		}catch (Exception e)
		{
			throw new Exception("Error in getEditOwnGodPageTitle()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on delete own god tab")
	public UYOGPage clickOnDeleteOwnGodTab() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on delete own god tab");
		awaitForElement(driver, deleteOwnGodTab);
		clickOnElement(deleteOwnGodTab);
		return this;
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnDeleteOwnGodTab()"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if share bottom sheet displayed in edit own god page")
	public boolean isShareBottomSheetDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if share bottom sheet displayed in edit own god page");
		awaitForElement(driver, shareBottomSheet);
		return shareBottomSheet.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in isShareBottomSheetDisplayed()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on remove own god button from waring pop up")
	public void clickOnRemoveOwnGodButton() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on remove own god button from waring pop up");
		awaitForElement(driver, removeOwnGodBtnFromWarningPopUp);
		clickOnElement(removeOwnGodBtnFromWarningPopUp);
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnRemoveOwnGodButton()"+e.getMessage());
		}
	}
	
	@StepInfo(info="swiping to any god frame")
	public MandirPage swipeBackFromUYOGFrame() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("swiping to any god frame");
		awaitForElement(driver, UYOGIcon);
		Horizontalscroll(driver, 925, 831, 188, 831, 1);
		return new MandirPage(driver);
		}catch (Exception e)
		{
			throw new Exception("Error in swipeBackFromUYOGFrame()"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if add own god icon is displayed in god listing page")
	public boolean isAddOwnGodIconDisplayedInGodListingPage() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if add own god icon is displayed in god listing page");
		awaitForElement(driver, addOwnGodIconInGodListingPage);
		return addOwnGodIconInGodListingPage.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in isAddOwnGodIconDisplayedInGodListingPage()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clikcing on add own god icon from the god listing page")
	public void clickOnAddOwnGodIconFromGodListingPage() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clikcing on add own god icon from the god listing page");
		awaitForElement(driver, addOwnGodIconInGodListingPage);
		clickOnElement(addOwnGodIconInGodListingPage);
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnAddOwnGodIconFromGodListingPage() "+e.getMessage());
		}
	}

}
