package Pages;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ListenerImplimentationclass;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class OrderDetailsPage extends AppGenericLib {
    ElementUtil elementUtil;

    public OrderDetailsPage(AppiumDriver driver) {
        this.driver = driver;
        elementUtil = new ElementUtil(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_order_status']/ancestor::android.view.ViewGroup[@resource-id='com.mandir.debug:id/constraint_root']//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_toolbar_title']")
    private WebElement bookingId;

    @FindBy(xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_order_status']/ancestor::android.view.ViewGroup[@resource-id='com.mandir.debug:id/constraint_root']//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_toolbar_location']")
    private WebElement countAndPriceOfOfferingItem;

    @FindBy(xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_order_status']/ancestor::android.view.ViewGroup[@resource-id='com.mandir.debug:id/constraint_root']//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_back']")
    private WebElement backBtn;

    @FindBy(xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_order_status']")
    private WebElement orderStatus;

    @FindBy(xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_order_status']/android.view.ViewGroup[2]/android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_status']")
    private WebElement statusIcon;

    @FindBy(xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_order_status']/android.view.ViewGroup[1]/android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_status']")
    private WebElement offeringIcon;

    @FindBy(xpath = "//android.view.View[@resource-id='com.mandir.debug:id/view_dotted_line']/preceding-sibling::android.widget.TextView")
    private WebElement orderDate;

    @FindBy(xpath = "//android.widget.Button[@resource-id='com.mandir.debug:id/btn_download']")
    private WebElement downloadKareBtn;

    @FindBy(xpath = "//android.widget.Button[@resource-id='com.mandir.debug:id/btn_whatsapp_share']")
    private WebElement whatsappShareIcon;

    @FindBy(id="com.mandir.debug:id/design_bottom_sheet")
    private WebElement sharingBottomSheet;

    @FindBy(xpath = "//android.view.ViewGroup[@resource-id='com.mandir.debug:id/constraint_bill']")
    private WebElement billSummaryCard;

    @FindBy(xpath = "//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_bill_info']")
    private WebElement offeringCountAndPriceFromBillSummaryCard;

    @FindBy(xpath = "//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_arrow']")
    private WebElement downCheveronFromBillSummaryCard;

    @FindBy(xpath = "//android.view.ViewGroup[@resource-id='com.mandir.debug:id/constraint_details']")
    private WebElement priceBreakUpDetails;

    @FindBy(xpath = "//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_total_price']")
    private WebElement totalPriceFromBillSummaryCard;

    @FindBy(id="com.mandir.debug:id/tv_download")
    private WebElement downloadBtnFromBillSummaryCard;

    @FindBy(id="com.mandir.debug:id/detail_image_view")
    private WebElement detailedImageView;

    @FindBy(xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_order_status']//android.widget.FrameLayout[@resource-id='com.mandir.debug:id/card_temple_image']")
    private WebElement certificateImage;

    @FindBy(xpath = "//android.widget.ImageView[@resource-id='com.mandir.debug:id/detail_image_view']/following-sibling::android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_back']")
    private WebElement backBtnFromDetailedImageView;

    @FindBy(xpath = "//android.widget.ImageView[@resource-id='com.mandir.debug:id/detail_image_view']/parent::android.view.ViewGroup//android.widget.Button[@resource-id='com.mandir.debug:id/btn_download']")
    private WebElement downloadBtnInDetailedImageView;

    @FindBy(xpath = "//android.widget.ImageView[@resource-id='com.mandir.debug:id/detail_image_view']/parent::android.view.ViewGroup//android.widget.Button[@resource-id='com.mandir.debug:id/btn_whatsapp_share']")
    private WebElement whatsAppShareBtnFromDetailedImageView;

    @FindBy(id="com.mandir.debug:id/linear_snack_bar")
    private WebElement bottomToastBarMessage;
    
    @FindBy(id="com.mandir.debug:id/tv_order_date")
    private WebElement completedOrderTitle;
    
    @FindBy(xpath = "//android.view.ViewGroup[@resource-id='com.mandir.debug:id/constraint_bill']/parent::	\r\n"
    		+ "android.view.ViewGroup//android.widget.FrameLayout[@resource-id='com.mandir.debug:id/card_temple_image']")
    private WebElement completedOfferingVideo;
    
    @FindBy(xpath = "//android.view.ViewGroup[@resource-id='com.mandir.debug:id/constraint_bill']/parent::	\r\n"
    		+ "android.view.ViewGroup//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_play_icon']/following-sibling::android.widget.LinearLayout/android.widget.Button[@resource-id='com.mandir.debug:id/btn_download']")
    private WebElement completedOfferingVideoDownloadBtn;
    
    @FindBy(xpath = "//android.view.ViewGroup[@resource-id='com.mandir.debug:id/constraint_bill']/parent::	\r\n"
    		+ "android.view.ViewGroup//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_play_icon']/following-sibling::android.widget.LinearLayout/android.widget.Button[@resource-id='com.mandir.debug:id/btn_whatsapp_share']")
    private WebElement completedOfferingVideoShareBtn;
    
    @StepInfo(info = "checking if booking id displayed")
    public boolean isBookingIdDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if booking id displayed in oreder details page");
            awaitForElement(driver, bookingId);
            return bookingId.isDisplayed();
        } catch (Exception e) {
            throw new Exception("Error in isBookingIdDisplayed()" + e.getMessage());
        }
    }

    @StepInfo(info = "checking if count and price of offering item displayed")
    public boolean isCountAndPriceOfOfferingItemDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if count and price of offering item displayed");
            awaitForElement(driver, countAndPriceOfOfferingItem);
            return countAndPriceOfOfferingItem.isDisplayed();
        } catch (Exception e) {
            throw new Exception("Error in isCountAndPriceOfOfferingItemDisplayed()" + e.getMessage());
        }
    }

    @StepInfo(info = "clicking on back button from order details page")
    public void clickOnBackBtn() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("clicking on back button from order details page");
            awaitForElement(driver, backBtn);
            clickOnElement(backBtn);
        } catch (Exception e) {
            throw new Exception("Error in clickOnBackBtn() " + e.getMessage());
        }
    }

    @StepInfo(info = "checking if order status displayed")
    public boolean isOrderStatusDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if order status displayed");
            awaitForElement(driver, orderStatus);
            return orderStatus.isDisplayed();
        } catch (Exception e) {
            throw new Exception("Error in isOrderStatusDisplayed()  " + e.getMessage());
        }
    }

    @StepInfo(info = "checking if status icon is displayed ")
    public boolean isStatusIconDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if status icon is displayed ");
            awaitForElement(driver, statusIcon);
            return statusIcon.isDisplayed();
        } catch (Exception e) {
            throw new Exception("Error in isStatusIconDisplayed() " + e.getMessage());
        }
    }

    @StepInfo(info = "checking if offering icon displayed")
    public boolean isOfferingIconDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if offering icon displayed ");
            awaitForElement(driver, offeringIcon);
            return offeringIcon.isDisplayed();
        } catch (Exception e) {
            throw new Exception("Error in isOfferingIconDisplayed() " + e.getMessage());
        }
    }

    @StepInfo(info = "checking if order date displayed in order details page")
    public boolean isOrderDateDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if order date displayed in order details page");
            awaitForElement(driver, orderDate);
            return orderDate.isDisplayed();
        } catch (Exception e) {
            throw new Exception("Error in isOrderDateDisplayed() " + e.getMessage());
        }
    }

    @StepInfo(info = "checking if download kare button is displayed")
    public boolean isDownloadKareBtnDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if download kare button is displayed");
            awaitForElement(driver, downloadKareBtn);
            return downloadKareBtn.isDisplayed();
        } catch (Exception e) {
            throw new Exception("Error in isDownloadKareBtnDisplayed()" + e.getMessage());
        }
    }

    @StepInfo(info = "clicking on whatsappShare icon")
    public void clickingOnWhatsappShareIcon() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("clicking on whatsappShare icon");
            awaitForElement(driver, whatsappShareIcon);
            clickOnElement(whatsappShareIcon);
        } catch (Exception e) {
            throw new Exception("Error in clickingOnWhatsappShareIcon()" + e.getMessage());
        }
    }

    @StepInfo(info = "checking if share bottom sheet displayed on clicking whatsapp icon")
    public boolean isShareBottomSheetDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if share bottom sheet displayed on clicking whatsapp icon");
            awaitForElement(driver, sharingBottomSheet);
            return sharingBottomSheet.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isShareBottomSheetDisplayed() "+e.getMessage());
        }
    }

    @StepInfo(info = "checking if bill summary card displayed")
    public boolean isBillSummaryCardDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if bill summary card displayed");
            awaitForElement(driver, billSummaryCard);
            return billSummaryCard.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isBillSummaryCardDisplayed() "+e.getMessage());
        }
    }

    @StepInfo(info = "checking if offering price and offering count displayed on bill summary card")
    public boolean isOfferingCountAndPriceDisplayedInBillSummaryCard() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if offering price and offering count displayed on bill summary card");
            awaitForElement(driver, offeringCountAndPriceFromBillSummaryCard);
            return offeringCountAndPriceFromBillSummaryCard.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isBillSummaryCardDisplayed() "+e.getMessage());
        }
    }

    @StepInfo(info = "clicking on down cheveron from bill summary card")
    public void clickOnDownCheveronFromBillSummaryCard() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("clicking on down cheveron from bill summary card");
            awaitForElement(driver, downCheveronFromBillSummaryCard);
            clickOnElement(downCheveronFromBillSummaryCard);
        }catch (Exception e){
            throw new Exception("Error in clickOnDownCheveronFromBillSummaryCard()  "+e.getMessage());
        }
    }

    @StepInfo(info = "checking if price break up details displayed")
    public boolean isPriceBreakUpDetailsDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if price break up details displayed");
            awaitForElement(driver, priceBreakUpDetails);
            return priceBreakUpDetails.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isPriceBreakUpDetailsDisplayed() "+e.getMessage());
        }
    }

    @StepInfo(info = "checking if total price displayed in bill summary card")
    public boolean isTotalPriceDisplayedInBillSummaryCard() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if total price displayed in bill summary card");
            awaitForElement(driver, totalPriceFromBillSummaryCard);
            return totalPriceFromBillSummaryCard.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isTotalPriceDisplayedInBillSummaryCard()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if download button is displayed in bill summary card")
    public boolean isDownloadButtonDisplayedInBillSummaryCard() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if download button is displayed in bill summary card");
            awaitForElement(driver, downloadBtnFromBillSummaryCard);
            return downloadBtnFromBillSummaryCard.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isDownloadButtonDisplayedInBillSummaryCard()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if certificate image displayed in offering details page")
    public boolean isCertificateImageDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if certificate image displayed in offering details page");
            awaitForElement(driver, certificateImage);
            return certificateImage.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isCertificateImageDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "clicking on certificate image")
    public void clickOnCertificateImage() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("clicking on certificate image");
            awaitForElement(driver, certificateImage);
            clickOnElement(certificateImage);
        }catch (Exception e){
            throw new Exception("Error in clickOnCertificateImage()"+e.getMessage());
        }
    }
    @StepInfo(info = "checking if detailed image view displayed on clicking certificate image")
    public boolean isDetailedImageViewDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if detailed image view displayed on clicking certificate image");
            awaitForElement(driver, detailedImageView);
            return detailedImageView.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isDetailedImageViewDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if download button is displayed in detailed certificate image view page")
    public boolean isDownloadButtonDisplayedInCertificateImage() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if download button is displayed in detailed certificate image view page");
            awaitForElement(driver, downloadBtnInDetailedImageView);
            return downloadBtnInDetailedImageView.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isDownloadButtonDisplayedInCertificateImage()"+e.getMessage());
        }
    }

    @StepInfo(info = "clicking on whatsappShare icon from detailed image view page")
    public void clickingOnWhatsappShareIconFromDetailedImageView() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("clicking on whatsappShare icon from detailed image view page");
            awaitForElement(driver, whatsAppShareBtnFromDetailedImageView);
            clickOnElement(whatsAppShareBtnFromDetailedImageView);
        } catch (Exception e) {
            throw new Exception("Error in clickingOnWhatsappShareIconFromDetailedImageView() " + e.getMessage());
        }
    }

    @StepInfo(info = "clicking in back button from detailed image view page")
    public void clickOnBackBtnFromDetailedImageViewPage() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("clicking in back button from detailed image view page");
            awaitForElement(driver, backBtnFromDetailedImageView);
            clickOnElement(backBtnFromDetailedImageView);
        }catch (Exception e) {
            throw new Exception("Error in clickOnBackBtnFromDetailedImageViewPage()" + e.getMessage());
        }
    }

    @StepInfo(info = "checking if bottom toast bar message displayed")
    public boolean isBottomToastBarMessageDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if bottom toast bar message displayed");
            awaitForElement(driver, bottomToastBarMessage);
            return bottomToastBarMessage.isDisplayed();
        }catch (Exception e) {
            throw new Exception("Error in isBottomToastBarMessageDisplayed()" + e.getMessage());
        }
    }
    
    @StepInfo(info = "checking if completed offering order title displayed")
    public boolean isCompletedOrderTitleDisplayed() throws Exception {
    	try {
    	 ListenerImplimentationclass.testLog.info("checking if completed offering order title displayed");
    	 awaitForElement(driver, completedOrderTitle);
    	 return completedOrderTitle.isDisplayed();
    	}catch (Exception e) {
            throw new Exception("Error in isCompletedOrderTitleDisplayed()" + e.getMessage());
        }	 
    }
    
    @StepInfo(info = "checking if completed offering video displayed")
    public boolean isCompletedOfferingVideoDisplayed() throws Exception {
    	try {
    	 ListenerImplimentationclass.testLog.info("checking if completed offering order title displayed");
    	 awaitForElement(driver, completedOfferingVideo);
    	 return completedOfferingVideo.isDisplayed();
    	}catch (Exception e) {
            throw new Exception("Error in isCompletedOfferingVideoDisplayed()" + e.getMessage());
        }
    	
    }
    
    @StepInfo(info = "clicking on completed offering video")
    public void clickOnCompletedOfferingVideo() throws Exception {
    	try {
    	 ListenerImplimentationclass.testLog.info("clicking on completed offering video");
    	 awaitForElement(driver, completedOfferingVideo);
    	 clickOnElement(completedOfferingVideo);
    	 waitOrPause(5);
    	}catch (Exception e) {
            throw new Exception("Error in clickOnCompletedOfferingVideo()" + e.getMessage());
        }
    	
    }
    
    @StepInfo(info = "checking if download Button displayed below the completed offering video")
    public boolean isDownloadBtnDisplayedBelowVideo() throws Exception {
    	try {
    	 ListenerImplimentationclass.testLog.info("checking if download Button displayed below the completed offering video");
    	 awaitForElement(driver, completedOfferingVideoDownloadBtn);
    	 return completedOfferingVideoDownloadBtn.isDisplayed();
    	}catch (Exception e) {
            throw new Exception("Error in isDownloadBtnDisplayedBelowVideo()" + e.getMessage());
        }
    	
    }
    
    @StepInfo(info = "checking if share button is displayed below the complted offering video")
    public boolean isShareBtnDisplayedBelowVideo() throws Exception {
    	try {
    	 ListenerImplimentationclass.testLog.info("checking if share button is displayed below the complted offering video");
    	 awaitForElement(driver, completedOfferingVideoShareBtn);
    	 return completedOfferingVideoShareBtn.isDisplayed();
    	}catch (Exception e) {
            throw new Exception("Error in isShareBtnDisplayedBelowVideo()" + e.getMessage());
        }
    	
    }
    
    @StepInfo(info = "clicking on share button buttom from completed offering video")
    public void clickOnShareBtnFromVideo() throws Exception {
    	try {
    	 ListenerImplimentationclass.testLog.info("clicking on share button buttom from completed offering video");
    	 awaitForElement(driver, completedOfferingVideoShareBtn);
    	 clickOnElement(completedOfferingVideoShareBtn);
    	}catch (Exception e) {
            throw new Exception("Error in clickOnShareBtnFromVideo()" + e.getMessage());
        }
    	
    }   
}
