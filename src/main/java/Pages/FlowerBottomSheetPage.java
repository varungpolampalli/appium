package Pages;


import java.io.File;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ListenerImplimentationclass;
import io.appium.java_client.AppiumDriver;

public class FlowerBottomSheetPage extends  AppGenericLib{
	ElementUtil elementUtil;
	public FlowerBottomSheetPage(AppiumDriver driver)
	{
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}


	@FindBy(xpath="//android.view.View[@resource-id='com.mandir.debug:id/touch_outside']")
	private WebElement tapElementInMandirPage;


	@FindBy(id="com.mandir.debug:id/ll_offerings_root")
	private WebElement flowerBottomSheet ;

	@FindBy(id="com.mandir.debug:id/bt_purchase_flower")
	private WebElement tutorialToUnlockFlower;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_offerings']/child::android.widget.RelativeLayout[4]")
	private WebElement LockedFlowerIcon;


	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_offerings']/child::android.view.ViewGroup[1]")
	private WebElement phoolKiLadi;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_offerings']/child::android.widget.RelativeLayout[1]")
	private WebElement ghondaFlowerIcon;


	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_offerings']/android.widget.RelativeLayout[4]/android.widget.LinearLayout/android.widget.LinearLayout[@resource-id='com.mandir.debug:id/reward_price_linear']/android.widget.TextView")
	private WebElement coinNumbers;

	@FindBy(id="com.mandir.debug:id/nav_view")
	private WebElement mandirBottomNavigationBar;

	@FindBy(id="com.mandir.debug:id/rv_offerings")
	private WebElement listOfFlowers;

	@FindBy(id="com.mandir.debug:id/garland_selected")
	private WebElement greenTick;

	@FindBy(id="com.mandir.debug:id/bt_purchase_flower")
	private WebElement flowerParchasebtn;

	@FindBy(id="com.mandir.debug:id/offerings_title")
	private WebElement godTitleFromFlowerBottomSheet;
	
	@StepInfo(info="clicking out side flower Bottom sheet")
	public void clickOutsideFlowerBottomSheet() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("clicking out side flower Bottom sheet");
			awaitForElement(driver, tapElementInMandirPage);
			clickOnElement(tapElementInMandirPage);
		}
		catch (Exception e){
			throw new Exception("Error in clickOutsideFlowerBottomSheet()"+e.getMessage());
		}

	}


	@StepInfo(info="checking if the flower bottom sheet is displayed")
	public boolean isFlowerBottomSheetDisplayed()throws Exception {
		try {
			awaitForElement(driver, flowerBottomSheet);
			ListenerImplimentationclass.testLog.info("checking if the flower bottom sheet is displayed");
		}catch (Exception e)
		{
			throw new Exception("Error in isFlowerBottomSheetDisplayed()"+e.getMessage());
		}
		return flowerBottomSheet.isDisplayed();

	}



	@StepInfo(info="clicking on Locked flower from flower bottom sheet")
	public FlowerBottomSheetPage clickOnLockedFlower() throws Exception
	{

		try {
			ListenerImplimentationclass.testLog.info("Clicking on LockedFlower from flower bottom sheet");
			awaitForElement(driver, LockedFlowerIcon);
			clickOnElement(LockedFlowerIcon);
		}catch (Exception e){
			throw new Exception("Error in clickOnLockedFlower()"+e.getMessage());
		}
		return this;
	}


	@StepInfo(info="clicking the locked flower user is getting a tutorial to unlock a flower")
	public boolean isTutorialDisplayedForLockedFlower() throws Exception
	{
		try {
			awaitForElement(driver, tutorialToUnlockFlower);
			ListenerImplimentationclass.testLog.info("checking if the flower bottom sheet is displayed");
		}catch (Exception e)
		{
			throw new Exception("Error in isTutorialDisplayedForLockedFlower()"+e.getMessage());
		}
		return tutorialToUnlockFlower.isDisplayed();

	}


	@StepInfo(info="clicking over phool ki ladhi")
	public boolean isPhoolKiLadhiClickable() throws Exception
	{
		try {
			awaitForElement(driver, phoolKiLadi);
			ListenerImplimentationclass.testLog.info("clicking over phool ki ladhi");
		}catch (Exception e)
		{
			throw new Exception("Error in isPhoolKiLadhiClickable()"+e.getMessage());
		}
		return phoolKiLadi.isEnabled();

	}

	@StepInfo(info="clicking on Ghonda Flower")
	public FlowerBottomSheetPage clickOnGhondaFlowerIcon() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("Clicking on Ghonda flower");
			awaitForElement(driver, ghondaFlowerIcon);
			clickOnElement(ghondaFlowerIcon);
		}catch (Exception e){
			throw new Exception("Error in clickOnGhondaFlowerIcon()"+e.getMessage());
		}
		return this;
	}

	@StepInfo(info="checking  bottom navigation Bar is displayed")
	public boolean isBottomNavigationDisplayed() throws Exception {
		try {
			awaitForElement(driver, mandirBottomNavigationBar);
			ListenerImplimentationclass.testLog.info("checking  bottom navigation Bar is displayed");
		}catch (Exception e)
		{
			throw new Exception("Error in isMandirBottomSheetDisplayed()"+e.getMessage());
		}
		return mandirBottomNavigationBar.isDisplayed();
	}


	@StepInfo(info="clicking on PunyaMudra tutorial Button")
	public PunyaMudraPage clickOnPunyaMudraTutorialButton() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("Clicking on PunyaMudraTutorialButton");
			awaitForElement(driver, tutorialToUnlockFlower);
			clickOnElement(tutorialToUnlockFlower);
		}catch (Exception e){
			throw new Exception("Error in clickOnPunyaMudraTutorialButton()"+e.getMessage());		
		}
		return new  PunyaMudraPage(driver);
	}


	@StepInfo(info="checking list of flowers  is displayed")
	public boolean isListOfFlowersDisplayed() throws Exception
	{
		try{
			ListenerImplimentationclass.testLog.info("checking list of flowers  is displayed");
			awaitForElement(driver,listOfFlowers);
		}
		catch(Exception e) {
			throw new Exception("Error in isListOfFlowersDisplayed()" +e.getMessage() );
		}
		return listOfFlowers.isDisplayed();
	}



	@StepInfo(info="slide over  flower corousel ")
	public void slideOverFlowerCorousel() 
	{
		swipeByCoordinates(driver, 866, 1422,107, 1422);
		swipeByCoordinates(driver, 217, 1418,873, 1418);
	}


	@StepInfo(info="clicking on phol ki Flower")
	public FlowerBottomSheetPage clickOnPholkiLadhiFlowerIcon() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("Clicking on phol ki ladhi flower");
			awaitForElement(driver, phoolKiLadi);
			clickOnElement(phoolKiLadi);
		}catch (Exception e){
			throw new Exception("Error in clickOnPholkiLadhiFlowerIcon()"+e.getMessage());
		}
		return this;
	}


	@StepInfo(info="checking if the green tick  is displayed")
	public boolean isGreentickDisplayed() throws Exception {
		try {
			awaitForElement(driver, flowerBottomSheet);
			ListenerImplimentationclass.testLog.info("checking if the green tick  is displayed");
		}catch (Exception e)
		{
			throw new Exception("Error in isGreentickDisplayed()"+e.getMessage());
		}
		return flowerBottomSheet.isDisplayed();

	}
	@StepInfo(info="checking  phol ki ladhi Flower is displayed")
	public boolean isPholkiLadhiDisplayed() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("checking  phol ki ladhi Flower is displayed");
			awaitForElement(driver, phoolKiLadi);
		}catch (Exception e){
			throw new Exception("Error in isPholkiLadhiDisplayed()"+e.getMessage());
		}
		return phoolKiLadi.isDisplayed();
	}

	@StepInfo(info="Getting the screenshot of Flower shower ")
	public File getScreenshotOFlowerShower() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("Getting the screenshot of Flower shower");
			awaitForElement(driver, flowerBottomSheet);
			return takeScreenshot(driver, "actualFlowershowerImage");
		}catch (Exception e){
			throw new Exception("Error in getScreenshotOFlowerShower()"+e.getMessage());
		}
	}

	@StepInfo(info="fetching purchase button coin number of locked flower")
	public int getPurchaseCoinNumber() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("fetching purchase buttton coin number of locked flower");
			awaitForElement(driver, flowerParchasebtn);
			return Integer.parseInt( flowerParchasebtn.getText().split("  ")[1].split(" ")[0]);
		}catch (Exception e)
		{
			throw new Exception("Error in getPurchaseCoinNumber()"+e.getMessage());
		}
	}


	@StepInfo(info="fetching coin number of locked flower")
	public int getLockedCoinNumber() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("fetching coin number of locked flower");
			awaitForElement(driver,coinNumbers);
			return  Integer.parseInt(coinNumbers.getText());
		}catch(Exception e)
		{
			throw new Exception("Error in getLockedCoinNumber()"+e.getMessage());

		}
	}


	@StepInfo(info="fetching god name from flower bottom sheet page")
	public String getGodNameFromFlowerBottomSheet() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("fetching god name from flower bottom sheet page");
			awaitForElement(driver,godTitleFromFlowerBottomSheet);
			return godTitleFromFlowerBottomSheet.getText().split(" ")[0];
		}catch(Exception e)
		{
			throw new Exception("Error in getGodNameFromFlowerBottomSheet() "+e.getMessage());
		}
	}
	
	@StepInfo(info="swiping till phool ki ladhi")
	public void swipeTillPhoolkiLadhi() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("swiping till phool ki ladhi");
		swipeByCoordinates(driver, 128, 1511,427, 1507);
		}catch(Exception e)
		{
			throw new Exception("Error in swipeTillPhoolkiLadhi() "+e.getMessage());
		}
	}
	
	@StepInfo(info="Horizontal scroll till last locked flower")
	public void horizontalScrollTillLastLockedFlower() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("Horizontal scroll till last locked flower");
		Horizontalscroll(driver, 885, 1679, 151, 1672, 5);
		}catch(Exception e)
		{
			throw new Exception("Error in horizontalScrollTillLastLockedFlower() "+e.getMessage());
		}
	}
		
}


