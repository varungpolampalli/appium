package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ExcelUtility;
import base.ListenerImplimentationclass;
import io.appium.java_client.AppiumDriver;

public class CommentsPage  extends AppGenericLib{
	
	ElementUtil elementUtil;
	public CommentsPage(AppiumDriver driver) {
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}	
	
	@FindBy(xpath = "//android.widget.ImageView[@resource-id='com.mandir.debug:id/shareWhatsApp']/preceding-sibling::android.widget.TextView")
	private WebElement commentPageTitle;
	
	@FindBy(xpath = "//android.widget.ImageView[@resource-id='com.mandir.debug:id/shareWhatsApp']/parent::android.widget.LinearLayout/android.widget.ImageView[@resource-id='com.mandir.debug:id/back_button']")
	private WebElement backBtnFromCommentsPage;
	
	@FindBy(xpath="//android.widget.ScrollView[@resource-id='com.mandir:id/nestedScrollView']//android.widget.LinearLayout[@resource-id='com.mandir:id/reaction_ll']//android.widget.Button[@resource-id='com.mandir:id/heart']")
	private WebElement reactionBtn;
	
	@FindBy(id="com.mandir:id/bt_add_first_comment")
	private WebElement commentkareBtn;
	
	@FindBy(id="com.mandir:id/iv_close")
	private WebElement closeBtn;
	
	@FindBy(id="com.mandir:id/edit_comment")
	private WebElement addCommentField;
		
	@FindBy(id="com.mandir:id/bt_comment_add")
	private WebElement sendBtn;
	
	@FindBy(id="com.mandir:id/cta")
	private WebElement JoinBtn;
	
   @FindBy(id="com.mandir:id/tv_delete_comment")	
   private WebElement deleteBtn;
   
   @FindBy(id="android:id/content")
   private WebElement deleteCommentPopup;
   
   @FindBy(id="com.mandir:id/leave_no")
   private WebElement cancelBtnInDeleteCommentPopup;
   
   @FindBy(id="com.mandir:id/comments_rv")
   private WebElement commentSection;
   
   @FindBy(xpath="//android.widget.ScrollView[@resource-id='com.mandir:id/nestedScrollView']//android.widget.Button[@resource-id='com.mandir:id/comment']")
   private WebElement commentsBtn;
   
   @FindBy(id="com.mandir:id/leave_yes")
   private WebElement yesBtnFromDeletePopup;
   
   @FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir:id/comments_rv']/android.view.ViewGroup[position()=last()]/android.widget.ImageView[@resource-id='com.mandir:id/iv_deleteComment']")
   private WebElement lastEllipsisIcon;
   
   @FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir:id/comments_rv']/android.view.ViewGroup[position()=last()]//android.widget.ImageView[@resource-id='com.mandir:id/iv_profile_image']")
   private WebElement profileImage;
   
   @FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir:id/comments_rv']/android.view.ViewGroup[position()=last()]//android.widget.TextView[@resource-id='com.mandir:id/author_name']")
   private WebElement profileName;

   @FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir:id/comments_rv']/android.view.ViewGroup[position()=last()]//android.widget.TextView[@resource-id='com.mandir:id/comment_text']")
   private WebElement commentText;

   @StepInfo(info="checking if comment page title is displayed")
	public boolean isCommentPageTitleDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if comment page title is displayed");
			awaitForElement(driver, commentPageTitle);
			return commentPageTitle.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in  isCommentPageTitleDisplayed()"+e.getMessage()); 
		}
	}
   
   @StepInfo(info="click on Reaction Btn")
	public CommentsPage clickOnReactionBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("click on Reaction btn");
			awaitForElement(driver, reactionBtn);
			clickOnElement(reactionBtn);
			return this;
		}catch (Exception e) {
			throw new Exception("Error in  clickOnReactionBtn()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on back button from comments page")
	public void clickOnBackBtnFromCommentsPage() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on back button from comments page");
			awaitForElement(driver, backBtnFromCommentsPage);
			clickOnElement(backBtnFromCommentsPage);
		}catch (Exception e){
			throw new Exception("Error in clickOnBackBtnFromCommentsPage()"+e.getMessage());
		}
	}
	
	@StepInfo(info="click on back btn")
	public CommentsPage clickOnBackBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("click on back btn");
			awaitForElement(driver, backBtnFromCommentsPage);
			clickOnElement(backBtnFromCommentsPage);
			return this;
		}catch (Exception e) {
			throw new Exception("Error in  clickOnBackBtn()"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if comment kare btn is displayed")
	public boolean isCommentKareBtnDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if comment kare btn is displayed");
			awaitForElement(driver, commentkareBtn);
			return commentkareBtn.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in  isCommentKareBtnDisplayed()"+e.getMessage()); 
		}
	}	
	
	@StepInfo(info="click on close btn")
	public CommentsPage clickOnCloseBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("click on close btn");
			awaitForElement(driver, closeBtn);
			clickOnElement(closeBtn);
			return this;
		}catch (Exception e) {
			throw new Exception("Error in  clickOnCloseBtn()"+e.getMessage());
		}
	}
	
	@StepInfo(info="click on add CommentField  ")
	public CommentsPage clickOnAddCommentField() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("click on add CommentField");
			awaitForElement(driver, addCommentField);
			clickOnElement(addCommentField);
			return this;
		}catch (Exception e) {
			throw new Exception("Error in  clickOnAddCommentField()"+e.getMessage());
		}
	}
	
	@StepInfo(info="Entering comment to comment Text feild")
	public void enterComment() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("Entering comment to name Text feild");
			addCommentField.sendKeys("test");
		}catch (Exception e){
			throw new Exception("Error in enterComment()"+e.getMessage());
		}
	}
		
	@StepInfo(info="fetching user comment text")
	public String getCommentText() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("fetching user comment text");
			awaitForElement(driver, commentText);
			return  commentText.getText();
		}catch (Exception e){
			throw new Exception("Error in getCommentText()"+e.getMessage());
		}
	}	
	
	@StepInfo(info="checking if ellipsis Icon is displayed")
	public boolean isEllipsisDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if ellipsis Icon is displayed");
			awaitForElement(driver, lastEllipsisIcon);
			return lastEllipsisIcon.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in  isEllipsisDisplayed()"+e.getMessage()); 
		}
	}	
	
	@StepInfo(info="click on send btn")
	public CommentsPage clickOnSendBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("click on send btn");
			awaitForElement(driver, sendBtn);
			clickOnElement(sendBtn);
			return this;
		}catch (Exception e) {
			throw new Exception("Error in  clickOnSendBtn()"+e.getMessage());
		}
	}
		
	@StepInfo(info="clicking join btn")
	public CommentsPage clickOnJoinBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on join btn");
			awaitForElement(driver, JoinBtn);
			clickOnElement(JoinBtn);
			return this;
		}catch (Exception e){
			throw new Exception("Error in  clickOnJoinBtn()"+e.getMessage()); 
		}
	}	
	
	@StepInfo(info="checking if join btn is displayed")
	public boolean isJoinBtnDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if join btn is displayed");
			awaitForElement(driver, JoinBtn);
			return JoinBtn.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in  isJoinBtnDisplayed()"+e.getMessage()); 
		}
	}	
		
	@StepInfo(info="clicking ellipsis btn ")
	public CommentsPage clickOnEllipsisIcon() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking ellipsis btn");
			awaitForElement(driver, lastEllipsisIcon);
			clickOnElement(lastEllipsisIcon);
			return this;
		}catch (Exception e){
			throw new Exception("Error in  clickOnEllipsisIcon()"+e.getMessage()); 
		}
	}			
	
	@StepInfo(info="clicking delete btn ")
	public CommentsPage clickOnDeleteBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking delete btn");
			awaitForElement(driver, deleteBtn);
			clickOnElement(deleteBtn);
			return this;
		}catch (Exception e){
			throw new Exception("Error in  clickOnDeleteBtn()"+e.getMessage()); 
		}
	}	
		
	@StepInfo(info="checking if delete btn is displayed")
	public boolean isDeleteBtnDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if delete btn is displayed");
			awaitForElement(driver, deleteBtn);
			return deleteBtn.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in  isDeleteBtnDisplayed()"+e.getMessage()); 
		}
	}	
	
	@StepInfo(info="checking if delete btn is displayed")
	public boolean isDeleteCommentPopupDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if delete btn is displayed");
			awaitForElement(driver, deleteCommentPopup);
			return deleteCommentPopup.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in  isDeleteCommentPopupDisplayed()"+e.getMessage()); 
		}
	}	
		
	@StepInfo(info="clicking cancel  btn in delete comment popup ")
	public CommentsPage clickOnCancelBtninDeleteCommentPopup() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking cancel  btn in delete comment popup ");
			awaitForElement(driver, cancelBtnInDeleteCommentPopup);
			clickOnElement(cancelBtnInDeleteCommentPopup);
			return this;
		}catch (Exception e){
			throw new Exception("Error in  clickOnCancelBtninDeleteCommentPopup()"+e.getMessage()); 
		}
	}		
	
	@StepInfo(info="checking if  preview of the messsage in comments section  is displayed")
	public boolean isPreviewMessageDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if  preview of the messsage in comments section  is displayed");
			awaitForElement(driver, commentSection);
			return commentSection.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in  isPreviewMessageDisplayed()"+e.getMessage()); 
		}
	}	
		
	@StepInfo(info="checking if comments count is displayed")
	public boolean isCommentsDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if comments count is displayed");
			awaitForElement(driver, commentsBtn);
			return commentsBtn.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in  isCommentsDisplayed()"+e.getMessage()); 
		}
	}	
		
	@StepInfo(info="checking if reaction count is displayed")
	public boolean isReactionbtnDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if reaction count is displayed");
			awaitForElement(driver, reactionBtn);
			return reactionBtn.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in  isReactionbtnDisplayed()"+e.getMessage()); 
		}
	}	
	
	public void tapByCoordinatesInCommentPage() {
		waitOrPause(4);
		tapByCoordinates(488, 1396);		
	}

	@StepInfo(info="checking if profile image is displayed")
	public boolean isProfileImageDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if profile image is displayed");
			awaitForElement(driver, profileImage);
			return profileImage.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isProfileImageDisplayed()"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if profile name is displayed")
	public boolean isProfileNameDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if profile name is displayed");
			awaitForElement(driver, profileName);
			return profileName.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isProfileNameDisplayed()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking yes  btn in delete comment popup ")
	public CommentsPage clickOnYesBtnInDeleteCommentPopup() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking yes  btn in delete comment popup ");
			awaitForElement(driver, yesBtnFromDeletePopup);
			clickOnElement(yesBtnFromDeletePopup);
			return this;
		}catch (Exception e){
			throw new Exception("Error in  clickOnYesBtninDeleteCommentPopup()"+e.getMessage()); 
		}
	}	
	
	@StepInfo(info="clicking on last Ellipsis Icon ")
	public CommentsPage clickOnLastEllipsisIcon() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on last Ellipsis Icon ");
			awaitForElement(driver, lastEllipsisIcon);
			clickOnElement(lastEllipsisIcon);
			return this;
		}catch (Exception e){
			throw new Exception("Error in  clickOnLastEllipsisIcon()"+e.getMessage()); 
		}
	}	
	
	@StepInfo(info="scrolling till end ")
	public void scrollingTillEndInCommentPage() throws Exception {
		ListenerImplimentationclass.testLog.info("scrolling till end");
		waitOrPause(3);
		scrollTillEnd();	
	}
	
	@StepInfo(info="scrolling up")
	public void scrollingTillUp() throws Exception {
			ListenerImplimentationclass.testLog.info("scrolling up");
	           waitOrPause(3);
           Verticalscroll(driver, 515, 685, 454, 1342, 3);
	}

	
}
