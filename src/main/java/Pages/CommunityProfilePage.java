package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ListenerImplimentationclass;
import io.appium.java_client.AppiumDriver;

public class CommunityProfilePage extends AppGenericLib{
	
	ElementUtil elementUtil;
	public CommunityProfilePage(AppiumDriver driver) {
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}	
	
	@FindBy(id="com.mandir.debug:id/community_title")
	private WebElement communityDetailHeader;
	
	@FindBy(id="com.mandir.debug:id/img_background")
	private WebElement communityImage;

	@FindBy(xpath="//android.widget.RelativeLayout[@resource-id='com.mandir.debug:id/activity_landing']//android.widget.ImageView[@resource-id='com.mandir.debug:id/community_profile']")
	private WebElement communityProfileImage;
	
	@FindBy(id="com.mandir.debug:id/community_description")
	private WebElement communityDescription;
	
	@FindBy(id="com.mandir.debug:id/community_members")
	private WebElement memberCount;
	
	@FindBy(id="com.mandir.debug:id/cta")
	private WebElement joinBtn;
	
	@FindBy(id="com.mandir.debug:id/cta_whatsapp_share")
	private WebElement whatsAppShareIcon;
	
	@FindBy(id="com.mandir.debug:id/kebab")
	private WebElement horizontalEllipsis;
	
	@FindBy(id="com.mandir.debug:id/tv_delete_comment")
	private WebElement communityLeaveBtn;	
	
	@FindBy(id="android:id/content")
	private WebElement communityLeavePopup;
	
	@FindBy(id="com.mandir.debug:id/leave_no")
	private WebElement popupCancelBtn;
	
	@FindBy(id="com.mandir.debug:id/leave_yes")
	private WebElement yesBtnFromConfirmationPopup;
	
	@FindBy(xpath = "//android.widget.ImageView[@resource-id='com.mandir.debug:id/img_background']/following-sibling::android.widget.ImageView[@resource-id='com.mandir.debug:id/back_button']")
	private WebElement backBtnFromCommunityProfilePage;
	
	@FindBy(id="com.mandir:id/design_bottom_sheet")
	private WebElement shareBottomSheet;
	
	@FindBy(id="com.mandir:id/iv_close")
	private WebElement closeIconOfShareBottomsheet;
	
	@StepInfo(info="checking if community detail header is displayed after clicking on Community TopBar")
	public boolean isCommunityDetailHeaderDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if community detail header is displayed after clicking on Community TopBar");
			awaitForElement(driver, communityDetailHeader);
			return communityDetailHeader.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in  isCommunityDetailHeaderDisplayed()"+e.getMessage()); 
		}
	}	

		
	@StepInfo(info="checking if community Description is displayed ")
	public boolean iscommunityDescriptionDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if community Description is displayed");
			awaitForElement(driver, communityDescription);
			return communityDescription.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in  iscommunityDescriptionDisplayed()"+e.getMessage()); 
		}
	}	
	
	@StepInfo(info="checking if community Image is displayed ")
	public boolean isCommunityImageDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if community Image is displayed");
			awaitForElement(driver, communityImage);
			return communityImage.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in  isCommunityImageDisplayed()"+e.getMessage()); 
		}
	}	
	
	@StepInfo(info="checking if community Profile Image is displayed ")
	public boolean isCommunityProfileImageDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if community Profile Image is displayed");
			awaitForElement(driver, communityProfileImage);
			return communityProfileImage.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in  isCommunityProfileImageDisplayed()"+e.getMessage()); 
		}
	}	
	
	
	@StepInfo(info="checking if member count is displayed ")
	public boolean isMemberCountDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if member count is displayed");
			awaitForElement(driver, memberCount);
			return memberCount.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in  isMemberCountDisplayed()"+e.getMessage()); 
		}
	}		
	
	@StepInfo(info="clicking on join btn ")
	public CommunityProfilePage clickOnJoinBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on join btn");
			awaitForElement(driver, joinBtn);
			clickOnElement(joinBtn);
			return this;
		}catch (Exception e) {
			throw new Exception("Error in  clickOnJoinBtn()"+e.getMessage());
		}
	}
	
	
	@StepInfo(info="checking if whats APP share icon is displayed ")
	public boolean isWhatsAppShareIconDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if whats APP share icon is displayed");
			awaitForElement(driver, whatsAppShareIcon);
			return whatsAppShareIcon.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in  isWatsAppShareIconDisplayed()"+e.getMessage()); 
		}
	}	
	
	
	@StepInfo(info="clicking on Horizontal Ellipsis ")
	public CommunityProfilePage clickOnHorizontalEllipsis() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on Horizontal Ellipsis");
			awaitForElement(driver, horizontalEllipsis);
			clickOnElement(horizontalEllipsis);
			return this;
		}catch (Exception e) {
			throw new Exception("Error in  clickOnHorizontalEllipsis()"+e.getMessage());
		}
	}
	
	
	@StepInfo(info="checking if community leave btn is displayed ")
	public boolean isCommunityLeaveBtnDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if community leave btn is displayed ");
			awaitForElement(driver, communityLeaveBtn);
			return communityLeaveBtn.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in  isCommunityLeaveBtnDisplayed()"+e.getMessage()); 
		}
	}	
	
	
	@StepInfo(info="checking if community leave popup is displayed ")
	public boolean isCommunityLeavePopupDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if community leave popup is displayed ");
			awaitForElement(driver, communityLeavePopup);
			return communityLeavePopup.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in  isCommunityLeavePopupDisplayed()"+e.getMessage()); 
		}
	}	
	
	
	@StepInfo(info="clicking on community leave btn ")
	public CommunityProfilePage clickOnCommunityLeaveBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on community leave btn");
			awaitForElement(driver, communityLeaveBtn);
			clickOnElement(communityLeaveBtn);
			return this;
		}catch (Exception e) {
			throw new Exception("Error in  clickOnCommunityLeaveBtn()"+e.getMessage());
		}
	}
	
	
	public void tapByCoordinatesInCommunityProfilePage() {
		waitOrPause(5);
		tapByCoordinates(454, 1480);
		
	}
	
	@StepInfo(info="clicking on pop up cancel btn ")
	public CommunityProfilePage clickOnPopupCancelBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on pop up cancel btn");
			awaitForElement(driver, popupCancelBtn);
			clickOnElement(popupCancelBtn);
			return this;
		}catch (Exception e) {
			throw new Exception("Error in  clickOnPopupCancelBtn()"+e.getMessage());
		}
	}
	
		
	@StepInfo(info="clicking on pop up confirmation btn ")
	public CommunityProfilePage clickOnYesbtnFromComfirmationPopup() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on pop up confirmation btn");
			awaitForElement(driver, yesBtnFromConfirmationPopup);
			clickOnElement(yesBtnFromConfirmationPopup);
			return this;
		}catch (Exception e) {
			throw new Exception("Error in  clickOnYesbtnFromComfirmationPopup()"+e.getMessage());
		}
	}
	
	
	@StepInfo(info="checking if Join btn is displayed ")
	public boolean isJoinBtnDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if join btn is displayed ");
			awaitForElement(driver, joinBtn);
			return joinBtn.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in  isJoinBtnDisplayed()"+e.getMessage()); 
		}
	}	
	
	
	@StepInfo(info="checking if Horizontal Ellipsis is displayed after joining")
	public boolean isHorizontalEllipsisDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if Horizontal Ellipsis is displayed after joining");
			awaitForElement(driver, horizontalEllipsis);
			return horizontalEllipsis.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in  isHorizontalEllipsisDisplayed()"+e.getMessage()); 
		}
	}
	
	@StepInfo(info="clicking on back back button from community profile page")
	public void  clickOnBackBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on back back button from community profile page");
			awaitForElement(driver, backBtnFromCommunityProfilePage);
			clickOnElement(backBtnFromCommunityProfilePage);
		}catch (Exception e) {
			throw new Exception("Error in  clickOnBackBtn()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on whatsapp icon")
	public CommunityProfilePage clickOnWhatsAppIconBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on whatsapp icon");
			awaitForElement(driver, whatsAppShareIcon);
			clickOnElement(whatsAppShareIcon);
			return this;
		}catch (Exception e) {
			throw new Exception("Error in  clickOnBackBtn()"+e.getMessage());
		}
	}	
			
	@StepInfo(info="checking if share bottom sheet is displayed after joining")
	public boolean isShareBottomSheetDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if if share bottom sheet is displayed after joining");
			awaitForElement(driver, shareBottomSheet);
			return shareBottomSheet.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in  isShareBottomSheetDisplayed()"+e.getMessage()); 
		}
	}	
		
	@StepInfo(info="clicking on CloseShareBottomSheet btn ")
	public CommunityProfilePage clickOnCloseIconOfShareBottomSheet() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking ShareBottomSheet btn");
			awaitForElement(driver, closeIconOfShareBottomsheet);
			clickOnElement(closeIconOfShareBottomsheet);
			return this;
		}catch (Exception e) {
			throw new Exception("Error in  clickOnCloseShareBottomSheet()"+e.getMessage());
		}
	}	
	
 }


