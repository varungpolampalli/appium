package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ExcelUtility;
import base.ListenerImplimentationclass;
import base.PunyaMudraPopUpTest;
import io.appium.java_client.AppiumDriver;

public class MenuPage extends AppGenericLib {

	ElementUtil elementUtil;
	public MenuPage(AppiumDriver driver) {
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id="com.mandir.debug:id/tv_login")
	private WebElement logInBtn;

	@FindBy(id="com.mandir.debug:id/rv_menu")
	private WebElement menuOptions;

	@FindBy(id="com.mandir.debug:id/card_cancel")
	private WebElement cancelBtn;

	@FindBy(id="com.mandir.debug:id/loginSubtitle")
	private WebElement logInSubTitle;

	@FindBy(id="com.mandir.debug:id/iv_user")
	private WebElement profileImg;

	@FindBy(xpath = "//android.widget.Button[@text='Sign in']")
	private WebElement PlayStoreSignInBtn;

	@FindBy(id="com.mandir.debug:id/design_bottom_sheet")
	private WebElement messageInSharingBottomSheet;

	@FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/tvAppBarTitle']")
	private WebElement termsAndConditionsPageTitle;
	
	@FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/tvAppBarTitle']")
	private WebElement privacyPolicyPageTitle;
	
	@FindBy(id="com.mandir.debug:id/freshchat_contact_us_btn")
	private WebElement hameSandeshBhejeBtn;
	
	@FindBy(id="com.mandir.debug:id/freshchat_message_container")
	private WebElement chatContent;
	
	@FindBy(xpath="//android.widget.ImageButton[@content-desc='Navigate up']")
	private WebElement backBtnFromChatPage;
	
	@FindBy(id="com.mandir.debug:id/yes_logout")
	private WebElement yesBtnFromLogoutPopUp;

	private WebElement getGeethShlokTab(String geethShlokText) {
		String GeethShlokTextxpathValue="//android.widget.TextView[contains(@text,'"+geethShlokText+"')]";
		return elementUtil.getElement("xpath", GeethShlokTextxpathValue);
	}

	private WebElement getAppUpdateKareTab(String appUpdateKareText) {
		String appUpdateKareTextxpathValue="//android.widget.TextView[contains(@text,'"+appUpdateKareText+"')]";
		return elementUtil.getElement("xpath", appUpdateKareTextxpathValue);
	}

	private WebElement getAppShareKareTab(String appShareKareText) {
		String appShareKareTextxpathValue="//android.widget.TextView[contains(@text,'"+appShareKareText+"')]";
		return elementUtil.getElement("xpath", appShareKareTextxpathValue);
	}

	private WebElement getAppRateKareTab(String appRateKareText) {
		String appRateKareTextxpathValue="//android.widget.TextView[contains(@text,'"+appRateKareText+"')]";
		return elementUtil.getElement("xpath", appRateKareTextxpathValue);
	}

	private WebElement getTermsAndConditionsTab(String termsAndConditionsText) {
		String termsAndConditionsTextxpathValue="//android.widget.TextView[contains(@text,'"+termsAndConditionsText+"')]";
		return elementUtil.getElement("xpath", termsAndConditionsTextxpathValue);
	}

	private WebElement getPrivacyPolicyTab(String privacyPolicyText) {
		String privacyPolicyTextxpathValue="//android.widget.TextView[contains(@text,'"+privacyPolicyText+"')]";
		return elementUtil.getElement("xpath", privacyPolicyTextxpathValue);
	}
	
	private WebElement getYourSuggestionsTab(String apneSujavBhejeText) {
		String apneSujavBhejeTextxpathValue="//android.widget.TextView[contains(@text,'"+apneSujavBhejeText+"')]";
		return elementUtil.getElement("xpath", apneSujavBhejeTextxpathValue);
	}

	private WebElement getSahityaBhandarTab(String sahityaBhandarText) {
		String sahityaBhandarTextxpathValue="//android.widget.TextView[contains(@text,'"+sahityaBhandarText+"')]";
		return elementUtil.getElement("xpath", sahityaBhandarTextxpathValue);
	}


	private WebElement getpanchangTab(String panchangText) {
		String panchangTextxpathValue="//android.widget.TextView[contains(@text,'"+panchangText+"')]";
		return elementUtil.getElement("xpath", panchangTextxpathValue);
	}

	private WebElement getkundliTab(String kundliText) {
		String kundliTextxpathValue="//android.widget.TextView[@text='"+kundliText+"']";
		return elementUtil.getElement("xpath", kundliTextxpathValue);
	}
	
	private WebElement getLogoutTab(String logOutText) {
		String logOutTextxpathValue="//android.widget.TextView[contains(@text,'"+logOutText+"')]";
		return elementUtil.getElement("xpath", logOutTextxpathValue);
	}

	//business libraries
	@StepInfo(info="Clicking on loginKare/userslot button from the menu page")
	public LoginBottomSheetPage clickOnLoginBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("Clicking on loginKare button from the menu page");
			awaitForElement(driver, logInBtn);
			clickOnElement(logInBtn);
		}catch (Exception e){
			throw new Exception("Error in clickOnLoginBtn()"+e.getMessage());
		}
		return new LoginBottomSheetPage(driver);
	}

	@StepInfo(info="checking if menu options is displayed")
	public boolean isMenuOptionsDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if menu options is displayed");
			return menuOptions.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isMenuOptionsDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking on cancel button")
	public void clickOnCancelBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on cancel button");
			awaitForElement(driver, cancelBtn);
			clickOnElement(cancelBtn);
		}catch (Exception e){
			throw new Exception("Error in clickOnCancelBtn()"+e.getMessage());
		}
	}

	@StepInfo(info="fetching phoneNumber from login subtitle")
	public String getLogInSubTitle() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("fetching phoneNumber from login subtitle");
			awaitForElement(driver, logInSubTitle);
			return logInSubTitle.getText();
		}catch (Exception e){
			throw new Exception("Error in getLogInSubTitle()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking on panchang Tab")
	public MenuPage clickOnPanchangTab() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on panchang Tab");
			awaitForElement(driver, getpanchangTab(ExcelUtility.getExcelData("locators", "panchangText","value")));
			clickOnElement(getpanchangTab(ExcelUtility.getExcelData("locators", "panchangText", "value")));
			PunyaMudraPopUpTest.handlePunyaMudraPopUp(driver);
			return this;
		}catch (Exception e){
			throw new Exception("Error in clickOnPanchangText()"+e.getMessage());
		}

	}
	@StepInfo(info="clicking on kundli Tab")
	public MenuPage clickOnkundliTab() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on kundli Tab");
			awaitForElement(driver, getkundliTab(ExcelUtility.getExcelData("locators", "kundliText","value")));
			clickOnElement(getkundliTab(ExcelUtility.getExcelData("locators", "kundliText", "value")));
			return this;
		}catch (Exception e){
			throw new Exception("Error in clickOnkundliTab()"+e.getMessage());
		}

	}
	@StepInfo(info="checking if profile Image displayed in menu page")
	public boolean isProfileImageDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if profile Image displayed in menu page");
			awaitForElement(driver, profileImg);
			return profileImg.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isProfileImageDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if profile name is displayed")
	public boolean isProfileNameDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if profile name is displayed in menu page");
			awaitForElement(driver, logInBtn);
			return logInBtn.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isProfileNameDisplayed()"+e.getMessage());
		}
	}
	@StepInfo(info="clicking on GeethaShloka Tab")
	public MenuPage clickOnGeethShlokTab() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on GeethaShlok Tab");
			awaitForElement(driver, getGeethShlokTab(ExcelUtility.getExcelData("locators", "geethaShlokTabText","value")));
			clickOnElement(getGeethShlokTab(ExcelUtility.getExcelData("locators", "geethaShlokTabText", "value")));
			return this;
		}catch (Exception e){
			throw new Exception("Error in clickOnGeethShlokTab()"+e.getMessage());
		}

	}
	@StepInfo(info="clicking on Sahitya Bhandar Tab")
	public MenuPage clickOnSahityaBhandarTab() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on Sahitya Bhandar Tab");
			awaitForElement(driver, getSahityaBhandarTab(ExcelUtility.getExcelData("locators", "sahithyaBhandarText","value")));
			clickOnElement(getSahityaBhandarTab(ExcelUtility.getExcelData("locators", "sahithyaBhandarText", "value")));
			PunyaMudraPopUpTest.handlePunyaMudraPopUp(driver);
			return this;
		}catch (Exception e){
			throw new Exception("Error in clickOnSahityaBhandarTab()"+e.getMessage());
		}

	}
	@StepInfo(info="checking if playstore sign in button is displayed")
	public boolean isPlayStoreSignInBtnDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if playstore sign in button is displayed");
			awaitForElement(driver, PlayStoreSignInBtn);
			return PlayStoreSignInBtn.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isPlayStoreSignInBtnDisplayed()"+e.getMessage());
		}
	}
	@StepInfo(info="clicking on App update kare Tab")
	public MenuPage clickOnAppUpdateKareTab() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on App update kare Tab");
			awaitForElement(driver, getAppUpdateKareTab(ExcelUtility.getExcelData("locators", "appUpdateKareText","value")));
			clickOnElement(getAppUpdateKareTab(ExcelUtility.getExcelData("locators", "appUpdateKareText", "value")));
			return this;
		}catch (Exception e){
			throw new Exception("Error in clickOnAppUpdateKareTab()"+e.getMessage());
		}

	}

	@StepInfo(info="clicking on App rate kare Tab")
	public MenuPage clickOnAppRateKareTab() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on App rate kare Tab");
			awaitForElement(driver, getAppRateKareTab(ExcelUtility.getExcelData("locators", "appRateKareText","value")));
			clickOnElement(getAppRateKareTab(ExcelUtility.getExcelData("locators", "appRateKareText", "value")));
			return this;
		}catch (Exception e){
			throw new Exception("Error in clickOnAppRateKareTab()"+e.getMessage());
		}

	}

	@StepInfo(info="clicking on App Share kare Tab")
	public MenuPage clickOnAppShareKareTab() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on App Share kare Tab");
			awaitForElement(driver, getAppShareKareTab(ExcelUtility.getExcelData("locators", "appShareKareText","value")));
			clickOnElement(getAppShareKareTab(ExcelUtility.getExcelData("locators", "appShareKareText", "value")));
			return this;
		}catch (Exception e){
			throw new Exception("Error in clickOnAppShareKareTab()"+e.getMessage());
		}

	}

	@StepInfo(info="checking if share bottom sheet is displayed")
	public boolean isMessageDisplayedInShareBottomSheet() {
		ListenerImplimentationclass.testLog.info("checking if share bottom sheet is displayed");
		return messageInSharingBottomSheet.isDisplayed();
	}

	@StepInfo(info="clicking on Terms And Conditions Tab")
	public MenuPage clickOnTermsAndConditionsTab() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on Terms And Conditions Tab");
			awaitForElement(driver, getTermsAndConditionsTab(ExcelUtility.getExcelData("locators", "termsAndConditionsText","value")));
			clickOnElement(getTermsAndConditionsTab(ExcelUtility.getExcelData("locators", "termsAndConditionsText", "value")));
			return this;
		}catch (Exception e){
			throw new Exception("Error in clickOnTermsAndConditionsTab()"+e.getMessage());
		}
	}

	@StepInfo(info="fetching terms and conditions page title")
	public String getTermsAndConditionsPageTitle() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("fetching terms and conditions page title");
			awaitForElement(driver, termsAndConditionsPageTitle);
			return termsAndConditionsPageTitle.getText();
		}catch (Exception e){
			throw new Exception("Error in getTermsAndConditionsPageTitle()"+e.getMessage());
		}

	}

	@StepInfo(info="clicking on privacy policy Tab")
	public MenuPage clickOnPrivacypolicyTab() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on privacy policy Tab");
			awaitForElement(driver, getPrivacyPolicyTab(ExcelUtility.getExcelData("locators", "privacyPolicyText","value")));
			clickOnElement(getPrivacyPolicyTab(ExcelUtility.getExcelData("locators", "privacyPolicyText", "value")));
			return this;
		}catch (Exception e){
			throw new Exception("Error in clickOnPrivacypolicyTab()"+e.getMessage());
		}
	}
	
	@StepInfo(info="fetching Privacy Policy page title")
	public String getPrivacyPolicyPageTitle() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("fetching Privacy Policy page title");
			awaitForElement(driver, privacyPolicyPageTitle);
			return privacyPolicyPageTitle.getText();
		}catch (Exception e){
			throw new Exception("Error in getPrivacyPolicyPageTitle()"+e.getMessage());
		}
			
	}
	
	@StepInfo(info="clicking on apne Sujaav Bheje Tab")
	public MenuPage clickOnApneSujavBhejeTab() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on apne Sujaav Bheje Tab");
			awaitForElement(driver, getYourSuggestionsTab(ExcelUtility.getExcelData("locators", "apneSujaavBhejeText","value")));
			clickOnElement(getYourSuggestionsTab(ExcelUtility.getExcelData("locators", "apneSujaavBhejeText", "value")));
			return this;
		}catch (Exception e){
			throw new Exception("Error in clickOnApneSujavBhejeTab()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on Hame Sujaav Bheje Tab")
	public MenuPage clickOnHameSandeshBhejeTab() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on Hame Sandesh Bheje Tab");
			awaitForElement(driver, hameSandeshBhejeBtn);
			clickOnElement(hameSandeshBhejeBtn);
			return this;
		}catch (Exception e){
			throw new Exception("Error in clickOnApneSujavBhejeTab()"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if chat content displayed")
	public boolean isChatContentDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if chat content displayed");
		awaitForElement(driver, chatContent);
		return chatContent.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isChatContentDisplayed()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on back button from apne sujaav bheje chat page")
	public MenuPage clickOnNavigateBackBtn() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on back button from apne sujaav bheje chat page");
		awaitForElement(driver, backBtnFromChatPage);
		clickOnElement(backBtnFromChatPage);
		return this;
		}catch (Exception e){
			throw new Exception("Error in  clickOnNavigateBackBtn()"+e.getMessage());
		}
		
	}
	
	@StepInfo(info="clicking on logout Tab")
	public MenuPage clickOnLogoutTab() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on logout Tab");
		awaitForElement(driver,getLogoutTab(ExcelUtility.getExcelData("locators", "logOutText", "value")));
		clickOnElement(getLogoutTab(ExcelUtility.getExcelData("locators", "logOutText", "value")));
		waitOrPause(3);
		return this;
		}catch (Exception e){
			throw new Exception("Error in  clickOnLogoutTab()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on yes button from logout pop up")
	public MenuPage clickOnYesBtnFromLogoutPopup() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on yes button from logout pop up");
		awaitForElement(driver,yesBtnFromLogoutPopUp);
		clickOnElement(yesBtnFromLogoutPopUp);
		waitOrPause(3);
		return this;
		}catch (Exception e){
			throw new Exception("Error in  clickOnYesBtnFromLogoutPopup()"+e.getMessage());
		}
		
	}
	
	@StepInfo(info="swiping to beginning of the page")
	public MenuPage scrollToBeginning() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("swiping to beginning of the page");
		waitOrPause(3);
		swipeByCoordinates(driver, 502, 471, 502, 1330);
		return this;
		}catch (Exception e){
			throw new Exception("Error in  swipeToBeginning()"+e.getMessage());
		}
	}
	
	@StepInfo(info="fetching login title from the menu page")
	public String getLoginTitle() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("fetching login title from the menu page");
		awaitForElement(driver, logInBtn);
		return logInBtn.getText();
		}catch (Exception e){
			throw new Exception("Error in getLoginTitle()"+e.getMessage());
		}
	}
	
}	
