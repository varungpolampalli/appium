package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ExcelUtility;
import base.ListenerImplimentationclass;
import io.appium.java_client.AppiumDriver;

public class MyAlarmPage extends AppGenericLib {
	
	public ElementUtil elementUtil;
	public MyAlarmPage(AppiumDriver driver) 
	{
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="com.mandir.debug:id/back_button_alarm_list")
	private WebElement backBtnFromAlarmPage;
	
	private WebElement myAlarmPageTitle(String alarmPageTitle) {
		String myAlarmPageTitleXpathValue="//android.widget.TextView[@text='"+alarmPageTitle+"']";
		return elementUtil.getElement("xpath",myAlarmPageTitleXpathValue );
	}
	
	@StepInfo(info="fetching the Alarm PageTitle")
	public String getAlarmPageTitle() {
		ListenerImplimentationclass.testLog.info("fetching alarm Page Title");
		awaitForElement(driver, myAlarmPageTitle(ExcelUtility.getExcelData("locators", "alarmPageTitle", "value")));
		return myAlarmPageTitle(ExcelUtility.getExcelData("locators", "alarmPageTitle", "value")).getText();
	}
	
	@StepInfo(info="clicking on back button from my alarm page")
	public void clickOnBackBtn() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on back button from my alarm page");
		awaitForElement(driver,backBtnFromAlarmPage);
		clickOnElement(backBtnFromAlarmPage);
		}catch (Exception e){
			throw new Exception("Error in clickOnBackButton()"+e.getMessage());
		}
	}

}
