package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ExcelUtility;
import base.ListenerImplimentationclass;
import base.TestData;
import io.appium.java_client.AppiumDriver;

public class HoraPage extends  AppGenericLib {

	ElementUtil elementUtil;
	public HoraPage(AppiumDriver driver)
	{
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}


	@FindBy(id="com.mandir.debug:id/tv_location_selector")
	private WebElement locationSearchModal;

	private WebElement HoraTab(String  horaTabTitle) {
		String HoraTabTitlexpathValue="//android.widget.TextView[@text='"+horaTabTitle+"']";
		return elementUtil.getElement("xpath", HoraTabTitlexpathValue);
	}

	@FindBy(id="com.mandir.debug:id/radio_today")
	private WebElement aajButton;

	@FindBy(id="com.mandir.debug:id/radio_tomorrow")
	private WebElement kalButton;


	@FindBy(id="com.mandir.debug:id/iv_help_text")
	private WebElement helpButtton;

	@FindBy(id="com.mandir.debug:id/tv_title")
	private WebElement horakyahaibannertitle;

	@FindBy(id="com.mandir.debug:id/search_src_text")
	private WebElement locationSearchTextFeild;


	@FindBy(id="com.mandir.debug:id/tv_favorable_title")
	private WebElement anukulText;

	@FindBy(id="com.mandir.debug:id/tv_adverse_title")
	private WebElement pratikulText;

	@FindBy(id="com.mandir.debug:id/cta_whatsapp_share")
	private WebElement shareIcon;

	@FindBy(id="com.mandir.debug:id/design_bottom_sheet")
	private WebElement messageInSharingBottomSheet;
	
	@FindBy(id="com.mandir.debug:id/back_button_birth_place")
	private WebElement backBtnFromLocationPage;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_hora']/child::android.widget.FrameLayout[1]")
	private WebElement aageaneWalaCard;

	@FindBy(id="com.mandir.debug:id/tv_hora_title")
	private WebElement dateAndMonthTitle;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_hora']/android.widget.FrameLayout[1]/android.view.ViewGroup[1]//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_expand']")
	private WebElement downChevron;


	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_hora']//android.view.ViewGroup")
	private WebElement horaDetails;

	@FindBy(id="com.mandir.debug:id/tv_title_hora")
	private WebElement horaTitle;

	@FindBy(xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_container_birth_place']/android.widget.LinearLayout[1]//android.widget.TextView")
	private WebElement locationNameFromLocationSearchPage;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_hora_tomorrow']")
	private WebElement kalHora;
	
	@FindBy(id="com.mandir.debug:id/card_hora")
	private WebElement horadescription;


	@StepInfo(info="checking Hora tab is displayed")
	public boolean isHoraTabDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking Hora Tab  is displayed");
			awaitForElement(driver, HoraTab(ExcelUtility.getExcelData("locators", "horaTabTitle", "value")));
			return HoraTab(ExcelUtility.getExcelData("locators", "horaTabTitle", "value")).isDisplayed();


		}catch (Exception e){
			throw new Exception("Error in isHoraTabDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking on Hora Tab ")
	public HoraPage clickOnHoraTab() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("clicking on Hora Ta");
			awaitForElement(driver,HoraTab(ExcelUtility.getExcelData("locators", "horaTabTitle", "value")));
			clickOnElement(HoraTab(ExcelUtility.getExcelData("locators", "horaTabTitle", "value")));
		}catch (Exception e){
			throw new Exception("Error in clickOnHoraTab()"+e.getMessage());
		}
		return this;
	}

	@StepInfo(info="checking location search modal is displayed")
	public boolean isLocationSearchModalDisplayed() throws Exception
	{

		try {
			awaitForElement(driver, locationSearchModal);
			ListenerImplimentationclass.testLog.info("checking location search modal is displayed");
		}catch (Exception e)
		{
			throw new Exception("Error in isLocationSearchModalIsDisplayed()"+e.getMessage());
		}
		return locationSearchModal.isDisplayed();
	}

	@StepInfo(info="Fetching the aaj Button text")
	public String getAajButtonText() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("Fetching the aaj button text");
			awaitForElement(driver, aajButton);
			return aajButton.getText();
		}catch (Exception e)
		{
			throw new Exception("Error in getting getAajButtonText()"+e.getMessage());
		}
	}

	@StepInfo(info="Fetching the kal Button text")
	public String getKalButtonText() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("Fetching the kal button text");
			awaitForElement(driver, kalButton);
			return kalButton.getText();
		}catch (Exception e)
		{
			throw new Exception("Error in getting getKalButtonText()"+e.getMessage());
		}

	}

	@StepInfo(info="clicking on Help button ")
	public HoraPage clickOnHelpButton() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("clicking on Help button");
			awaitForElement(driver,helpButtton);
			clickOnElement(helpButtton);
		}catch (Exception e){
			throw new Exception("Error in clickOnHelpButton()"+e.getMessage());
		}
		return this;

	}

	@StepInfo(info="Fetching the hora kya title")
	public String getHorakyaBannerTitle() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("Fetching the hora kya title");
			awaitForElement(driver, horakyahaibannertitle);
			return horakyahaibannertitle.getText();
		}catch (Exception e)
		{
			throw new Exception("Error in getting getHorakyaBannerTitle()"+e.getMessage());
		}

	}


	@StepInfo(info="clicking on Location search Modal ")
	public HoraPage clickOnLocationSearchModal() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("clicking on Location search Modal");
			awaitForElement(driver,locationSearchModal);
			clickOnElement(locationSearchModal);
		}catch (Exception e){
			throw new Exception("Error in clickOnLocationSearchModal()"+e.getMessage());
		}
		return this;


	}


	@StepInfo(info="Typing location to loaction text feild")
	public HoraPage typeLoactionToLoactionTextFeild() throws Exception {
		try {
			awaitForElement(driver, locationSearchTextFeild);
			ListenerImplimentationclass.testLog.info("Typing Location to Location text feild");
			locationSearchTextFeild.sendKeys("Bangalore");
			return this;
		}catch (Exception e){
			throw new Exception("Error in typeLoactionToLoactionTextFeild()"+e.getMessage());
		}

	}


	@StepInfo(info="swipe down until AnukulPratikul")
	public void swipedownUntilPratikul() 
	{
		waitOrPause(2);
		swipeByCoordinates(driver, 505, 1457, 505, 518);
	}




	@StepInfo(info="Fetching the anukul title")
	public String getAnukulText() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("Fetching the anukul title");
			awaitForElement(driver, anukulText);
			return anukulText.getText();
		}catch (Exception e)
		{
			throw new Exception("Error in getting getAnukulText()"+e.getMessage());
		}
	}


	@StepInfo(info="Fetching the pratikul title")
	public String getPratikulText() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("Fetching the pratikul title");
			awaitForElement(driver, pratikulText);
			return pratikulText.getText();
		}catch (Exception e)
		{
			throw new Exception("Error in getting getPratikulText()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking on Share Icon ")
	public HoraPage clickOnShareIcon() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("clicking on Share Icon");
			awaitForElement(driver,shareIcon);
			clickOnElement(shareIcon);
		}catch (Exception e){
			throw new Exception("Error in clickOnShareIcon()"+e.getMessage());
		}
		return this;
	}

	@StepInfo(info="checking message sharing Bottom sheet is Displayed ")
	public boolean isMessageSharingBottomSheetDisplayed() throws Exception
	{

		try {
			awaitForElement(driver, messageInSharingBottomSheet);
			ListenerImplimentationclass.testLog.info("checking message sharing Bottom sheet is Displayed");
		}catch (Exception e)
		{
			throw new Exception("Error in isMessageSharingBottomSheetDisplayed()"+e.getMessage());
		}
		return messageInSharingBottomSheet.isDisplayed();
	}

	@StepInfo(info="swipe down until AageAnewalehora")
	public void verticalscrollTillAageAnewaleHora() 
	{
		ListenerImplimentationclass.testLog.info("swipe down until AageAnewalehora");
		scrollToElement(driver,TestData.aageanewaleHora);
		
		
	}

	@StepInfo(info="checking aage ane wala card is displayed")
	public boolean isAageaneWalaCardDisplayed() throws Exception
	{

		try {
			awaitForElement(driver, aageaneWalaCard);
			ListenerImplimentationclass.testLog.info("checking aage ane wala card is displayed");
		}catch (Exception e)
		{
			throw new Exception("Error in isAageaneWalaCardDisplayed()"+e.getMessage());
		}
		return aageaneWalaCard.isDisplayed();
	}

	@StepInfo(info="fetching date and month title from hora page")
	public String[] getDateAndMonthTitle() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("fetching date and month title from hora page");
			awaitForElement(driver, dateAndMonthTitle);
			return  dateAndMonthTitle.getText().split(" ");

		}catch (Exception e)
		{
			throw new Exception("getDateAndMonthTitle()"+e.getMessage());
		}

	}

	@StepInfo(info="clicking on Down Chevron  ")
	public HoraPage clickOnDownChevron() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("clicking on Down Chevron");
			awaitForElement(driver,downChevron);
			clickOnElement(downChevron);
		}catch (Exception e){
			throw new Exception("Error in clickOnDownChevron()"+e.getMessage());
		}
		return this;

	}

	@StepInfo(info="checking hora details is displyed")
	public boolean isHoraDetailsDisplayed() throws Exception
	{

		try {
			awaitForElement(driver, horaDetails);
			ListenerImplimentationclass.testLog.info("checking hora details is displyed");
		}catch (Exception e)
		{
			throw new Exception("Error in isHoraDetailsDisplayed()"+e.getMessage());
		}
		return horaDetails.isDisplayed();

	}


	@StepInfo(info="Fetching the Hora tab title")
	public String getHoraTitle() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("Fetching the Hora tab title");
			awaitForElement(driver, horaTitle);
			return horaTitle.getText();
		}catch (Exception e)
		{
			throw new Exception("Error in getting getHoraTitle()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking on location Name from location search Page")
	public void clickOnLocationName() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on location Name from location search Page");
			awaitForElement(driver, locationNameFromLocationSearchPage);
			clickOnElement(locationNameFromLocationSearchPage);
		}catch (Exception e){
			throw new Exception("Error in  clickOnLocation()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking on kal button")
	public HoraPage clickOnKalBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on kal button");
			awaitForElement(driver, kalButton);
			clickOnElement(kalButton);
		}catch (Exception e){
			throw new Exception("Error in  clickOnKalBtn()"+e.getMessage());
		}
		return this;

	}
	@StepInfo(info="checking kal hora cards is displayed")
	public boolean iskalHoraDisplayed() throws Exception
	{

		try {
			awaitForElement(driver, kalHora);
			ListenerImplimentationclass.testLog.info("checking kal hora cards is displayed");
		}catch (Exception e)
		{
			throw new Exception("Error in iskalHoraDisplayed()"+e.getMessage());
		}
		return kalHora.isDisplayed();

	}
	
	@StepInfo(info="clicking on back btn from location")
    public HoraPage clickOnBackBtnFromLocation() throws Exception
    {
        try {
            awaitForElement(driver,backBtnFromLocationPage);
            ListenerImplimentationclass.testLog.info("clicking on back btn from location");
            clickOnElement(backBtnFromLocationPage);

        }catch (Exception e)
        {
            throw new Exception("Error in clickOnBackBtnFromLocation()"+e.getMessage());
        }
        return this;
    }
	
	@StepInfo(info="clicking on Aaj btn")
    public HoraPage clickOnAajBtn() throws Exception
    {
        try {
            awaitForElement(driver,aajButton);
            ListenerImplimentationclass.testLog.info("clicking on Aaj btn");
            clickOnElement(aajButton);

        }catch (Exception e)
        {
            throw new Exception("Error in clickOnBackBtnFromLocation()"+e.getMessage());
        }
        return this;
    }
	
}



