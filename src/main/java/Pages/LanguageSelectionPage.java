package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import io.appium.java_client.AppiumDriver;

public class LanguageSelectionPage extends AppGenericLib {
	
	  ElementUtil elementUtil;

	    public LanguageSelectionPage(AppiumDriver driver) {
	        this.driver = driver;
	        elementUtil = new ElementUtil(driver);
	        PageFactory.initElements(driver, this);
	    }

	    @FindBy(xpath = "//androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[2]/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[2]")
	    private WebElement selectHindiLanguage;

	    @FindBy(xpath  = "//android.widget.TextView[@text='Welcome to Sri Mandir']")
	    private WebElement languageSelectionTitle;


	    @StepInfo(info = "Select hindi as language in language selection page")
	    public void selectHindi() throws Exception {
	        try {
	      //      ListenerImplimentationclass.testLog.info("select hindi in language selection page");
	            awaitForElement(driver, selectHindiLanguage);
	            clickOnElement(selectHindiLanguage);
	        } catch (Exception e) {
	            throw new Exception("Error in Hindi language selection()" + e.getMessage());
	        }
	    }

	    @StepInfo(info = "checking if language screen is displayed ")
	    public boolean isLanguagescreenIsDisplayed() throws Exception {
	        try {
//			ListenerImplimentationclass.testLog.info("checking if mandir stapith kare button is displayed in god selection page");
	        	awaitForElement(driver, languageSelectionTitle);
	            return languageSelectionTitle.isDisplayed();
	        } catch (Exception e) {
	            throw new Exception("Error in isLanguagescreenIsDisplayed()" + e.getMessage());
	        }

	    }

}
