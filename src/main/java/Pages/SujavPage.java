package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ExcelUtility;
import base.ListenerImplimentationclass;
import base.PunyaMudraPopUpTest;
import base.TestData;
import io.appium.java_client.AppiumDriver;

public class SujavPage extends AppGenericLib {

	ElementUtil elementUtil;
	public SujavPage(AppiumDriver driver)
	{
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	} 
	
	
	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/recyclerView_feeds']/android.view.ViewGroup[2]/androidx.recyclerview.widget.RecyclerView[1]/android.view.ViewGroup[1]/android.widget.FrameLayout")
	private WebElement songCard;

	@FindBy(id="com.mandir.debug:id/imageView7")
	private WebElement abhisunesongbutton;

	private WebElement sujavTabTitle(String  sujavText)
	{
		String sujavTabTitlexpathValue="//android.widget.TextView[@text='"+sujavText+"']";
		return elementUtil.getElement("xpath", sujavTabTitlexpathValue);
	}


	private WebElement raashifalCard(String  raashifalCardText)
	{
		String rashifalTextxpathValue="//android.widget.TextView[@text='"+raashifalCardText+"']";
		return elementUtil.getElement("xpath", rashifalTextxpathValue);
	}


	private WebElement horaCard(String  horaTabTitle)
	{
		String horaTextxpathValue="//android.widget.TextView[@text='"+horaTabTitle+"']";
		return elementUtil.getElement("xpath", horaTextxpathValue);
	}


	private WebElement sahithyaCard(String sahithyaText)
	{
		String sahithyaTextxpathValue="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_mb_cards']//android.widget.TextView[@text='"+sahithyaText+"']";
		return elementUtil.getElement("xpath", sahithyaTextxpathValue);
	}

	private WebElement chowgadiyaPageCard(String chowgadiyaText)
	{
		String chowgadiyaTextxpathValue="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_mb_cards']//android.widget.TextView[@text='"+chowgadiyaText+"']";
		return elementUtil.getElement("xpath", chowgadiyaTextxpathValue);
	}


	private WebElement suvichaarCard(String suvicharTabText)
	{
		String suvichaarTextxpathValue="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_mb_cards']//android.widget.TextView[@text='"+suvicharTabText+"']";
		return elementUtil.getElement("xpath", suvichaarTextxpathValue);
	}

	private WebElement geethaShlokCard(String geethaShlokTabText)
	{
		String geethaShlokCardxpathValue="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_mb_cards']//android.widget.TextView[@text='"+geethaShlokTabText+"']";
		return elementUtil.getElement("xpath",geethaShlokCardxpathValue);
	}

	private WebElement tvCard(String tvCardText)
	{
		String tvCardxpathValue="//android.widget.LinearLayout[@resource-id='com.mandir.debug:id/feed_footer']//android.widget.TextView[@text='"+tvCardText+"']";
		return elementUtil.getElement("xpath", tvCardxpathValue);
	}

	@StepInfo(info="clicking on sujav tab")
	public SujavPage clickOnSujavtab() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("clicking on sujav tab");
			awaitForElement(driver,sujavTabTitle(ExcelUtility.getExcelData("locators", "sujavText", "value")));
			clickOnElement(sujavTabTitle(ExcelUtility.getExcelData("locators", "sujavText", "value")));
			waitOrPause(5);
		}
		catch (Exception e)
		{
			throw new Exception("Error in clickOnSujavtab()"+e.getMessage());
		}
		return this;
	}

	@StepInfo(info="fetching  sujav Tab Text")
	public String getSuajavTabTitle() throws Exception {
		try { 
			ListenerImplimentationclass.testLog.info("fetching  sujav Tab Text");
			awaitForElement(driver, sujavTabTitle(ExcelUtility.getExcelData("locators", "sujavText", "value")));
			return sujavTabTitle(ExcelUtility.getExcelData("locators", "sujavText", "value")).getText();
		}catch (Exception e)
		{
			throw new Exception("Error in  getSuajavTabText()"+e.getMessage());
		}
	}

	@StepInfo(info = "clicking on rashiphal card")
	public RashifalPage clickOnRashifalCard() throws Exception
	{
		try
		{
			ListenerImplimentationclass.testLog.info("clicking on rashiphal card");
			awaitForElement(driver, raashifalCard(ExcelUtility.getExcelData("locators", "rashifalText", "value")));
			clickOnElement(raashifalCard(ExcelUtility.getExcelData("locators", "rashifalText", "value")));
			PunyaMudraPopUpTest.handlePunyaMudraPopUp(driver);
			new RashifalPage(driver).handlingRashiBottomSheet();
		}catch(Exception e)
		{
			throw new Exception("Error in clickOnRashiphalCard()"+e.getMessage());
		}
		return new RashifalPage(driver);
	}

	@StepInfo(info="swipe until sujav tab")
	public void swipeuntilsujav() 
	{
		ListenerImplimentationclass.testLog.info("swipe until sujav tab");
//		Horizontalscroll(driver, 474, 222, 47,241, 3);
		Horizontalscroll(driver, 819, 222, 300,241, 3);
	}

	@StepInfo(info = "clicking on Hora card")
	public HoraPage clickOnHoraCard() throws Exception
	{
		try
		{
			ListenerImplimentationclass.testLog.info("clicking on Hora card");
			awaitForElement(driver, horaCard(ExcelUtility.getExcelData("locators", "horaTabTitle", "value")));
			clickOnElement(horaCard(ExcelUtility.getExcelData("locators", "horaTabTitle", "value")));
		}
		catch(Exception e)
		{
			throw new Exception("Error in clickOnHoraCard()"+e.getMessage());
		}
		return new HoraPage(driver);
	}

	@StepInfo(info = "clicking on Sahithya card")
	public SahithyaPage clickOnSahithyaCard() throws Exception
	{
		try
		{
			ListenerImplimentationclass.testLog.info("clicking on sahithya card");
			awaitForElement(driver, sahithyaCard(ExcelUtility.getExcelData("locators", "sahithyaText", "value")));
			clickOnElement(sahithyaCard(ExcelUtility.getExcelData("locators", "sahithyaText", "value")));
			return new SahithyaPage(driver);
		}
		catch(Exception e)
		{
			throw new Exception("Error in clickOnSahithyaCard()"+e.getMessage());
		}
		
	}

	@StepInfo(info = "clicking on Chowgadiya card")
	public ChowgadiyaPage clickOnChowgadiyaCard() throws Exception
	{
		try
		{
			ListenerImplimentationclass.testLog.info("clicking on Chowgadiya card");
			awaitForElement(driver, chowgadiyaPageCard(ExcelUtility.getExcelData("locators", "chowgadiyaText", "value")));
			clickOnElement(chowgadiyaPageCard(ExcelUtility.getExcelData("locators", "chowgadiyaText", "value")));
		}
		catch(Exception e)
		{

			throw new Exception("Error in clickOnChowgadiyaCard()"+e.getMessage());
		}
		return new ChowgadiyaPage(driver);
	}

	@StepInfo(info = "clicking on Suvichaar card")
	public SuvicharPage clickOnSuvicharCard() throws Exception
	{
		try
		{
			ListenerImplimentationclass.testLog.info("clicking on Suvichaar card");
			awaitForElement(driver, suvichaarCard(ExcelUtility.getExcelData("locators", "suvicharTabText", "value")));
			clickOnElement(suvichaarCard(ExcelUtility.getExcelData("locators", "suvicharTabText", "value")));
		}
		catch(Exception e)
		{
			throw new Exception("Error in clickOnSuvicharCard()"+e.getMessage());
		}
		return new SuvicharPage(driver);
	}

	@StepInfo(info = "clicking on GeethaShlokCard card")
	public GeethaShlokPage clickOnGeethaShlokCard() throws Exception
	{
		try
		{
			ListenerImplimentationclass.testLog.info("clicking on GeethaShlok card");
			awaitForElement(driver, geethaShlokCard(ExcelUtility.getExcelData("locators", "geethaShlokTabText", "value")));
			clickOnElement(geethaShlokCard(ExcelUtility.getExcelData("locators", "geethaShlokTabText", "value")));
			return new GeethaShlokPage(driver);
		}
		catch(Exception e)
		{

			throw new Exception("Error in clickOnGeethaShlokCard()"+e.getMessage());
		}
		
	}

	@StepInfo(info= "clicking on Tv card")
	public TvPage clickOnTvCard() throws Exception
	{
		try
		{
			ListenerImplimentationclass.testLog.info("clicking on Tv card");
			awaitForElement(driver,tvCard(ExcelUtility.getExcelData("locators", "tvText", "value")));
			clickOnElement(tvCard(ExcelUtility.getExcelData("locators", "tvText", "value")));
		}
		catch(Exception e)
		{
			throw new Exception("Error in clickOnTvCard()"+e.getMessage());
		}
		return new TvPage(driver);
	}

	@StepInfo(info="checking if sujav tab is displayed")
	public boolean isSujavTabDisplayed() throws Exception {
		try 
		{
			ListenerImplimentationclass.testLog.info("checking if sujav tab  is displayed");
			awaitForElement(driver, sujavTabTitle(ExcelUtility.getExcelData("locators", "sujavText", "value")));
			return sujavTabTitle(ExcelUtility.getExcelData("locators", "sujavText", "value")).isDisplayed();
		}
		catch (Exception e)
		{
			throw new Exception("Error in isSujavTabDisplayed()"+e.getMessage());
		}
}
	@StepInfo(info= "clicking on Abhi sune  button")
	public SujavPage clickOnAbhiSuneButton() throws Exception
	{
		try
		{
			ListenerImplimentationclass.testLog.info("clicking on Abhi sune  button");
			awaitForElement(driver,abhisunesongbutton);
			clickOnElement(abhisunesongbutton);
			return this;
		}
		catch(Exception e)
		{

			throw new Exception("Error in clickOnAbhiSuneButton()"+e.getMessage());
		}
		
	}
	
	@StepInfo(info="checking song card is displayed")
	public boolean isSongCardDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking song card is displayed");
			awaitForElement(driver,songCard);
			return (songCard).isDisplayed();
       }
		catch (Exception e){
			throw new Exception("Error in isSongCardDisplayed()"+e.getMessage());
		}
	}
}





