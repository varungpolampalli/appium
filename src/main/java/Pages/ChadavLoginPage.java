package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ExcelUtility;
import base.ListenerImplimentationclass;
import io.appium.java_client.AppiumDriver;

public class ChadavLoginPage extends AppGenericLib {
	ElementUtil elementUtil;
	public ChadavLoginPage(AppiumDriver driver) {
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}	

	@FindBy(id="com.mandir.debug:id/design_bottom_sheet")
	private WebElement loginBottomSheet;
	
	@FindBy(xpath="//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_close']")
	private WebElement loginBottomSheetCloseBtn;
	
	private WebElement getcartPageTitle(String cartPageTitle)
	{
		String cartPageTitlexpathValue="//android.widget.TextView[@text='"+cartPageTitle+"']";
		return elementUtil.getElement("xpath", cartPageTitlexpathValue);
	}
	
	public void tapByCoordinatesInItemListingPage() 
	{
		tapByCoordinates(528, 580);
	}
	
	
	@StepInfo(info="checking if login Bottom sheet is displayed")
	public boolean isLoginBottomSheetDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if login Bottom sheet is displayed");
		awaitForElement(driver, loginBottomSheet);
		return loginBottomSheet.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isLoginBottomSheetDisplayed()"+e.getMessage());
		}
	}
	
	
	@StepInfo(info="getting cart page title")
	public String getCartPageTitle() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("getting cart page title");
			awaitForElement(driver,getcartPageTitle(ExcelUtility.getExcelData("locators", "cartPageTitle", "value")));
			return getcartPageTitle(ExcelUtility.getExcelData("locators", "cartPageTitle", "value")).getText();
		}catch (Exception e){
			throw new Exception("Error in getCartPageTitle()"+e.getMessage());
		}
	}
		
	
	@StepInfo(info="clicking on login bottom sheet close btn")
	public void clickOnLoginBottomSheetCloseBtn() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("clicking on login bottom sheet close btn");
			awaitForElement(driver,loginBottomSheetCloseBtn);
			clickOnElement(loginBottomSheetCloseBtn);
		}catch (Exception e){
			throw new Exception("Error in clickOnLoginBottomSheetCloseBtn()"+e.getMessage());
		}
	}
}
