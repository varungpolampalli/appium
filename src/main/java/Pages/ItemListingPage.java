package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ListenerImplimentationclass;
import base.TestData;
import io.appium.java_client.AppiumDriver;

public class ItemListingPage extends AppGenericLib 
{	
	ElementUtil elementUtil;
	public ItemListingPage(AppiumDriver driver) {
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);		
	}	
	
	
	@FindBy(xpath="//android.widget.FrameLayout[@resource-id='com.mandir.debug:id/design_bottom_sheet']")
	private WebElement nameBottomSheet;

	@FindBy(id="com.mandir.debug:id/et_enter_name")
	private WebElement nameTextField;

	@FindBy(id="com.mandir.debug:id/btn_add_to_cart")
	private WebElement ageBadeOrBhogatanKareBtn;		
	
	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_stores']//android.view.ViewGroup[@resource-id='com.mandir.debug:id/constraint_root'][2]//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_product_name']")
	private WebElement itemBhajarangbaliText;
		
	
	@FindBy(xpath = "//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_whatsapp']/preceding-sibling::android.widget.ImageView")
    private WebElement backBtnFrmItemListingpage;

    @FindBy(xpath = "//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_whatsapp']")
    private WebElement whatsAppShareIcon;

    @FindBy(xpath = "//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_whatsapp']/preceding-sibling::android.widget.TextView[@resource-id='com.mandir.debug:id/tv_toolbar_title']")
    private WebElement itemListingPageTitle;

    @FindBy(xpath = "//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_whatsapp']/following-sibling::android.widget.TextView[@resource-id='com.mandir.debug:id/tv_toolbar_location']")
    private WebElement templeLocation;

    @FindBy(xpath = "//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_store_video']")
    private WebElement templeVideo;

    @FindBy(xpath = "//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_store_image']")
    private WebElement productInfoImage;

    @FindBy(xpath = "//android.widget.LinearLayout[@resource-id='com.mandir.debug:id/indicator']/following-sibling::android.widget.TextView")
    private WebElement templeDescription;

    @FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_metric_text']")
    private WebElement offeringsDoneText;

    @FindBy(xpath = "//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_info_slide_header']/following-sibling::androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.FrameLayout[1]")
    private WebElement offeringItemImage;

    @FindBy(xpath = "//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_info_slide_header']/following-sibling::androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.TextView[@resource-id='com.mandir.debug:id/tv_product_name']")
    private WebElement offeringItemName;

    @FindBy(xpath = "//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_info_slide_header']/following-sibling::androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.TextView[@resource-id='com.mandir.debug:id/tv_product_desc']")
    private WebElement offeringItemDescription;

    @FindBy(xpath = "//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_info_slide_header']/following-sibling::androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.TextView[@resource-id='com.mandir.debug:id/tv_product_price']")
    private WebElement offeringItemPrice;

    @FindBy(xpath = "//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_info_slide_header']/following-sibling::androidx.recyclerview.widget.RecyclerView/\t\n" +
            "android.view.ViewGroup[1]/child::android.widget.FrameLayout[@resource-id='com.mandir.debug:id/fl_add_remove_item']/android.widget.Button")
    private WebElement addToCartBtn;

    @FindBy(xpath = "//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_info_slide_header']/following-sibling::androidx.recyclerview.widget.RecyclerView/\t\n" +
            "android.view.ViewGroup[1]/child::android.widget.TextView[@resource-id='com.mandir.debug:id/tv_info_text']")
    private WebElement offeringItemBalanceDetails;

    @FindBy(xpath = "//android.widget.FrameLayout[@resource-id='com.mandir.debug:id/design_bottom_sheet']")
    private WebElement productDescriptionBottomSheet;

    @FindBy(xpath = "//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_cancel']")
    private WebElement productDescriptionBottomSheetCancelBtn;

    @FindBy(id="com.mandir.debug:id/iv_product_image")
    private WebElement productImageFromBottomSheet;

    @FindBy(xpath = "//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_product']")
    private WebElement productNameFromBottomSheet;

    @FindBy(id="com.mandir.debug:id/tv_product_price")
    private WebElement productPriceFromBottomSheet;

    @FindBy(id="com.mandir.debug:id/tv_product_desc")
    private WebElement productDescriptionFromBottomSheet;

    @FindBy(id="android:id/profile_tabhost")
    private WebElement sharingBottomSheet;

    @FindBy(id="com.mandir.debug:id/btn_add_to_cart")
    private WebElement productAddToCartBtnFromBottomSheet;

    @FindBy(xpath = "//android.widget.LinearLayout[@resource-id='com.mandir.debug:id/linear_button']")
    private WebElement bottomCartCTA;

    @FindBy(xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_offerings_details']//androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[2]/android.widget.TextView[@resource-id='com.mandir.debug:id/tv_product_price']")
    private WebElement secondItemproductPrice;

    @FindBy(xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_offerings_details']//androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[2]/android.widget.FrameLayout[@resource-id='com.mandir.debug:id/fl_add_remove_item']/android.widget.Button")
    private WebElement secondItemAddToCartBtn;

    @FindBy(xpath = "//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_cart_value']")
    private WebElement bottomCartCTAText;

    @FindBy(xpath = "//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_go_ahead']")
    private WebElement aageBadeBtn;

    @FindBy(xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_info_slide']/android.view.ViewGroup[1]")
    private WebElement userReviewCard;

    @FindBy(xpath = "//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_user_name']")
    private WebElement ratingCardName;

    @FindBy(xpath = "//android.widget.RatingBar[@resource-id='com.mandir.debug:id/rating_bar']")
    private WebElement ratingIcon;

    @FindBy(xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_faq']")
    private WebElement faqSection;

    @FindBy(xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_faq']/android.view.ViewGroup[1]")
    private WebElement faqList;

    @FindBy(xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_faq']/android.view.ViewGroup[1]/android.widget.ImageView")
    private WebElement downCheveronInFAQSection;

    @FindBy(id = "com.mandir.debug:id/tv_faq_answer")
    private WebElement faqAnswer;

    @FindBy(xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_offerings_details']//androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.FrameLayout[@resource-id='com.mandir.debug:id/fl_add_remove_item']/android.widget.Button")
    private WebElement firstProductAddToCartBtn;
    
    @FindBy(xpath = "//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_whatsapp']/preceding-sibling::android.widget.TextView")
    private WebElement templeName;
	
    @FindBy(xpath="//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_whatsapp']/preceding-sibling::android.widget.TextView[@resource-id='com.mandir.debug:id/tv_toolbar_title']")
	private WebElement templeTitle;    
	
	@StepInfo(info="clicking on AgeBadeBtn ")
	public ItemListingPage clickOnAgeBadeBtn() throws Exception
	{			
		try {
			ListenerImplimentationclass.testLog.info("clicking on AgeBadeBtn Btn");
			awaitForElement(driver, aageBadeBtn);
			clickOnElement(aageBadeBtn);
			return this;
		}catch (Exception e){
			throw new Exception("Error in clickOnAgeBadeBtn()"+e.getMessage());
		}				
	}				
			
	@StepInfo(info="checking name bottom sheet is displayed")
	public boolean isNameBottomSheetDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking name bottom sheet is displayed");
			awaitForElement(driver,nameBottomSheet);
			return nameBottomSheet.isDisplayed();
		}catch(Exception e)
		{
			throw new Exception("Error in getting isNameBottomSheetDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="clearing name text feild")
	public ItemListingPage clearNameTextFeild() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clearing name text feild");
			awaitForElement(driver, nameTextField);
			nameTextField.clear();
			return this;
		}catch (Exception e){
			throw new Exception("Error in clearNameTextFeild()"+e.getMessage());
		}
	}

	@StepInfo(info="Entering name to name Text feild in name bottom sheet")
	public void enterName() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("Entering name to name Text feild in name bottom sheet");
			nameTextField.sendKeys(TestData.userName);
		}catch (Exception e){
			throw new Exception("Error in enterName()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking on ageBadeOrBhogatanKare Btn ")
	public CartPage clickOnageBadeOrBhogatanKareBtn() throws Exception
	{			
		try {
			ListenerImplimentationclass.testLog.info("clicking on ageBadeOrBhogatanKare Btn");
			awaitForElement(driver, ageBadeOrBhogatanKareBtn);
			clickOnElement(ageBadeOrBhogatanKareBtn);					
			return new CartPage(driver);
		}catch (Exception e){
			throw new Exception("Error in clickOnageBadeOrBhogatanKareBtn()"+e.getMessage());
		}				
	}			

	public void tapByCoordinatesInItemListingPage() {
		tapByCoordinates(549, 197);
	}
	
	
	@StepInfo(info="swipe till item ")
	public void swipeupTillItem() 
	{
		ListenerImplimentationclass.testLog.info("swipe  till item");
		swipeByCoordinates(driver, 555,1531 ,508 ,508 );
	}		
		
	@StepInfo(info="fetching Item Bhajarangbali text")
	public String getItemBhajarangbaliText() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("fetching Item Bhajarangbali text");
		awaitForElement(driver, itemBhajarangbaliText);
		return itemBhajarangbaliText.getText();
		}catch (Exception e)
		{
			throw new Exception("Error in  getItemBhajarangbaliText()"+e.getMessage());
		}		
	}
	
	

    @StepInfo(info = "clicking on back button from item listing page")
    public void clickOnBackBtnFromItemListingPage() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("clicking on back button from item listing page");
            awaitForElement(driver, backBtnFrmItemListingpage);
            clickOnElement(backBtnFrmItemListingpage);
        }catch (Exception e){
            throw new Exception("Error in clickOnBackBtnFromItemListingPage()"+e.getMessage());
        }
    }

    @StepInfo(info = "clicking on whatsApp share icon from item listing page")
    public boolean isWhatsAppShareIconDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("clicking on whatsApp share icon from item listing page");
            awaitForElement(driver, whatsAppShareIcon);
            return whatsAppShareIcon.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isWhatsAppShareIcon()"+e.getMessage());
        }
    }

    @StepInfo(info = "clicking on whatsApp share icon from item listing page")
    public void clickOnWhatsAppShareIcon() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("clicking on whatsapp share icon from item listing page");
            awaitForElement(driver, whatsAppShareIcon);
            clickOnElement(whatsAppShareIcon);
        }catch (Exception e){
            throw new Exception("Error in clickOnWhatsAppShareIcon()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if share bottom sheet displayed on clicking whatsapp icon")
    public boolean isShareBottomSheetDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if share bottom sheet displayed on clicking whatsapp icon");
            awaitForElement(driver, sharingBottomSheet);
            return sharingBottomSheet.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isShareBottomSheetDisplayed() "+e.getMessage());
        }
    }

    @StepInfo(info = "checking if item listing page title is displayed")
    public boolean isItemListingPageTitleDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if item listing page title is displayed");
            awaitForElement(driver, itemListingPageTitle);
            return itemListingPageTitle.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isItemListingPageTitleDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if temple location displayed in item listing page")
    public boolean isTempleLocationDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if temple location displayed in item listing page");
            awaitForElement(driver, templeLocation);
            return templeLocation.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isTempleLocationDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "clicking on temple video from temple listing page")
    public void clickOnTempleVideo() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("clicking on temple video from temple listing page");
            awaitForElement(driver, templeVideo);
            clickOnElement(templeVideo);
            waitOrPause(5);
        }catch (Exception e){
            throw new Exception("Error in clickOnTempleVideo()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if product info image is displayed")
    public boolean isProductInfoImageDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if product info image is displayed");
            awaitForElement(driver, productInfoImage);
            return productInfoImage.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isProductInfoImageDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "scrolling temple info images")
    public void scrollTempleInfoImages() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("scrolling temple info images");
            waitOrPause(3);
            swipeByCoordinates(driver, 890, 495, 240, 495);
        }catch (Exception e){
            throw new Exception("Error in scrollTempleInfoImages()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if temple description is displayed")
    public boolean isTempleDescriptionDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if temple description is displayed");
            awaitForElement(driver, templeDescription);
            return templeDescription.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isTempleDescriptionDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if respective count of offerings done is displayed in item listing page")
    public boolean isCountOfOfferingsDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if respective count of offerings done is displayed in item listing page");
            awaitForElement(driver, offeringsDoneText);
            return offeringsDoneText.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isTempleSlotsDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "scrolling till offering list")
    public void scrollToOfferingItem() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("scrolling till offering list");
            swipeByCoordinates(driver, 473, 1542, 473, 453);
        }catch (Exception e){
            throw new Exception("Error in scrollToOfferingItem()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if offering item image displayed")
    public boolean isOfferingItemImageDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if offering item image displayed");
            awaitForElement(driver, offeringItemImage);
            return offeringItemImage.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isOfferingItemImageDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if offering item name displayed")
    public boolean isOfferingItemNameDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if offering item name displayed");
            awaitForElement(driver, offeringItemName);
            return offeringItemName.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isOfferingItemNameDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if offering item description displayed")
    public boolean isOfferingItemDescriptionDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if offering item description displayed");
            awaitForElement(driver, offeringItemDescription);
            return offeringItemDescription.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isOfferingItemDescriptionDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if offering item price displayed")
    public boolean isOfferingItemPriceDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if offering item price displayed");
            awaitForElement(driver, offeringItemPrice);
            return offeringItemPrice.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isOfferingItemPriceDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "fetching offering item price")
    public int getOfferingItemPrice() throws Exception {
            try {
                ListenerImplimentationclass.testLog.info("fetching offering item price");
                awaitForElement(driver, offeringItemPrice);
                return Integer.parseInt(offeringItemPrice.getText().split(" ")[1].split(".0")[0]);
            }catch (Exception e){
                throw new Exception("Error in getOfferingItemPrice()"+e.getMessage());
            }

    }

    @StepInfo(info = "checking if offering item balance details displayed")
    public boolean isOfferingItemBalanceDetailsDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if offering item balance details displayed ");
            awaitForElement(driver, offeringItemBalanceDetails);
            return offeringItemBalanceDetails.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isOfferingItemBalanceDetailsDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "clicking on aaur pade from product description")
    public void clickOnAaurPadeInProductDescription() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("clicking on aaur pade from product description");
            awaitForElement(driver, offeringItemDescription);
            clickOnElement(offeringItemDescription);
        }catch (Exception e){
            throw new Exception("Error in clickOnAaurPadeInProductDescription()"+e.getMessage());
        }

    }

    @StepInfo(info = "checking if product description bottom sheeet is displayed")
    public boolean isProductDescriptionBottomSheetDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if product description bottom sheeet is displayed");
            awaitForElement(driver, productDescriptionBottomSheet);
            return productDescriptionBottomSheet.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isProductDescriptionBottomSheetDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "clicking on cancel button from product description bottom sheet")
    public void clickOnProductDescriptionBottomSheetCancelBtn() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("clicking on cancel button from product description bottom sheet");
            awaitForElement(driver, productDescriptionBottomSheetCancelBtn);
            clickOnElement(productDescriptionBottomSheetCancelBtn);
        }catch (Exception e){
            throw new Exception("Error in clickOnProductDescriptionBottomSheetCancelBtn()"+e.getMessage());
        }

    }

    @StepInfo(info = "checking if product image is displayed in bottom sheet")
    public boolean isProductImageIsDisplayedInBottomSheet() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if product image is displayed in bottom sheet");
            awaitForElement(driver, productImageFromBottomSheet);
            return productImageFromBottomSheet.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isProductImageIsDisplayedInBottomSheet()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if product name displayed in product description bottom sheet")
    public boolean isProductNameDisplayedInProductBottomSheet() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if product name displayed in product description bottom sheet");
            awaitForElement(driver, productNameFromBottomSheet);
            return productNameFromBottomSheet.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isProductNameDisplayedInProductBottomSheet()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if product price displayed in product description bottom sheet")
    public boolean isProductPriceDisplayedInBottomSheet() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if product price displayed in product description bottom sheet");
            awaitForElement(driver, productPriceFromBottomSheet);
            return productPriceFromBottomSheet.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isProductPriceDisplayedInBottomSheet()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if product description displayed in product description bottom sheet")
    public boolean isProductDescriptionDisplayedInBottomSheet() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if product description displayed in product description bottom sheet");
            awaitForElement(driver, productDescriptionFromBottomSheet);
            return productDescriptionFromBottomSheet.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isProductDescriptionDisplayedInBottomSheet()"+e.getMessage());
        }
    }

    @StepInfo(info = "tapping on the screen")
    public void tapOnScreen() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("tapping on the screen");
            tapByCoordinates(923, 460);
        }catch (Exception e){
            throw new Exception("Error in tapOnScreen()"+e.getMessage());
        }
    }

    @StepInfo(info = "clicking on add to cart button from product bottom sheet")
    public void clickOnAddToCartBtnFromProductBottomSheet() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("clicking on add to cart button from product bottom sheet");
            awaitForElement(driver, productAddToCartBtnFromBottomSheet);
            clickOnElement(productAddToCartBtnFromBottomSheet);
        }catch (Exception e){
            throw new Exception("Error in clickOnAddToCartBtnFromProductBottomSheet()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if bottom cart CTA icon displayed")
    public boolean isBottomCartCTAIconDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if bottom cart CTA icon displayed");
            awaitForElement(driver, bottomCartCTA);
            return bottomCartCTA.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isBottomCartCTAIconDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "fetching second item product price")
    public int getSecondItemProductPrice() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("fetching second item product price");
            awaitForElement(driver, secondItemproductPrice);
            return Integer.parseInt(secondItemproductPrice.getText().split(" ")[1].split(".0")[0]);
        }catch (Exception e){
            throw new Exception("Error in getSecondItemProductPrice()"+e.getMessage());
        }
    }

    @StepInfo(info = "clicking on second item add to cart button")
    public void clickOnSecondItemAddToCartBtn() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("clicking on second item add to cart button");
            awaitForElement(driver, secondItemAddToCartBtn);
            clickOnElement(secondItemAddToCartBtn);
        }catch (Exception e){
            throw new Exception("Error in clickOnSecondItemAddToCartBtn()"+e.getMessage());
        }
    }

    @StepInfo(info = "fetching product count and price from bottom cart CTA tab")
    public String getProductCountAndPriceFromCartCTA() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("fetching text from bottom cart CTA tab");
            awaitForElement(driver, bottomCartCTAText);
            return bottomCartCTAText.getText();
        }catch (Exception e){
            throw new Exception("Error in getProductCountAndPriceFromCartCTA()"+e.getMessage());
        }
    }

    @StepInfo(info = "fetching product count  from bottom cart CTA ")
    public int getProductCountFromBottomCartCTA() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("fetching product count  from bottom cart CTA");
            awaitForElement(driver, bottomCartCTAText);
            return Integer.parseInt(getProductCountAndPriceFromCartCTA().split(" ")[0]);
        }catch (Exception e){
            throw new Exception("Error in getProductCountFromBottomCartCTA()"+e.getMessage());
        }
    }

    @StepInfo(info = "fetching product price  from bottom cart CTA ")
    public int getProductPriceFromBottomCartCTA() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("fetching product price  from bottom cart CTA");
            awaitForElement(driver, bottomCartCTAText);
            String productPrice = getProductCountAndPriceFromCartCTA().split(" ")[4];
            return Integer.parseInt(productPrice.split("/")[0]);
        }catch (Exception e){
            throw new Exception("Error in getProductPriceFromBottomCartCTA()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if aage bade CTA icon is displayed")
    public boolean isAageBtnDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if aage bade CTA icon is displayed");
            awaitForElement(driver, aageBadeBtn);
            return aageBadeBtn.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isAageBtnDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if user review card displayed in offering listing page")
    public boolean isUserReviewCardDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if user review card displayed in offering listing page");
            awaitForElement(driver, userReviewCard);
            return userReviewCard.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isUserReviewCardDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "scrolling user review card ")
    public void scrollUserReviewCardCorousel() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("scrolling user review card ");
            waitOrPause(4);
            swipeByCoordinates(driver, 865, 890, 150, 890);
        }catch (Exception e){
            throw new Exception("Error in scrollUserReviewCardCorousel()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if user name is displayed in rating card")
    public boolean isUserNameDisplayedInRatingCard() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if user name is displayed in rating card");
            awaitForElement(driver, ratingCardName);
            return ratingCardName.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isUserNameDisplayedInRatingCard()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if rating is displayed in review card")
    public boolean isRatingsDisplayedInReviewCard() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if rating is displayed in review card");
            awaitForElement(driver, ratingIcon);
            return ratingIcon.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isRatingsDisplayedInReviewCard()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if FAQ section displayed")
    public boolean isFAQSectionDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if FAQ section displayed");
            awaitForElement(driver, faqSection);
            return faqSection.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isFAQSectionDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if FAQ list displayed in FAQ section")
    public boolean isRespectiveFAQListDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if FAQ list displayed in FAQ section");
            awaitForElement(driver, faqList);
            return faqList.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isRespectiveFAQListDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "clicking on down cheveron")
    public void clickOnDownCheveron() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("clicking on down cheveron");
            awaitForElement(driver, downCheveronInFAQSection);
            clickOnElement(downCheveronInFAQSection);
        }catch (Exception e){
            throw new Exception("Error in clickOnDownCheveron()"+e.getMessage());
        }
    }

    @StepInfo(info = "checking if FAQ answer displayed")
    public boolean isFAQAnswerDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if FAQ answer displayed");
            awaitForElement(driver, faqAnswer);
            return faqAnswer.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in isFAQAnswerDisplayed()"+e.getMessage());
        }
    }

    @StepInfo(info = "scrolling till user review card")
    public void scrollTillUserReviewCard(){
        ListenerImplimentationclass.testLog.info("scrolling till user review card");
        waitOrPause(3);
        swipeByCoordinates(driver,543,379,543,927);
    }

    @StepInfo(info = "clicking on first product add to cart button")
    public void clickOnFirstProductAddToCartBtn() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("clicking on first product add to cart button");
            awaitForElement(driver, firstProductAddToCartBtn);
            clickOnElement(firstProductAddToCartBtn);
        }catch (Exception e){
            throw new Exception("Error in clickOnFirstProductAddToCartBtn()"+e.getMessage());
        }
    }

    @StepInfo(info = "fetching chune/hataye text from the first offering item")
    public String getAddToCartTextFromFirstProduct() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("fetching chune/hataye text from the first offering item");
            awaitForElement(driver, firstProductAddToCartBtn);
            return firstProductAddToCartBtn.getText();
        }catch (Exception e){
            throw new Exception("Error in getAddToCartText()"+e.getMessage());
        }
    }

    @StepInfo(info = "fetching chune/hataye text from the second offering item")
    public String getAddToCartTextFromSecondProduct() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("fetching chune/hataye text from the second offering item");
            awaitForElement(driver, secondItemAddToCartBtn);
            return secondItemAddToCartBtn.getText();
        }catch (Exception e){
            throw new Exception("Error in getAddToCartTextFromSecondProduct()"+e.getMessage());
        }
    }

    @StepInfo(info = "scrolling to end of the page")
    public void scrollToEndOfPage() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("scrolling to end of the page");
            Verticalscroll(driver, 520, 1395, 520, 387, 3);
        }catch (Exception e){
            throw new Exception("Error in scrollToEndOfPage()"+e.getMessage());
        }
    }
    
    
    @StepInfo(info="checking if Temple Title is Displayed")
	public boolean isTempleTitleDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if Temple Title is Displayed");
			awaitForElement(driver, templeTitle);
			return templeTitle.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in  isTempleTitleDisplayed()"+e.getMessage()); 
		}
	}
    
    
    @StepInfo(info = "fetching temple name from item lisiting page")
    public String getTempleNameFromItemListingPage() throws Exception {
    	try {
    	ListenerImplimentationclass.testLog.info("fetching temple name from item lisiting page");
    	awaitForElement(driver, templeName);
    	return templeName.getText();
    	}catch (Exception e){
            throw new Exception("Error in getTempleNameFromItemListingPage()"+e.getMessage());
        }
    }
}
