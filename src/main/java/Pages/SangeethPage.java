package Pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ExcelUtility;
import base.ListenerImplimentationclass;
import io.appium.java_client.AppiumDriver;

public class SangeethPage extends AppGenericLib {
	ElementUtil elementUtil;
	public SangeethPage(AppiumDriver driver) {
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="com.mandir.debug:id/cv_empty_song_list")
	private WebElement emptySongList;
	
	@FindBy(id="com.mandir.debug:id/rv_trending_list")
	private WebElement trendingList;
	
	@FindBy(id="com.mandir.debug:id/title_music")
	private WebElement sangeethSangrahaPageSubTitle;
	
	@FindBy(id="com.mandir.debug:id/tv_empty_list")
	private WebElement addSongIconBtnToFavList;
	
	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_music']/android.view.ViewGroup[@resource-id='com.mandir.debug:id/parent']/	\r\n"
			+ "android.widget.FrameLayout[@resource-id='com.mandir.debug:id/fl_visible_action']")
	private WebElement whatsAppShareIcon;
	
	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_music']/	\r\n"
			+ "android.view.ViewGroup[@resource-id='com.mandir.debug:id/parent']/android.widget.FrameLayout[@resource-id='com.mandir.debug:id/fl_kebab']/	\r\n"
			+ "android.widget.ImageView")
	private WebElement ellipseIcon;
	
	@FindBy(id="com.mandir.debug:id/design_bottom_sheet")
	private WebElement ellipseBottomSheet;
	
	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_song_actions']/	\r\n"
			+ "android.view.ViewGroup[1]")
	private WebElement removeFromfavouritesTab;
	
	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_song_actions']/	\r\n"
			+ "android.view.ViewGroup[2]")
	private WebElement alarmTab;
	
	@FindBy(id="com.mandir.debug:id/iv_close")
	private WebElement closeBottomSheetBtn;
	
	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_music']/	\r\n"
			+ "android.view.ViewGroup/android.widget.LinearLayout[1]/android.widget.TextView[1]")
	private WebElement songTitleFromFavouritesPage;
	
	@FindBy(xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_music']/android.view.ViewGroup")
	private List<WebElement> favouriteSongCount;
	
	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_music']/android.view.ViewGroup")
	private WebElement favouriteSongFromList;
	
	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_trending_list']/android.view.ViewGroup[1]/android.widget.FrameLayout[@resource-id='com.mandir.debug:id/fl_visible_action']/android.widget.ImageView")
	private WebElement favouriteIconFromTrendingPlaylist;
	
	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_music']/	\r\n"
			+ "android.view.ViewGroup[1]/android.widget.FrameLayout[2]/android.widget.ImageView")
	private WebElement favSongIconFromPlaylist;
	
	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_music']/	\r\n"
			+ "android.view.ViewGroup[1]/android.widget.LinearLayout[@resource-id='com.mandir.debug:id/ll_content']/android.widget.TextView[1]")
	private WebElement songTitleFromThePlayList;
	
	@FindBy(id="com.mandir.debug:id/menu_left")
	private WebElement menuBtn;
	
	private WebElement sangeethPageTitle(String sangeethPageTitle) {
		String sangeethPageTitlexpathValue="//android.widget.TextView[@text='"+sangeethPageTitle+"']";
		return elementUtil.getElement("xpath", sangeethPageTitlexpathValue);
	}
	
	private WebElement pasandidaTabText(String pasandidaText) {
		String pasandidaTextxpathValue="//android.widget.TextView[@text='"+pasandidaText+"']";
		return elementUtil.getElement("xpath", pasandidaTextxpathValue);
	}

	@StepInfo(info="Fetching the sangeeth page tilte")
	public String getSangeethPageTitle() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("Fetching the sangeeth page tilte");
		awaitForElement(driver, sangeethPageTitle(ExcelUtility.getExcelData("locators", "sangeethPageTitle", "value")));
		}catch (Exception e){
			throw new Exception("Error in getSangeethPageTitle()"+e.getMessage());
		}
		return sangeethPageTitle(ExcelUtility.getExcelData("locators", "sangeethPageTitle", "value")).getText();
	}
	
	@StepInfo(info="checking if empty song list is displayed in favorite list")
	public boolean isEmptySongListDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if empty song list is displayed in favorite list");
		awaitForElement(driver, emptySongList);
		return emptySongList.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isEmptySongListDisplayed()"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if trending playlist is displayed below favourites icon")
	public boolean isTrendingListDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if trending playlist is displayed below favourites icon");
		awaitForElement(driver, trendingList);
		return trendingList.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isTrendingListDisplayed()"+e.getMessage());
		}
	}
	
	@StepInfo(info="fetching sangeeth sangraha page sub title")
	public String getSangeethSangrahaPageSubTitle() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("fetching sangeeth sangraha page sub title");
		awaitForElement(driver, sangeethSangrahaPageSubTitle);
		return sangeethSangrahaPageSubTitle.getText();
		}catch (Exception e){
			throw new Exception("Error in getSangeethSangrahaPageSubTitle()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on add song btn to fav list")
	public void clickOnAddSongToFavouriteListBtn() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on add song btn to fav list");
		awaitForElement(driver, addSongIconBtnToFavList);
		clickOnElement(addSongIconBtnToFavList);
		}catch (Exception e){
			throw new Exception("Error in clickOnAddSongToFavouriteListBtn()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on pasandida tab")
	public void clickOnPasandidaTab() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on pasandida tab");
		awaitForElement(driver, pasandidaTabText(ExcelUtility.getExcelData("locators", "pasandidaText", "value")));
		clickOnElement(pasandidaTabText(ExcelUtility.getExcelData("locators", "pasandidaText", "value")));
		}catch (Exception e){
			throw new Exception("Error in clickOnPasandidaTab()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on whatsapp share icon")
	public void clickOnWhatsAppShareIcon() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on whatsapp share icon");
		awaitForElement(driver, whatsAppShareIcon);
		clickOnElement(whatsAppShareIcon);
		}catch (Exception e){
			throw new Exception("Error in clickOnWhatsAppShareIcon()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on ellipse icon")
	public SangeethPage clickOnEllipseIcon() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on ellipse icon");
		awaitForElement(driver, ellipseIcon);
		clickOnElement(ellipseIcon);
		return this;
		}catch (Exception e){
			throw new Exception("Error in clickOnEllipseIcon()"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if  bottom sheet is displayed on clicking ellipse icon from fav list")
	public boolean isEllipseBottmSheetDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if  bottom sheet is displayed on clicking ellipse icon from fav list");
		awaitForElement(driver, ellipseBottomSheet);
		return ellipseBottomSheet.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isEllipseBottmSheetDisplayed()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on remove from favourites tab")
	public void clickOnRemoveFromFavouritesTab() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on remove from favourites tab");
		awaitForElement(driver, removeFromfavouritesTab);
		clickOnElement(removeFromfavouritesTab);
		}catch (Exception e){
			throw new Exception("Error in clickOnRemoveFromFavouritesTab()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on alarm icon")
	public void clickOnAlarmIcon() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on alarm icon");
		awaitForElement(driver, alarmTab);
		clickOnElement(alarmTab);
		}catch (Exception e){
			throw new Exception("Error in clickOnAlarmIcon()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on close button from the bottom sheet")
	public void clickOnCloseBottomSheetBtn() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on close button from the bottom sheet");
		awaitForElement(driver, closeBottomSheetBtn);
		clickOnElement(closeBottomSheetBtn);
		}catch (Exception e){
			throw new Exception("Error in clickOnCloseBottomSheetBtn()"+e.getMessage());
		}
	}
	
	@StepInfo(info="swiping till pasandida tab")
	public SangeethPage swipeToPasandidaTab() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("swiping till pasandida tab");
		Horizontalscroll(driver, 248, 237, 801,237,2);
		return this;
		}catch (Exception e){
			throw new Exception("Error in swipeToPasandidaTab()"+e.getMessage()); 
		}
	}
	
	@StepInfo(info="fetching song title from favourites")
	public String getSongTitleFromFavourites() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("fetching song title from favourites");
		awaitForElement(driver, songTitleFromFavouritesPage);
		return songTitleFromFavouritesPage.getText();
		}catch (Exception e){
			throw new Exception("Error in getSongTitleFromFavourites()"+e.getMessage()); 
		}
	}
	
	@StepInfo(info="chceking if ellipse icon is displayed in favourites tab")
	public void isEllipseIconDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("chceking if ellipse icon is displayed in favouritess tab");
		awaitForElement(driver, ellipseIcon);
		ellipseIcon.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isEllipseIconDisplayed()"+e.getMessage()); 
		}
	}
	
	@StepInfo(info="fetching favourite song count")
	public int getFavouriteSongCount() {
		ListenerImplimentationclass.testLog.info("fetching favourite song count");
		ArrayList<WebElement> list=new ArrayList<>();
		for (WebElement favSongs : favouriteSongCount) {
			list.add(favSongs);
		}
		return list.size();
	}
	
	@StepInfo(info="clicking on favourite icon from trending playlist")
	public void clickOnFavIconFromTrendingPlaylist() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on favourite icon from trending playlist");
		awaitForElement(driver, favouriteIconFromTrendingPlaylist);
		clickOnElement(favouriteIconFromTrendingPlaylist);
		}catch (Exception e){
			throw new Exception("Error in clickOnFavIconFromTrendingPlaylist()"+e.getMessage()); 
		}
	}
	
	@StepInfo(info="checking if favourite song is displayed")
	public boolean isFavSongListIsDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if favourite song is displayed");
		awaitForElement(driver, favouriteSongFromList);
		return favouriteSongFromList.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isFavSongIconIsDisplayed()"+e.getMessage()); 
		}
	}
	
	@StepInfo(info="clicking on favourite icon from the playlist")
	public SangeethPage clickOnFavIconFromThePlayist() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on favourite icon from the playlist");
		awaitForElement(driver, favSongIconFromPlaylist);
		clickOnElement(favSongIconFromPlaylist);
		return this;
		}catch (Exception e){
			throw new Exception("Error in clickOnFavIconFromThePlayist()"+e.getMessage()); 
		}
	}
	
	@StepInfo(info="fetching song title from the playlist")
	public String getSongTitleFromThePlaylist() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("fetching song title from the playlist");
		awaitForElement(driver, songTitleFromThePlayList);
		return songTitleFromThePlayList.getText();
		}catch (Exception e){
			throw new Exception("Error in getSongTitleFromThePlaylist()"+e.getMessage()); 
		}
	}
	
	@StepInfo(info="clicking on menu button")
	public void clickOnMenuBtn() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on menu button");
		awaitForElement(driver, menuBtn);
		clickOnElement(menuBtn);
		}catch (Exception e){
			throw new Exception("Error in clickOnMenuBtn()"+e.getMessage()); 
		}
	}
}
