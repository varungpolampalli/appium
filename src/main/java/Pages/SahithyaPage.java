package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ExcelUtility;
import base.ListenerImplimentationclass;
import io.appium.java_client.AppiumDriver;

public class SahithyaPage extends AppGenericLib{
	ElementUtil elementUtil;

	public SahithyaPage(AppiumDriver driver) {
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}

	private WebElement sahithyaBhandarPageText(String sahithyaText) {
		String sahithyaTabXpathValue="//android.widget.TextView[@text='"+sahithyaText+"']";
		return elementUtil.getElement("xpath", sahithyaTabXpathValue);
	}


	private WebElement LokPriyaMoreButton(String lokPriyaText)
	{
		String lokPriyaMoreButtonbuttonXpathValue="//android.widget.TextView[@text='"+lokPriyaText+"']/following-sibling::android.widget.ImageView[@resource-id='com.mandir.debug:id/view_more_icon']";
		return elementUtil.getElement("xpath", lokPriyaMoreButtonbuttonXpathValue);
	}

	@FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/heading']")
	private WebElement lokpriyaText;

	@FindBy(id="com.mandir.debug:id/back_button_lib_cat_list")
	private WebElement backButton;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/library_cat_list_rv']/android.view.ViewGroup[1]")
	private WebElement featurePage;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/library_list_horizontal_cards_rv']/descendant::android.widget.TextView[@resource-id='com.mandir.debug:id/library_tag'][1]")
	private WebElement articleTag;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/library_rv']/child::android.view.ViewGroup[2]//androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.FrameLayout")
	private WebElement videoArticle;

	@FindBy(xpath="//android.widget.ImageView[@resource-id='com.mandir.debug:id/signifier_video']")
	private WebElement videoPlayOrPauseButton;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/library_rv']/child::android.view.ViewGroup[2]//android.view.ViewGroup[2]//android.widget.ImageView[@resource-id='com.mandir.debug:id/image_article']")
	private WebElement textArticle;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/library_rv']/descendant::androidx.recyclerview.widget.RecyclerView[2]//android.view.ViewGroup[3]//android.widget.FrameLayout")
	private WebElement audioArticle;

	@FindBy(xpath="//android.widget.RelativeLayout[@resource-id='com.mandir.debug:id/rlAudio']")
	private WebElement audioArticleButton;

	@FindBy(id="com.mandir.debug:id/player_ll")
	private WebElement miniPlayer;

	@FindBy(id="com.mandir.debug:id/song_play_pause_button")
	private WebElement audioPlayOrPauseButton;

	@FindBy(id="com.mandir.debug:id/ic_mini_player_close")
	private WebElement closeButtonMiniPlayer;

	@FindBy(xpath="//android.widget.ImageView[@resource-id='com.mandir.debug:id/change_font']")
	private WebElement fontSize;

	@FindBy(id="com.mandir.debug:id/seekbar_font")
	private WebElement increaseFontSeekBar;

	@FindBy(id="com.mandir.debug:id/close_font")
	private WebElement closeButton;

	@FindBy(xpath="//android.widget.ImageView[@resource-id='com.mandir.debug:id/share_icon']")
	private WebElement watsAppIcon;

	@FindBy(id="com.mandir.debug:id/design_bottom_sheet")
	private WebElement ShareBottomSheet;

	@FindBy(xpath="//android.widget.ImageView[@resource-id='com.mandir.debug:id/back_arrow']")
	private WebElement audioArticleBackButton;

	@FindBy(id="com.mandir.debug:id/menu_left")
	private WebElement menuBtn;
	
//	@FindBy(id="com.mandir.debug:id/signifier_video")
//	private WebElement videoArticleButton;
	

	//Buisness Libraries

	@StepInfo(info="Click on Sahithya feature")
	public String getSahithyaTabText() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("Click on Sahithya feature");
			awaitForElement(driver,sahithyaBhandarPageText(ExcelUtility.getExcelData("locators", "sahithyaText","value")));
			return sahithyaBhandarPageText(ExcelUtility.getExcelData("locators", "sahithyaText","value" )).getText();
		}catch (Exception e){
			throw new Exception("Error in getSahithyaBhandarTitle()"+e.getMessage());
		}
		
	}

	@StepInfo(info="checking if more button is displayed")
	public boolean isMoreButtonDisplayed() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("checking if more button is displayed");
			awaitForElement(driver,LokPriyaMoreButton(ExcelUtility.getExcelData("locators", "lokPriyaText", "value")));
			return LokPriyaMoreButton(ExcelUtility.getExcelData("locators", "lokPriyaText", "value")).isDisplayed();	
		} catch (Exception e) {
			throw new Exception("Error in isMoreButtonDisplayed()"+e.getMessage());	
		}

	}
	

	@StepInfo(info="Click on lok priya more button")
	public void clickOnLokPriyaMoreButton() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("clicking on LokPriya more button ");
			awaitForElement(driver,LokPriyaMoreButton(ExcelUtility.getExcelData("locators", "lokPriyaText", "value")));
			LokPriyaMoreButton(ExcelUtility.getExcelData("locators", "lokPriyaText", "value")).click();		
		} catch (Exception e) {
			throw new Exception("Error in clickOnMoreLokPriyaTag()"+e.getMessage());	
		}
	}

	@StepInfo(info="fetching lok priya text")
	public String getLokPriyaText() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("fetching lok priya text");
			awaitForElement(driver,lokpriyaText);
			return lokpriyaText.getText();
		} catch (Exception e) {
			throw new Exception("Error in getLokPriyaText()"+e.getMessage());
		}
	}

	@StepInfo(info="Click on Back Button ")
	public SahithyaPage clickBackButton() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("Click on Back Button ");
			awaitForElement(driver,backButton);
			backButton.click();
			return this;
		} catch (Exception e) {
			throw new Exception("Error in clickBackButton()"+e.getMessage());
		}

	}

	@StepInfo(info="checking if feature page displayed")
	public boolean isFeaturePageDisplayed() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("checking if feature page displayed");
			awaitForElement(driver, featurePage);
			return featurePage.isDisplayed();
		} catch (Exception e) {
			throw new Exception("Error in isFeaturePageDisplayed()"+e.getMessage());
		}
	}
	@StepInfo(info="checking if article tags displayed")
	public boolean isArticleTagsDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking if article tags displayed");
			awaitForElement(driver, articleTag);
			return articleTag.isDisplayed();
		}
		catch (Exception e) {
			throw new Exception("Error in isArticleTagsDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking on TextArticle")
	public SahithyaPage clickOnTextArticle() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("clicking On TextArticle");
			awaitForElement(driver,textArticle);
			textArticle.click();
			return this;
		} catch (Exception e) {
			throw new Exception("Error in clickOnTextArticle()"+e.getMessage());
		}		
	}

	@StepInfo(info="clicking on  video article")
	public SahithyaPage clickVideoArticle() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("Clicking on the Video Article");
			clickOnElement(videoArticle);
			return this;

		} catch (Exception e) {
			throw new Exception("Error in clickVideoArticle()"+e.getMessage());
		}
	}

	@StepInfo(info="verifying play or pause button in video article")
	public boolean isVideoPlayOrPauseButtonDisplayed() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("verifying play or pause button in video article");
			awaitForElement(driver, videoPlayOrPauseButton);
			return videoPlayOrPauseButton.isDisplayed();

		} catch (Exception e) {
			throw new Exception("Error in  isVideoPlayOrPauseButtonDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="verifying Text article")
	public boolean verifyTextArticle() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("verifying Text article");
			textArticle.click();
			return fontSize.isDisplayed();

		} catch (Exception e) {
			throw new Exception("Error in verifyTextArticle()"+e.getMessage());
		}
	}


	@StepInfo(info="verifying Font size of text article")
	public boolean verifyFontSize() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("Verifying the Font Size ");
			awaitForElement(driver, fontSize);
			clickOnElement(fontSize);
			return increaseFontSeekBar.isDisplayed();			
		} catch (Exception e) {
			throw new Exception("Error in verifyFontSize()"+e.getMessage());
		}
	}
	@StepInfo(info="clicking on Font size close button")
	public SahithyaPage clickOnFontSizeCloseButton() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("clicking on Font size close button");
			clickOnElement(closeButton);
			return this; 
		} catch (Exception e) {
			throw new Exception("Error in clickOnFontSizeCloseButton()"+e.getMessage());
		}
	}

	@StepInfo(info="verifying whatsapp share button")
	public boolean verifyWatsAppShareButton() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("Verifying the WhatsAppShareButton ");
			clickOnElement(watsAppIcon);
			return ShareBottomSheet.isDisplayed();

		} catch (Exception e) {
			throw new Exception("Error in verifyWatsAppShareButton()"+e.getMessage());
		}
	}
	@StepInfo(info="Scrolling to next feature tag")
	public void scrollToNextTag() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("scroll To Next Feature Tag");
			Verticalscroll(driver, 1050, 950, 1040, 450, 1);

		} catch (Exception e) {
			throw new Exception("Error in scrollToNextTag()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking on audio article")
	public SahithyaPage clickOnAudioArticle() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("clicking On Audio Article");
			awaitForElement(driver,audioArticle);
			audioArticle.click();
			return this;
		} catch (Exception e) {
			throw new Exception("Error in clickOnAudioArticle()"+e.getMessage());
		}		
	}
	@StepInfo(info="Clicking on audio button")
	public SahithyaPage clickOnAudioButton() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("clicking On Audio Button");
			awaitForElement(driver,audioArticleButton);
			audioArticleButton.click();
			return this;
		} catch (Exception e) {
			throw new Exception("Error in clickOnAudioButton()"+e.getMessage());
		}		
	}

	@StepInfo(info="Verifying song mini player displayed or not")
	public boolean isMiniPlayerDisplayed() throws Exception
	{

		ListenerImplimentationclass.testLog.info("isMiniPlayerDisplayedOrNot");
		try {

			return miniPlayer.isDisplayed();

		} catch (Exception e) {
			throw new Exception("Error in isMiniPlayerDisplayed()"+e.getMessage());
		}

	}

	@StepInfo(info="Click on Pause or play button")
	public SahithyaPage clickAudioPauseOrPlayButton() throws Exception
	{
		ListenerImplimentationclass.testLog.info("Click on Pause or play button");
		try {
			audioPlayOrPauseButton.click();
			return this;
		} catch (Exception e) {
			throw new Exception("Error in ClickAudioPauseOrPlayButton()"+e.getMessage());
		}

	}

	@StepInfo(info="Click on close button on mininplayer")
	public SahithyaPage CloseMiniPlayer() throws Exception
	{
		ListenerImplimentationclass.testLog.info("Click on close button on mininplayer");
		try {
			closeButtonMiniPlayer.click();
			return this;
		} catch (Exception e) {
			throw new Exception("Error in CloseMiniPlayer()"+e.getMessage());
		}
	}

	@StepInfo(info="Click on audio article Back button")
	public SahithyaPage clickaudioArticleBackButton() throws Exception
	{
		ListenerImplimentationclass.testLog.info("Clicking on audio Article Back Button");
		try {
			audioArticleBackButton.click();
			return this;
		} catch (Exception e) {
			throw new Exception("Error in clickaudioArticleBackButton()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if scroll is working")
	public SahithyaPage isScrollable() throws Exception
	{
		ListenerImplimentationclass.testLog.info("checking if scroll is working");
		try {
			scrollTillEnd();
			return this;
		} catch (Exception e) {
			throw new Exception("Error in isScrollable()"+e.getMessage());
		}
	}

	
	@StepInfo(info="checking sahithya tab is displayed")
	public boolean isSahithyaTabDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking sahithya tab  is displayed");
			awaitForElement(driver, sahithyaBhandarPageText(ExcelUtility.getExcelData("locators", "sahithyaText", "value")));
			return sahithyaBhandarPageText(ExcelUtility.getExcelData("locators", "sahithyaText", "value")).isDisplayed();
       }
		catch (Exception e){
			throw new Exception("Error in isSahithyaTabDisplayed()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on Hamburger menu")
	public void clickOnHamburgerMenuBtn() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on Hamburger menu");
		clickOnElement(menuBtn);
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnHamburgerMenuBtn()"+e.getMessage());
		}
	}
	
	
}


