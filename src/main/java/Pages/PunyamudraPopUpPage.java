package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ListenerImplimentationclass;
import io.appium.java_client.AppiumDriver;

public class PunyamudraPopUpPage extends AppGenericLib{
	public ElementUtil elementUtil;
	public PunyamudraPopUpPage(AppiumDriver driver) 
	{
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="com.mandir.debug:id/tv_accept_reward")
	private WebElement praptKareBtnFromPunyamudraPopUp;
	
	@FindBy(id="com.mandir.debug:id/tv_reward_reason")
	private WebElement rewardTitle;
	
	
	@StepInfo(info = "checking if punya mudra bottom sheet is displayed")
	public boolean isPunyaMudraBottomSheetDisplayed() throws Exception {
		try {
//		ListenerImplimentationclass.testLog.info("checking if punya mudra bottom sheet is displayed");
		awaitForElement(driver, rewardTitle);
		return rewardTitle.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isPunyaMudraBottomSheetDisplayed()"+e.getMessage());
		}
	}
	
	@StepInfo(info = "clicking on prapt kare button from punya mudra pop up")
	public void clickOnPraptKareBtn() throws Exception {
		try {
//		ListenerImplimentationclass.testLog.info("clicking on prapt kare button from punya mudra pop up");
		awaitForElement(driver, praptKareBtnFromPunyamudraPopUp);
		clickOnElement(praptKareBtnFromPunyamudraPopUp);
		}catch (Exception e){
			throw new Exception("Error in clickOnPraptKareBtn()"+e.getMessage());
		}
	}


}
