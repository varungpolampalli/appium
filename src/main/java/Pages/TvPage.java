package Pages;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ExcelUtility;
import base.ListenerImplimentationclass;
import io.appium.java_client.AppiumDriver;
public class TvPage extends AppGenericLib{
	ElementUtil elementUtil;
	public TvPage(AppiumDriver driver)
	{
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}
	private WebElement tvTabText(String tvTabText  ) {
		String tvTabTextxpathValue="//android.widget.HorizontalScrollView[@resource-id='com.mandir.debug:id/tab_bar']/android.widget.LinearLayout//android.widget.TextView[@text='"+tvTabText+"']";
		return elementUtil.getElement("xpath", tvTabTextxpathValue);
	}
	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/suggested_rv_container']/descendant::android.widget.ImageView[1]")
	private WebElement tvShow;
	private WebElement episodesCount(String  episodesCount) {
		String episodesCountXpathValue="//android.widget.TextView[@text='"+episodesCount+"']";
		return elementUtil.getElement("xpath", episodesCountXpathValue);
	}
	private WebElement tvShowTitle(String  showTitle) {
		String tvShowTitleXpathValue="//android.widget.TextView[@text='"+showTitle+"']";
		return elementUtil.getElement("xpath", tvShowTitleXpathValue);
	}
	@FindBy(xpath="//android.widget.ImageView[@resource-id='com.mandir.debug:id/back_button']")
	private WebElement tvShowBackButton;
	@FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_ep_count']")
	private WebElement tvShowLanguage;
	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_container']/android.widget.LinearLayout[1]")
	private WebElement tvEpisode;
	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_container']/descendant::android.widget.ImageView[3]")
	private WebElement lastTvShow;
	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_container']/android.widget.LinearLayout[1]")
	private WebElement firstEpisode;
	@FindBy(id="com.mandir.debug:id/tv_title")
	private WebElement ganeshTvShow;
	//Buisness Libraries
	@StepInfo(info="fetching the tv  tab title")
	public String getTvTabText() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("fetching the tv tab title ");
			awaitForElement(driver, tvTabText(ExcelUtility.getExcelData("locators", "tvText", "value")));
			return tvTabText(ExcelUtility.getExcelData("locators", "tvText", "value")).getText();
		}catch (Exception e)
		{
			throw new Exception("Error in  getTvtabText()"+e.getMessage());
		}
	}
	@StepInfo(info="verify tv tab is displayed")
	public boolean isTvTabDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking tv tab  is displayed");
			awaitForElement(driver, tvTabText(ExcelUtility.getExcelData("locators", "tvText", "value")));
			return tvTabText(ExcelUtility.getExcelData("locators", "tvText", "value")).isDisplayed();
		}
		catch (Exception e){
			throw new Exception("Error in isTvTabDisplayed()"+e.getMessage());
		}
	}
	@StepInfo(info="scrolling to Tv tab ")
	public void scrollToTvTab() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("scrolling to Tv tab ");
			scrollToHorizontalElementAndClick(driver, 400, 900, tvTabText(ExcelUtility.getExcelData("locators", "tvText", "value")), 1);
		} catch (Exception e) {
			throw new Exception("Error in scrollToTvTab()"+e.getMessage());
		}
	}
	@StepInfo(info="verifying Tv shows ")
	public boolean isTvShowTabDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("verifying Tv shows feature");
			awaitForElement(driver, tvShow);
			return tvShow.isDisplayed();
		} catch (Exception e) {
			throw new Exception("Error in isTvShowTabDisplayed()"+e.getMessage());
		}
	}
	@StepInfo(info="fetching tv episodes count ")
	public String getTvEpisodesCount() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("fetching tv episodes count ");
			awaitForElement(driver,episodesCount(ExcelUtility.getExcelData("locators", "tvEpisode", "value")));
			return episodesCount(ExcelUtility.getExcelData("locators", "tvEpisode", "value")).getText();
		} catch (Exception e) {
			throw new Exception("Error in getTvEpisodesCount()"+e.getMessage());
		}
	}
	@StepInfo(info="verifying tv show title ")
	public String getTvShowTitle() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("verifying tv show title ");
			awaitForElement(driver,tvShowTitle(ExcelUtility.getExcelData("locators", "mahaBharath", "value")));
			return tvShowTitle(ExcelUtility.getExcelData("locators", "mahaBharath", "value")).getText();
		} catch (Exception e) {
			throw new Exception("Error in getTvShowTitle()"+e.getMessage());
		}
	}
	@StepInfo(info="clicking the tv show")
	public void clickOnTvShow() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking the tv show ");
			awaitForElement(driver,tvShowTitle(ExcelUtility.getExcelData("locators", "mahaBharath", "value")));
			clickOnElement(tvShowTitle(ExcelUtility.getExcelData("locators", "mahaBharath", "value")));
		} catch (Exception e) {
			throw new Exception("Error in clickOnTvShow()"+e.getMessage());
		}
	}
	@StepInfo(info="clicking on Tv back Button")
	public void clickOnTvBackButton() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on Tv back Button");
			awaitForElement(driver, tvShowBackButton);
			clickOnElement(tvShowBackButton);
		} catch (Exception e) {
			throw new Exception("Error in clickOnTvBackButton()"+e.getMessage());
		}
	}
	@StepInfo(info="fetching Tv show language")
	public String getTvShowLanguage() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("fetching Tv show language");
			awaitForElement(driver,tvShowLanguage);
			return tvShowLanguage.getText();
		} catch (Exception e) {
			throw new Exception("Error in getTvShowLanguage()"+e.getMessage());
		}
	}
	@StepInfo(info="clicking on Tv show episode")
	public TvPage clickOnTvEpisode() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on Tv show episode");
			awaitForElement(driver, tvEpisode);
			clickOnElement(tvEpisode);
			return this;

		} catch (Exception e) {
			throw new Exception("Error in clickOnTvEpisode()"+e.getMessage());
		}
	}
	@StepInfo(info="scrolling Tv show episodes")
	public TvPage scrollEpisodes() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("scrolling Tv show episodes");
			Horizontalscroll(driver, 900, 1000, 100, 1000, 10);
			return this;
		} catch (Exception e) {
			throw new Exception("Error in viewEpisodes()"+e.getMessage());
		}
	}
	@StepInfo(info="scrolling Tv shows")
	public TvPage scrollTvShows() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("scrolling Tv shows");
			swipeByCoordinates(driver, 1000, 600, 100, 600);
			return this;
		} catch (Exception e) {
			throw new Exception("Error in scrollTvShows()"+e.getMessage());
		}
		
	}
	@StepInfo(info="click on last tv show")
	public TvPage clickLastTvShow() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("click on last tv show");
			awaitForElement(driver, lastTvShow);
			clickOnElement(lastTvShow);
			return this;
		} catch (Exception e) {
			throw new Exception("Error in lastTvShow()"+e.getMessage());
		}
	}

	@StepInfo(info="click on last ganesh Tv Episode")
	public TvPage clickOnganeshTvEpisode() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("click on last ganesh Tv Episode");
			awaitForElement(driver, lastTvShow);
			clickOnElement(lastTvShow);
			return this;
		} catch (Exception e) {
			throw new Exception("Error in lastTvShow()"+e.getMessage());
		}
	}
	@StepInfo(info="fetching on last tv show text")
	public String getTvShowText() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("fetching on last tv show text");
			awaitForElement(driver, ganeshTvShow);
			return ganeshTvShow.getText();
		} catch (Exception e) {
			throw new Exception("Error in clickOnEpisode()"+e.getMessage());
		}
	}

} 			


