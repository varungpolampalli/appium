package Pages;

import java.util.Random;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.python.antlr.PythonParser.return_stmt_return;
import org.testng.annotations.Test;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ExcelUtility;
import base.ListenerImplimentationclass;
import base.TestData;
import io.appium.java_client.AppiumDriver;

public class EditProfilePage extends AppGenericLib {

	ElementUtil elementUtil;
	public EditProfilePage(AppiumDriver driver) {
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id="com.mandir.debug:id/back_button_kundli_form")
	private WebElement backBtn;

	@FindBy(id="com.mandir.debug:id/edit_name")
	private WebElement nameTextFeild;

	@FindBy(id="com.mandir.debug:id/save_karein_ep")
	private WebElement saveBtn;

	@FindBy(id="com.mandir.debug:id/male")
	private WebElement maleGenderTab;

	@FindBy(id="com.mandir.debug:id/other")
	private WebElement otherGenderTab;

	@FindBy(xpath="//android.widget.FrameLayout[@resource-id='com.mandir.debug:id/male']/following-sibling::android.widget.FrameLayout[1]")
	private WebElement femaleGenderTab;

	@FindBy(id="com.mandir.debug:id/ep_birth_date")
	private WebElement birthDateSelectionTab;

	@FindBy(id="com.mandir.debug:id/design_bottom_sheet")
	private WebElement calendarBottomSheet;

	@FindBy(id="com.mandir.debug:id/btn_save")
	private WebElement bottomSheetSaveBtn;

	@FindBy(xpath = "//android.widget.LinearLayout[@resource-id='android:id/pickers']/android.widget.NumberPicker[1]/android.widget.EditText")
	private WebElement currentSelectedDay;

	@FindBy(xpath="//android.widget.LinearLayout[@resource-id='android:id/pickers']/android.widget.NumberPicker[3]/android.widget.EditText")
	private WebElement currentSelectedYear;

	@FindBy(id="com.mandir.debug:id/ep_birth_time")
	private WebElement birthTimeSelectionTab;

	@FindBy(id="com.mandir.debug:id/iv_cancel")
	private WebElement closeBtn;

	@FindBy(xpath="//android.widget.FrameLayout[@resource-id='com.mandir.debug:id/profile_photo']")
	private WebElement profilePhoto;

	@FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/birth_city']")
	private WebElement phoneNumber;

	@FindBy(xpath="//android.widget.LinearLayout[@resource-id='android:id/timePickerLayout']/android.widget.LinearLayout/android.widget.NumberPicker[1]/android.widget.EditText")
	private WebElement currentSelectedHour;

	@FindBy(xpath="//android.widget.LinearLayout[@resource-id='android:id/timePickerLayout']/android.widget.LinearLayout/android.widget.NumberPicker[2]/android.widget.EditText")
	private WebElement currentSelectedMinute;

	@FindBy(id="com.mandir.debug:id/tv_changePhoto")
	private WebElement changePhotoBtn;

	@FindBy(id="com.google.android.apps.photos:id/image")
	private WebElement ImagesBtnFromGallery;

	@FindBy(id="com.mandir.debug:id/location_edit_profile")
	private WebElement locationTab;

	@FindBy(id="com.mandir.debug:id/rv_container_birth_place")
	private WebElement locationList;

	@FindBy(id="com.mandir.debug:id/et_place_name_birth_place")
	private WebElement locationSearchTextFeild;

	@FindBy(id="com.mandir.debug:id/clear_search_text")
	private WebElement locationSearchTextFeildClearBtn;

	@FindBy(id="com.mandir.debug:id/location_name")
	private WebElement locationNameFromLocationSearchList;

	@FindBy(id="com.mandir.debug:id/back_button_birth_place")
	private WebElement backBtnFromLocationSearchPage;

	@FindBy(id="com.mandir.debug:id/ep_occupation")
	private WebElement occupationTab;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_container_occupation']/android.widget.LinearLayout[2]//android.widget.TextView")
	private WebElement occupationNameFromOccupationList;
	
	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_container_occupation']")
	private WebElement occupationList;
	
	@FindBy(id="com.mandir.debug:id/back_button_occupation")
	private WebElement backBtnFromOcccupationListPage;
	
	@FindBy(id="com.mandir.debug:id/save_changes")
	private WebElement saveBtnFromConfirmationPopUp;

	private WebElement editProfileTitle(String editProfilePageTitle) {
		String editProfilePageTitlexpathValue="//android.widget.TextView[@text='"+editProfilePageTitle+"']";
		return elementUtil.getElement("xpath", editProfilePageTitlexpathValue);
	}

	@StepInfo(info="Fetching  edit profile  page Title")
	public String getEditProfilePageTitle() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("Fetching  edit profile  page Title");
			awaitForElement(driver,  editProfileTitle(ExcelUtility.getExcelData("locators", "editProfilePageTitleText", "value")));
		}catch (Exception e){
			throw new Exception("Error in getEditProfilePageTitle()"+e.getMessage());
		}
		return  editProfileTitle(ExcelUtility.getExcelData("locators", "editProfilePageTitleText", "value")).getText();
	}

	@StepInfo(info="clicking on back button from my Edit Profile page")
	public EditProfilePage clickOnBackBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on back button from my Edit Profile page");
			awaitForElement(driver, backBtn);
			clickOnElement(backBtn);
			return this;
		}catch (Exception e){
			throw new Exception("Error in clickOnBackBtn()"+e.getMessage());
		}
	}

	@StepInfo(info="clearing name text feild")
	public EditProfilePage clearNameTextFeild() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clearing name text feild");
			awaitForElement(driver, nameTextFeild);
			nameTextFeild.clear();
			return this;
		}catch (Exception e){
			throw new Exception("Error in clearNameTextFeild()"+e.getMessage());
		}
	}

	@StepInfo(info="Entering name to name Text feild")
	public String enterName() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("Entering name to name Text feild");
			Random randromNum=new Random();
			 String actualUserName = TestData.userName+"_"+randromNum.nextInt(1000);
			nameTextFeild.sendKeys(actualUserName);
			return actualUserName;
		}catch (Exception e){
			throw new Exception("Error in enterName()"+e.getMessage());
		}
	}
	
	@StepInfo(info="Entering default name to name Text feild")
	public void enterDefaultName() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("Entering default name to name Text feild");
		nameTextFeild.sendKeys(TestData.userName);
		}catch (Exception e){
			throw new Exception("Error in EnterDefaultName()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking on save button")
	public ProfilePage clickOnSaveButton() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on save button");
			awaitForElement(driver, saveBtn);
			clickOnElement(saveBtn);
			return new ProfilePage(driver);
		}catch (Exception e){
			throw new Exception("Error in clickOnSaveButton()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking on male gender tab")
	public boolean isMaleGenderTabSelected() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on male gender tab");
			awaitForElement(driver, maleGenderTab);
			clickOnElement(maleGenderTab);
			return maleGenderTab.isEnabled();
		}catch (Exception e){
			throw new Exception("Error in clickOnMaleGenderTab()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking on female gender tab")
	public boolean isFemaleGenderTabSelected() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on female gender tab");
			awaitForElement(driver, femaleGenderTab);
			clickOnElement(femaleGenderTab);
			return femaleGenderTab.isEnabled();
		}catch (Exception e){
			throw new Exception("Error in clickOnFemaleGenderTab()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking on other gender tab")
	public boolean isOtherGenderTabSelected() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on other gender tab");
			awaitForElement(driver, otherGenderTab);
			clickOnElement(otherGenderTab);
			return otherGenderTab.isEnabled();
		}catch (Exception e){
			throw new Exception("Error in clickOnOtherGenderTab()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking on birth date selection tab")
	public EditProfilePage clickOnBirthDateSelectionTab() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on birth date selection tab");
			awaitForElement(driver, birthDateSelectionTab);
			clickOnElement(birthDateSelectionTab);
			return this;
		}catch (Exception e){
			throw new Exception("Error in clickOnBirthDateSelectionTab()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if calendar bottom sheet is displayed")
	public boolean isCalendarBottomSheetDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if calendar bottom sheet is displayed");
			awaitForElement(driver, calendarBottomSheet);
			return calendarBottomSheet.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isCalendarBottomSheetDisplayed()"+e.getMessage());
		}

	}

	@StepInfo(info="clicking on save button from calendar bottom sheet")
	public ProfilePage clickOnSaveButtonFromCalenderBottomSheet() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on save button from calendar bottom sheet");
			awaitForElement(driver, bottomSheetSaveBtn);
			clickOnElement(bottomSheetSaveBtn);
			return new ProfilePage(driver);
		}catch (Exception e){
			throw new Exception("Error in clickOnSaveButtonFromCalenderBottomSheet()"+e.getMessage());
		}
	}

	@StepInfo(info="swiping   day selection button")
	public EditProfilePage swipeDaySelectionTab() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("swiping   day selection button");
			awaitForElement(driver, bottomSheetSaveBtn);
			swipeByCoordinates(driver, 259, 1457, 259, 1023);
			return this;
		}catch (Exception e){
			throw new Exception("Error in swipeDaySelectionTab()"+e.getMessage());
		}
	}

	@StepInfo(info="swiping month selection tab")
	public EditProfilePage swipeMonthSelectionTab() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("swiping month selection tab");
			swipeByCoordinates(driver, 538, 1457, 538, 1023);
			return this;
		}catch (Exception e){
			throw new Exception("Error in swipeMonthSelectionTab()"+e.getMessage());
		}
	}

	@StepInfo(info="swiping year selection tab")
	public EditProfilePage swipeYearSelectionTab() throws Exception {

		ListenerImplimentationclass.testLog.info("swiping year selection tab");
		int yearValue = Integer.parseInt(currentSelectedYear.getText());
		try
		{
			while(yearValue>2010 ) {
				swipeByCoordinates(driver, 807, 1185, 807, 1457);
				yearValue = Integer.parseInt(currentSelectedYear.getText());
			}
			while(yearValue<2010 ) {
				swipeByCoordinates(driver, 807, 1457, 807, 1185);
				yearValue = Integer.parseInt(currentSelectedYear.getText());
			}
		}catch(Exception e) {
			throw new Exception("Error in selecting year"+e.getMessage());
		}
		return this;

	}

	@StepInfo(info="fetching current selected day from the calendar bottom sheet")
	public String getCurrentSelectedDay() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("fetching current selected day from the calendar bottom sheet");
			awaitForElement(driver, currentSelectedDay);
			return currentSelectedDay.getText();
		}catch (Exception e){
			throw new Exception("Error in getCurrentSelectedDay()"+e.getMessage());
		}
	}

	@StepInfo(info="fetching current selected day from the calendar bottom sheet")
	public String getCurrentSelectedYear() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("fetching current selected day from the calendar bottom sheet");
			awaitForElement(driver, currentSelectedYear);
			return currentSelectedYear.getText();
		}catch (Exception e){
			throw new Exception("Error in getCurrentSelectedYear()"+e.getMessage());
		}
	}

	@StepInfo(info="fetching selected DOB from date selection tab")
	public String[] getSelectedDOB() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("fetching current selected day from the calendar bottom sheet");
			awaitForElement(driver, birthDateSelectionTab);
			return birthDateSelectionTab.getText().split("-");
		}catch (Exception e){
			throw new Exception("Error in getSelectedDOB()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking on birth time selection tab")
	public void clickOnBirthTimeSelectionTab() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on birth time selection tab");
			awaitForElement(driver, birthTimeSelectionTab);
			clickOnElement(birthTimeSelectionTab);
			waitOrPause(5);
		}catch (Exception e){
			throw new Exception("Error in clickOnBirtTimeSelectionTab()"+e.getMessage());
		}

	}

	@StepInfo(info="clicking on cancel button from time bottom sheet")
	public void clickOnCloseButtonFromTimeBottomSheet() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on cancel button from time bottom sheet");
			awaitForElement(driver, bottomSheetSaveBtn);
			clickOnElement(closeBtn);
		}catch (Exception e){
			throw new Exception("Error in clickOnCancelButton()"+e.getMessage());
		}
	}

	@StepInfo(info="fetching selected time from time selection tab")
	public String[] getSelectedTime() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("fetching selected time from time selection tab");
			awaitForElement(driver,birthTimeSelectionTab );
			return birthTimeSelectionTab.getText().split(":");
		}catch (Exception e){
			throw new Exception("Error in getSelectedTime()"+e.getMessage());
		}
	}

	@StepInfo(info="swiping hour selection tab")
	public EditProfilePage swipeHourSelectionTab() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("swiping hour selection tab");
			awaitForElement(driver, bottomSheetSaveBtn);
			swipeByCoordinates(driver, 259, 1457, 259, 1023);
			return this;
		}catch (Exception e){
			throw new Exception("Error in swipeHourSelectionTab()"+e.getMessage());
		}
	}

	@StepInfo(info="swiping minute selection tab")
	public EditProfilePage swipeMinuteSelectionTab() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("swiping minute selection tab");
			swipeByCoordinates(driver, 538, 1457, 538, 1023);
			return this;
		}catch (Exception e){
			throw new Exception("Error in swipeMinuteSelectionTab()"+e.getMessage());
		}
	}

	@StepInfo(info="fetching current selected Hour from the Time bottom sheet")
	public String getCurrentSelectedHour() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("fetching current selected Hour from the Time bottom sheet");
			awaitForElement(driver, currentSelectedHour);
			return currentSelectedHour.getText();
		}catch (Exception e){
			throw new Exception("Error in getCurrentSelectedHour()"+e.getMessage());
		}
	}

	@StepInfo(info="fetching current selected Minute from the Time bottom sheet")
	public String getCurrentSelectedMinute() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("fetching current selected Minute from the Time bottom sheet");
			awaitForElement(driver, currentSelectedMinute);
			return currentSelectedMinute.getText();
		}catch (Exception e){
			throw new Exception("Error in getCurrentSelectedMinute()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if save button is displayed")
	public boolean isSaveButtonDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if save button is displayed");
			awaitForElement(driver, saveBtn);
			return saveBtn.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isSaveButtonDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if profile photo is displayed")
	public boolean isProfilePhotoDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if profile photo is displayed");
			awaitForElement(driver, profilePhoto);
			return profilePhoto.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isProfilePhotDisplayed()"+e.getMessage());
		}
	}


	@StepInfo(info="fetching phoneNumber from my profile page")
	public String getphoneNumber() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("fetching phoneNumber from my profile page");
			awaitForElement(driver, phoneNumber);
			return phoneNumber.getText();
		}catch (Exception e){
			throw new Exception("Error in getphoneNumber()"+e.getMessage());
		}
	}

	@StepInfo(info="converting String Day Value to Integer")
	public int getSelectedDayValue() throws Exception   {
		try {
			ListenerImplimentationclass.testLog.info("converting String Day Value to Integer");
			return Integer.parseInt(getCurrentSelectedDay());
		}catch (Exception e){
			throw new Exception("Error in getSelectedDayValue() "+e.getMessage());
		}

	}

	@StepInfo(info="converting String Year Value to Integer")
	public int getSelectedYearValue() throws Exception   {
		try {
			ListenerImplimentationclass.testLog.info("converting String Year Value to Integer");
			return Integer.parseInt(getCurrentSelectedYear());
		}catch (Exception e){
			throw new Exception("Error in getSelectedYearValue() "+e.getMessage());
		}

	}

	@StepInfo(info="converting String Hour Value to Integer")
	public int getSelectedHourValue() throws Exception   {
		try {
			ListenerImplimentationclass.testLog.info("converting String Hour Value to Integer");
			return Integer.parseInt(getCurrentSelectedHour());
		}catch (Exception e){
			throw new Exception("Error in getSelectedHourValue() "+e.getMessage());
		}

	}

	@StepInfo(info="converting String Minute Value to Integer")
	public int getSelectedMinuteValue() throws Exception   {
		try {
			ListenerImplimentationclass.testLog.info("converting String Minute Value to Integer");
			return Integer.parseInt(getCurrentSelectedMinute());
		}catch (Exception e){
			throw new Exception("Error in getSelectedMinuteValue() "+e.getMessage());
		}

	}

	@StepInfo(info="Fetching actual day value from date selection tab and converting String Actual Day Value to Integer")
	public int getActualDayValue() throws Exception   {
		try {
			ListenerImplimentationclass.testLog.info("Fetching actual day value from date selection tab and converting String Actual Day Value to Integer");
			return Integer.parseInt(getSelectedDOB()[0]);
		}catch (Exception e){
			throw new Exception("Error in getActualDayValue() "+e.getMessage());
		}

	}

	@StepInfo(info="Fetching actual Year value from date selection tab and converting String Actual Year Value to Integer")
	public int getActualYearValue() throws Exception   {
		try {
			ListenerImplimentationclass.testLog.info("Fetching actual Year value from date selection tab and converting String Actual Year Value to Integer");
			return Integer.parseInt(getSelectedDOB()[2]);
		}catch (Exception e){
			throw new Exception("Error in getActualDayValue() "+e.getMessage());
		}

	}

	@StepInfo(info="Fetching actual Hour value from date selection tab and converting String Actual Hour Value to Integer")
	public int getActualHourValue() throws Exception   {
		try {
			ListenerImplimentationclass.testLog.info("Fetching actual Hour value from date selection tab and converting String Actual Hour Value to Integer");
			return Integer.parseInt(getSelectedTime()[0]);
		}catch (Exception e){
			throw new Exception("Error in  getActualHourValue() "+e.getMessage());
		}

	}

	@StepInfo(info="Fetching actual Minute value from date selection tab and converting String Actual Minute Value to Integer")
	public int getActualMinuteValue() throws Exception   {
		try {
			ListenerImplimentationclass.testLog.info("Fetching actual Minute value from date selection tab and converting String Actual Minute Value to Integer");
			return Integer.parseInt(getSelectedTime()[1].split(" ")[0]);
		}catch (Exception e){
			throw new Exception("Error in  getActualMinuteValue() "+e.getMessage());
		}

	}

	@StepInfo(info="clicking on change photo button")
	public EditProfilePage clickOnChangePhotoBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on change photo button");
			awaitForElement(driver, changePhotoBtn);
			clickOnElement(changePhotoBtn);
			return this;
		}catch (Exception e){
			throw new Exception("Error in   clickOnChangePhotoBtn() "+e.getMessage());
		}
	}

	@StepInfo(info="clicking on images from  device")
	public EditProfilePage clickOnImagesBtnFromDevice() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on images from  device");
			awaitForElement(driver, ImagesBtnFromGallery);
			clickOnElement(ImagesBtnFromGallery);
			return this;
		}catch (Exception e){
			throw new Exception("Error in   clickOnChangePhotoBtn() "+e.getMessage());
		}
	}

	@StepInfo(info="clicking on picture tab from  device")
	public void clickOnPictureTabFromDevice() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on picture tab from  device");
			waitOrPause(5);
			tapByCoordinates(131, 484);
		}catch (Exception e){
			throw new Exception("Error in  clickOnPictureTabFromDevice() "+e.getMessage());
		}
	}

	@StepInfo(info="clicking on location Tab from edit profile page")
	public EditProfilePage clickOnLocationTab() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on location Tab from edit profile page");
			awaitForElement(driver, locationTab);
			clickOnElement(locationTab);
			return this;
		}catch (Exception e){
			throw new Exception("Error in   clickOnLocationTab() "+e.getMessage());
		}
	}

	@StepInfo(info="checking if location list displayed")
	public boolean isLocationListDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if location list displayed");
			awaitForElement(driver, locationList);
			return locationList.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in   isLocationListDisplayed() "+e.getMessage());
		}
	}

	@StepInfo(info="typing location to location search text feild")
	public EditProfilePage typeLocationToLocationSearchFeild() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("typing location to location search text feild");
			awaitForElement(driver, locationSearchTextFeild);
			locationSearchTextFeild.sendKeys("Bangalore");
			return this;
		}catch (Exception e){
			throw new Exception("Error in   typeLocationToLocationSearchFeild() "+e.getMessage());
		}
	}

	@StepInfo(info="clicking on clear button from location search text feild")
	public void clickOnClearBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on clear button from location search text feild");
			awaitForElement(driver, locationSearchTextFeildClearBtn);
			clickOnElement(locationSearchTextFeildClearBtn);
		}catch (Exception e){
			throw new Exception("Error in   clickOnClearBtn() "+e.getMessage());
		}
	}

	@StepInfo(info="fetching location name from location search list")
	public String getLocationNameFromLocationSearchList() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("fetching location name from location search list");
			awaitForElement(driver, locationNameFromLocationSearchList);
			return locationNameFromLocationSearchList.getText();
		}catch (Exception e){
			throw new Exception("Error in getLocationNameFromLocationList() "+e.getMessage());
		}
	}


	@StepInfo(info="fetching location name from location Tab from edit profile Page")
	public String getLocationNameFromLocationTab() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("fetching location name from location Tab from edit profile Page");
			awaitForElement(driver, locationTab);
			return locationTab.getText();
		}catch (Exception e){
			throw new Exception("Error in getLocationNameFromLocationTab() "+e.getMessage());
		}
	}

	@StepInfo(info="clicking on back button from location search Page")
	public void clickOnBackBtnFromLocationSearchPage() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on back button from location search Page");
			awaitForElement(driver, backBtnFromLocationSearchPage);
			clickOnElement(backBtnFromLocationSearchPage);
		}catch (Exception e){
			throw new Exception("Error in clickOnBackBtnFromLocationSearchPage() "+e.getMessage());
		}
	}

	@StepInfo(info="clicking on location from location list")
	public void clickOnLocationFromLocationList() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on location from location list");
			awaitForElement(driver, locationNameFromLocationSearchList);
			clickOnElement(locationNameFromLocationSearchList);
		}catch (Exception e){
			throw new Exception("Error in clickOnLocationFromLocationList() "+e.getMessage());
		}
	}

	@StepInfo(info="clicking on Occupation Tab")
	public EditProfilePage clickOnOccupationTab() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on Occupation Tab from edit profile page");
			awaitForElement(driver, occupationTab);
			clickOnElement(occupationTab);
			return this;
		}catch (Exception e){
			throw new Exception("Error in clickOnOccupationTab() "+e.getMessage());
		}
	}

	@StepInfo(info="clicking on respective occupation from occupation list")
	public void clickOnOccupationNameFromOccupationList() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on respective occupation from occupation list");
			awaitForElement(driver, occupationNameFromOccupationList);
			clickOnElement(occupationNameFromOccupationList);
		}catch (Exception e){
			throw new Exception("Error in clickOnOccupationNameFromOccupationList() "+e.getMessage());
		}
	}
	
	@StepInfo(info="fetching occupation Name from occupation List")
	public String getOccupationNameFromoccupationList() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("fetching occupation Name from occupation List");
			awaitForElement(driver, occupationNameFromOccupationList);
			return occupationNameFromOccupationList.getText();
		}catch (Exception e){
			throw new Exception("Error in getOccupationNameFromoccupationList() "+e.getMessage());
		}
	}
	
	@StepInfo(info="fetching occupation Name From Occupation Tab from edit profile page")
	public String getOccupationNameFromOccupationTab() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on Occupation Tab from edit profile page");
			awaitForElement(driver, occupationTab);
			return occupationTab.getText();
		}catch (Exception e){
			throw new Exception("Error in clickOnOccupationTab() "+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if occupation list is displayed on clicking occupation tab from edit profile page")
	public boolean isOccupationListDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if occupation list is displayed on clicking occupation tab from edit profile page");
			awaitForElement(driver, occupationList);
			return occupationList.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isOccupationListDisplayed() "+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on back button from occupation list  Page")
	public void clickOnBackBtnFromOccupationListPage() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on back button from occupation list  Page");
			awaitForElement(driver, backBtnFromOcccupationListPage);
			clickOnElement(backBtnFromOcccupationListPage);
		}catch (Exception e){
			throw new Exception("Error in clickOnBackBtnFromOccupationListPage() "+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on save button from confirmation pop up")
	public void clickOnSaveBtnFromConfirmationPopUp() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on save button from confirmation pop up");
			awaitForElement(driver, saveBtnFromConfirmationPopUp);
			clickOnElement(saveBtnFromConfirmationPopUp);
		}catch (Exception e){
			throw new Exception("Error in clickOnSaveBtnFromConfirmationPopUp() "+e.getMessage());
		}
	}

}
