package Pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ExcelUtility;
import base.ListenerImplimentationclass;
import io.appium.java_client.AppiumDriver;

public class SocialLandingPage extends AppGenericLib{

	ElementUtil elementUtil;
	public SocialLandingPage(AppiumDriver driver) {
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}	

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/social_list_vertical_cards_rv']/android.view.ViewGroup[1]//android.widget.FrameLayout[@resource-id='com.mandir.debug:id/cvMain']/android.view.ViewGroup")
	private WebElement communitycard;

	@FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/menu_title']")
	private WebElement samudayaPageTitle;

	@FindBy(xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/social_list_vertical_cards_rv']")
	private WebElement allCommunityCards;

	@FindBy(xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/social_list_vertical_cards_rv']/android.view.ViewGroup[1]//android.widget.FrameLayout[@resource-id='com.mandir.debug:id/cvMain']/android.view.ViewGroup/android.widget.FrameLayout[@resource-id='com.mandir.debug:id/cv_image']")
	private WebElement firstCommunityCardImage;

	@FindBy(xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/social_list_vertical_cards_rv']/android.view.ViewGroup[1]//android.widget.FrameLayout[@resource-id='com.mandir.debug:id/cvMain']/android.view.ViewGroup/android.widget.TextView[@resource-id='com.mandir.debug:id/title']")
	private WebElement communityCardTitle;

	@FindBy(xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/social_list_vertical_cards_rv']/android.view.ViewGroup[1]//android.widget.FrameLayout[@resource-id='com.mandir.debug:id/cvMain']/android.view.ViewGroup/android.widget.TextView[@resource-id='com.mandir.debug:id/subtitle']")
	private WebElement firstCardUserCount;

	@FindBy(xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/social_list_vertical_cards_rv']/android.view.ViewGroup[1]//android.widget.FrameLayout[@resource-id='com.mandir.debug:id/cvMain']/android.view.ViewGroup/android.widget.TextView[@resource-id='com.mandir.debug:id/bt_add_community']")
	private WebElement firstCommunityAddBtn;

	@FindBy(xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/social_list_vertical_cards_rv']/android.view.ViewGroup[1]//android.widget.FrameLayout[@resource-id='com.mandir.debug:id/cvMain']/android.view.ViewGroup/android.widget.TextView[@resource-id='com.mandir.debug:id/tv_description']")
	private WebElement communityCardDescription;

	@FindBy(id="com.mandir.debug:id/design_bottom_sheet")
	private WebElement loginBottomSheet;

	@FindBy(id="com.mandir.debug:id/iv_close")
	private WebElement closeBtnFromLogInBottomSheet;
	
	@FindBy(xpath = "//android.widget.ImageView[@resource-id='com.mandir.debug:id/menu_left']")
	private WebElement menuIcon;

	private WebElement getApkaSamudayaText(String apkaSamudayText)
	{
		String apkaSamudayaTextxpathValue="//android.widget.TextView[@text='"+apkaSamudayText+"']";
		return elementUtil.getElement("xpath", apkaSamudayaTextxpathValue);
	}

	private WebElement getApkaSamudayCards(String apkaSamudayText)
	{
		String apkaSamudayCardsxpathValue="//android.widget.TextView[@text='"+apkaSamudayText+"']/following-sibling::androidx.recyclerview.widget.RecyclerView";
		return elementUtil.getElement("xpath", apkaSamudayCardsxpathValue);
	}

	private WebElement getProfileImageOfJoinedCommunity(String apkaSamudayText)
	{
		String profileImageOfJoinedCommunityxpathValue="//android.widget.TextView[@text='"+apkaSamudayText+"']/following-sibling::androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]//android.widget.FrameLayout[@resource-id='com.mandir.debug:id/cv_profile']";
		return elementUtil.getElement("xpath", profileImageOfJoinedCommunityxpathValue);
	}

	private WebElement getCommunityNameOfJoinedCommunity(String apkaSamudayText)
	{
		String communityNameOfJoinedCommunityxpathValue="//android.widget.TextView[@text='"+apkaSamudayText+"']/following-sibling::androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]//android.widget.TextView[@resource-id='com.mandir.debug:id/title']";
		return elementUtil.getElement("xpath", communityNameOfJoinedCommunityxpathValue);
	}

	private WebElement getAuthorImageOfJoinedCommunityDisplayed(String apkaSamudayText)
	{
		String authorImageOfJoinedCommunityxpathValue="//android.widget.TextView[@text='"+apkaSamudayText+"']/following-sibling::androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]//android.widget.FrameLayout[@resource-id='com.mandir.debug:id/cv_author']";
		return elementUtil.getElement("xpath", authorImageOfJoinedCommunityxpathValue);
	}

	private WebElement getRecentCommentDisplayed(String apkaSamudayText)
	{
		String recentCommentxpathValue="//android.widget.TextView[@text='"+apkaSamudayText+"']/following-sibling::androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]//android.widget.TextView[@resource-id='com.mandir.debug:id/subtitle']";
		return elementUtil.getElement("xpath", recentCommentxpathValue);
	}

	private WebElement getUpdatesDhekeTab(String apkaSamudayText)
	{
		String updatesDhekeTabxpathValue="//android.widget.TextView[@text='"+apkaSamudayText+"']/following-sibling::androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]//android.widget.TextView[@resource-id='com.mandir.debug:id/bt_see_update']";
		return elementUtil.getElement("xpath", updatesDhekeTabxpathValue);
	}

	private WebElement getAnyaSamudayCards(String anyaSamudayText)
	{
		String anyaSamudayCardsTabxpathValue="//android.widget.TextView[@text='"+anyaSamudayText+"']/following-sibling::androidx.recyclerview.widget.RecyclerView";
		return elementUtil.getElement("xpath", anyaSamudayCardsTabxpathValue);
	}

	private WebElement getAdhikSamudayDhekeTab(String adhikSamudayDhekeText)
	{
		String adhikSamudayDhekeTabxpathValue="//android.widget.TextView[@text='"+adhikSamudayDhekeText+"']";
		return elementUtil.getElement("xpath", adhikSamudayDhekeTabxpathValue);
	}
	
	private WebElement getFirstJoinedCommunityCard(String apkaSamudayText)
	{
		String firstJoinedCommunityCardxpathValue="//android.widget.TextView[@text='"+apkaSamudayText+"']/following-sibling::androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]//android.view.ViewGroup";
		return elementUtil.getElement("xpath", firstJoinedCommunityCardxpathValue);
	}
	
	@FindBy(xpath = "//android.widget.TextView[@text='आप का समुदाय']/following-sibling::androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup")
	private List<WebElement> countOfJoinedCommunitycard;

	@StepInfo(info="click on Community Card")
	public SocialLandingPage  clickOnCommunityCard() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("click on Community Card");
			awaitForElement(driver, communitycard);
			clickOnElement(communitycard);
			return this;
		}catch (Exception e) {
			throw new Exception("Error in  clickOnCommunityCard()"+e.getMessage());
		}
	}

	@StepInfo(info="Fetching Samudaya Page title")
	public String getSamudayaPageTitle() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("Fetching Samudaya Page title");
			awaitForElement(driver, samudayaPageTitle);
			return samudayaPageTitle.getText();
		}catch (Exception e){
			throw new Exception("Error in getSamudayaPageTitle"+e.getMessage());
		}
	}

	@StepInfo(info="checking if all community cards are displayed")
	public boolean isAllCommunityCardsDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if all community cards are displayed");
			awaitForElement(driver, allCommunityCards);
			return allCommunityCards.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isAllCommunityCardsDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if community card image displayed")
	public boolean isCommunityCardProfileImageDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if community card image displayed");
			awaitForElement(driver, firstCommunityCardImage);
			return firstCommunityCardImage.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isCommunityCardProfileImageDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if community card title is displayed")
	public boolean isCommunityCardTitleDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if community card title is displayed");
			awaitForElement(driver, communityCardTitle);
			return communityCardTitle.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isCommunityCardTitleDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if user count displayed in community card")
	public boolean isUserCountDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if user count displayed in community card");
			awaitForElement(driver, firstCardUserCount);
			return firstCardUserCount.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isUserCountDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if add button displayed in the  first community card")
	public boolean isAddButtonDisplayedInFirstCommunityCard() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if add button displayed");
			awaitForElement(driver, firstCommunityAddBtn);
			return firstCommunityAddBtn.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isAddButtonDisplayedInFirstCommunityCard()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if community card desription displayed")
	public boolean isCommunityCardDescriptionDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if community card desription displayed");
			awaitForElement(driver, communityCardDescription);
			return communityCardDescription.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isCommunityCardDescriptionDisplayed() "+e.getMessage());
		}
	}

	@StepInfo(info="clicking on add button from firstCommunityCardBtn")
	public void clickOnAddBtnFromFirstCommunityCard() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on add button from firstCommunityCardBtn");
			awaitForElement(driver, firstCommunityAddBtn);
			clickOnElement(firstCommunityAddBtn);
		}catch (Exception e){
			throw new Exception("Error in clickOnAddBtnFromFirstCommunityCard()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if login bottom sheet displayed")
	public boolean isLoginBottomSheetDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if login bottom sheet displayed");
			awaitForElement(driver, loginBottomSheet);
			return loginBottomSheet.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isLoginBottomSheetDisplayed() "+e.getMessage());
		}
	}

	@StepInfo(info="clicking on close button from login bottom sheet")
	public void clickOnCloseBtnFromLogInBottomSheet() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on close button from login bottom sheet");
			awaitForElement(driver, closeBtnFromLogInBottomSheet);
			clickOnElement(closeBtnFromLogInBottomSheet);
		}catch (Exception e){
			throw new Exception("Error in clickOnCloseBtnFromLogInBottomSheet() "+e.getMessage());
		}
	}

	@StepInfo(info="clicking on first community card")
	public void clickOnFirstCommunityCard() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on first community card");
			awaitForElement(driver, communitycard);
			clickOnElement(communitycard);
		}catch (Exception e){
			throw new Exception("Error in clickOnFirstCommunityCard()  "+e.getMessage());
		}
	}

	@StepInfo(info="checking if apka samuday title displayed")
	public boolean isApkaSamudayaTextDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if apka samuday title displayed");
			awaitForElement(driver, getApkaSamudayaText(ExcelUtility.getExcelData("locators", "apkaSamudayText","value")));
			return getApkaSamudayaText(ExcelUtility.getExcelData("locators", "apkaSamudayText","value")).isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isApkaSamudayaTextDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if apka samuday cards displayed")
	public boolean isApkaSamudayaCardsDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if apka samuday cards displayed");
			awaitForElement(driver, getApkaSamudayCards(ExcelUtility.getExcelData("locators", "apkaSamudayText","value")));
			return getApkaSamudayCards(ExcelUtility.getExcelData("locators", "apkaSamudayText","value")).isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isApkaSamudayaCardsDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if profile image of joined community displayed")
	public boolean isProfileImageOfJoinedCommunityDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if profile image of joined community displayed");
			awaitForElement(driver, getProfileImageOfJoinedCommunity(ExcelUtility.getExcelData("locators", "apkaSamudayText","value")));
			return getProfileImageOfJoinedCommunity(ExcelUtility.getExcelData("locators", "apkaSamudayText","value")).isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isProfileImageOfJoinedCommunityDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if community name is displayed of joined community")
	public boolean isCommunityNameOfJoinedCommunityDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if community name is displayed of joined community");
			awaitForElement(driver, getCommunityNameOfJoinedCommunity(ExcelUtility.getExcelData("locators", "apkaSamudayText","value")));
			return getCommunityNameOfJoinedCommunity(ExcelUtility.getExcelData("locators", "apkaSamudayText","value")).isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isCommunityNameOfJoinedCommunityDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if authorImage Displayed Beside Message of joined community")
	public boolean isAuthorImageDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if authorImage Displayed Beside Message of joined community");
			awaitForElement(driver, getAuthorImageOfJoinedCommunityDisplayed(ExcelUtility.getExcelData("locators", "apkaSamudayText","value")));
			return getAuthorImageOfJoinedCommunityDisplayed(ExcelUtility.getExcelData("locators", "apkaSamudayText","value")).isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isAuthorImageDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if last message  Displayed in joined community card")
	public boolean isLastMessageDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if last message  Displayed in joined community card");
			awaitForElement(driver, getRecentCommentDisplayed(ExcelUtility.getExcelData("locators", "apkaSamudayText","value")));
			return getRecentCommentDisplayed(ExcelUtility.getExcelData("locators", "apkaSamudayText","value")).isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isLastMessageDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if updates Dheke tab is displayed in joined community card")
	public boolean isUpdatesDhekeTabDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if updates Dheke tab is displayed in joined community card");
			awaitForElement(driver, getUpdatesDhekeTab(ExcelUtility.getExcelData("locators", "apkaSamudayText","value")));
			return getUpdatesDhekeTab(ExcelUtility.getExcelData("locators", "apkaSamudayText","value")).isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isUpdatesDhekeTabDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if anya samuday cards displayed")
	public boolean isAnyaSamudayCardsDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if anya samuday cards displayed");
			awaitForElement(driver, getAnyaSamudayCards(ExcelUtility.getExcelData("locators", "anyaSamudayText","value")));
			return getAnyaSamudayCards(ExcelUtility.getExcelData("locators", "anyaSamudayText","value")).isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isAnyaSamudayCardsDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if adhik samuday tab displayed")
	public boolean isAdhikSamudayTabDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if adhik samuday tab displayed");
			awaitForElement(driver, getAdhikSamudayDhekeTab(ExcelUtility.getExcelData("locators", "adhikSamudayDhekeText","value")));
			return getAdhikSamudayDhekeTab(ExcelUtility.getExcelData("locators", "adhikSamudayDhekeText","value")).isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isAdhikSamudayTabDisplayed()"+e.getMessage());
		}
	}	
	
	@StepInfo(info="clicking on first joined community card")
	public void clickOnFirstJoinedCommunityCard() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on first joined community card");
		awaitForElement(driver, getFirstJoinedCommunityCard(ExcelUtility.getExcelData("locators", "apkaSamudayText","value")));
		clickOnElement( getFirstJoinedCommunityCard(ExcelUtility.getExcelData("locators", "apkaSamudayText","value")));
		}catch (Exception e) {
			throw new Exception("Error in clickOnFirstJoinedCommunityCard()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on hamburger menu from samuday")
	public void clickOnHamburgerMenuFromSamuday() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on hamburger menu from samuday");
		awaitForElement(driver, menuIcon);
		clickOnElement(menuIcon);
		}catch (Exception e) {
			throw new Exception("Error in clickOnHamburgerMenuFromSamuday()"+e.getMessage());
		}
	}
	
	@StepInfo(info="fetching count of joined community cards")
	public int getCountOfJoinedCommunityCard() {
		ListenerImplimentationclass.testLog.info("fetching count of joined community cards");
		ArrayList<WebElement> list=new ArrayList<>();
		for (WebElement joinedCommunityCard : countOfJoinedCommunitycard) {
			list.add(joinedCommunityCard);
		}
		return list.size();
	}
	
	@StepInfo(info="scrolling till end in social landing page")
	public void scrollingTillEndInSocialpage() throws Exception {
		ListenerImplimentationclass.testLog.info("scrolling till end in social landing page");
		waitOrPause(3);
		scrollTillEnd();	
	}
	
	@StepInfo(info="clicking on more samuday btn")
	public void clickOnMoreCommunitiesBtn() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on more samuday btns");
		awaitForElement(driver, getAdhikSamudayDhekeTab(ExcelUtility.getExcelData("locators", "adhikSamudayDhekeText","value")));
		clickOnElement(getAdhikSamudayDhekeTab(ExcelUtility.getExcelData("locators", "adhikSamudayDhekeText","value")));
		}catch (Exception e) {
			throw new Exception("Error in clickOnMoreCommunitiesBtn()"+e.getMessage());
		}
	}
}
