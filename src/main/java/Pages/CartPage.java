package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ExcelUtility;
import base.ListenerImplimentationclass;
import io.appium.java_client.AppiumDriver;

public class CartPage extends AppGenericLib{

	ElementUtil elementUtil;
	public CartPage(AppiumDriver driver) {
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}	
	
	@FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_info_slide_header']")
	private WebElement cartPageTitle;		

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_item_info']/android.view.ViewGroup[2]//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_offerings_title']")
	private WebElement itemBhajarangbaliIteminCartlist;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_item_info']/android.view.ViewGroup[2]//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_cart_remove']")
	private WebElement secondRemoveBtnInCartList;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_item_info']/android.view.ViewGroup[1]//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_cart_remove']")
	private WebElement firstRemoveBtnInCartList;


	@FindBy(xpath="//android.widget.FrameLayout[@resource-id='com.mandir.debug:id/card_root']/android.view.ViewGroup")
	private WebElement offeringItemsuggestion;

	@FindBy(xpath="//android.widget.FrameLayout[@resource-id='com.mandir.debug:id/card_root']/android.view.ViewGroup//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_product_name']")
	private WebElement offeringItemSuggestionName;

	@FindBy(xpath="//android.widget.FrameLayout[@resource-id='com.mandir.debug:id/card_root']/android.view.ViewGroup//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_product_desc']")
	private WebElement offeringItemSuggestionDescription;

	@FindBy(xpath="//android.widget.FrameLayout[@resource-id='com.mandir.debug:id/card_root']/android.view.ViewGroup//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_product_price']")
	private WebElement offeringItemSuggestionPrize;

	@FindBy(xpath="//android.widget.FrameLayout[@resource-id='com.mandir.debug:id/card_root']/android.view.ViewGroup//android.widget.FrameLayout[@resource-id='com.mandir.debug:id/card_product_image']")
	private WebElement offeringItemSuggestionImage;	

	@FindBy(xpath="//android.widget.FrameLayout[@resource-id='com.mandir.debug:id/card_root']/android.view.ViewGroup//android.widget.Button[@resource-id='com.mandir.debug:id/btn_add_to_cart']")
	private WebElement offeringItemSuggestionChuneBtn;

	@FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_cart_value']")
	private WebElement cartValue;

	@FindBy(xpath="//android.widget.FrameLayout[@resource-id='com.mandir.debug:id/frame_cart_proceed_button']//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_go_ahead']")
	private WebElement bhugadaankareBtn;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_item_info']/android.view.ViewGroup[1]//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_offerings_title']")
	private WebElement firstItemNameInAddeditems;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_item_info']/android.view.ViewGroup[2]//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_offerings_title']")
	private WebElement secondItemNameInAddeditems;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_item_info']/android.view.ViewGroup[1]//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_price']")
	private WebElement firstItemPrizeInAddeditems;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_item_info']/android.view.ViewGroup[2]//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_price']")
	private WebElement secondItemPrizeInAddeditems;

	@FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_bill_info']")
	private WebElement billInfo;

	@FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_commodity_amount']")
	private WebElement vastuRashiTextInBillSummary;

	@FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_commodity_price']")
	private WebElement vastuRashiAmountInBillSummary;

	@FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_convenience_amount']")
	private WebElement sriMandirSuvidashlokTextInBillSummary;

	@FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_convenience_price']")
	private WebElement sriMandirSuvidashlokPrizeInBillSummary;

	@FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_total_amount']")
	private WebElement totalAmountText;

	@FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_total_price']")
	private WebElement totalAmountPrize;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_faq']")
	private WebElement faqSection;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_faq']//android.view.ViewGroup[@resource-id='com.mandir.debug:id/constraint_faq'][1]//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_expand']")
	private WebElement downchevaronOfFaqSection;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_faq']//android.view.ViewGroup[@resource-id='com.mandir.debug:id/constraint_faq'][1]//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_faq_answer']")
	private WebElement answerOfFaqSection;

	@FindBy(xpath="//android.widget.FrameLayout[@resource-id='com.mandir.debug:id/card_root']//android.view.ViewGroup//android.widget.Button[@resource-id='com.mandir.debug:id/btn_add_to_cart']")
	private WebElement chuneBtnfromItemSuggestion;

	@FindBy(xpath="//android.widget.FrameLayout[@resource-id='com.mandir.debug:id/card_root']//android.view.ViewGroup//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_product_name']")
	private WebElement itemNameFromItemSuggestion;

	@FindBy(xpath="//android.widget.FrameLayout[@resource-id='com.mandir.debug:id/frame_cart_proceed_button']//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_cart_value']")
	private WebElement totalCartValue;

	private WebElement getcartPageTitle(String cartPageTitle)
	{
		String cartPageTitlexpathValue="//android.widget.TextView[@text='"+cartPageTitle+"']";
		return elementUtil.getElement("xpath", cartPageTitlexpathValue);
	}

	private WebElement getBackBtnfromCartPage(String cartPageHeaderText)
	{
		String backBtnfromcartpagexpathValue="//android.view.ViewGroup[@resource-id='com.mandir.debug:id/toolbar']//android.widget.TextView[@text='"+cartPageHeaderText+"']/preceding-sibling::android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_back']";
		return elementUtil.getElement("xpath", backBtnfromcartpagexpathValue);
	}

	@StepInfo(info="getting cart page title")
	public String getCartPageTitle() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("getting cart page title");
			awaitForElement(driver,getcartPageTitle(ExcelUtility.getExcelData("locators", "cartPageTitle", "value")));
			return getcartPageTitle(ExcelUtility.getExcelData("locators", "cartPageTitle", "value")).getText();
		}catch (Exception e){
			throw new Exception("Error in getCartPageTitle()"+e.getMessage());
		}
	}

	@StepInfo(info="checking offering item suggestion is displayed")
	public boolean isOfferingItemSuggestionDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking offering item suggestion is displayed");
			awaitForElement(driver,offeringItemsuggestion);
			return offeringItemsuggestion.isDisplayed();
		}catch(Exception e)
		{
			throw new Exception("Error in getting isOfferingItemSuggestionDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="scrolling through the cart screen ")
	public void scrollingThroughCart() 
	{
		ListenerImplimentationclass.testLog.info("scrolling through the cart screen");
		swipeByCoordinates(driver, 480,1418 ,500 ,466 );
		swipeByCoordinates(driver, 500,466 ,480 ,1418 );

	}	

	@StepInfo(info="slide over the offering item suggestion")
	public void slideOverOfferingItem()
	{
		ListenerImplimentationclass.testLog.info("slide over the offering item suggestion");
		awaitForElement(driver, offeringItemsuggestion);
		swipeByCoordinates(driver, 84,932 ,1016 ,925 );
		swipeByCoordinates(driver, 1016,925 ,84 ,932 );

	}

	@StepInfo(info="clicking on removeBtn In CartList  ")
	public CartPage clickOnSecondRemoveBtnInCartList() throws Exception
	{			
		try {
			ListenerImplimentationclass.testLog.info("clicking on removeBtn In CartList");
			awaitForElement(driver, secondRemoveBtnInCartList);
			clickOnElement(secondRemoveBtnInCartList);					
			return new CartPage(driver);
		}catch (Exception e){
			throw new Exception("Error in clickOnSecondRemoveBtnInCartList()"+e.getMessage());
		}				
	}	

	@StepInfo(info="clicking on removeBtn In CartList  ")
	public CartPage clickOnFirstRemoveBtnInCartList() throws Exception
	{			
		try {
			ListenerImplimentationclass.testLog.info("clicking on removeBtn In CartList");
			awaitForElement(driver, firstRemoveBtnInCartList);
			clickOnElement(firstRemoveBtnInCartList);					
			return new CartPage(driver);
		}catch (Exception e){
			throw new Exception("Error in clickOnFirstRemoveBtnInCartList()"+e.getMessage());
		}				
	}	

	@StepInfo(info="getting cart value title")
	public int getCartValue() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("getting cart value title");
			awaitForElement(driver,cartValue);
			return Integer.parseInt( cartValue.getText().split(" ")[0]);
		}catch (Exception e){
			throw new Exception("Error in getCartValue()"+e.getMessage());
		}
	}


	@StepInfo(info="checking if bhugadan kare btn is displayed")
	public boolean isBhugadaankareBtnDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking if bhugadan kare btn is displayed");
			awaitForElement(driver,bhugadaankareBtn);
			return bhugadaankareBtn.isDisplayed();
		}catch(Exception e)
		{
			throw new Exception("Error in getting isbhugadaankareBtnDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if remove btn of first item is displayed")
	public boolean isRemoveBtnOfFirstItemDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking if remove btn of first item is displayed");
			awaitForElement(driver,firstRemoveBtnInCartList);
			return firstRemoveBtnInCartList.isDisplayed();
		}catch(Exception e)
		{
			throw new Exception("Error in getting isRemoveBtnInFirstItemDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if remove btn of second item is displayed")
	public boolean isRemoveBtnOfSecondItemDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking if remove btn of first item is displayed");
			awaitForElement(driver,secondRemoveBtnInCartList);
			return secondRemoveBtnInCartList.isDisplayed();
		}catch(Exception e)
		{
			throw new Exception("Error in getting isRemoveBtnOfSecondItemDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if first item name in added item is displayed")
	public boolean isFirstItemNameOfAddeditemsDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking if first item name of added item is displayed");
			awaitForElement(driver,firstItemNameInAddeditems);
			return firstItemNameInAddeditems.isDisplayed();
		}catch(Exception e)
		{
			throw new Exception("Error in getting isFirstItemNameOfAddeditemsDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if second item name of added item is displayed")
	public boolean isSecondItemNameOfAddeditemsDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking if second item name of added item is displayed");
			awaitForElement(driver,secondItemNameInAddeditems);
			return secondItemNameInAddeditems.isDisplayed();
		}catch(Exception e)
		{
			throw new Exception("Error in getting isSecondItemNameOfAddeditemsDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking first item prize of added items is displayed")
	public boolean isFirstItemPrizeOfAddeditemsDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking first item prize of added items is displayed");
			awaitForElement(driver,firstItemPrizeInAddeditems);
			return firstItemPrizeInAddeditems.isDisplayed();
		}catch(Exception e)
		{
			throw new Exception("Error in getting isFirstItemPrizeOfAddeditemsDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking second item prize of added items is displayed")
	public boolean isSecondItemPrizeOfAddeditemsDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking second item prize of added items is displayed");
			awaitForElement(driver,secondItemPrizeInAddeditems);
			return secondItemPrizeInAddeditems.isDisplayed();
		}catch(Exception e)
		{
			throw new Exception("Error in getting isSecondtemPrizeOfAddeditemsDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="fetching Item Bhajarangbali text")
	public String getItemBhajarangbaliTextInCart() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("fetching Item Bhajarangbali text");
			awaitForElement(driver, secondItemNameInAddeditems);
			return secondItemNameInAddeditems.getText();
		}catch (Exception e)
		{
			throw new Exception("Error in  getItemBhajarangbaliText()"+e.getMessage());
		}
	}

	@StepInfo(info="checking offering item suggestion is displayed")
	public boolean isOfferingItemSuggestionNameDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking offering item suggestion is displayed");
			awaitForElement(driver,offeringItemSuggestionName);
			return offeringItemSuggestionName.isDisplayed();
		}catch(Exception e)
		{
			throw new Exception("Error in getting isOfferingItemSuggestionNameDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking offering item desciption is displayed")
	public boolean isOfferingItemSuggestionDescriptionDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking offering item suggestion is displayed");
			awaitForElement(driver,offeringItemSuggestionDescription);
			return offeringItemSuggestionDescription.isDisplayed();
		}catch(Exception e)
		{
			throw new Exception("Error in getting isofferingItemSuggestionDescriptionDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking offering item prize is displayed")
	public boolean isOfferingItemSuggestionPrizeDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking offering item prize is displayed");
			awaitForElement(driver,offeringItemSuggestionPrize);
			return offeringItemSuggestionPrize.isDisplayed();
		}catch(Exception e)
		{
			throw new Exception("Error in getting isOfferingItemSuggestionPrizeDisplayed()"+e.getMessage());
		}
	}


	@StepInfo(info="checking offering suggestion Image is displayed")
	public boolean isOfferingItemSuggestionImageDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking offering suggestion Image is displayed");
			awaitForElement(driver,offeringItemSuggestionImage);
			return offeringItemSuggestionImage.isDisplayed();
		}catch(Exception e)
		{
			throw new Exception("Error in getting isOfferingItemSuggestionImageDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking offering suggestion chune btn is displayed")
	public boolean isOfferingItemSuggestionChuneBtnDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking offering suggestion chune btn is displayed");
			awaitForElement(driver,offeringItemSuggestionChuneBtn);
			return offeringItemSuggestionChuneBtn.isDisplayed();
		}catch(Exception e)
		{
			throw new Exception("Error in getting isOfferingItemSuggestionChuneBtnDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking bill summary is displayed")
	public boolean isBillSummaryDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking bill summary is displayed");
			awaitForElement(driver,billInfo);
			return billInfo.isDisplayed();
		}catch(Exception e)
		{
			throw new Exception("Error in getting isBillSummaryDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="scrolling till FAQ")
	public void swipeupTillFAQ() 
	{
		ListenerImplimentationclass.testLog.info("scrolling till FAQ");
		swipeByCoordinates(driver, 508,1511 ,471 ,431 );
	}	

	@StepInfo(info="scrolling  up ")
	public void scrollToBeginning() 
	{
		ListenerImplimentationclass.testLog.info("scrolling up");
		swipeByCoordinates(driver, 444,569 ,471 ,1316 );
	}	

	@StepInfo(info="checking vastu rashi text in bill summary is displayed")
	public boolean isVastuRashiTextInBillSummaryDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking vastu rashi text in bill summary is displayed");
			awaitForElement(driver,vastuRashiTextInBillSummary);
			return vastuRashiTextInBillSummary.isDisplayed();
		}catch(Exception e)
		{
			throw new Exception("Error in getting isVastuRashiTextInBillSummaryDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking vastu rashi amount in bill summary is displayed")
	public boolean isVastuRashiAmountInBillSummaryDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking vastu rashi amount in bill summary is displayed");
			awaitForElement(driver,vastuRashiAmountInBillSummary);
			return vastuRashiAmountInBillSummary.isDisplayed();
		}catch(Exception e)
		{
			throw new Exception("Error in getting isVastuRashiAmountInBillSummaryDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking SriMandirSuvidashlok Text in bill summary is displayed")
	public boolean isSriMandirSuvidashlokTextInBillSummaryDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking SriMandirSuvidashlok Text in bill summary is displayed");
			awaitForElement(driver,sriMandirSuvidashlokTextInBillSummary);
			return sriMandirSuvidashlokTextInBillSummary.isDisplayed();
		}catch(Exception e)
		{
			throw new Exception("Error in getting isSriMandirSuvidashlokTextInBillSummaryDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking SriMandirSuvidashlok prize in bill summary is displayed")
	public boolean isSriMandirSuvidashlokPrizeInBillSummaryDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking SriMandirSuvidashlok prize in bill summary is displayed");
			awaitForElement(driver,sriMandirSuvidashlokTextInBillSummary);
			return sriMandirSuvidashlokTextInBillSummary.isDisplayed();
		}catch(Exception e)
		{
			throw new Exception("Error in getting isSriMandirSuvidashlokPrizeInBillSummaryDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking Total amount in bill summary is displayed")
	public boolean isTotalAmountTextDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking total amount in bill summary is displayed");
			awaitForElement(driver,totalAmountText);
			return totalAmountText.isDisplayed();
		}catch(Exception e)
		{
			throw new Exception("Error in getting isTotalAmountTextDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking Total amount prize in bill summary is displayed")
	public boolean isTotalAmountPrizeDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking total amount  prize in bill summary is displayed");
			awaitForElement(driver,totalAmountText);
			return totalAmountText.isDisplayed();
		}catch(Exception e)
		{
			throw new Exception("Error in getting isTotalAmountPrizeDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking FAQ's in cart section is displayed")
	public boolean isFaqSectionDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking FAQ's in cart section is displayed");
			awaitForElement(driver,faqSection);
			return faqSection.isDisplayed();
		}catch(Exception e)
		{
			throw new Exception("Error in getting isFaqSectionDisplayed()"+e.getMessage());
		}		
	}

	@StepInfo(info="clicking on down chevaron of FAQ's in cart section")
	public CartPage clickOnDownChevaron() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("clicking on down chevaron of FAQ's in cart section");
			awaitForElement(driver,downchevaronOfFaqSection);
			clickOnElement(downchevaronOfFaqSection);
			return this;
		}catch(Exception e)
		{
			throw new Exception("Error in getting clickOnDownChevaron()"+e.getMessage());
		}
	}

	@StepInfo(info="checking answer ofFAQ's in cart section is displayed")
	public boolean isAnswerOfFaqSectionDisplayed() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("checking answer ofFAQ's in cart section is displayed");
			awaitForElement(driver,answerOfFaqSection);
			return answerOfFaqSection.isDisplayed();
		}catch(Exception e)
		{
			throw new Exception("Error in getting isAnswerOfFaqSectionDisplayed()"+e.getMessage());
		}		
	}

	@StepInfo(info="clicking on chune btn from item suggestion")
	public CartPage clickOnChuneBtnFromSuggestion() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("clicking on chune btn from item suggestion");
			awaitForElement(driver,chuneBtnfromItemSuggestion);
			clickOnElement(chuneBtnfromItemSuggestion);
			return this;
		}catch(Exception e)
		{
			throw new Exception("Error in getting clickOnChuneBtnFromSuggestion()"+e.getMessage());
		}
	}

	@StepInfo(info="fetching Item name from item suggestion text")
	public String getItemNameFromItemsuggestion() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("fetching Item name from item suggestion text");
			awaitForElement(driver, itemNameFromItemSuggestion);
			return itemNameFromItemSuggestion.getText();
		}catch (Exception e)
		{
			throw new Exception("Error in  getItemNameFromItemsuggestion()"+e.getMessage());
		}
	}

	@StepInfo(info="fetching Item name of suggestion after adding")
	public String getItemSuggestionTextInCart() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("fetching Item name of suggestion after adding");
			awaitForElement(driver, secondItemNameInAddeditems);
			return secondItemNameInAddeditems.getText();
		}catch (Exception e)
		{
			throw new Exception("Error in  getItemSuggestionTextInCart()"+e.getMessage());
		}
	}

	@StepInfo(info="fetching first Item prize from cart list")
	public int getFirstItemPrizeFromCartList() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("fetching Item prize from cart list");
			awaitForElement(driver, firstItemPrizeInAddeditems);
			return Integer.parseInt( firstItemPrizeInAddeditems.getText().split(" ")[1].split(".0")[0]);
		}catch (Exception e)
		{
			throw new Exception("Error in  getFirstItemPrizeFromCartList()"+e.getMessage());
		}
	}

	@StepInfo(info="fetching second Item prize from cart list")
	public int getSecondItemPrizeFromCartList() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("fetching second Item prize from cart list");
			awaitForElement(driver, secondItemPrizeInAddeditems);
			return Integer.parseInt( secondItemPrizeInAddeditems.getText().split(" ")[1].split(".0")[0]);
		}catch (Exception e)
		{
			throw new Exception("Error in  getSecondItemPrizeFromCartList()"+e.getMessage());
		}
	}

	@StepInfo(info="fetching total cart value from cart list")
	public int getTotalCartValueFromCartList() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("fetching total cart value from cart list");
			awaitForElement(driver, totalCartValue);
			return Integer.parseInt( totalCartValue.getText().split(" ")[4].split("/")[0]);

		}catch (Exception e)
		{
			throw new Exception("Error in  getTotalCartValueFromCartList()"+e.getMessage());
		}
	}

	@StepInfo(info="getting item prize after removing another item ")
	public String getItemPrizeAfterRemovingItem() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("getting item prize after removing another item");
			awaitForElement(driver,firstItemPrizeInAddeditems);
			return firstItemPrizeInAddeditems.getText().split(".0")[0];
		}catch(Exception e)
		{
			throw new Exception("Error in getting getItemPrizeAfterRemovingItem()"+e.getMessage());
		}
	}

	@StepInfo(info="getting item prize after removing item in bill summary")
	public String getItemPrizeInBillSummary() throws Exception 
	{
		try {
			ListenerImplimentationclass.testLog.info("getting item prize after removing item in bill summary");
			awaitForElement(driver,vastuRashiAmountInBillSummary);
			return vastuRashiAmountInBillSummary.getText();
		}catch(Exception e)
		{
			throw new Exception("Error in getting getItemPrizeInBillSummary()"+e.getMessage());
		}
	}

	@StepInfo(info="scrolling through the cart screen ")
	public void swipingThroughFirstCart() 
	{
		ListenerImplimentationclass.testLog.info("scrolling through the cart screen");
		waitOrPause(5);
		swipeByCoordinates(driver, 84,932 ,1016 ,925 );
	}		

	@StepInfo(info="clicking on back btn fron cart page  ")
	public CartPage clickOnBackBtnFromCartpage() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("clicking on back btn fron cart page ");
			awaitForElement(driver,getBackBtnfromCartPage(ExcelUtility.getExcelData("locators", "cartPageHeaderText", "value")));
			clickOnElement(getBackBtnfromCartPage(ExcelUtility.getExcelData("locators", "cartPageHeaderText", "value")));
		}catch (Exception e){
			throw new Exception("Error in clickOnBackBtnFromCartpage()"+e.getMessage());
		}
		return this;
	}

	@StepInfo(info="clicking on Bhugadaan kare btn ")
	public CartPage clickOnBhugadaankareBtn() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("clicking on Bhugadaan kare btn");
			awaitForElement(driver,bhugadaankareBtn);
			clickOnElement(bhugadaankareBtn);
			waitOrPause(4);
		}catch (Exception e){
			throw new Exception("Error in clickOnBhugadaankareBtn()"+e.getMessage());
		}
		return this;
	}
	
}



