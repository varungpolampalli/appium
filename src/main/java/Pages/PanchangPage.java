package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ExcelUtility;
import base.ListenerImplimentationclass;
import base.TestData;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.MultiTouchAction;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class PanchangPage extends AppGenericLib {
	ElementUtil elementUtil;

	public PanchangPage(AppiumDriver driver) {
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id="com.mandir.debug:id/radio_daily")
	private WebElement dailyTab;

	@FindBy(id="com.mandir.debug:id/radio_monthly")
	private WebElement monthlyTab;

	@FindBy(id="com.mandir.debug:id/radio_tomorrow")
	private WebElement tomorrowTab;

	@FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_dayInfo_title']/following-sibling::android.widget.FrameLayout//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_day']")
	private WebElement panchagCardTitle;

	@FindBy(id="com.mandir.debug:id/iv_help_text")
	private WebElement helpBtn;

	@FindBy(id="com.mandir.debug:id/tv_desc")
	private WebElement helpBannerDescription;

	@FindBy(id="com.mandir.debug:id/btn_okay")
	private WebElement okBtn;

	@FindBy(id="com.mandir.debug:id/tv_date_selector")
	private WebElement dateSelectionTab;

	@FindBy(id="com.mandir.debug:id/iv_date_right")
	private WebElement dateForwardArrowBtn;

	@FindBy(id="com.mandir.debug:id/iv_date_left")
	private WebElement dateBackwardArrowbtn;

	@FindBy(id="com.mandir.debug:id/cta")
	private WebElement shareIcon;

	@FindBy(id="com.mandir.debug:id/design_bottom_sheet")
	private WebElement messageInSharingBottomSheet;

	@FindBy(xpath="//android.widget.TextView[@text='13']")
	private WebElement dateValue;

	@FindBy(id="com.mandir.debug:id/tv_header_title")
	private WebElement montlyPanchangText;

	@FindBy(id="com.mandir.debug:id/btn_okay")
	private WebElement monthlypanchangOkBtn;

	@FindBy(id="com.mandir.debug:id/rv_festivals")
	private WebElement festivalDetailsCard;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_calendar']")
	private WebElement calendarCard;
	
	@FindBy(id="com.mandir.debug:id/constraint_root")
	private WebElement moonCard;
	
	@FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_month_selector']")
	private WebElement monthSelectionTab;
	
	@FindBy(id="com.mandir.debug:id/iv_month_left")
	private WebElement monthBackwardBtn;
	
	@FindBy(id="com.mandir.debug:id/iv_month_right")
	private WebElement monthForwardBtn;
	
	@FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_goodTimes_title']/parent::android.view.ViewGroup")
	private WebElement auspiciousUnauspiciousCard;
	
	@FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_wholeCalendar_title']/following-sibling::android.widget.FrameLayout[1]")
	private WebElement sampoornaPanchangCard;
	
	@FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_recommendation_title']/following-sibling::android.widget.ImageView")
	private WebElement horaAurChowgaidyaHelpBtn;
	
	@FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_wholeCalendar_title']/following-sibling::android.widget.ImageView")
	private WebElement sampoornaPanchangHelpBtn;
	
	@FindBy(id="com.mandir.debug:id/tv_title")
	private WebElement sampoornaPanchangHelpBaneerTitle;
	
	@FindBy(id="com.mandir.debug:id/tv_title")
	private WebElement horaAurChowgadiyaHelpBannerTitle;
	
	@FindBy(id="com.mandir.debug:id/btn_okay")
	private WebElement sampoornaPanchangHelpSheetOkBtn;
	
	@FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_title_toolbar']")
	private WebElement dateHighlightBanner;
	
	@FindBy(id="com.mandir.debug:id/radio_today")
	private WebElement aajBtn;

	@FindBy(id="com.mandir.debug:id/radio_tomorrow")
	private WebElement kalBtn;
	
	@FindBy(id="com.mandir.debug:id/datePicker")
	private WebElement datePickerBottomSheet;
	
	@FindBy(id="com.mandir.debug:id/menu_left")
	private WebElement menuBtn;
	
	@FindBy(id="com.mandir.debug:id/tv_location_selector")
	private WebElement locationTab;
	
	@FindBy(id="com.mandir.debug:id/search_src_text")
	private WebElement locationSearchTextFeild;
	
	@FindBy(id="com.mandir.debug:id/iv_cancel")
	private WebElement cancelCalendarBtn;
	
	@FindBy(xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_container_birth_place']/android.widget.LinearLayout[1]//android.widget.TextView")
	private WebElement locationNameFromLocationSearchPage;
	
	@FindBy(id="com.mandir.debug:id/back_button_birth_place")
	private WebElement backBtnFromLocationSearchPage;
	
	private WebElement getAajKaShubhChadavTab(String aajKaShubhChadavText) {
		String aajKaShubhChadavTextXpathValue="//android.widget.TextView[@text='"+aajKaShubhChadavText+"']";
		return elementUtil.getElement("xpath", aajKaShubhChadavTextXpathValue);
	}
	
	private WebElement getDinKaShubhMantrSuneTab(String dinKaShubhMantraText) {
		String dinKaShubhMantraSuneTextXpathValue="//android.widget.TextView[@text='"+dinKaShubhMantraText+"']";
		return elementUtil.getElement("xpath", dinKaShubhMantraSuneTextXpathValue);
	}
	
	private WebElement getNishulkKundliTab(String nishulkKundliText) {
		String nishulkKundliTextXpathValue="//android.widget.TextView[@text='"+nishulkKundliText+"']";
		return elementUtil.getElement("xpath", nishulkKundliTextXpathValue);
	}
	
	@StepInfo(info="Fetching Montly Tab Text From Panchang page")
	public String getMontlyTabText() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("Fetching Montly Tab Text From Panchang page");
			awaitForElement(driver, monthlyTab);
			return monthlyTab.getText();
		}catch (Exception e)
		{
			throw new Exception("Error in getMontlyTabText()"+e.getMessage());
		}
	}

	@StepInfo(info="Fetching Daily Tab Text From Panchang page")
	public String getDailyTabText() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("Fetching Daily Tab Text From Panchang page");
			awaitForElement(driver, dailyTab);
			return dailyTab.getText();
		}catch (Exception e)
		{
			throw new Exception("Error in getDailyTabText()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking on Kal Tab")
	public void clickOnTomorrowTab() throws Exception {
		try {
			awaitForElement(driver, tomorrowTab);
			ListenerImplimentationclass.testLog.info("clicking on Kal Tab");
			clickOnElement(tomorrowTab);
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnTomorrowTab()"+e.getMessage());
		}

	}

	@StepInfo(info="checking if Panchang text is displayed")
	public boolean isPanchangCardTitleDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if panchang text is displayed");
			awaitForElement(driver, panchagCardTitle);
			return panchagCardTitle.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in isPanchangCardTitleDisplayed()"+e.getMessage());
		}

	}

	@StepInfo(info="clicking on help Button")
	public PanchangPage clickOnHelpBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on help button");
			awaitForElement(driver, helpBtn);
			clickOnElement(helpBtn);
			return this;
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnHelpBtn()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if help banner is displayed on clicking ? icon")
	public boolean isHelpBannerDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if help banner is displayed on clicking ? icon");
			awaitForElement(driver, helpBannerDescription);
			return helpBannerDescription.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in isHelpBannerDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking on OK button from the help banner")
	public PanchangPage clickOnOkBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on OK button from the help banner");
			clickOnElement(okBtn);
			return this;
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnOkBtn()"+e.getMessage());
		}

	}

	@StepInfo(info="clicking on DateSelection Tab")
	public PanchangPage clickOnDateSelectionTab() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on DateSelection Tab");
			awaitForElement(driver, dateSelectionTab);
			clickOnElement(dateSelectionTab);
			return this;
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnDateSelectionTab()"+e.getMessage());
		}

	}

	@StepInfo(info="clicking on Date Forward Btn")
	public PanchangPage clickOnDateForwardBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on date forward button");
			awaitForElement(driver, dateBackwardArrowbtn);
			clickOnElement(dateForwardArrowBtn);
			return this;
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnDateForwardBtn()"+e.getMessage());
		}

	}

	@StepInfo(info="clicking on Date Backward Btn")
	public PanchangPage clickOnDateBackwardBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on date backward button");
			awaitForElement(driver, dateBackwardArrowbtn);
			clickOnElement(dateBackwardArrowbtn);
			return this;
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnDateBackwardBtn()"+e.getMessage());
		}

	}

	@StepInfo(info="clicking on whatsapp share icon")
	public PanchangPage clickOnShareIcon() throws Exception {
		try {
			awaitForElement(driver, shareIcon);
			ListenerImplimentationclass.testLog.info("clicking on whatsAppshare icon");
			clickOnElement(shareIcon);
			return this;
		}catch (Exception e){
			throw new Exception("Error in  clickOnShareIcon()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if share bottom sheet is displayed")
	public boolean isMessageDisplayedInShareBottomSheet() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if share bottom sheet is displayed");
			return messageInSharingBottomSheet.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in  isMessageDisplayedInShareBottomSheet()"+e.getMessage());
		}
	}

	@StepInfo(info="scrolling till end of the page")
	public void scrollTillEndOfThePage(int count) throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("scrolling till end of the page");
			Verticalscroll(driver, 535, 1164, 535, 454, count);
		}catch (Exception e){
			throw new Exception("Error in  scrollTillEndOfThePage()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking On Montly Tab")
	public PanchangPage clickOnMontlyTab() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on montly Tab from panchang Page");
			awaitForElement(driver, monthlyTab);
			clickOnElement(monthlyTab);
			return this;
		}catch (Exception e){
			throw new Exception("Error in clickOnMontlyTab()"+e.getMessage());
		}

	}

	@StepInfo(info="clicking on date value")
	public PanchangPage clickOnDateValue() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on date value");
			awaitForElement(driver, dateValue);
			clickOnElement(dateValue);
			return this;
		}catch (Exception e){
			throw new Exception("Error in clickOnDateValue()"+e.getMessage());
		}
	}

	@StepInfo(info="fetching monthly panchang text")
	public String getMontlypanchangText() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("fetching monthly panchang text");
			awaitForElement(driver, montlyPanchangText);
			return montlyPanchangText.getText();
		}catch (Exception e){
			throw new Exception("Error in getMontlypanchangText()"+e.getMessage());
		}

	}

	@StepInfo(info="clicking on OK button from the montly panchang page")
	public PanchangPage clickOnMonthlyPanchangOkBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on OK button from bottom sheet of monthly panchang screen");
			clickOnElement(okBtn);
			return this;
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnMonthlyPanchangOkBtn()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if festival details is displayed in monthly panchang Page")
	public boolean isFestivalCardDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if festival details is displayed in monthly panchang Page");
			awaitForElement(driver, festivalDetailsCard);
			return festivalDetailsCard.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in isFestivalCardDisplayed()"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if calendar view displayed in  monthly panchang Page")
	public boolean isCalenadarViewDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if calendar view displayed in  monthly panchang Page");
		awaitForElement(driver, calendarCard);
		return calendarCard.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in isCalenadarViewDisplayed()"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if moon card is displayed in monthly panchang page")
	public boolean isMoonCardDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if moon card is displayed in  monthly panchang Page");
		awaitForElement(driver, moonCard);
		return moonCard.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in isMoonCardDisplayed()"+e.getMessage());
		}
	}
	
	@StepInfo(info="fetching month and year from month selection tab")
	public String[] getMonthSelectionValueText() throws Exception {
		try {
		awaitForElement(driver, monthSelectionTab);
		return monthSelectionTab.getText().split(" ");
		}catch (Exception e)
		{
			throw new Exception("Error in getMonthSelectionValueText()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on month forward Button")
	public void clickOnMonthForwardBtn() throws Exception {
		try {
		awaitForElement(driver, monthForwardBtn);
		clickOnElement(monthForwardBtn);
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnMonthForwardBtn()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on month backward Button")
	public void clickOnMonthBackwardBtn() throws Exception {
		try {
		awaitForElement(driver, monthBackwardBtn);
		clickOnElement(monthBackwardBtn);
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnMonthBackwardBtn())"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if auspicious/UnAuspicious card is displayed in panchang page")
	public boolean isAuspiciousUnAuspiciousCardDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if auspicious/UnAuspicious card is displayed in panchang page");
		awaitForElement(driver, auspiciousUnauspiciousCard);
		return auspiciousUnauspiciousCard.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in isAuspiciousUnAuspiciousCardDisplayed()"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if Sampoorna Panchang Card is displayed")
	public boolean isSampoornaPanchangCardDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if Sampoorna Panchang Card is displayed");
		awaitForElement(driver, sampoornaPanchangCard);
		swipeByCoordinates(driver, 488, 1304, 488, 698);
		return sampoornaPanchangCard.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in isSampoornaPanchangCardDisplayed()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on sampoorna Panchang Help Button")
	public String clickOnSampoornaPanchangHelpBtn() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on Sampoorna Panchang Help Button");
		awaitForElement(driver, sampoornaPanchangHelpBtn);
		clickOnElement(sampoornaPanchangHelpBtn);
		ListenerImplimentationclass.testLog.info("fetching sampoorna Panchang Help Banner Title");
		return sampoornaPanchangHelpBaneerTitle.getText();
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnSampoornaPanchangHelpBtn()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on Hora Aur chowgadiya Help Button")
	public String clickOnHoraAurChowgadiyaHelpBtn() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on HoraAur chowgadiya Help Button");
		awaitForElement(driver, horaAurChowgaidyaHelpBtn);
		clickOnElement(horaAurChowgaidyaHelpBtn);
		ListenerImplimentationclass.testLog.info("fetching HoraAurChowgadiyaKyaHai Help Banner Title");
		return horaAurChowgadiyaHelpBannerTitle.getText();
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnHoraAurChowgadiyaHelpBtn()"+e.getMessage());
		}
		
	}
	
	@StepInfo(info="Fetching date highlight banner Title from Panchang page")
	public String getDateHighLightBanner() throws Exception {
		try {
		awaitForElement(driver, dateHighlightBanner);
		ListenerImplimentationclass.testLog.info("Fetching date highlight banner title from Panchang page");
		return dateHighlightBanner.getText();
		}catch (Exception e)
		{
			throw new Exception("Error in getDateHighLightBanner()"+e.getMessage());
		}
	}
	
	@StepInfo(info="Fetching aajBtn Text")
	public String getAajBtnText() {
		awaitForElement(driver, aajBtn);
		ListenerImplimentationclass.testLog.info("Fetching Aaj Button Text from Panchang Page");
		return aajBtn.getText();
	}

	@StepInfo(info="Fetching kalBtn Text")
	public String getKalBtnText() {
		awaitForElement(driver, kalBtn);
		ListenerImplimentationclass.testLog.info("Fetching Kal Button Text from Panchang Page");
		return kalBtn.getText();
	}
	
	@StepInfo(info="scrolling till shubha ashubha samay")
	public void scrollTillShubhaAshubhaSamay() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("scrolling till shubha ashubha samay");
		Verticalscroll(driver, 498, 1302, 498, 754, 1);
		}catch (Exception e)
		{
			throw new Exception("Error in scrollTillShubhaAshubhaSamay()"+e.getMessage());
		}
	}
	
	@StepInfo(info="scrolling till festival card")
	public void scrollTillfestivalCards() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("scrolling till festival card");
		Verticalscroll(driver, 454, 1379, 454, 620, 1);
		}catch (Exception e)
		{
			throw new Exception("Error in scrollTillfestivalCards()"+e.getMessage());
		}
	}
	
	@StepInfo(info="scrolling to beginning of the page")
	public void scrollToBeginning(int count) throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("scrolling to beginning of the page");
		Verticalscroll(driver, 454, 620, 454, 1379,count);
		}catch (Exception e)
		{
			throw new Exception("Error in scrollToBeginning()"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if date picker bottom sheet displayed")
	public boolean isDatePickerBottomSheetDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if date picker bottom sheet displayed");
		return datePickerBottomSheet.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in isDatePickerBottomSheetDisplayed()"+e.getMessage());
		}
		
	}
	
	@StepInfo(info="clicking on Hamburger menu")
	public void clickOnHamburgerMenuBtn() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on Hamburger menu");
		clickOnElement(menuBtn);
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnHamburgerMenuBtn()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on location tab")
	public PanchangPage clickOnLocationTab() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on location tab");
		awaitForElement(driver, locationTab);
		clickOnElement(locationTab);
		return this;
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnLocationTab()"+e.getMessage());
		}
		
	}
	
	@StepInfo(info="Typing location to loaction text feild")
	public PanchangPage typeLoactionToLoactionTextFeild() throws Exception {
		try {
			awaitForElement(driver, locationSearchTextFeild);
			ListenerImplimentationclass.testLog.info("Typing Location to Location text feild");
			locationSearchTextFeild.sendKeys("Bangalore");
			return this;
		}catch (Exception e){
			throw new Exception("Error in typeLoactionToLoactionTextFeild()"+e.getMessage());
		}

	}
	
	@StepInfo(info="clicking on location Name from location search Page")
	public void clickOnLocationName() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on location Name from location search Page");
		awaitForElement(driver, locationNameFromLocationSearchPage);
		clickOnElement(locationNameFromLocationSearchPage);
		}catch (Exception e){
			throw new Exception("Error in  clickOnLocation()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on calendar close button")
	public void clickOnCalendarCancelBtn() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on calendar close button");
		awaitForElement(driver, cancelCalendarBtn);
		clickOnElement(cancelCalendarBtn);
		}catch (Exception e){
			throw new Exception("Error in  clickOnCalendarCancelBtn()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on aaj ka shubh chadav tab from panchang page")
	public void clickOnAajKaShubhChadavTab() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on aaj ka shubh chadav tab from panchang page");
		scrollToElement(driver, TestData.aajKaShubhaChadavText);
		awaitForElement(driver, getAajKaShubhChadavTab(ExcelUtility.getExcelData("locators", "aajKaShubhaChadavText", "value")));
		clickOnElement(getAajKaShubhChadavTab(ExcelUtility.getExcelData("locators", "aajKaShubhaChadavText", "value")));
		}catch (Exception e){
			throw new Exception("Error in  clickOnAajKaShubhChadavTab()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on dina ka shubh mantra tab")
	public void clickOnDinkaShubhMantraTab() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on dina ka shubh mantra tab");
		scrollToElement(driver, TestData.dinKaShubhMantraSuneText);
		awaitForElement(driver, getAajKaShubhChadavTab(ExcelUtility.getExcelData("locators", "dinKaShubhMantraText", "value")));
		clickOnElement(getDinKaShubhMantrSuneTab(ExcelUtility.getExcelData("locators", "dinKaShubhMantraText", "value")));
		}catch (Exception e){
			throw new Exception("Error in  clickOnAajKaDinkaShubhMantraTab()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on kundli tab from panchang page")
	public void clickOnKundliTab() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on kundli tab from panchang page");
		scrollToElement(driver,TestData.nishulkKundliText);
		awaitForElement(driver, getAajKaShubhChadavTab(ExcelUtility.getExcelData("locators", "kundliText", "value")));
		clickOnElement(getNishulkKundliTab(ExcelUtility.getExcelData("locators", "kundliText", "value")));
		}catch (Exception e){
			throw new Exception("Error in  clickOnKundliTab()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on back button from location search page")
	public void clickOnBackBtnFromLocationSearchPage() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on back button from location search page");
		awaitForElement(driver, backBtnFromLocationSearchPage);
		clickOnElement(backBtnFromLocationSearchPage);
		}catch (Exception e){
			throw new Exception("Error in  clickOnBackBtnFromLocationSearchPage()"+e.getMessage());
		}
	}
	
	@StepInfo(info="swiping the help banner bottom sheet")
	public void swipingTheHelpBottomSheet() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("swiping the help banner bottom sheet");
		waitOrPause(3);
		swipeByCoordinates(driver, 475,1544, 475, 1173);
		}catch (Exception e){
			throw new Exception("Error in  swipingTheHelpBottomSheet()"+e.getMessage());
		}
	}
}

