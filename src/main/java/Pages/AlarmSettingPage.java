package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ListenerImplimentationclass;
import io.appium.java_client.AppiumDriver;

public class AlarmSettingPage extends AppGenericLib {
	public ElementUtil elementUtil;
	public AlarmSettingPage(AppiumDriver driver) 
	{
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="com.mandir.debug:id/save_karein_ep")
	private WebElement alarmSaveBtn;
	
	@FindBy(id="com.mandir.debug:id/tv_deny")
	private WebElement alarmDenyBtn;
	
	@StepInfo(info="clicking on Alarm Save Button")
	public AlarmSettingPage clickOnAlarmSaveBtn() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on alarm save button");
		awaitForElement(driver, alarmSaveBtn);
		clickOnElement(alarmSaveBtn);
		}catch (Exception e){
			throw new Exception("Error in clickOnAlarmSaveBtn"+e.getMessage());
		}
		return this;
	}
	
	@StepInfo(info="clicking on Allow Button")
	public MyAlarmPage clickOnDenyBtn() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on Deny Button");
		awaitForElement(driver, alarmDenyBtn);
		clickOnElement(alarmDenyBtn);
		}catch (Exception e){
			logger.info("permission already given");
		}
		return new MyAlarmPage(driver);
	}
}
