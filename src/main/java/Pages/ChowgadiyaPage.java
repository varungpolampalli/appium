package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ListenerImplimentationclass;
import io.appium.java_client.AppiumDriver;

public class ChowgadiyaPage extends AppGenericLib{

	ElementUtil elementUtil;

	public ChowgadiyaPage(AppiumDriver driver) {
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id="com.mandir.debug:id/radio_today")
	private WebElement aajBtn;

	@FindBy(id="com.mandir.debug:id/radio_tomorrow")
	private WebElement kalBtn;

	@FindBy(id="com.mandir.debug:id/tv_hora_title")
	private WebElement dateAndMonthTitle;

	@FindBy(id="com.mandir.debug:id/tv_location_selector")
	private WebElement locationTab;

	@FindBy(id="com.mandir.debug:id/search_src_text")
	private WebElement searchTextFeild;

	@FindBy(id="com.mandir.debug:id/iv_help_text")
	private WebElement helpBtn;

	@FindBy(id="com.mandir.debug:id/tv_title")
	private WebElement chowgadiyaKyaHaiBannerTitle;

	@FindBy(id="com.mandir.debug:id/tv_title_toolbar")
	private WebElement dateHighLightBanner;

	@FindBy(id="com.mandir.debug:id/btn_okay")
	private WebElement okBtn;

	@FindBy(xpath="//android.widget.LinearLayout[@resource-id='com.mandir.debug:id/linear_header']/following-sibling::android.widget.FrameLayout//android.widget.ImageView")
	private WebElement cardDownChevronBtn;

	@FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_chogadhiya_desc']")
	private WebElement chowgadiyaDescription;

	@FindBy(id="com.mandir.debug:id/cta_whatsapp_share")
	private WebElement shareIcon;

	@FindBy(id="com.mandir.debug:id/design_bottom_sheet")
	private WebElement messageInSharingBottomSheet;
	
	@FindBy(id="com.mandir.debug:id/tv_title_chogadhiya")
	private WebElement chowgadiyaTitle;
	
	@FindBy(id="com.mandir.debug:id/rv_chogadhiya")
	private WebElement tomorrowChowgadiyaCards;
	
	@FindBy(id="com.mandir.debug:id/back_button_birth_place")
	private WebElement backBtnFromLocationSearchPage;
	
	@FindBy(xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_container_birth_place']/	\r\n"
			+ "android.widget.LinearLayout[1]//	\r\n"
			+ "android.widget.TextView")
	private WebElement locationNameFromLocationSearchPage;

	@StepInfo(info="Fetching aajBtn Text")
	public String getAajBtnText() {
		awaitForElement(driver, aajBtn);
		ListenerImplimentationclass.testLog.info("Fetching Aaj Button Text from Chowgadiya Page");
		return aajBtn.getText();
	}

	@StepInfo(info="Fetching kalBtn Text")
	public String getKalBtnText() {
		awaitForElement(driver, kalBtn);
		ListenerImplimentationclass.testLog.info("Fetching Kal Button Text from Chowgadiya Page");
		return kalBtn.getText();
	}

	@StepInfo(info="fetching date and month title from chowgadiya page")
	public String[] getdateAndMonthTitle() {
		awaitForElement(driver, dateAndMonthTitle);
		ListenerImplimentationclass.testLog.info("Fetching date and Month title from chowgadiya page");
		String actualDateAndMonthTitle = dateAndMonthTitle.getText();
		return actualDateAndMonthTitle.split(" ");	
	}

	@StepInfo(info="checking if location tab is displayed")
	public boolean isLocationTabDisplayed() {
		awaitForElement(driver, locationTab);
		ListenerImplimentationclass.testLog.info("checking if the location tab is displayed in chowgaidya page");
		return locationTab.isDisplayed();
	}

	@StepInfo(info="clicking on loaction search modal")
	public ChowgadiyaPage clickOnLocationSearchModal() throws Exception {
		try {
			clickOnElement(locationTab);
			ListenerImplimentationclass.testLog.info("clicking on location search modal");
		}catch (Exception e){
			throw new Exception("Error in clickOnLocationSearchModal()"+e.getMessage());
		}
		return this;

	}

	@StepInfo(info="Typing location to loaction text feild")
	public ChowgadiyaPage typeLoactionToLoactionTextFeild() throws Exception {
		try {
			awaitForElement(driver, searchTextFeild);
			ListenerImplimentationclass.testLog.info("Typing Location to Location text feild");
			searchTextFeild.sendKeys("Bangalore");
			return this;
		}catch (Exception e){
			throw new Exception("Error in typeLoactionToLoactionTextFeild()"+e.getMessage());
		}

	}
	@StepInfo(info="clicking on help Button")
	public String clickOnHelpBtn() {
		ListenerImplimentationclass.testLog.info("clicking on help button in chowgadiya page");
		clickOnElement(helpBtn);
		ListenerImplimentationclass.testLog.info("Fetching help bottom sheet title from chowgadiya page");
		return chowgadiyaKyaHaiBannerTitle.getText();
	}

	@StepInfo(info="Fetching date highlight banner Title from chowgadiya page")
	public String getDateHighLightBanner() {
		awaitForElement(driver, dateHighLightBanner);
		ListenerImplimentationclass.testLog.info("Fetching date highlight banner title from chowgadiya page");
		return dateHighLightBanner.getText();
	}

	@StepInfo(info="clicking on OK button in help bottom sheet")
	public ChowgadiyaPage clickOnOkBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on OK button in help bottom sheet");
			clickOnElement(okBtn);
		}catch (Exception e){
			throw new Exception("Error in clickOnOkBtn"+e.getMessage());
		}
		return this;
	}

	@StepInfo(info="scrolling through chowgadiya page")
	public ChowgadiyaPage scrollThroughChowgadiyaCards() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("scrolling through chowgadiya page");
			swipeByCoordinates(driver, 569, 1275, 569, 437);
		}catch (Exception e){
			throw new Exception("Error in  scrollThroughChowgadiyaCards"+e.getMessage());
		}
		return this;
	}

	@StepInfo(info="clicking on down chevron Button")
	public ChowgadiyaPage clickOnDownChevronBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on down chevron button");
			clickOnElement(cardDownChevronBtn);
			return this;
		}catch (Exception e){
			throw new Exception("Error in  clickOnDownChevronBtn()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if chowgadiya description is displayed under chowgadiya card")
	public boolean isChowgadiyaDescriptionDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("Checking if chowgadiya description is displayed for chowgadiya card");
			return chowgadiyaDescription.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in  isChowgadiyaDescriptionDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking on whatsAppShare icon")
	public ChowgadiyaPage clickOnShareIcon() throws Exception {
		try {
			awaitForElement(driver, shareIcon);
			ListenerImplimentationclass.testLog.info("clicking on whatsAppshare icon");
			clickOnElement(shareIcon);
			return this;
		}catch (Exception e){
			throw new Exception("Error in  clickOnShareIcon()"+e.getMessage());
		}
	}	

	@StepInfo(info="checking if share bottom sheet is displayed")
	public boolean isMessageDisplayedInShareBottomSheet() {
		ListenerImplimentationclass.testLog.info("checking if share bottom sheet is displayed");
		return messageInSharingBottomSheet.isDisplayed();
	}

	

	@StepInfo(info="fetching  chowgadiya Tab Text")
	public String getChowgadiyaTabText() throws Exception {
		try { 
			ListenerImplimentationclass.testLog.info("fetching  chowgadiya Tab Text");
			awaitForElement(driver, chowgadiyaTitle);
			return chowgadiyaTitle.getText();
		}
		catch (Exception e)
		{
			throw new Exception("Error in  getChowgadiyaTabText()"+e.getMessage());
		}
	}
	
	
	@StepInfo(info="checking if chowgadiya tab is displayed")
	public boolean isChowgadiyaTabDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("Checking if chowgadiya tab is displayed ");
			awaitForElement(driver, chowgadiyaTitle);
           return chowgadiyaTitle.isDisplayed();
		}
		catch (Exception e){
			throw new Exception("Error in  isChowgadiyaTabDisplayed()"+e.getMessage());
		}
	
	}
	
	@StepInfo(info="clicking on location Name from location search Page")
	public void clickOnLocation() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on location Name from location search Page");
		awaitForElement(driver, locationNameFromLocationSearchPage);
		clickOnElement(locationNameFromLocationSearchPage);
		}catch (Exception e){
			throw new Exception("Error in  clickOnLocation()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking in Kal Tab")
	public void clickOnKalTab() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking in Kal Tab");
		awaitForElement(driver, kalBtn);
		clickOnElement(kalBtn);
		}catch (Exception e){
			throw new Exception("Error in  clickOnKalTab()"+e.getMessage());
		}
		
	}
	
	@StepInfo(info="checking if tomorrow chowgadiya card displayed")
	public boolean isTomorrowChowgadiyaCardDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if tomorrow chowgadiya card displayed");
		awaitForElement(driver, tomorrowChowgadiyaCards);
		return tomorrowChowgadiyaCards.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in  isTomorrowChowgadiyaCardDisplayed()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on Aaj  Tab")
	public void clickOnAajTab() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on Aaj Tab");
		awaitForElement(driver, aajBtn);
		clickOnElement(aajBtn);
		}catch (Exception e){
			throw new Exception("Error in  clickOnAajTab()"+e.getMessage());
		}
		
	}
	
	@StepInfo(info="clicking on back button from location search page")
	public void clickOnBackBtnFromLocationSearchPage() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on back button from location search page");
		awaitForElement(driver, backBtnFromLocationSearchPage);
		clickOnElement(backBtnFromLocationSearchPage);
		}catch (Exception e){
			throw new Exception("Error in  clickOnBackBtnFromLocationSearchPage()"+e.getMessage());
		}
	}
}
	

