package Pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ExcelUtility;
import base.ListenerImplimentationclass;
import base.PunyaMudraPopUpTest;
import io.appium.java_client.AppiumDriver;

public class RashifalPage extends AppGenericLib{
	ElementUtil elementUtil;

	public RashifalPage(AppiumDriver driver) {
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver,this);
	}

	private WebElement rashifalTab(String rashifalText)
	{
		String rashifalTabXapthValue="//android.widget.LinearLayout[@resource-id='com.mandir.debug:id/tab_item_ll']/descendant::android.widget.TextView[@text='"+rashifalText+"']";
		return elementUtil.getElement("xpath",rashifalTabXapthValue);
	}

	private WebElement rashifalText(String rashifalText)
	{
		String rashifalXpathtextValue="//android.widget.LinearLayout[@resource-id='com.mandir.debug:id/appBarLayouts']/descendant::android.widget.TextView[@text='"+rashifalText+"']";
		return elementUtil.getElement("xpath", rashifalXpathtextValue);
	}

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_signs']/descendant::android.widget.TextView[@resource-id='com.mandir.debug:id/tv_sign']")
	private List<WebElement> allRashiCount;	

	@FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_sign_selector']")
	private WebElement rashiDropDown;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_signs']/androidx.cardview.widget.CardView[1]")
	private WebElement selectRashi;

	@FindBy(id="com.mandir.debug:id/design_bottom_sheet")
	private WebElement rashiBottomSheet;

	@FindBy(id="com.mandir.debug:id/tv_title1")
	private WebElement dateAndMonthTitle;
	
	private WebElement rashiText(String Mesha)
	{
		String rashiXpathValue="//android.widget.TextView[@text='"+Mesha+"']";
		return elementUtil.getElement("xpath", rashiXpathValue);
	}

	@FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_title1']")
	private WebElement aajDate;

	@FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_title2']")
	private WebElement shubhMantraText;

	@FindBy(id="com.mandir.debug:id/cta_whatsapp_share")
	private WebElement watsAppShareKareButton;

	@FindBy(id="com.mandir.debug:id/design_bottom_sheet")
	private WebElement messageInSharingBottomSheet;

	@FindBy(id="com.mandir.debug:id/iv_shub_mantra")
	private WebElement shubMantraSongIcon;

	@FindBy(id="com.mandir.debug:id/player_ll")
	private WebElement miniPlayer;

	@FindBy(id="com.mandir.debug:id/song_play_pause_button")
	private WebElement pauseOrPlayButton;

	@FindBy(id="com.mandir.debug:id/ic_mini_player_close")
	private WebElement closeButtonMiniPlayer;

	@FindBy(id="com.mandir.debug:id/iv_help_text")
	private WebElement whatIsRashifalHelpButton;

	@FindBy(id="com.mandir.debug:id/btn_okay")
	private WebElement okBtn;

	@FindBy(id="com.mandir.debug:id/iv_exit")
	private WebElement closeRashiBottomSheetButton;



	//Buisness Library


	@StepInfo(info="Clicking on Rasihfal tab title")
	public RashifalPage clickOnRashifalTab() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("Clicking on Rasihfal tab title");

			awaitForElement(driver,rashifalTab(ExcelUtility.getExcelData("locators", "rashifalText", "value")));
			clickOnElement(rashifalTab(ExcelUtility.getExcelData("locators", "rashifalText", "value")));
			PunyaMudraPopUpTest.handlePunyaMudraPopUp(driver);
			handlingRashiBottomSheet();
			return this;
		}
		catch (Exception e){
			throw new Exception("Error in clickOnRashifalTab()"+e.getMessage());

		}
	}

	@StepInfo(info="Rashifal dropdown displayed")
	public void handlingRashiBottomSheet() {
		try {
			ListenerImplimentationclass.testLog.info("Selecting Rashi from DropDown");
			if(rashiBottomSheet.isDisplayed()) {
				clickOnElement(closeRashiBottomSheetButton);
			}
		}catch (Exception e) {
			System.out.println("Rashifal Bottom sheet not displayed");
		}
	}


	@StepInfo(info="swipe up rashifal bottom sheet")
	public void swipeRashifalBottomSheet() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("swipe up rashifal bottom sheet");
			awaitForElement(driver, rashiBottomSheet);
			swipeByCoordinates(driver,800, 1400, 800, 1000);


		} catch (Exception e) {
			throw new Exception("Error in  swipeRashifalBottomSheet()"+e.getMessage());
		}
	}

	@StepInfo(info="click on Rashifal DropDown")
	public RashifalPage clickOnRashiDropDown() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("click on Rashifal DropDown");
			awaitForElement(driver, rashiDropDown);
			clickOnElement(rashiDropDown);
			return this;
		}catch (Exception e) {
			throw new Exception("Error in  clickOnRashiDropDown()"+e.getMessage());
		}
	}

	@StepInfo(info="Selecting Rashi from DropDown")
	public RashifalPage selectRashiOnDropDown() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("Selecting Rashi from DropDown");
			awaitForElement(driver, selectRashi);
			clickOnElement(selectRashi);
			return this;
		}catch (Exception e) {
			throw new Exception("Error in  selectRashiOnDropDown()"+e.getMessage());
		}
	}

	@StepInfo(info="Select Rashi page title")
	public String getRashifalTitle() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("Get rashifal text");
			awaitForElement(driver,rashifalText(ExcelUtility.getExcelData("locators", "rashifalText", "value")));
			return rashifalText(ExcelUtility.getExcelData("locators", "rashifalText", "value")).getText();
		}
		catch (Exception e) {
			throw new Exception("Error in  getRashifalTitle()"+e.getMessage());
		}

	}


	public String getSelectedRashiValue() throws Exception
	{

		try {
			ListenerImplimentationclass.testLog.info("Fetching Rashi from Carousel");
			awaitForElement(driver,rashiText(ExcelUtility.getExcelData("locators","Mesha","value")));
			return rashiText(ExcelUtility.getExcelData("locators","Mesha","value")).getText();
		}
		catch(Exception e) {
			throw new Exception("Error in getSelectedRashiValue()"+e.getMessage());
		}

	}

	@StepInfo(info="fetching date and month title from rashifal page")
	public String[] getDateAndMonthTitle() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("fetching date and month title from rashifal page");
			awaitForElement(driver, dateAndMonthTitle);
			return dateAndMonthTitle.getText().split(" ");
		}catch(Exception e) {
			throw new Exception("Error in getDateAndMonthTitle()"+e.getMessage());
		}

	}

	@StepInfo(info="fetching selected Rashi Value")
	public String getSelectedRashiText() throws Exception
	{
		ListenerImplimentationclass.testLog.info("Fetching selected rashi in Dropdown");
		try {
			awaitForElement(driver, rashiText(ExcelUtility.getExcelData("locators","Mesha","value")));
			rashiText(ExcelUtility.getExcelData("locators","Mesha","value")).getText();
			return rashiText(ExcelUtility.getExcelData("locators","Mesha","value")).getText();
		}catch(Exception e){
			throw new Exception("Error in getSelecetdRashiText()"+e.getMessage());

		}

	}


	@StepInfo(info="fetching Shubh Mantra title")
	public String getSelectedRashiInShubMantra() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("fetching Shubh Mantra title");
			String[] shubhMatraTextArray = shubhMantraText.getText().split(" ");
			String actualShubMantra = shubhMatraTextArray[0];
			return actualShubMantra;
		} 
		catch (Exception e) {
			throw new Exception("Error in getSelectedRashiInShubMantra()"+e.getMessage());
		}

	}
	@StepInfo(info="Click on watsApp share button")
	public void ClickWatsAppShareKareButton() throws Exception
	{

		try {
			ListenerImplimentationclass.testLog.info("Click on watsApp share button");
			awaitForElement(driver, watsAppShareKareButton);
			clickOnElement(watsAppShareKareButton);
		} catch (Exception e) {
			throw new Exception("Error inClickWatsAppShareKareButton()"+e.getMessage());
		}

	}


	@StepInfo(info="checking if share bottom sheet is displayed")
	public boolean isMessageDisplayedInShareBottomSheet() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("checking if share bottom sheet is displayed");
			return messageInSharingBottomSheet.isDisplayed();

		} catch (Exception e) {
			throw new Exception("Error in scrollToNextPage()"+e.getMessage());		

		}

	}


	@StepInfo(info="Clicking on shub mantra song button")
	public RashifalPage ClickOnShubManatraSongIcon() throws Exception
	{
		ListenerImplimentationclass.testLog.info("Clicking on shub mantra song icon");

		try {
			awaitForElement(driver, shubMantraSongIcon);
			clickOnElement(shubMantraSongIcon);

			return this;


		} catch (Exception e) {
			throw new Exception("Error in scrollToNextPage()"+e.getMessage());		
		}
	}

	@StepInfo(info="Verifying song mini player displayed or not")
	public boolean isMiniPlayerDisplayed() throws Exception
	{

		ListenerImplimentationclass.testLog.info("isMiniPlayerDisplayedOrNot");
		try {

			return miniPlayer.isDisplayed();

		} catch (Exception e) {
			throw new Exception("Error in scrollToNextPage()"+e.getMessage());
		}

	}


	@StepInfo(info="Scrolling to next page ")
	public RashifalPage scrollToNextPage() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("scrollToNextPage");
			Horizontalscroll(driver, 990 , 650, 50, 650, 3);
			return this;
		} catch (Exception e) {
			throw new Exception("Error in scrollToNextPage()"+e.getMessage());
		}

	}


	@StepInfo(info="Scroll Back to Rashifal")
	public RashifalPage ScrollbackToRashifal() throws Exception
	{
		ListenerImplimentationclass.testLog.info("Scroll Back to Rashifal");		
		try {
			Horizontalscroll(driver, 74 , 390, 1000, 384, 3);
			return this;
		} catch (Exception e) {
			throw new Exception("Error in ScrollbackToRashifal()"+e.getMessage());
		}

	}

	@StepInfo(info="Click on Pause or play button")
	public RashifalPage ClickPauseOrPlayButton() throws Exception
	{
		ListenerImplimentationclass.testLog.info("Click on Pause or play button");
		try {
			Verticalscroll(driver, 865, 865, 865, 703,1);
			clickOnElement(pauseOrPlayButton);
			return this;
		} catch (Exception e) {
			throw new Exception("Error in ClickPauseOrPlayButton()"+e.getMessage());
		}

	}

	@StepInfo(info="Click on close button on mininplayer")
	public RashifalPage CloseMiniPlayer() throws Exception
	{
		ListenerImplimentationclass.testLog.info("Click on close button on mininplayer");
		try {
			awaitForElement(driver, closeButtonMiniPlayer);
			clickOnElement(closeButtonMiniPlayer);
			return this;
		} catch (Exception e) {
			throw new Exception("Error in CloseMiniPlayer()"+e.getMessage());
		}
	}

	@StepInfo(info="Click on what is rashifal button")
	public RashifalPage ClickOnWhatisRashifal() throws Exception
	{
		ListenerImplimentationclass.testLog.info("Click on what is rashifal button");
		try {
			awaitForElement(driver, whatIsRashifalHelpButton);
			clickOnElement(whatIsRashifalHelpButton);
			return this;

		} catch (Exception e) {
			throw new Exception("Error in ClickOnWhatisRashifal()"+e.getMessage());

		}
	}

	@StepInfo(info="Click on Teek Hai Button")
	public void ClickOnTeekHai() throws Exception
	{

		ListenerImplimentationclass.testLog.info("Click on Teek Hai Button");
		try {
			awaitForElement(driver,okBtn);
			clickOnElement(okBtn);
		} catch (Exception e) {
			throw new Exception("Error in Click on ClickOnTeekHai()"+e.getMessage());
		}

	}


	@StepInfo(info="Fetching Rashi Count")
	public int getAllRashiCount() throws Exception
	{
		ListenerImplimentationclass.testLog.info("Fetching Rashi Count");
		try {
			ArrayList<WebElement> list=new ArrayList<>();
			for (WebElement rashiWebElement : allRashiCount) {
				list.add(rashiWebElement);
			}
			return list.size();

		} catch (Exception e) {
			throw new Exception("Error in getAllRashiCount()"+e.getMessage());
		}
	}

	@StepInfo(info=" Rashi Bottomsheet on Onboarding ")
	public void closeRashiBottomSheet() throws Exception
	{
		ListenerImplimentationclass.testLog.info("Closing Rashi Bottomsheet on Onboarding ");
		try {		
			if(rashiBottomSheet.isDisplayed()) {
				closeRashiBottomSheetButton.click();
			}
		}
		catch (Exception e) {
			throw new Exception("Error in closeRashiBottomSheet()"+e.getMessage());		}
	}

	@StepInfo(info="checking if Rashifal Title is displayed")
	public boolean isRashifalTitleDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if Rashifal Title is displayed");
			awaitForElement(driver,rashiText(ExcelUtility.getExcelData("locators","rashifalText","value")));
			return rashiText(ExcelUtility.getExcelData("locators","rashifalText","value")).isDisplayed();

		}catch (Exception e)
		{
			throw new Exception("Error in isRashifalTitleDisplayed()"+e.getMessage());
		}	
	}
}