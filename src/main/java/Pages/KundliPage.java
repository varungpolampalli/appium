package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ExcelUtility;
import base.ListenerImplimentationclass;
import base.PunyaMudraPopUpTest;
import base.TestData;
import io.appium.java_client.AppiumDriver;

public class KundliPage extends AppGenericLib{
	ElementUtil elementUtil;
	public KundliPage(AppiumDriver driver) {
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id="com.mandir.debug:id/youtube_icon")
	private WebElement youtubeBtn;
	
	@FindBy(id="com.mandir.debug:id/back_button_kundli")
	private WebElement youtubeBackBtn;

	@FindBy(id="com.mandir.debug:id/share_kundli_wa")
	private WebElement shareIcon;

	@FindBy(id="com.mandir.debug:id/design_bottom_sheet")
	private WebElement messageInSharingBottomSheet;

	@FindBy(id="com.mandir.debug:id/design_bottom_sheet")
	private WebElement punyaMudraBottomSheet;

	@FindBy(id="com.mandir.debug:id/how_to_earn_coin_cta")
	private WebElement punyaMudraTutorialBtn;

	@FindBy(id="com.mandir.debug:id/location_kundali")
	private WebElement locationTab;

	@FindBy(id="com.mandir.debug:id/location_name")
	private WebElement locationNameFromLocationSearchList;

	@FindBy(id="com.mandir.debug:id/search_src_text")
	private WebElement locationSearchTextFeild;
	
	@FindBy(id="com.mandir.debug:id/edit_name")
	private WebElement nameTextFeild;
	
	@FindBy(id="com.mandir.debug:id/kundali_birth_date")
	private WebElement birthDateSelectionTab;
	
	@FindBy(id="com.mandir.debug:id/btn_save")
	private WebElement bottomSheetSaveBtn;
	
	@FindBy(id="com.mandir.debug:id/other_kundali")
	private WebElement otherGenderTab;
	 
	@FindBy(id="com.mandir.debug:id/female_kundali")
	private WebElement femaleGenderTab;
	
	@FindBy(id="com.mandir.debug:id/male_kundali")
	private WebElement maleGenderTab;
	
	@FindBy(id="com.mandir.debug:id/cta_save_details")
	private WebElement ageBadeBtn;
	
	@FindBy(id="com.mandir.debug:id/go_back")
	private WebElement vapasjaayeBtn;
	
	@FindBy(id="com.mandir.debug:id/kundali_birth_time")
	private WebElement birthTimeSelectionTab;
	
	@FindBy(id="com.mandir.debug:id/confirm_name")
	private WebElement nameFromConfirmationPopup;
	
	@FindBy(id="com.mandir.debug:id/confirm_gender")
	private WebElement genderFromConfirmationPopup;
	
	@FindBy(id="com.mandir.debug:id/confirm_location")
	private WebElement locationFromConfirmationPopup;		

	@FindBy(id="com.mandir.debug:id/confirm_date_time")
	private WebElement datetimeonconfirmationpopup;
	
	@FindBy(id="com.mandir.debug:id/iv_cancel")
	private WebElement closeBtn;
	
	@FindBy(id="com.mandir.debug:id/design_bottom_sheet")
	private WebElement calendarBottomSheet;
	
	@FindBy(id="com.mandir.debug:id/back_button_kundli_form")
	private WebElement backBtnFromKundliFormPage ;

	@FindBy(id="android:id/content")
	private WebElement saveInformationPopup; 
	
	@FindBy(id="com.mandir.debug:id/cancel_confirm")
	private WebElement cancelBtnInConfirmationPopup;
		
	@FindBy(id="com.mandir.debug:id/dont_save_kundli")
	private WebElement dontSaveBtnInConfirmationPopup;
	
	@FindBy(id="com.mandir.debug:id/headerTitle")
	private WebElement onBoardingTutorialTitle;
	
	@FindBy(xpath="//android.widget.LinearLayout[@resource-id='android:id/pickers']/android.widget.NumberPicker[3]/android.widget.EditText")
	private WebElement currentSelectedYear;
	
	@FindBy(xpath = "//android.widget.LinearLayout[@resource-id='android:id/pickers']/android.widget.NumberPicker[1]/android.widget.EditText")
	private WebElement currentSelectedDay;
		
	@FindBy(xpath="//android.widget.LinearLayout[@resource-id='android:id/timePickerLayout']/android.widget.LinearLayout/android.widget.NumberPicker[1]/android.widget.EditText")
	private WebElement currentSelectedHour;

	@FindBy(xpath="//android.widget.LinearLayout[@resource-id='android:id/timePickerLayout']/android.widget.LinearLayout/android.widget.NumberPicker[2]/android.widget.EditText")
	private WebElement currentSelectedMinute;
	
	@FindBy(xpath="//android.widget.FrameLayout[@resource-id='com.mandir.debug:id/design_bottom_sheet']")
	private WebElement confirmationPopup;
	
	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_container_birth_place']/android.widget.LinearLayout[1]//android.widget.TextView[@resource-id='com.mandir.debug:id/location_name']")
	private WebElement getFirstLocationName;
	
	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_container_kundali']/android.widget.FrameLayout[3]//android.widget.TextView[@resource-id='com.mandir.debug:id/kundali_points']")
	private WebElement thirdKundliPoints;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_container_kundali']/android.widget.FrameLayout[4]//android.widget.TextView[@resource-id='com.mandir.debug:id/kundali_points']")
	private WebElement fourthKundliPoints;
		
	@FindBy(xpath = "//android.widget.ImageView[@resource-id='com.mandir.debug:id/back_button_kundli']")
	private WebElement kundliBackBtn;
	
	
	private WebElement getkundliPageTitle(String kundliPageTitleText) {
		String kundliPageTitleTextxpathValue="//android.widget.TextView[@text='"+kundliPageTitleText+"']";
		return elementUtil.getElement("xpath", kundliPageTitleTextxpathValue);
	}

	public WebElement getFirstKundliSlotTab(String firstKundiSlotTabText)
	{
		String firstKundliSlotxpathValue="//android.widget.TextView[@text='"+firstKundiSlotTabText+"']";
		return elementUtil.getElement("xpath", firstKundliSlotxpathValue);
	}

	public WebElement getSecondKundliSlotTab(String secondKundliSlotTabText)
	{
		String secondKundliSlotxpathValue="//android.widget.TextView[@text='"+secondKundliSlotTabText+"']";
		return elementUtil.getElement("xpath", secondKundliSlotxpathValue);
	}

	public WebElement getThirdKundliSlotTab(String thirdKundliSlotTabText)
	{
		String thirdKundliSlotxpathValue="//android.widget.TextView[@text='"+thirdKundliSlotTabText+"']";
		return elementUtil.getElement("xpath", thirdKundliSlotxpathValue);
	}

	public WebElement getFourthKundliSlotTab(String fourthKundliSlotTabText)
	{
		String fourthKundliSlotxpathValue="//android.widget.TextView[@text='"+fourthKundliSlotTabText+"']";
		return elementUtil.getElement("xpath", fourthKundliSlotxpathValue);
	}

	public WebElement getKundliFormPageTitle(String kundliFormPageTitle)
	{
		String kundliFormPageTitlexpathValue="//android.widget.TextView[@text='"+kundliFormPageTitle+"']";
		return elementUtil.getElement("xpath", kundliFormPageTitlexpathValue);
	}

	@StepInfo(info="fetching kundli Page title")
	public String getKundliPageTitle() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("fetching kundli Page title");
			awaitForElement(driver,getkundliPageTitle(ExcelUtility.getExcelData("locators", "kundliPageTitleText","value")));
			return getkundliPageTitle(ExcelUtility.getExcelData("locators", "kundliPageTitleText", "value")).getText();
		}catch (Exception e){
			throw new Exception("Error in getKundliPageTitle() "+e.getMessage());
		}
	}

	@StepInfo(info="clicking on back button")
	public void clickOnBackBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on back button");
			awaitForElement(driver, kundliBackBtn);
			clickOnElement(kundliBackBtn);
		}catch (Exception e){
			throw new Exception("Error in clickOnBackBtn() "+e.getMessage());
		}
	}	

	@StepInfo(info="checking if Kundli  title is displayed")
	public boolean isKundliTitleDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if Kundli title is displayed");
			awaitForElement(driver,getkundliPageTitle(ExcelUtility.getExcelData("locators","kundliPageTitleText","value")));
			return getkundliPageTitle(ExcelUtility.getExcelData("locators","kundliPageTitleText","value")).isDisplayed();

		}catch (Exception e){
			throw new Exception("Error in isKundliTitleDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="fetching first kundli slot tab text")
	public String getFirstKundliSlotTabText() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("fetching first kundli slot tab text");
			awaitForElement(driver,getFirstKundliSlotTab(ExcelUtility.getExcelData("locators", "firstKundliSlotTabText","value")));
			return getFirstKundliSlotTab(ExcelUtility.getExcelData("locators", "firstKundliSlotTabText", "value")).getText();
		}catch (Exception e){
			throw new Exception("Error in getFirstKundliSlotTabText() "+e.getMessage());
		}
	}

	@StepInfo(info="feching second kundli slot tab text")
	public String getSecondKundliSlotTabText() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("fetching second kundli slot tab text");
			awaitForElement(driver,getSecondKundliSlotTab(ExcelUtility.getExcelData("locators","secondKundliSlotTabText", "value")));
			return getSecondKundliSlotTab(ExcelUtility.getExcelData("locators","secondKundliSlotTabText", "value")).getText();
		}catch (Exception e) {
			throw new Exception("Error in getSecondKundliSlotTabText() "+e.getMessage());
		}
	}

	@StepInfo(info="feching third kundli slot tab text")
	public String getThirdKundliSlotTabText() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("fetching third kundli slot tab text");
			awaitForElement(driver,getThirdKundliSlotTab(ExcelUtility.getExcelData("locators","thirdKundliSlotTabText", "value")));
			return getThirdKundliSlotTab(ExcelUtility.getExcelData("locators","thirdKundliSlotTabText", "value")).getText();
		}catch (Exception e) {
			throw new Exception("Error in getThirdKundliSlotTabText() "+e.getMessage());
		}
	}


	@StepInfo(info="feching fourth kundli slot tab text")
	public String getFourthKundliSlotTabText() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("fetching fourth kundli slot tab text");
			awaitForElement(driver,getFourthKundliSlotTab(ExcelUtility.getExcelData("locators","fourthKundliSlotTabText", "value")));
			return getFourthKundliSlotTab(ExcelUtility.getExcelData("locators","fourthKundliSlotTabText", "value")).getText();
		}catch (Exception e) {
			throw new Exception("Error in getFourthKundliSlotTabText() "+e.getMessage());
		}
	}

	
	@StepInfo(info="vertical scrolling")
	public void verticalScroll()
	{
	    waitOrPause(5);
		ListenerImplimentationclass.testLog.info("vertical scrolling");
	    Verticalscroll(driver, 515, 834, 505, 350, 2);

	}

	@StepInfo(info="clicking on youtube button")
	public KundliPage clickOnYoutubeBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on youtube button");
			awaitForElement(driver, youtubeBtn);
			clickOnElement(youtubeBtn);
			return this;
          }catch (Exception e){
			throw new Exception("Error in clickOnYoutubeBtn() "+e.getMessage());
		}
	}	

	@StepInfo(info="clicking on youtube back button")
	public void clickOnYoutubeBackBtn() throws Exception {
		try {
			waitOrPause(5);
			ListenerImplimentationclass.testLog.info("clicking on youtube back button");
			awaitForElement(driver, youtubeBackBtn);
			clickOnElement(youtubeBackBtn);
		}catch (Exception e){
			throw new Exception("Error in clickOnYoutubeBackBtn() "+e.getMessage());
		}
	}	
   
	@StepInfo(info="checking youtube back button is Displayed ")
	public boolean isYouTubeBackBtnDisplayed() throws Exception
	{
		try {
			awaitForElement(driver, youtubeBackBtn);
			ListenerImplimentationclass.testLog.info("checking youtube back button is Displayed");
			return youtubeBackBtn.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in isYouTubeBackBtnDisplayed()"+e.getMessage());
		}
	}
	
	@StepInfo(info="clicking on whatsapp icon")
	public void clickOnShareIcon() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on whatsapp button");
			awaitForElement(driver, shareIcon);
			clickOnElement(shareIcon);
		}catch (Exception e){
			throw new Exception("Error in clickOnShareIcon() "+e.getMessage());
		}
	}	

	@StepInfo(info="checking message sharing Bottom sheet in kundli is Displayed ")
	public boolean isMessageSharingBottomSheetDisplayedInKundli() throws Exception
	{

		try {
			awaitForElement(driver, messageInSharingBottomSheet);
			ListenerImplimentationclass.testLog.info("checking message sharing Bottom sheet in kundli is Displayed");
			return messageInSharingBottomSheet.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in isMessageSharingBottomSheetDisplayedInKundli()"+e.getMessage());
		}
	}

	@StepInfo(info = "clicking on fourth kundli slot button")
	public KundliPage clickOnFourthKundliSlotTabText() throws Exception
	{
		try{
			ListenerImplimentationclass.testLog.info("clicking on fourth kundli slot button");
			awaitForElement(driver, getFourthKundliSlotTab(ExcelUtility.getExcelData("locators", "fourthKundliSlotTabText", "value")));
			clickOnElement(getFourthKundliSlotTab(ExcelUtility.getExcelData("locators", "fourthKundliSlotTabText", "value")));
		}catch(Exception e)
		{
			throw new Exception("Error in clickOnFourthKundliSlotTabText()"+e.getMessage());
		}
		return this;
	}


	@StepInfo(info="checking punya mudra Bottom sheet in kundli is Displayed ")
	public boolean isPunyaMudraBottomSheetDisplayed() throws Exception
	{
		try {
			awaitForElement(driver, punyaMudraBottomSheet);
			ListenerImplimentationclass.testLog.info("checking punya mudra Bottom sheet in kundli is Displayed");
			return punyaMudraBottomSheet.isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in isPunyaMudraBottomSheetDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking on punya mudra tutorial button")
	public PunyaMudraPage clickOnPunyaMudraTutorialBtn() throws Exception
	{
		try {
			awaitForElement(driver,punyaMudraTutorialBtn);
			ListenerImplimentationclass.testLog.info("clicking on punya mudra tutorial button");
			clickOnElement(punyaMudraTutorialBtn);
		}
		catch(Exception e){
			throw new Exception("Error in clickOnFirstKundliSlotTabText()"+e.getMessage());
		}
		return new  PunyaMudraPage(driver);
       }

	@StepInfo(info="getting on boarding page")
	public void gettingOnBoardingPage() throws Exception
	{
		ListenerImplimentationclass.testLog.info("getting on boarding page");
		GodSelectionPage godSelectionPage=new GodSelectionPage(driver);
		godSelectionPage.clickOnGodSelectionSaveBtn();
		MandirPage mandirPage=new MandirPage(driver);
		awaitForElement(driver, onBoardingTutorialTitle);
		for(int i=0;i<3;i++)
		{
			mandirPage.tapAnywhereOnScreen();
		}
		PunyaMudraPopUpTest.handlePunyaMudraPopUp(driver);
	}

	@StepInfo(info = "clicking on fourth kundli slot button")
	public KundliPage clickOnFirstKundliSlotTabText() throws Exception
	{
		try{
			ListenerImplimentationclass.testLog.info("clicking on first kundli slot button");
			awaitForElement(driver, getFirstKundliSlotTab(ExcelUtility.getExcelData("locators", "firstKundliSlotTabText", "value")));
			clickOnElement(getFirstKundliSlotTab(ExcelUtility.getExcelData("locators", "firstKundliSlotTabText", "value")));
			return this;
      }catch(Exception e)
		{
			throw new Exception("Error in clickOnFourthKundliSlotTabText()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking on location Tab from kundli  page")
	public  KundliPage clickOnLocationTab() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on location Tab from kundli  page");
			awaitForElement(driver, locationTab);
			clickOnElement(locationTab);
			return this;
		}catch (Exception e){
			throw new Exception("Error in   clickOnLocationTab() "+e.getMessage());
		}
	}


	@StepInfo(info="typing location to location search text feild")
	public KundliPage typeLocationInLocationSearchFeild() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("typing location to location search text feild");
			awaitForElement(driver, locationSearchTextFeild);
			locationSearchTextFeild.sendKeys("Bangalore");
			return this;
		}catch (Exception e){
			throw new Exception("Error in   typeLocationToLocationSearchFeild() "+e.getMessage());
		}
	}
	
	@StepInfo(info="typing location to location search text feild")
	public KundliPage typeLocationInLocationSearchFeildAfterSeleting() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("typing location to location search text feild");
			awaitForElement(driver, locationSearchTextFeild);
			locationSearchTextFeild.sendKeys(TestData.locationName);
			return this;
		}catch (Exception e){
			throw new Exception("Error in   typeLocationToLocationSearchFeild() "+e.getMessage());
		}
	}
	
	@StepInfo(info="fetching location name from location Tab from kundliPage")
	public String getLocationNameFromLocationTab() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("fetching location name from location Tab from kundli Page");
			awaitForElement(driver, locationTab);
			return locationTab.getText();
		}catch (Exception e){
			throw new Exception("Error in getLocationNameFromLocationTab() "+e.getMessage());
		}
	}

	@StepInfo(info="clicking on location from location list")
	public void clickOnLocationFromLocationList() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on location from location list");
			awaitForElement(driver, locationNameFromLocationSearchList);
			clickOnElement(locationNameFromLocationSearchList);
		}catch (Exception e){
			throw new Exception("Error in clickOnLocationFromLocationList() "+e.getMessage());
		}
	}


	@StepInfo(info="fetching location name from location Tab from kundli Page")
	public String getFirstLocationNameFromLocationTab() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("fetching location name from location Tab from  kundli Page");
			awaitForElement(driver, getFirstLocationName);
			return getFirstLocationName.getText();
		}catch (Exception e){
			throw new Exception("Error in getLocationNameFromLocationTab() "+e.getMessage());
		}
	}

	@StepInfo(info="checking if location name is displayed before searching")
	public boolean isLocationNameDisplayedBeforeSearching() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if location name is displayed before searching");
			awaitForElement(driver, getFirstLocationName);
			return getFirstLocationName.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isLocationNameDisplayedBeforeSearching()"+e.getMessage());
		}
	}

	@StepInfo(info="fetching third kundli punya mudra eligibility coins")
	public String getThirdKundliPunyaMudraEligibilityCoins() throws Exception{
		try {
			ListenerImplimentationclass.testLog.info("fetching third kundli punya mudra eligibility coins");
			awaitForElement(driver,thirdKundliPoints);
			return thirdKundliPoints.getText();
		}catch( Exception e) {
			throw new Exception("Error in getThirdKundliPunyaMudraEligibilityCoins()"+e.getMessage());
			
			}
	 }
	  @StepInfo(info="fetching fourth kundli punya mudra eligibility coins")
	  public String getFourthKundliPunyaMudraEligibilityCoins() throws Exception
	  {
		  try {
			  ListenerImplimentationclass.testLog.info("fetching fourth kundli punya mudra eligibility coins");
			  awaitForElement(driver,fourthKundliPoints);
			  return fourthKundliPoints.getText();
		  }catch(Exception e) {
			  throw new Exception("Error in getFourthKundliPunyaMudraEligibilityCoins()"+e.getMessage());
			  
		  }
	  }
	
	  @StepInfo(info="clearing name text feild")
		public KundliPage clearNameTextFeild() throws Exception {
			try {
				ListenerImplimentationclass.testLog.info("clearing name text feild");
				awaitForElement(driver, nameTextFeild);
				nameTextFeild.clear();
				return this;
			}catch (Exception e){
				throw new Exception("Error in clearNameTextFeild()"+e.getMessage());
			}
		}
  
	  
	  @StepInfo(info="Entering name to name Text feild")
		public void enterName() throws Exception {
			try {
				ListenerImplimentationclass.testLog.info("Entering name to name Text feild");
				nameTextFeild.sendKeys(TestData.userName);
			}catch (Exception e){
				throw new Exception("Error in enterName()"+e.getMessage());
			}
		}
    
	  
	  @StepInfo(info="clicking on birth date selection tab")
		public KundliPage clickOnBirthDateSelectionTab() throws Exception {
			try {
				ListenerImplimentationclass.testLog.info("clicking on birth date selection tab");
				awaitForElement(driver, birthDateSelectionTab);
				clickOnElement(birthDateSelectionTab);
				return this;
			}catch (Exception e){
				throw new Exception("Error in clickOnBirthDateSelectionTab()"+e.getMessage());
			}
	}
	  
	  
	  @StepInfo(info="swiping day selection button")
		public KundliPage swipeDaySelectionTab() throws Exception {
			try {
				ListenerImplimentationclass.testLog.info("swiping day selection button");
				awaitForElement(driver, bottomSheetSaveBtn);
				swipeByCoordinates(driver, 259, 1457, 259, 1023);
				return this;
			}catch (Exception e){
				throw new Exception("Error in swipeDaySelectionTab()"+e.getMessage());
			}
		}

		@StepInfo(info="swiping month selection tab")
		public KundliPage swipeMonthSelectionTab() throws Exception {
			try {
				ListenerImplimentationclass.testLog.info("swiping month selection tab");
				swipeByCoordinates(driver, 538, 1457, 538, 1023);
				return this;
			}catch (Exception e){
				throw new Exception("Error in swipeMonthSelectionTab()"+e.getMessage());
			}
		}
		
		
		@StepInfo(info="swiping year selection tab")
		public KundliPage swipeYearSelectionTab() throws Exception {

			ListenerImplimentationclass.testLog.info("swiping year selection tab");
			int yearValue = Integer.parseInt(currentSelectedYear.getText());
			try
			{
				while(yearValue>2010 ) {
					swipeByCoordinates(driver, 807, 1185, 807, 1457);
					yearValue = Integer.parseInt(currentSelectedYear.getText());
				}
				while(yearValue<2010 ) {
					swipeByCoordinates(driver, 807, 1457, 807, 1185);
					yearValue = Integer.parseInt(currentSelectedYear.getText());
				}
			}catch(Exception e) {
				throw new Exception("Error in selecting year"+e.getMessage());
			}
			return this;
			}
		
		@StepInfo(info="converting String Day Value to Integer")
		public int getSelectedDayValue() throws Exception   {
			try {
				ListenerImplimentationclass.testLog.info("converting String Day Value to Integer");
				return Integer.parseInt(getCurrentSelectedDay());
			}catch (Exception e){
				throw new Exception("Error in getSelectedDayValue() "+e.getMessage());
			}

		}

		@StepInfo(info="converting String Year Value to Integer")
		public int getSelectedYearValue() throws Exception   {
			try {
				ListenerImplimentationclass.testLog.info("converting String Year Value to Integer");
				return Integer.parseInt(getCurrentSelectedYear());
			}catch (Exception e){
				throw new Exception("Error in getSelectedYearValue() "+e.getMessage());
			}

		}
		
		@StepInfo(info="fetching current selected day from the calendar bottom sheet")
		public String getCurrentSelectedDay() throws Exception {
			try {
				ListenerImplimentationclass.testLog.info("fetching current selected day from the calendar bottom sheet");
				awaitForElement(driver, currentSelectedDay);
				return currentSelectedDay.getText();
			}catch (Exception e){
				throw new Exception("Error in getCurrentSelectedDay()"+e.getMessage());
			}
		}
		
		@StepInfo(info="fetching current selected day from the calendar bottom sheet")
		public String getCurrentSelectedYear() throws Exception {
			try {
				ListenerImplimentationclass.testLog.info("fetching current selected day from the calendar bottom sheet");
				awaitForElement(driver, currentSelectedYear);
				return currentSelectedYear.getText();
			}catch (Exception e){
				throw new Exception("Error in getCurrentSelectedYear()"+e.getMessage());
			}
		}
		
		@StepInfo(info="clicking on save button from calendar bottom sheet")
		public ProfilePage clickOnSaveButtonFromCalenderBottomSheet() throws Exception {
			try {
				ListenerImplimentationclass.testLog.info("clicking on save button from calendar bottom sheet");
				awaitForElement(driver, bottomSheetSaveBtn);
				clickOnElement(bottomSheetSaveBtn);
				return new ProfilePage(driver);
			}catch (Exception e){
				throw new Exception("Error in clickOnSaveButtonFromCalenderBottomSheet()"+e.getMessage());
			}
		}
		
		@StepInfo(info="Fetching actual day value from date selection tab and converting String Actual Day Value to Integer")
		public int getActualDayValue() throws Exception   {
			try {
				ListenerImplimentationclass.testLog.info("Fetching actual day value from date selection tab and converting String Actual Day Value to Integer");
				return Integer.parseInt(getSelectedDOB()[0]);
			}catch (Exception e){
				throw new Exception("Error in getActualDayValue() "+e.getMessage());
			}

		}

		@StepInfo(info="Fetching actual Year value from date selection tab and converting String Actual Year Value to Integer")
		public int getActualYearValue() throws Exception   {
			try {
				ListenerImplimentationclass.testLog.info("Fetching actual Year value from date selection tab and converting String Actual Year Value to Integer");
				return Integer.parseInt(getSelectedDOB()[2]);
			}catch (Exception e){
				throw new Exception("Error in getActualDayValue() "+e.getMessage());
			}
		}
		
		@StepInfo(info="fetching selected DOB from date selection tab")
		public String[] getSelectedDOB() throws Exception {
			try {
				ListenerImplimentationclass.testLog.info("fetching selected DOB from date selection tab");
				awaitForElement(driver, birthDateSelectionTab);
				return birthDateSelectionTab.getText().split("-");
			}catch (Exception e){
				throw new Exception("Error in getSelectedDOB()"+e.getMessage());
			}
		}

		
		@StepInfo(info="clicking on other gender tab")
		public boolean isOtherGenderTabSelected() throws Exception {
			try {
				ListenerImplimentationclass.testLog.info("clicking on other gender tab");
				awaitForElement(driver, otherGenderTab);
				clickOnElement(otherGenderTab);
				return otherGenderTab.isEnabled();
			}catch (Exception e){
				throw new Exception("Error in clickOnOtherGenderTab()"+e.getMessage());
			}
		}
		
		@StepInfo(info="clicking on female gender tab")
		public boolean isFemaleGenderTabSelected() throws Exception {
			try {
				ListenerImplimentationclass.testLog.info("clicking on female gender tab");
				awaitForElement(driver, femaleGenderTab);
				clickOnElement(femaleGenderTab);
				return femaleGenderTab.isEnabled();
			}catch (Exception e){
				throw new Exception("Error in clickOnFemaleGenderTab()"+e.getMessage());
			}
		}		
		
		@StepInfo(info="clicking on male gender tab")
		public boolean isMaleGenderTabSelected() throws Exception {
			try {
				ListenerImplimentationclass.testLog.info("clicking on male gender tab");
				awaitForElement(driver, maleGenderTab);
				clickOnElement(maleGenderTab);
				return maleGenderTab.isEnabled();
			}catch (Exception e){
				throw new Exception("Error in clickOnMaleGenderTab()"+e.getMessage());
			}
		}	
		
		@StepInfo(info="fetching username text")
		public String getUserNameText() throws Exception {
			try {
			ListenerImplimentationclass.testLog.info("fetching username text");
			awaitForElement(driver, nameTextFeild);
			return nameTextFeild.getText();
			}catch (Exception e){
				throw new Exception("Error in getUserNameText()"+e.getMessage()); 
			}
		}

		@StepInfo(info="clicking on birth time selection tab")
		public void clickOnBirthTimeSelectionTab() throws Exception {
			try {
				ListenerImplimentationclass.testLog.info("clicking on birth time selection tab");
				awaitForElement(driver, birthTimeSelectionTab);
				clickOnElement(birthTimeSelectionTab);
				waitOrPause(5);
			}catch (Exception e){
				throw new Exception("Error in clickOnBirtTimeSelectionTab()"+e.getMessage());
			}
		}
		
		@StepInfo(info="swiping hour selection tab")
		public KundliPage swipeHourSelectionTab() throws Exception {
			try {
				ListenerImplimentationclass.testLog.info("swiping hour selection tab");
				awaitForElement(driver, bottomSheetSaveBtn);
				swipeByCoordinates(driver, 259, 1457, 259, 1023);
				return this;
			}catch (Exception e){
				throw new Exception("Error in swipeHourSelectionTab()"+e.getMessage());
			}
		}

		@StepInfo(info="swiping minute selection tab")
		public KundliPage swipeMinuteSelectionTab() throws Exception {
			try {
				ListenerImplimentationclass.testLog.info("swiping minute selection tab");
				swipeByCoordinates(driver, 538, 1457, 538, 1023);
				return this;
			}catch (Exception e){
				throw new Exception("Error in swipeMinuteSelectionTab()"+e.getMessage());
			}
		}
				
		@StepInfo(info="converting String Hour Value to Integer")
		public int getSelectedHourValue() throws Exception   {
			try {
				ListenerImplimentationclass.testLog.info("converting String Hour Value to Integer");
				return Integer.parseInt(getCurrentSelectedHour());
			}catch (Exception e){
				throw new Exception("Error in getSelectedHourValue() "+e.getMessage());
			}
       	}
		
		@StepInfo(info="converting String Minute Value to Integer")
		public int getSelectedMinuteValue() throws Exception   {
			try {
				ListenerImplimentationclass.testLog.info("converting String Minute Value to Integer");
				return Integer.parseInt(getCurrentSelectedMinute());
			}catch (Exception e){
				throw new Exception("Error in getSelectedMinuteValue() "+e.getMessage());
			}
        }
		
		@StepInfo(info="fetching current selected Hour from the Time bottom sheet")
		public String getCurrentSelectedHour() throws Exception {
			try {
				ListenerImplimentationclass.testLog.info("fetching current selected Hour from the Time bottom sheet");
				awaitForElement(driver, currentSelectedHour);
				return currentSelectedHour.getText();
			}catch (Exception e){
				throw new Exception("Error in getCurrentSelectedHour()"+e.getMessage());
			}
		}

		@StepInfo(info="fetching current selected Minute from the Time bottom sheet")
		public String getCurrentSelectedMinute() throws Exception {
			try {
				ListenerImplimentationclass.testLog.info("fetching current selected Minute from the Time bottom sheet");
				awaitForElement(driver, currentSelectedMinute);
				return currentSelectedMinute.getText();
			}catch (Exception e){
				throw new Exception("Error in getCurrentSelectedMinute()"+e.getMessage());
			}
		}
		
		@StepInfo(info="fetching selected time from time selection tab")
		public String[] getSelectedTime() throws Exception {
			try {
				ListenerImplimentationclass.testLog.info("fetching selected time from time selection tab");
				awaitForElement(driver,birthTimeSelectionTab );
				return birthTimeSelectionTab.getText().split(":");
			}catch (Exception e){
				throw new Exception("Error in getSelectedTime()"+e.getMessage());
			}
		}
		
		@StepInfo(info="Fetching actual Hour value from date selection tab and converting String Actual Hour Value to Integer")
		public int getActualHourValue() throws Exception   {
			try {
				ListenerImplimentationclass.testLog.info("Fetching actual Hour value from date selection tab and converting String Actual Hour Value to Integer");
				return Integer.parseInt(getSelectedTime()[0]);
			}catch (Exception e){
				throw new Exception("Error in  getActualHourValue() "+e.getMessage());
			}
		}

		@StepInfo(info="Fetching actual Minute value from date selection tab and converting String Actual Minute Value to Integer")
		public int getActualMinuteValue() throws Exception   {
			try {
				ListenerImplimentationclass.testLog.info("Fetching actual Minute value from date selection tab and converting String Actual Minute Value to Integer");
				return Integer.parseInt(getSelectedTime()[1].split(" ")[0]);
			}catch (Exception e){
				throw new Exception("Error in  getActualMinuteValue() "+e.getMessage());
			}
		}
		
		@StepInfo(info="clicking on Aage bade button")
		public KundliPage clickOnAgebadeBtn() throws Exception {
			try {
				ListenerImplimentationclass.testLog.info("clicking on Aage bade button");
				awaitForElement(driver, ageBadeBtn);
				clickOnElement(ageBadeBtn);
				return this;
			}catch (Exception e){
				throw new Exception("Error in clickOnAgebadeBtn()"+e.getMessage());
			}
        }
		
		@StepInfo(info="clicking on vapas jaaaye button")
		public KundliPage clickOnVapasJaayeBtn() throws Exception {
			try {
				ListenerImplimentationclass.testLog.info("clicking on vapas jaaaye button");
				awaitForElement(driver, vapasjaayeBtn);
				clickOnElement(vapasjaayeBtn);
				return this;
			}catch (Exception e){
				throw new Exception("Error in clickOnVapasJaayeBtn()"+e.getMessage());
			}
         }
		
		@StepInfo(info="checking confirmation popup displayed before submitting the kundli form ")
		public boolean isConfirmationPopupDisplayed() throws Exception
		{
			try {
				awaitForElement(driver, confirmationPopup);
				ListenerImplimentationclass.testLog.info("checking confirmation popup displayed before submitting the kundli form");
				return confirmationPopup.isDisplayed();
			}catch (Exception e)
			{
				throw new Exception("Error in isConfirmationPopupDisplayed()"+e.getMessage());
			}
		}
		
		@StepInfo(info="fetching name from confirmation pop up")
		public String getnameFromConfirmationPopup() throws Exception {
			try {
				ListenerImplimentationclass.testLog.info("fetching name from confirmation pop up");
				awaitForElement(driver, nameFromConfirmationPopup);
				return nameFromConfirmationPopup.getText();
			}catch (Exception e){
				throw new Exception("Error in getnameFromConfirmationPopup()"+e.getMessage());
			}
		}
		
		@StepInfo(info="fetching gender from confirmation gender")
		public String getGenderFromConfirmationPopup() throws Exception {
			try {
				ListenerImplimentationclass.testLog.info("fetching gender from confirmation gender");
				awaitForElement(driver, genderFromConfirmationPopup);
				return genderFromConfirmationPopup.getText();
			}catch (Exception e){
				throw new Exception("Error in getGenderFromConfirmationPopup()"+e.getMessage());
			}
		}
		
		
		@StepInfo(info="fetching location from confirmation popup")
		public String getLocationFromConfirmationPopup() throws Exception {
			try {
				ListenerImplimentationclass.testLog.info("fetching location from confirmation popup");
				awaitForElement(driver, locationFromConfirmationPopup);
				return locationFromConfirmationPopup.getText().split(",")[0];
			}catch (Exception e){
				throw new Exception("Error in getLocationFromConfirmationPopup()"+e.getMessage());
			}
		}
		
		@StepInfo(info="checking date and time on confirmation pop up  is displayed")
		public boolean isDateTimeOnConfirmationPopupDisplayed() throws Exception {
			try {
				awaitForElement(driver, datetimeonconfirmationpopup);
				ListenerImplimentationclass.testLog.info("checking date and time on confirmation pop up  is displayed");
				return datetimeonconfirmationpopup.isDisplayed();
			}catch (Exception e)
			{
				throw new Exception("Error in isDateTimeOnConfirmationPopupDisplayed()"+e.getMessage());
			}
	}
		
		@StepInfo(info="clicking on close button from time bottom sheet")
		public void clickOnCloseButtonFromTimeBottomSheet() throws Exception {
			try {
				ListenerImplimentationclass.testLog.info("clicking on close button from time bottom sheet");
				awaitForElement(driver, bottomSheetSaveBtn);
				clickOnElement(closeBtn);
			}catch (Exception e){
				throw new Exception("Error in clickOnCloseButtonFromTimeBottomSheet()"+e.getMessage());
			}
	  }
		
		@StepInfo(info="checking if Aage bade Btn is displayed")
		public boolean isAageBadeButtonDisplayed() throws Exception {
			try {
				ListenerImplimentationclass.testLog.info("checking if Aage bade Btn is displayed");
				awaitForElement(driver, ageBadeBtn);
				return ageBadeBtn.isDisplayed();
			}catch (Exception e){
				throw new Exception("Error in isSaveButtonDisplayed()"+e.getMessage());
			}
		}

		
		@StepInfo(info="checking if calendar bottom sheet is displayed")
		public boolean isCalendarBottomSheetDisplayed() throws Exception {
			try {
				ListenerImplimentationclass.testLog.info("checking if calendar bottom sheet is displayed");
				awaitForElement(driver, calendarBottomSheet);
				return calendarBottomSheet.isDisplayed();
			}catch (Exception e){
				throw new Exception("Error in isCalendarBottomSheetDisplayed()"+e.getMessage());
			}
		}
		
		@StepInfo(info="clicking on back button from kundli form page")
		public KundliPage clickOnBackBtnFromKundliFormPage() throws Exception {
			try {
				ListenerImplimentationclass.testLog.info("clicking on back button from kundli form page");
				awaitForElement(driver, backBtnFromKundliFormPage);
				clickOnElement(backBtnFromKundliFormPage);
				return this;
 		  }catch (Exception e){
				throw new Exception("Error in clickOnBackBtnFromKundliFormPage()"+e.getMessage());
		   }
	  }
		
		@StepInfo(info="checking if save information pop up  is displayed")
		public boolean isSaveInformationPopupDisplayed() throws Exception {
			try {
				ListenerImplimentationclass.testLog.info("checking if save information pop up  is displayed");
				awaitForElement(driver, saveInformationPopup);
				return saveInformationPopup.isDisplayed();
			}catch (Exception e){
				throw new Exception("Error in isSaveInformationPopupDisplayed()"+e.getMessage());
			}
		}
		
		@StepInfo(info="clicking on cancel Btn in confirmation pop up")
		public void clickOnCancelBtnInConfirmationPopup() throws Exception {
			try {
				ListenerImplimentationclass.testLog.info("clicking on cancel Btn in confirmation pop up");
				awaitForElement(driver, cancelBtnInConfirmationPopup);
				clickOnElement(cancelBtnInConfirmationPopup);
 		}catch (Exception e){
				throw new Exception("Error in clickOnCancelBtnInConfirmationPopup()"+e.getMessage());
			}
	    }
    
		@StepInfo(info="fetching kundli form page title ")
		public String getKundliFormPageTitle() throws Exception
		{
			try {
				ListenerImplimentationclass.testLog.info("fetching kundli form page title");
				awaitForElement(driver,getKundliFormPageTitle(ExcelUtility.getExcelData("locators", "kundliFormPageTitle", "value")));
				return getKundliFormPageTitle(ExcelUtility.getExcelData("locators", "kundliFormPageTitle", "value")).getText();
			}catch(Exception e) {
				throw new Exception("Error in getKundliFormPageTitle()"+e.getMessage());
			}
		}

   
     @StepInfo(info="clicking on cancel btn in save information popup")
     public KundliPage clickOnCancelBtnOnSaveInformationpopup() throws Exception
     {
    	 try {
    		 ListenerImplimentationclass.testLog.info("clicking on cancel btn in save information popup");
    		 awaitForElement(driver,dontSaveBtnInConfirmationPopup);
    		 clickOnElement(dontSaveBtnInConfirmationPopup);  
    		 return this;
        }catch(Exception e) {
    	 throw new Exception("Error in clickOnCancelBtnOnSaveInformationpopup()"+e.getMessage());
     }
    	 
     }
     
     @StepInfo(info="clicking on back btn from kundli report page")
     public void clickOnBackBtnfromKundliReportPage() throws Exception
     {
    	 try {
    		 ListenerImplimentationclass.testLog.info("clicking on back button from kundli report page");
    		 awaitForElement(driver, kundliBackBtn);
    		 clickOnElement(kundliBackBtn);
    	 }catch(Exception e) {
    		 throw new Exception("Error in clickOnBackBtnfromKundliReportPage()"+e.getMessage());
    	 }
     }
     
     
     
}
