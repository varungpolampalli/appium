package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ListenerImplimentationclass;
import io.appium.java_client.AppiumDriver;

public class SamudayPage extends AppGenericLib {
	ElementUtil elementUtil;
	public SamudayPage(AppiumDriver driver) 
	{
		this.driver=driver;
		 elementUtil=new ElementUtil(driver);
		 PageFactory.initElements(driver, this);
	}

	@FindBy(id="//android.widget.TextView[@resource-id='com.mandir.debug:id/menu_title']")
	private WebElement samudayaPageTitle;
	
	@StepInfo(info="Fetching Samudaya Page title")
	public String getSamudayaPageTitle() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("Fetching Samudaya Page title");
		awaitForElement(driver, samudayaPageTitle);
		}catch (Exception e){
			throw new Exception("Error in getSamudayaPageTitle"+e.getMessage());
		}
		return samudayaPageTitle.getText();
	}

}
