package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ExcelUtility;
import base.ListenerImplimentationclass;
import io.appium.java_client.AppiumDriver;

public class GeethaShlokPage extends AppGenericLib {


	ElementUtil elementUtil;
	public GeethaShlokPage(AppiumDriver driver)
	{
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}

	private WebElement GeethaShlokTab(String geethaShlokTabText)
	{
		String GeethaShlokXpathValue="//android.view.ViewGroup[@resource-id='com.mandir.debug:id/linear_root']/descendant::android.widget.TextView[@text='"+geethaShlokTabText+"'][1]";
		return elementUtil.getElement("xpath",GeethaShlokXpathValue);
	}

	private WebElement LokaPriyaTab(String lokaPriyaText)
	{
		String LokaPriyaTabtXpathValue="//android.widget.TextView[@text='"+lokaPriyaText+"']";
		return elementUtil.getElement("xpath",LokaPriyaTabtXpathValue);
	}

	private WebElement FirstAdyayTab(String firstAdyayText)
	{
		String FirstAdyayTabXpathValue="//android.widget.TextView[@text='"+firstAdyayText+"']";
		return elementUtil.getElement("xpath",FirstAdyayTabXpathValue);
	}

	private WebElement LastAdyayTab(String LastAdyayText)
	{
		String LastAdyayTabXpathValue="//android.widget.TextView[@text='"+LastAdyayText+"']";
		return elementUtil.getElement("xpath",LastAdyayTabXpathValue);
	}
	
	
	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_tabs']/android.widget.FrameLayout[1]/android.widget.TextView")
	private WebElement lokaPriyaTab;
	

	@FindBy(id="com.mandir.debug:id/menu_left")
	private WebElement menuBtn;
	
	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_container']/android.widget.FrameLayout[1]/child::android.widget.ImageView")
	private WebElement watsAppIcon;
	
	@FindBy(id="android:id/profile_tabhost")
	private WebElement ShareBottomSheet;
	
	//Buisness Libraries
	
	@StepInfo(info="Click on Geethashlok tab")
	public GeethaShlokPage clickOnGeethaShlokTab() throws Exception
	{
		ListenerImplimentationclass.testLog.info("Click on Geethashlok tab");
		try {
			awaitForElement(driver, GeethaShlokTab(ExcelUtility.getExcelData("locators", "geethaShlokTabText", "value")));
			clickOnElement(GeethaShlokTab(ExcelUtility.getExcelData("locators", "geethaShlokTabText", "value")));
			return this;
		} catch (Exception e) {
			throw new Exception("Error in clickOnGeethaShlokTab()"+e.getMessage());
		}
	}


	@StepInfo(info="Fetching geethShloka tab title")
	public String getGeethShlokaTabTitle() throws Exception
	{
		ListenerImplimentationclass.testLog.info("Fetching geethShloka tab title");
		try {
			awaitForElement(driver, GeethaShlokTab(ExcelUtility.getExcelData("locators", "geethaShlokTabText", "value")));
			return GeethaShlokTab(ExcelUtility.getExcelData("locators", "geethaShlokTabText", "value")).getText();
		} catch (Exception e) {
			throw new Exception("Error in getGeethShlokaTabTitle()"+e.getMessage());
		}
	}


	@StepInfo(info="scroll the adyay carousel")
	public void scrollAdyayCarousel() throws Exception
	{
		ListenerImplimentationclass.testLog.info("scroll the adyay carousel");
		try {
			Horizontalscroll(driver, 1040 , 430, 80, 430, 5);

		} catch (Exception e) {
			throw new Exception("Error in scrollAdyayCarousel()"+e.getMessage());
		}
	}


	@StepInfo(info="Scroll through adyay cards")
	public void scrollAdyayCards() throws Exception 
	{
		ListenerImplimentationclass.testLog.info("scroll the adyay carousel");
		try {
			scrollTillEnd();

		} catch (Exception e) {
			throw new Exception("Error in scrollAdyayCards()"+e.getMessage());		}

	}

	@StepInfo(info="fetching lokapriya tab Text")
	public GeethaShlokPage getLokaPriyaText() throws Exception
	{
		ListenerImplimentationclass.testLog.info("fetching lokapriya tab Text");
		try {
			awaitForElement(driver, LokaPriyaTab(ExcelUtility.getExcelData("locators", "lokaPriyaText", "value")));
			LokaPriyaTab(ExcelUtility.getExcelData("locators", "lokaPriyaText", "value")).getText();
			clickOnElement(LokaPriyaTab(ExcelUtility.getExcelData("locators", "lokaPriyaText", "value")));
			 return this;
		} catch (Exception e) {
			throw new Exception("Error in getLokaPriyaText()"+e.getMessage());	
		}
	}
	@StepInfo(info="fetching FirstAdyay tab Text")
	public String getFirstAdyayText() throws Exception
	{
		ListenerImplimentationclass.testLog.info("fetching FirstAdyay tab Text");
		try {
			awaitForElement(driver, FirstAdyayTab(ExcelUtility.getExcelData("locators", "firstAdyayText", "value")));
			clickOnElement(FirstAdyayTab(ExcelUtility.getExcelData("locators", "firstAdyayText", "value")));
			return FirstAdyayTab(ExcelUtility.getExcelData("locators", "firstAdyayText", "value")).getText();

		} catch (Exception e) {
			throw new Exception("Error in getFirstAdyayText()"+e.getMessage());	
		}
	}

	@StepInfo(info="fetching LastAdyay tab Text")
	public String getLastAdyayText() throws Exception
	{
		ListenerImplimentationclass.testLog.info("fetching LastAdyay tab Text");
		try {
			awaitForElement(driver, LastAdyayTab(ExcelUtility.getExcelData("locators", "LastAdyayText", "value")));
			clickOnElement(LastAdyayTab(ExcelUtility.getExcelData("locators", "LastAdyayText", "value")));
			return LastAdyayTab(ExcelUtility.getExcelData("locators", "LastAdyayText", "value")).getText();

		} catch (Exception e) {
			throw new Exception("Error in getLastAdyayText()"+e.getMessage());	
		}
	}

	
	@StepInfo(info="clicking on Hamburger menu")
	public void clickOnHamburgerMenuBtn() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on Hamburger menu");
		clickOnElement(menuBtn);
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnHamburgerMenuBtn()"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking geethaShloka tab is displayed")
	public boolean isGeethaShlokTabDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking geethaShloka tab  is displayed");
			awaitForElement(driver, GeethaShlokTab(ExcelUtility.getExcelData("locators", "geethaShlokTabText", "value")));
			return GeethaShlokTab(ExcelUtility.getExcelData("locators", "geethaShlokTabText", "value")).isDisplayed();
       }
		catch (Exception e){
			throw new Exception("Error in isGeethaShlokTabDisplayed()"+e.getMessage());
		}
	}
	


	@StepInfo(info="Clicking on WatsApp icon on Cards")
	public boolean isWatsApppIconDisplayed() throws Exception
	{
		ListenerImplimentationclass.testLog.info("Clicking on WatsApp icon on Cards");
		try {
			clickOnElement(watsAppIcon);
			return ShareBottomSheet.isDisplayed();

			
		} catch (Exception e) {
			throw new Exception("isWatsApppIconDisplayed()"+e.getMessage());	
		}	
}


}
