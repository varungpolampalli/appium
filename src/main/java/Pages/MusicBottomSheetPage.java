package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ExcelUtility;
import base.ListenerImplimentationclass;
import io.appium.java_client.AppiumDriver;

public class MusicBottomSheetPage extends AppGenericLib {

	public ElementUtil elementUtil;
	public MusicBottomSheetPage(AppiumDriver driver) 
	{
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id="com.mandir.debug:id/design_bottom_sheet")
	private WebElement musicBottomSheet;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/song_list']/child::android.view.ViewGroup[1]")
	private WebElement song;
	
	@FindBy(id="com.mandir.debug:id/song_categories_rv")
	private WebElement musicCategoryList;
	
	@FindBy(id="com.mandir.debug:id/song_thumbnail_bg_glow")
	private WebElement songThumbnail;
	
	@FindBy(id="com.mandir.debug:id/song_title_tv")
	private WebElement songTitle;
	
	@FindBy(id="com.mandir.debug:id/end_duration_tv")
	private WebElement audioLength;
		
	@FindBy(id="com.mandir.debug:id/lyrics_tv")
	private WebElement songLyrics;
	
	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/song_list']/	\r\n"
			+ "android.view.ViewGroup[1]/	\r\n"
			+ "android.widget.FrameLayout[@resource-id='com.mandir.debug:id/fl_kebab']/	\r\n"
			+ "android.widget.ImageView")
	private WebElement ellipse;
	
	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/song_list']/child::android.view.ViewGroup[1]/child::	\r\n"
			+ "android.widget.LinearLayout/android.widget.TextView")
	private WebElement songName;
	
	@FindBy(xpath="//android.view.ViewGroup[@resource-id='com.mandir.debug:id/no_internet_state_layout']//android.widget.TextView[1]")
    private WebElement noInternetConnectionMessage;
	
	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/song_list']/	\r\n"
			+ "android.view.ViewGroup[1]/	\r\n"
			+ "android.widget.FrameLayout[@resource-id='com.mandir.debug:id/fl_visible_action']/	\r\n"
			+ "android.widget.ImageView")
	private WebElement favoriteIcon;
	
	@FindBy(xpath="//android.widget.FrameLayout[@resource-id='com.mandir.debug:id/design_bottom_sheet']")
	private WebElement shareIconAndAlarmBottomSheet;


	private WebElement alarmIcon(String alarmLagayeText) {
		String alarmLagayeTextXpathValue="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_song_actions']//android.widget.TextView[@text='"+alarmLagayeText+"']";
		return elementUtil.getElement("xpath",alarmLagayeTextXpathValue );
	}

	private WebElement whatsappShareIcon(String shareKareText) {
		String shareKareTextXpathValue="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_song_actions']//android.widget.TextView[@text='"+shareKareText+"']";
		return elementUtil.getElement("xpath",shareKareTextXpathValue );
	}
		
	@FindBy(xpath="//android.widget.Button[@resource-id='com.mandir.debug:id/btn_dismiss']")
	private WebElement okBtnFromConfirmationPopUp;
	
	@FindBy(xpath="//android.widget.ImageView[@resource-id='com.mandir.debug:id/play_pause_iv']")
	private WebElement playOrPauseBtn;
	
	private WebElement sangeethKeBol(String sangeethKeBol) {
		String sangeethKeBolXpathValue="//android.widget.TextView[@text='"+sangeethKeBol+"']";
		return elementUtil.getElement("xpath",sangeethKeBolXpathValue );
	}
	
	@FindBy(id="android:id/content_preview_text")
	private WebElement messageInSharingBottomSheet;
	
	@FindBy(xpath="//android.widget.ImageView[@resource-id='com.mandir.debug:id/share_iv']")
	private WebElement whatsappShareIconInMainPlayer;
	
	@FindBy(xpath="//android.widget.ImageView[@resource-id='com.mandir.debug:id/ic_repeat_mode']")
	private WebElement repeatModeIcon;
	
	@FindBy(xpath = "//android.widget.FrameLayout[@resource-id='android:id/content']")
	private WebElement repeatModeOptionsPopUp;
	
	@FindBy(id="com.mandir.debug:id/btn_save")
	private WebElement saveBtnFromRepeatModeOptionsPopUp;
	
	@FindBy(id="com.mandir.debug:id/fab_cv")
	private WebElement mandirMeinSunoTab;
	
	@FindBy(xpath="//android.widget.HorizontalScrollView[@resource-id='com.mandir.debug:id/mandir_music_tab_layout']/	\r\n"
			+ "android.widget.LinearLayout[1]/	\r\n"
			+ "android.widget.LinearLayout[1]")
	private WebElement sangeethKaKramTab;
	
	@FindBy(id="com.mandir.debug:id/design_bottom_sheet")
	private WebElement logInBottomSheet;

	@FindBy(xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/song_list']/android.view.ViewGroup[1]//android.widget.ImageView[@resource-id='com.mandir.debug:id/lyrics_badge_iv']")
	private WebElement lyricsIcon;

	@FindBy(xpath = "//android.view.ViewGroup[@resource-id='com.mandir.debug:id/no_lyrics_cl']")
	private WebElement noLyricsMessage;

	//business libraries
	@StepInfo(info="checking if music bottom sheet is displayed")
	public boolean isMusicBottomsheetDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("Checking if music bottom sheet is displayed");
		}catch (Exception e){
			throw new Exception("Error in isMusicBttomSheetDisplayed()"+e.getMessage());
		}
		return musicBottomSheet.isDisplayed();
	}

	@StepInfo(info="clicking on respective music from the playlist")
	public void clickOnSong() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on respective song");
			clickOnElement(song);
		}catch (Exception e){
			throw new Exception("Error in clickOnSong"+e.getMessage());
		}
	}
	
	@StepInfo(info="Checking if music category is displayed")
	public boolean isMusicCategoriesDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if music category is displayed");
		awaitForElement(driver, musicCategoryList);
		}catch (Exception e){
			throw new Exception("Error in checkingIfMusicCategoryDisplayed"+e.getMessage());
		}
		return musicCategoryList.isDisplayed();
	}
	
	@StepInfo(info="checking if main player is displayed")
	public boolean isMainPlayerDisplayed() throws Exception {
		ListenerImplimentationclass.testLog.info("checking if the main player is displayed on playing song from the music bottom sheet");
		return songThumbnail.isDisplayed();
		
	}
	
	@StepInfo(info="checking if song Title is Displayed In The Main Player")
	public boolean isSongTitleDisplayed() {
		ListenerImplimentationclass.testLog.info("checking if the song title is displayed on Playing song ");
		return songTitle.isDisplayed();	
	}
	
	@StepInfo(info="checking if Audio Length is Displayed in The Main Player")
	public boolean isAudioLengthDisplayed() {
		ListenerImplimentationclass.testLog.info("checking if Audio length is displayed in the main Player");
		return audioLength.isDisplayed();
	}
	
	@StepInfo(info="Clicking on Alarm Icon")
	public AlarmSettingPage clickOnAlarmIcon() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("Clicking On Alarm Icon");
		awaitForElement(driver,alarmIcon(ExcelUtility.getExcelData("locators","alarmLagayeText","value")));
		clickOnElement(alarmIcon(ExcelUtility.getExcelData("locators","alarmLagayeText","value")));
		}catch (Exception e){
			throw new Exception("Error in clickOnAlarmIcon"+e.getMessage());
		}
		return new AlarmSettingPage(driver);
	}
	
	
	@StepInfo(info="clicking on lyricsTab")
	public MusicBottomSheetPage clickOnSangeethKeBol() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("Clicking On lyricsTab");
			clickOnElement(sangeethKeBol(ExcelUtility.getExcelData("locators", "sangeethKeBol", "value")));
		}catch (Exception e){
			throw new Exception("Error in clickingOnSangeethKeBol()"+e.getMessage());
		}
		return this;
	}
	
	@StepInfo(info="Checking if song lyrics is displayed")
	public boolean isSongLyricsDisplayed() throws Exception {
		try {
		 ListenerImplimentationclass.testLog.info("checking if the lyrics is displayed");
		 awaitForElement(driver,songLyrics);
		 return songLyrics.isDisplayed();
		}catch (Exception e) {
			return noLyricsMessage.isDisplayed();
		}
	}
	
	@StepInfo(info="fetching the Song name From the Music List")
	public String getSongNameFromMusicList() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("fetching song name from the music list");
		awaitForElement(driver, songName);
		}catch (Exception e) {
			throw new Exception("Error in getSongNameFromMusicList"+e.getMessage());
		}
		return songName.getText();
		
	}
	@StepInfo(info="clicking on Share Icon from the music bottom sheet")
	public MusicBottomSheetPage clickOnShareIcon() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on share icon from the music bottom sheet");
		awaitForElement(driver, whatsappShareIcon(ExcelUtility.getExcelData("locators","shareKareText","value")));
		clickOnElement(whatsappShareIcon(ExcelUtility.getExcelData("locators","shareKareText","value")));
		return this;
		}catch (Exception e) {
			throw new Exception("Error in clickOnShareIcon()"+e.getMessage());
		}
		
	}
	
	@StepInfo(info="checking if share bottom sheet is displayed")
	public boolean isMessageDisplayedInShareBottomSheet() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if share bottom sheet is displayed");
		awaitForElement(driver, messageInSharingBottomSheet);
		return messageInSharingBottomSheet.isDisplayed();
		}catch (Exception e) {
			throw new Exception("Error in isMessageDisplayedInShareBottomSheet"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if no internet connection message is displayed ")
	public boolean isNoInternetConnectionMessageDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if no internet connection message is displayed ");
			awaitForElement(driver, noInternetConnectionMessage);
			return noInternetConnectionMessage.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isNoInternetConnectionMessageDisplayed()"+e.getMessage()); 
		}
	}
	
	@StepInfo(info="clicking on ellipse from song")
	public void clickOnEllipse() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on ellipse from song");
		awaitForElement(driver, ellipse);
		clickOnElement(ellipse);
		}catch (Exception e){
			throw new Exception("Error in clickOnEllipse()"+e.getMessage()); 
		}
	}
	
	@StepInfo(info="clicking on favorite icon")
	public void clickOnFavoriteicon() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on favorite icon");
		awaitForElement(driver, favoriteIcon);
		clickOnElement(favoriteIcon);
		}catch (Exception e){
			throw new Exception("Error in clickOnFavoriteicon()"+e.getMessage()); 
		}
	}
	
	@StepInfo(info="checking if share and alarm bottom sheet is displayed on clicking ellipse from the song")
	public boolean isShareIconAndAlarmBottomSheetDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if share and alarm bottom sheet is displayed on clicking ellipse from the song");
		awaitForElement(driver, shareIconAndAlarmBottomSheet);
		return shareIconAndAlarmBottomSheet.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isShareIconAndAlarmBottomSheetDisplayed()"+e.getMessage()); 
		}
	}
	
	@StepInfo(info="clicking on ok buttom from confirmation pop up")
	public void clickOnOkBtnFromConfirmationPopup() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on ok buttom from confirmation pop up");
		awaitForElement(driver, okBtnFromConfirmationPopUp);
		clickOnElement(okBtnFromConfirmationPopUp);
		}catch (Exception e){
			throw new Exception("Error in clickOnOkBtnFromConfirmationPopup()"+e.getMessage()); 
		}
	}
	
	@StepInfo(info="clicking on play or pause button from the main player")
	public void clickOnPlayorPauseBtn() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on play or pause button from the main player");
		awaitForElement(driver, playOrPauseBtn);
		clickOnElement(playOrPauseBtn);
		}catch (Exception e){
			throw new Exception("Error in clickOnPlayorPauseBtn()"+e.getMessage()); 
		}
	}
	
	@StepInfo(info="clicking on whatsappShareIconFromMainPlayer")
	public void clickOnWhatsappShareIconFromMainPlayer() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on whatsappShareIconFromMainPlayer");
		awaitForElement(driver, whatsappShareIconInMainPlayer);
		clickOnElement(whatsappShareIconInMainPlayer);
		}catch (Exception e){
			throw new Exception("Error in clickOnWhatsappShareIconFromMainPlayer()"+e.getMessage()); 
		}
	}
	
	@StepInfo(info="clicking on repeat mode icon from the main player")
	public void clickOnRepeatModeIcon() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on repeat mode icon from the main player");
		awaitForElement(driver, repeatModeIcon);
		clickOnElement(repeatModeIcon);
		}catch (Exception e){
			throw new Exception("Error in clickOnRepeatModeIcon()"+e.getMessage()); 
		}
	}
	
	@StepInfo(info="checking if repeat mode options pop up displayed")
	public  boolean isRepeatModeOptionsDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if repeat mode options pop up displayed");
		awaitForElement(driver, repeatModeOptionsPopUp);
		return repeatModeOptionsPopUp.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isRepeatModeOptionsDisplayed()"+e.getMessage()); 
		}
	}
	
	@StepInfo(info="clicking on save button from repeat mode options pop up")
	public void clickOnSaveButtonFromRepeatModeOptionsPopUp() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on save button from repeat mode options pop up");
		awaitForElement(driver, saveBtnFromRepeatModeOptionsPopUp);
		clickOnElement(saveBtnFromRepeatModeOptionsPopUp);
		}catch (Exception e){
			throw new Exception("Error in clickOnSaveButtonFromRepeatModeOptionsPopUp()"+e.getMessage()); 
		}
	}
	
	@StepInfo(info="clicking on mandir mein suno tab from the main player")
	public void clickOnMandirMeinSunoTab() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on mandir mein suno tab from the main player");
		awaitForElement(driver, mandirMeinSunoTab);
		clickOnElement(mandirMeinSunoTab);
		}catch (Exception e){
			throw new Exception("Error in clickOnMandirMeinSunoTab()"+e.getMessage()); 
		}
	}
	
	@StepInfo(info="clicking on sangeeth ka kram tab")
	public MusicBottomSheetPage clickOnSangeethKaKramTab() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on sangeeth ka kram tab");
		awaitForElement(driver, sangeethKaKramTab);
		clickOnElement(sangeethKaKramTab);
		return this;
		}catch (Exception e){
			throw new Exception("Error in clickOnSangeethKaKramTab()"+e.getMessage()); 
		}
	}
	
	@StepInfo(info="checking if login bottom sheet is displayed")
	public boolean isLogInBottomSheetDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if login bottom sheet is displayed");
		awaitForElement(driver, logInBottomSheet);
		return logInBottomSheet.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isLogInBottomSheetDisplayed()"+e.getMessage()); 
		}
	}
}
