package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ExcelUtility;
import base.ListenerImplimentationclass;
import base.TestData;
import io.appium.java_client.AppiumDriver;

public class ChadavFeedPage extends AppGenericLib {

	ElementUtil elementUtil;
	public ChadavFeedPage(AppiumDriver driver) {
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id="com.mandir.debug:id/iv_back")
	private WebElement backBtn;
	
	@FindBy(id="com.mandir.debug:id/tv_category_header")
	private WebElement thirdCategoryHeader;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_stores_feed']/android.view.ViewGroup[1]//android.widget.FrameLayout[@resource-id='com.mandir.debug:id/frame_video_player_root']")
	private WebElement heroCard;
	
	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_categories']//android.view.ViewGroup[@resource-id='com.mandir.debug:id/constraint_root'][3]//androidx.cardview.widget.CardView[@resource-id='com.mandir.debug:id/cv_categories']")
	private WebElement thirdCategoryTag;
	
	@FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_info_slide_header']/following-sibling::androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/androidx.cardview.widget.CardView")
	private WebElement userReviewCard;
	
	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_stores_feed']/android.view.ViewGroup[1]//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_sub_title']")
	private WebElement heroCardDescription;
	
	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_stores_feed']/android.view.ViewGroup[1]//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_title']")
	private WebElement heroCardTitle;
	
	@FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_category_header']")
     private WebElement skuCardTitle;
	
	@FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_category_desc']")
    private WebElement skuCardDescription;

	private WebElement getChadavShevayeTabText(String chadavShevayeTabText)
	{
		String chadavShevayeTabTextexpathValue="//android.widget.HorizontalScrollView[@resource-id='com.mandir.debug:id/tabLayout']/android.widget.LinearLayout//android.widget.TextView[@text='"+chadavShevayeTabText+"']";
		return elementUtil.getElement("xpath", chadavShevayeTabTextexpathValue);
	}		

	@StepInfo(info="clicking on chadav shevaye Tab")
	public ChadavFeedPage clickOnChadavShevaye() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("clicking on chadav shevaye Tab");
			awaitForElement(driver,getChadavShevayeTabText(ExcelUtility.getExcelData("locators", "chadavShevayeTabText", "value")));
			clickOnElement(getChadavShevayeTabText(ExcelUtility.getExcelData("locators", "chadavShevayeTabText", "value")));
		}catch (Exception e){
			throw new Exception("Error in clickOnChadavShevaye()"+e.getMessage());
		}
		return this;
	}

	@StepInfo(info="clicking on Hero card ")
	public ItemListingPage clickOnHeroCard() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("clicking on Hero card");
			awaitForElement(driver, heroCard);
			clickOnElement(heroCard);
			return new ItemListingPage(driver);
		}catch (Exception e){
			throw new Exception("Error in clickOnHeroCard()"+e.getMessage());
		}		
	}		

	@StepInfo(info = "scrolling through the tab list")
	public void scrollThroughTab() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("scrolling through the tab list");
			scrollTillEnd();
		}catch (Exception e){
			throw new Exception("Error in scrollThroughTab()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking on back btn ")
	public void clickOnBackBtn() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("clicking on back btn");
			awaitForElement(driver, backBtn);
			clickOnElement(backBtn);
		}catch (Exception e){
			throw new Exception("Error in clickOnBackBtn()"+e.getMessage());
		}		
	}	
	
	@StepInfo(info = "scrolling till SKU cards")
	public ChadavFeedPage scrollingTillSKUCards() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("scrolling till SKU cards");
			swipeByCoordinates(driver, 414, 1709, 462, 438);
		}catch (Exception e){
			throw new Exception("Error in scrollingSKUCards()"+e.getMessage());
		}
		return this;
	}
	
	@StepInfo(info = "scrolling through SKU cards")
	public void scrollingThroughSKUCards() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("scrolling through SKU cards");
			swipeByCoordinates(driver, 818, 1187, 100, 1084);
		}catch (Exception e){
			throw new Exception("Error in scrollingThroughSKUCards()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking on third Category Tag ")
	public ChadavFeedPage clickOnThirdCategoryTag() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("clicking on third Category Tag ");
			awaitForElement(driver, thirdCategoryTag);
			clickOnElement(thirdCategoryTag);
		}catch (Exception e){
			throw new Exception("Error in clickOnThirdCategoryTag()"+e.getMessage());
		}	
		return this;
	}	
	
	@StepInfo(info="fetching categoryHeader text")
	public String getCategoryHeaderText() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("fetching categoryHeader text");
			awaitForElement(driver, thirdCategoryHeader);
			return thirdCategoryHeader.getText();
		}catch (Exception e){
			throw new Exception("Error in getCategoryHeaderText()"+e.getMessage()); 
		}
	}
	
	@StepInfo(info="checking if category header is displayed after clicking on third category tags")
	public boolean isCategoryHeaderDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if category header is displayed after clicking on third category tags");
			awaitForElement(driver, thirdCategoryHeader);
			return thirdCategoryHeader.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in  isCategoryHeaderDisplayed()"+e.getMessage()); 
		}
	}		
	
	@StepInfo(info = "scrolling till review section")
	public void scrollingTillReviewSection() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("scrolling till review section");
			swipeByCoordinates(driver, 414, 1622, 468, 922);
		}catch (Exception e){
			throw new Exception("Error in scrollingTillReviewSection()"+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if Review Card is Displayed ")
	public boolean isReviewCardDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if Review Card is Displayed ");
			awaitForElement(driver, userReviewCard);
			return userReviewCard.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in  isReviewCardDisplayed()"+e.getMessage()); 
		}
	}
		
	@StepInfo(info="checking if Herocard Description is Displayed")
	public boolean isHeroCardDescriptionDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if Herocard Description is Displayed");
			awaitForElement(driver, heroCardDescription);
			return heroCardDescription.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in  isHerocardDescriptionDisplayed()"+e.getMessage()); 
		}
	}
	
	@StepInfo(info="checking if Herocard Title is Displayed")
	public boolean isHeroCardTitleDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if Herocard Title is Displayed");
			awaitForElement(driver, heroCardTitle);
			return heroCardTitle.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in  isHeroCardTitleDisplayed()"+e.getMessage()); 
		}
	}
		
	@StepInfo(info="checking if SKU Card Title is Displayed")
	public boolean isSKUCardTitleDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if SKU Card Title is Displayed");
			awaitForElement(driver, skuCardTitle);
			return skuCardTitle.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in  isSKUCardTitleDisplayed()"+e.getMessage()); 
		}
	}
	
	@StepInfo(info="checking if SKU Card Description is Displayed")
	public boolean isSKUCardDescriptionDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if SKU Card Description is Displayed");
			awaitForElement(driver, skuCardDescription);
			return skuCardDescription.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in  isSKUCardDescriptionDisplayed()"+e.getMessage()); 
		}
	}	
		
	@StepInfo(info="scrolling to hero card ")
    public void scrollToFullHeroCard() {
        ListenerImplimentationclass.testLog.info("scrolling to hero card ");
        swipeByCoordinates(driver, 414,1598 ,452, 680);
    }


	@StepInfo(info="checking if hero card is displayed in feed tab")
    public boolean isHeroCardDisplayed() throws Exception {
        try {
            ListenerImplimentationclass.testLog.info("checking if hero card is displayed in feed tab");
            awaitForElement(driver, heroCard);
            return heroCard.isDisplayed();
        }catch (Exception e){
            throw new Exception("Error in  isHeroCardDisplayed()"+e.getMessage()); 
        }
    }
	
	@StepInfo(info="scrolling to hero card ")
	public void scrollToHeroCard() {
		ListenerImplimentationclass.testLog.info("scrolling to hero card ");
		swipeByCoordinates(driver, 498,1359 ,498, 1050);
	}
}

