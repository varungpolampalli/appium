package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ListenerImplimentationclass;
import base.TestData;
import io.appium.java_client.AppiumDriver;

public class TutorialCardPage extends AppGenericLib {

	ElementUtil elementUtil;
	public TutorialCardPage(AppiumDriver driver) {
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}	


	@FindBy(xpath="//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_help_icon']/ancestor::androidx.cardview.widget.CardView")
	private WebElement tutorialCard;

	@FindBy(id="com.mandir.debug:id/mobile_number")
	private WebElement mobileNumberTextFeild;

	@StepInfo(info="checking if TutorialCard  is displayed in feed tab")
	public boolean isTutorialCardDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if TutorialCard is displayed in feed tab");
			awaitForElement(driver, tutorialCard);
			return tutorialCard.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in  isTutorialCardDisplayed()"+e.getMessage()); 
		}
	}


	@StepInfo(info="clicking on tutorial card ")
	public void clickOnTutorialCard() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("clicking on tutorial card");
			awaitForElement(driver, tutorialCard);
			clickOnElement(tutorialCard);
		}catch (Exception e){
			throw new Exception("Error in clickOnTutorialCard()"+e.getMessage());
		}		
	}	

	@StepInfo(info="Typing Mobile Number to Mobile Number Text feild")
	public LoginBottomSheetPage enterMobileNumberOfUserWithNoOfferingOrder() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("Typing Mobile Number to Mobile Number Text feild");
			awaitForElement(driver, mobileNumberTextFeild);
			type(mobileNumberTextFeild, TestData.phoneNumberOfNoOfferingOrder);
			return new LoginBottomSheetPage(driver);
		}catch (Exception e){
			throw new Exception("Error in EnteringmobileNumber()"+e.getMessage());
		}
	}


}
