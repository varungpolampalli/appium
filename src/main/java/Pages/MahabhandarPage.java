package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ExcelUtility;
import base.ListenerImplimentationclass;
import base.PunyaMudraPopUpTest;
import io.appium.java_client.AppiumDriver;

public class MahabhandarPage extends AppGenericLib{
	ElementUtil elementUtil;

	public MahabhandarPage(AppiumDriver driver) {
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}
	
	RashifalPage rashifalPage=new RashifalPage(driver);
	@FindBy(id="com.mandir.debug:id/menu_title")
	private WebElement mahabhandarPageTitle;
	
	@FindBy(id="com.mandir.debug:id/snackbar_text")
	private WebElement noInternetConnectionSnackBarText;
	
	@FindBy(xpath="//android.widget.ImageView[@content-desc='no_internet_image']/following-sibling::android.widget.TextView[1]")
	private WebElement noInternetConnectionMessage;

	private WebElement panchangTab(String panchangText) {
		String panchangTabxpathValue="//android.widget.HorizontalScrollView[@resource-id='com.mandir.debug:id/tab_bar']//android.widget.TextView[@text='"+panchangText+"']";
		return elementUtil.getElement("xpath",panchangTabxpathValue );
	}

	private WebElement chowgadiyaTab(String chowgadiyaText) {
		String chowgadiyaTextxpathValue="//android.widget.TextView[@text='"+chowgadiyaText+"']";
		return elementUtil.getElement("xpath", chowgadiyaTextxpathValue);
	}

	@StepInfo(info="Fetching  mahabhandar page tilte")
	public String getMahabhandarPageTitle() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("Fetching  mahabhandar page tilte");
			awaitForElement(driver,mahabhandarPageTitle);
			return mahabhandarPageTitle.getText();
		}catch (Exception e){
			throw new Exception("Error in gettingMahabhandarPageTitle"+e.getMessage());
		}
	}

	@StepInfo(info="clicking On chowgadiya Tab")
	public ChowgadiyaPage clickOnChowgadiyaTab() throws Exception {
		try {
			awaitForElement(driver, chowgadiyaTab(ExcelUtility.getExcelData("locators", "chowgadiyaText", "value")));
			ListenerImplimentationclass.testLog.info("clicking on chowgadiya Tab");
			clickOnElement(chowgadiyaTab(ExcelUtility.getExcelData("locators", "chowgadiyaText", "value")));
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnChowgadiyaTab()"+e.getMessage());
		}
		return new ChowgadiyaPage(driver);
	}

	@StepInfo(info="clicking on panchang Tab")
	public PanchangPage clickOnPanchangTab() throws Exception {
		try {
			awaitForElement(driver, panchangTab(ExcelUtility.getExcelData("locators","panchangText", "value")));
			clickOnElement(panchangTab(ExcelUtility.getExcelData("locators", "panchangText", "value")));
			PunyaMudraPopUpTest.handlePunyaMudraPopUp(driver);
			ListenerImplimentationclass.testLog.info("clicking on panchang Tab");
			return new PanchangPage(driver);			
		}catch (Exception e)
		{
			throw new Exception("Error in clickOnPanchangTab()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if panchang option is displayed in mahaBhandar Page")
	public boolean isPanchangOptionDisplayed() throws Exception {
		try {
			awaitForElement(driver, panchangTab(ExcelUtility.getExcelData("locators", "panchangText", "value")));
			ListenerImplimentationclass.testLog.info("checking if panchang option is displayed in mahaBhandar Page");
			return panchangTab(ExcelUtility.getExcelData("locators", "panchangText", "value")).isDisplayed();
		}catch (Exception e)
		{
			throw new Exception("Error in isPanchangOptionDisplayed() "+e.getMessage());
		}
	}

	@StepInfo(info="Performing Swipe action ")
	public MahabhandarPage swipeHorizontalBar() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("Performing Swipe action");
			Horizontalscroll(driver, 230, 919, 960,922,4);
			PunyaMudraPopUpTest.handlePunyaMudraPopUp(driver);
			new RashifalPage(driver).handlingRashiBottomSheet();
			return this;
		}catch (Exception e)
		{
			throw new Exception("Error in swipeHorizontalBar() "+e.getMessage());
		}
	}
	
	@StepInfo(info="checking if no internet connection message is displayed in snackbar")
	public boolean isNoInternetConnectionSnakBarMessageDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if no internet connection message is displayed ");
			awaitForElement(driver, noInternetConnectionSnackBarText);
			return noInternetConnectionSnackBarText.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isNoInternetConnectionSnakBarMessageDisplayed() "+e.getMessage()); 
		}
	}
	
	@StepInfo(info="checking if no internet connection message is displayed ")
	public boolean isNoInternetConnectionMessageDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if no internet connection message is displayed ");
			awaitForElement(driver, noInternetConnectionMessage);
			return noInternetConnectionMessage.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isNoInternetConnectionMessageDisplayed()"+e.getMessage()); 
		}
	}

}
