package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ListenerImplimentationclass;
import io.appium.java_client.AppiumDriver;

public class GodSelectionPage extends AppGenericLib {
	public ElementUtil elementUtil;

	public GodSelectionPage(AppiumDriver driver) 
	{
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id="com.mandir.debug:id/btn_save")
	private WebElement mandirStapithKareBtn;
	
    @FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_god_list']/android.view.ViewGroup[1]//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_god_name']")
	private WebElement firstGodText;
    
    @FindBy(id="com.mandir.debug:id/btn_save")
	private WebElement cancelBtn;

	//business libraries

	@StepInfo(info="Clicking on save button in god selection page")
	public void clickOnGodSelectionSaveBtn() throws Exception
	{
		try 
		{
//			ListenerImplimentationclass.testLog.info("Clicking on save button in god selection page");
			awaitForElement(driver, mandirStapithKareBtn);
			clickOnElement(mandirStapithKareBtn);
		}catch (Exception e){
			throw new Exception("Error in clickOnGodSelectionSaveBtn()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if mandir stapith kare button is displayed in god selection page")
	public boolean isMandirStapithKareBtnDisplayed() throws Exception {
		try {
//		ListenerImplimentationclass.testLog.info("checking if mandir stapith kare button is displayed in god selection page");
		return mandirStapithKareBtn.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isMandirStapithKareBtnDisplayed()"+e.getMessage());
		}
	}
    
	
	@StepInfo(info="fetching gods first name from  god edit page")
	public String getGodNamefromGodEditPage() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("fetching gods first name from  god edit page");
			awaitForElement(driver, firstGodText);
			return firstGodText.getText().split(" ")[0];
		}catch (Exception e){
			throw new Exception("Error in getGodNamefromGodEditPage() "+e.getMessage());
		}
	}
	
	@StepInfo(info="Clicking on cancel button in god selection page")
	public void clickOnGodSelectionCancelBtn() throws Exception
	{
		try 
		{
			ListenerImplimentationclass.testLog.info("Clicking on cancel button in god selection page");
			awaitForElement(driver, cancelBtn);
			clickOnElement(cancelBtn);
		}catch (Exception e){
			throw new Exception("Error in clickOnGodSelectionCancelBtn()"+e.getMessage());
		}
	}
	
	
}
