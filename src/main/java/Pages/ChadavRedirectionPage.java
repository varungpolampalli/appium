package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ExcelUtility;
import base.ListenerImplimentationclass;
import io.appium.java_client.AppiumDriver;

public class ChadavRedirectionPage extends AppGenericLib {
	ElementUtil elementUtil;
	public ChadavRedirectionPage(AppiumDriver driver) {
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);

   }
	
	@FindBy(xpath="//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_offerings']")
	private WebElement  ChadavaIconInMandirpage;	
	
	@FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_toolbar_title']")
	private WebElement chadavTabTitle;
	
	@StepInfo(info="checking if chadav icon is displayed in mandir page")
	public boolean isChadavIconDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if chadav icon is displayed in mandir page");
		awaitForElement(driver, ChadavaIconInMandirpage);
		return ChadavaIconInMandirpage.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isChadavIconDisplayed()"+e.getMessage());
		}
	}	
	
	@StepInfo(info="clicking on chadav icon in mandir page")
	public void clickOnChadavIcon() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("clicking on chadav icon in mandir page");
			awaitForElement(driver, ChadavaIconInMandirpage);
			clickOnElement(ChadavaIconInMandirpage);
		}catch (Exception e){
			throw new Exception("Error in clickOnChadavIcon()"+e.getMessage());
		}
	}
	
	@StepInfo(info="fetching  chadav tab title ")
	public String getChadavTabTitle() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("fetching on chadav tab title");
			awaitForElement(driver,chadavTabTitle);
			return(chadavTabTitle.getText());
		}catch (Exception e){
			throw new Exception("Error in getChadavTabTitle()"+e.getMessage());
		}
	}	
}