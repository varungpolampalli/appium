package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ExcelUtility;
import base.ListenerImplimentationclass;
import base.PunyaMudraPopUpTest;
import io.appium.java_client.AppiumDriver;

public class PunyaMudraPage extends AppGenericLib {
	ElementUtil elementUtil;
	public PunyaMudraPage(AppiumDriver driver) {
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id="com.mandir.debug:id/iv_close")
	private WebElement backButton;

	@FindBy(xpath="//android.view.View[contains(@text,'श्री मंदिर')]")
	private WebElement sriMandirKulUpasthithiText;

	private WebElement punyaMudraPageTitleText(String punyaMudraTutorialPageTitle) {
		String punyaMudraTutorialPageTitlexpathValue="//android.view.View[@text='"+punyaMudraTutorialPageTitle+"']";
		return elementUtil.getElement("xpath", punyaMudraTutorialPageTitlexpathValue);
	}

	private WebElement getLoginTab(String loginKareText) {
		String loginKareTextxpathValue="//android.widget.TextView[@text='"+loginKareText+"']";
		return elementUtil.getElement("xpath", loginKareTextxpathValue);
	}

	private WebElement getPunyaKaiseKamayeTitle(String punyaKaiseKamayeTitle) {
		String punyaKaiseKamayeXpathValue="//android.widget.TextView[@text='"+punyaKaiseKamayeTitle+"']";
		return elementUtil.getElement("xpath", punyaKaiseKamayeXpathValue);
	}

	private WebElement teekHaiButton(String teekHaiButton) {
		String teekHaiButtonXpathValue="//android.view.View[@text='"+teekHaiButton+"']";
		return elementUtil.getElement("xpath", teekHaiButtonXpathValue);
	}

	private WebElement abhiSuneButtonButton(String abhiSuneButton) {
		String abhiSuneButtonXpathValue="//android.view.View[@text='"+abhiSuneButton+"']";
		return elementUtil.getElement("xpath", abhiSuneButtonXpathValue);
	}

	@FindBy(xpath="//android.view.ViewGroup[@resource-id='com.mandir.debug:id/player_ll']")
	private WebElement miniPlayer;

	@FindBy(id="com.mandir.debug:id/ic_mini_player_close")
	private WebElement closeMiniPlayer;

	@FindBy(id="com.mandir.debug:id/song_play_pause_button")
	private WebElement pauseOrPlayButton;

	@FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/punya_mudra_text']/following-sibling::android.widget.TextView")
	private WebElement coinsAvailableOnProfile;

	@FindBy(xpath="//android.view.View[@resource-id='app']/android.view.View[2]/descendant::android.view.View[2]")
	private WebElement coinsAvailableOnPunyaMudraPage;

	private WebElement dhekeApnaBhaktiChakraTab(String dhekeApnaBhaktiChakra) {
		String dhekeApnaBhaktiChakraTabXpathValue="//android.view.View[@content-desc='"+dhekeApnaBhaktiChakra+"']";
		return elementUtil.getElement("xpath", dhekeApnaBhaktiChakraTabXpathValue);
	}

	private WebElement aapkiPassBookTitle(String aapkiPassBook) {
		String aapkiPassBookTitleXpathValue="//android.widget.TextView[@text='"+aapkiPassBook+"']";
		return elementUtil.getElement("xpath", aapkiPassBookTitleXpathValue);
	}

	private WebElement dhekeAbTakPassBookTab(String dhekeAbTakPassBook) {
		String dhekeAbTakPassBookTabXpathValue="//android.view.View[@content-desc='"+dhekeAbTakPassBook+"']";
		return elementUtil.getElement("xpath", dhekeAbTakPassBookTabXpathValue);
	}
	private WebElement passBookTab(String passBookTab) {
		String passBookTabXpathValue="//android.view.View[@content-desc='"+passBookTab+"']";
		return elementUtil.getElement("xpath", passBookTabXpathValue);
	}
	private WebElement loginOnPassBookTab(String loginPassBookTab) {
		String loginOnPassBookTabXpathValue="//android.widget.TextView[@text='"+loginPassBookTab+"']";
		return elementUtil.getElement("xpath", loginOnPassBookTabXpathValue);
	}
	
	
	//Buisness Library

	@StepInfo(info="clicking on Back button from Punya Mudra page")
	public PunyaMudraPage clickOnBackButton() throws Exception
	{

		try {
			ListenerImplimentationclass.testLog.info("clicking on Back button from Punya Mudra page");
			awaitForElement(driver, backButton);
			clickOnElement(backButton);
			return this;
		}catch (Exception e){
			throw new Exception("Error in clickOnBackButton()"+e.getMessage());
		}
	}

	@StepInfo(info="Fetching  punya Mudra Tutorial page Title")
	public String getPunyaMudraPageTitle() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("Fetching  punya Mudra Tutorial page Title");
			awaitForElement(driver, punyaMudraPageTitleText(ExcelUtility.getExcelData("locators", "punyaMudraTutorialPageTitle", "value")));
		}catch (Exception e){
			throw new Exception("Error in getPunyaMudraTutorialpageTitle()"+e.getMessage());
		}
		return punyaMudraPageTitleText(ExcelUtility.getExcelData("locators", "punyaMudraTutorialPageTitle", "value")).getText();
	}

	@StepInfo(info="Fetching punya Kaise Kamaye Title")
	public String getpunyaKaiseKamayeTitle() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("Fetching punya Kaise Kamaye Title");
			awaitForElement(driver, getPunyaKaiseKamayeTitle(ExcelUtility.getExcelData("locators", "punyaKaiseKamayeTitle", "value")));
			return getPunyaKaiseKamayeTitle(ExcelUtility.getExcelData("locators", "punyaKaiseKamayeTitle", "value")).getText();
		}catch (Exception e){
			throw new Exception("Error in getBhakthiChakraTitle()"+e.getMessage());
		}
	}

	@StepInfo(info="Fetching  sri mandir kul upasthithiText")
	public String getSriMandirKulUpastithiTitle() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("Fetching  sri mandir kul upasthithiText");
			awaitForElement(driver, sriMandirKulUpasthithiText);
			return sriMandirKulUpasthithiText.getText();
		}catch (Exception e){
			throw new Exception("Error in getBhakthiChakraTitle()"+e.getMessage());
		}

	}


	@StepInfo(info="clicking on login tab from punya mudra page")
	public LoginBottomSheetPage clickOnLoginTabFromPunyamudraPage() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on login tab from punya mudra page");
			awaitForElement(driver, getLoginTab(ExcelUtility.getExcelData("locators", "loginKareText", "value")));
			clickOnElement(getLoginTab(ExcelUtility.getExcelData("locators", "loginKareText", "value")));
			return new LoginBottomSheetPage(driver);
		}catch (Exception e){
			throw new Exception("Error in clickOnLoginTabFromPunyamudraPage()"+e.getMessage()); 
		}
	}

	@StepInfo(info="checking if login tab is displayed in punya mudra page")
	public boolean IsclickOnLoginTabFromPunyamudraPageDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if login tab is displayed in punya mudra page");
			awaitForElement(driver, getLoginTab(ExcelUtility.getExcelData("locators", "loginKareText", "value")));
			return (getLoginTab(ExcelUtility.getExcelData("locators", "loginKareText", "value"))).isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in clickOnLoginTabFromPunyamudraPage()"+e.getMessage()); 
		}
	}
	@StepInfo(info="clicking on TeekHai Button")
	public PunyaMudraPage clickTeekHaiButton() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("clicking on TeekHai Button");
			awaitForElement(driver,teekHaiButton(ExcelUtility.getExcelData("locators", "teekHai", "value")));
			clickOnElement(teekHaiButton(ExcelUtility.getExcelData("locators", "teekHai", "value")));
			return this;
		}catch (Exception e){
			throw new Exception("Error in clickTeekHaiButton()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking on abhiSune Button")
	public PunyaMudraPage clickAbhiSuneButton() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("clicking on AbhiSune Button");
			awaitForElement(driver,abhiSuneButtonButton(ExcelUtility.getExcelData("locators", "abhiSune", "value")));
			clickOnElement(abhiSuneButtonButton(ExcelUtility.getExcelData("locators", "abhiSune", "value")));
			PunyaMudraPopUpTest.handlePunyaMudraPopUp(driver);
			return this;
		}catch (Exception e){
			throw new Exception("Error in clickAbhiSuneButton()"+e.getMessage());
		}
	}

	@StepInfo(info="verifying on abhiSune Button is displayed or not")
	public boolean isAbhiSuneButtonDisplayed() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("verifying on abhiSune Button is displayed or not");
			awaitForElement(driver,abhiSuneButtonButton(ExcelUtility.getExcelData("locators", "abhiSune", "value")));
			return abhiSuneButtonButton(ExcelUtility.getExcelData("locators", "abhiSune", "value")).isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in clickAbhiSuneButton()"+e.getMessage());
		}
	}

	@StepInfo(info="Verifying miniplayer displayed or not")
	public boolean isMiniPlayerDispalyed() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("Verifying miniplayer displayed or not");
			awaitForElement(driver,miniPlayer);
			return miniPlayer.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isMiniPlayerDispalyed()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking pause on MiniPlayer")
	public PunyaMudraPage clickPauseOnMiniPlayer() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("clicking pause on MiniPlayer");
			awaitForElement(driver,pauseOrPlayButton);
			clickOnElement(pauseOrPlayButton);
			return this;
		}catch (Exception e){
			throw new Exception("Error in clickPauseOnMiniPlayer()"+e.getMessage());
		}
	}

	@StepInfo(info="Click on close MiniPlayer")
	public PunyaMudraPage clickCloseMiniPlayer() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("Verifying miniplayer displayed or not");
			awaitForElement(driver,closeMiniPlayer);
			clickOnElement(closeMiniPlayer);
			return this;
		}catch (Exception e){
			throw new Exception("Error in clickCloseMiniPlayer()"+e.getMessage());
		}
	}

	@StepInfo(info="Fetching Coins Available On profile")
	public String getCoinsAvailableOnProfile() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("Fetching Coins Available On Mandir");
			awaitForElement(driver,coinsAvailableOnProfile);
			return coinsAvailableOnProfile.getText();
		}catch (Exception e){
			throw new Exception("Error in getCoinsAvailableOnMandir()"+e.getMessage());
		}
	}

	@StepInfo(info="Fetching Coins Available On PunyaMudraPage")
	public String getCoinsAvailableOnPunyaMudraPage() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("Fetching Coins Available On PunyaMudraPage");
			awaitForElement(driver,coinsAvailableOnPunyaMudraPage);
			return coinsAvailableOnPunyaMudraPage.getText();
		}catch (Exception e){
			throw new Exception("Error in getCoinsAvailableOnPunyaMudraPage()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking on dhekeApnaBhaktiChakra Tab ")
	public PunyaMudraPage clickOnDhekeApnaBhaktiChakra() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("Fetching Coins Available On PunyaMudraPage");
			awaitForElement(driver,dhekeApnaBhaktiChakraTab(ExcelUtility.getExcelData("locators", "dhekeApnaBhaktiChakra", "value")));
			clickOnElement(dhekeApnaBhaktiChakraTab(ExcelUtility.getExcelData("locators", "dhekeApnaBhaktiChakra", "value")));
			return this;
		}catch (Exception e){
			throw new Exception("Error in clickOnDhekeApnaBhaktiChakra()"+e.getMessage());
		}
	}


	@StepInfo(info="Fetching aapkiPassBook Title")
	public String aapkiPassBookTitle() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("Fetching aapkiPassBook Title");
			awaitForElement(driver,aapkiPassBookTitle(ExcelUtility.getExcelData("locators", "aapkiPassBook", "value")));
			return (aapkiPassBookTitle(ExcelUtility.getExcelData("locators", "aapkiPassBook", "value"))).getText();
		}catch (Exception e){
			throw new Exception("Error in aapkiPassBookTitle()"+e.getMessage());
		}
	}

	@StepInfo(info="Clicking on Dheke Abtak ki PassBook tab")
	public PunyaMudraPage clickDhekeAapkiPassBookTab() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("Clicking on Dheke Abtak ki PassBook tab");
			awaitForElement(driver,dhekeAbTakPassBookTab(ExcelUtility.getExcelData("locators", "dhekeAbTakPassBook", "value")));
			clickOnElement(dhekeAbTakPassBookTab(ExcelUtility.getExcelData("locators", "dhekeAbTakPassBook", "value")));
			return this;
		}catch (Exception e){
			throw new Exception("Error in clickAapkiPassBookTab()"+e.getMessage());
		}
	}
	@StepInfo(info="Clicking on PassBook tab")
	public PunyaMudraPage clickOnPassBookTab() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("Clicking on PassBook tab");
			awaitForElement(driver,passBookTab(ExcelUtility.getExcelData("locators", "passBook", "value")));
			clickOnElement(passBookTab(ExcelUtility.getExcelData("locators", "passBook", "value")));
			return this;
		}catch (Exception e){
			throw new Exception("Error in clickOnPassBookTab()"+e.getMessage());
		}
	}
	@StepInfo(info="Clicking on Login In PassBook tab")
	public LoginBottomSheetPage clickOnLoginInPassBookTab() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("Clicking on Login In PassBook tab");
			awaitForElement(driver,loginOnPassBookTab(ExcelUtility.getExcelData("locators", "loginKareInPassBook", "value")));
			clickOnElement(loginOnPassBookTab(ExcelUtility.getExcelData("locators", "loginKareInPassBook", "value")));
			return new LoginBottomSheetPage(driver);
		}catch (Exception e){
			throw new Exception("Error in clickOnLoginInPassBookTab()"+e.getMessage());
		}
	}
	@StepInfo(info="Login Kare In PassBook tab is dispalyed")
	public boolean IsLoginKareInPassBookTabDisplayed() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("Login Kare In PassBook tab is dispalyed");
			awaitForElement(driver,loginOnPassBookTab(ExcelUtility.getExcelData("locators", "loginKareInPassBook", "value")));
			return loginOnPassBookTab(ExcelUtility.getExcelData("locators", "loginKareInPassBook", "value")).isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in clickOnLoginInPassBookTab()"+e.getMessage());
		}
	}
}



