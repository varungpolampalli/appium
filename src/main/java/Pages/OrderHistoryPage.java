package Pages;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.FileUtility;
import base.ListenerImplimentationclass;
import base.TestData;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class OrderHistoryPage extends AppGenericLib {

	ElementUtil elementUtil;
	public  OrderHistoryPage(AppiumDriver driver) {
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}
	@FindBy(xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_offerings_transactions']/\t\n" +
			"android.view.ViewGroup[1]//android.view.ViewGroup[@resource-id='com.mandir.debug:id/constraint_item']//android.widget.FrameLayout[@resource-id='com.mandir.debug:id/fl_status']")
	private WebElement orderStatusBtn;

	@FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_book_offerings']")
	private WebElement logInTab;

	@FindBy(xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_offerings_transactions']/android.view.ViewGroup[1]/android.view.ViewGroup[1]")
	private WebElement firstOrderCard;

	@FindBy(id="com.mandir.debug:id/snack_bar")
	private WebElement chadavaBookKare;

	@FindBy(id="com.mandir.debug:id/cl_filter")
	private WebElement filterTab;

	@FindBy(id="com.mandir.debug:id/ic_filter_icon")
	private WebElement filterIcon;

	@FindBy(id="com.mandir.debug:id/ic_close")
	private WebElement closeFilterButton;

	@FindBy(xpath="//*[@resource-id='com.mandir.debug:id/design_bottom_sheet']/descendant::android.widget.TextView[@resource-id='com.mandir.debug:id/tv_filter_name'][1]")
	private WebElement sevenDaysHistoryFilterOptions;

	@FindBy(xpath="//*[@resource-id='com.mandir.debug:id/design_bottom_sheet']/descendant::android.widget.TextView[@resource-id='com.mandir.debug:id/tv_filter_name'][2]")
	private WebElement monthHistoryFilterOptions;

	@FindBy(xpath="//*[@resource-id='com.mandir.debug:id/design_bottom_sheet']/descendant::android.widget.TextView[@resource-id='com.mandir.debug:id/tv_filter_name'][3]")
	private WebElement allHistoryFilterOptions;

	@FindBy(xpath="//android.widget.FrameLayout[@resource-id='com.mandir.debug:id/design_bottom_sheet']")
	private WebElement filterBottomSheet;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_offerings_transactions']/descendant::android.view.ViewGroup[@resource-id='com.mandir.debug:id/constraint_item'][1]")
	private WebElement orderHistory;

	@FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_transactions_header']")
	private WebElement aaneWalaChadavaText;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_offerings_transactions']/descendant::android.view.ViewGroup[@resource-id='com.mandir.debug:id/constraint_item'][3]")
	private WebElement completedOrderHistory;

	@FindBy(xpath="//android.widget.TextView[contains(@text,'सम्पूर्ण')]/following-sibling::android.view.ViewGroup/android.widget.FrameLayout[@resource-id='com.mandir.debug:id/fl_status']")
	private WebElement greenTick;

	@FindBy(xpath = "//android.widget.TextView[contains(@text,'सम्पूर्ण')]/following-sibling::android.view.ViewGroup")
	private WebElement completedOfferingCard;
	
	@FindBy(xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_offerings_transactions']/android.view.ViewGroup[1]/android.view.ViewGroup[1]//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_date_day']")
	private WebElement orderDate;
	
	@FindBy(xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_offerings_transactions']/android.view.ViewGroup[1]/android.view.ViewGroup[1]//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_name']")
	private WebElement nameOnOrderCard;
	
	@FindBy(xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_offerings_transactions']/android.view.ViewGroup[1]/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_item_images']")
	private WebElement offeringItemImageOnOrderCard;
	
	@FindBy(xpath = "//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_empty_title']")
	private WebElement emptyTitle;
	
	@FindBy(xpath = "//android.widget.FrameLayout[@resource-id='com.mandir.debug:id/design_bottom_sheet']")
	private WebElement logInBottomSheet;
	
	@FindBy(id="com.mandir.debug:id/iv_close")
	private WebElement LoginBottomSheetCloseBtn;
	
	@FindBy(id="com.mandir.debug:id/mobile_number")
	private WebElement mobileNumberTextFeild;
	
	@StepInfo(info = "click on order status button")
	public void clickOnOrderStatusBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("click on order status button");
			awaitForElement(driver, orderStatusBtn);
			clickOnElement(orderStatusBtn);
		}catch (Exception e) {
			throw new Exception("Error in clickOnOrderStatusBtn()" + e.getMessage());
		}
	}

	@StepInfo(info = "clicking on login tab")
	public LoginBottomSheetPage clickOnLogInTab() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on login tab from order history page");
			awaitForElement(driver, logInTab);
			clickOnElement(logInTab);
			return new LoginBottomSheetPage(driver);
		}catch (Exception e) {
			throw new Exception("Error in clickOnLogInTab()" + e.getMessage());
		}
	}

	@StepInfo(info = "checking if login tab displayed")
	public boolean isLogInTabDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if login tab displayed");
			awaitForElement(driver, logInTab);
			return logInTab.isDisplayed();
		}catch (Exception e) {
			throw new Exception("Error in isLogInTabDisplayed()" + e.getMessage());
		}
	}

	@StepInfo(info = "clicking on first order card")
	public void clickOnFirstOrderCard() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on first order card");
			awaitForElement(driver, firstOrderCard);
			clickOnElement(firstOrderCard);
		}catch (Exception e) {
			throw new Exception("Error in clickOnFirstOrderCard()" + e.getMessage());
		}
	}

	@StepInfo(info = "checking if first order card displayed")
	public boolean isFirstOrderCardDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if first order card displayed");
			awaitForElement(driver, firstOrderCard);
			return firstOrderCard.isDisplayed();
		}catch (Exception e) {
			throw new Exception("Error in isFirstOrderCardDisplayed()" + e.getMessage());
		}
	}

	@StepInfo(info = "Clicking on Chadava Book Kare")
	public void clickingOnChadavaBookKare() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("Clicking on Chadava Book Kare");
			awaitForElement(driver, chadavaBookKare);
			clickOnElement(chadavaBookKare);
		}catch (Exception e) {
			throw new Exception("Error in clickingOnChadavaBookKare()" + e.getMessage());
		}
	}
	
	@StepInfo(info = "checking if completed offering card displayed")
    public boolean isCompletedOfferingCardDisplayed() throws Exception {
    	try {
    	 ListenerImplimentationclass.testLog.info("checking if completed offering card displayed");
    	 awaitForElement(driver, completedOfferingCard);
    	 return completedOfferingCard.isDisplayed();
    	}catch (Exception e) {
            throw new Exception("Error in isCompletedOfferingCardDisplayed()" + e.getMessage());
        }
    	
    }
	
	@StepInfo(info = "checking if green tick displayed in completed offering card")
	public boolean isGreenTickDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if green tick displayed in completed offering card");
		awaitForElement(driver, greenTick);
		return greenTick.isDisplayed();
		}catch (Exception e) {
            throw new Exception("Error in  isGreenTickDisplayed()" + e.getMessage());
        }
	}
	
	@StepInfo(info = "clicking on completed offering card")
    public void clickOnCompletedOfferingCard() throws Exception {
    	try {
    	 ListenerImplimentationclass.testLog.info("clicking on completed offering card");
    	 awaitForElement(driver, completedOfferingCard);
    	 clickOnElement(completedOfferingCard);
    	}catch (Exception e) {
            throw new Exception("Error in clickOnCompletedOfferingCard()" + e.getMessage());
        }
    	
    }
	
	@StepInfo(info = "checking if filter icon displayed")
    public boolean isFilterIconDisplayed() throws Exception {
    	try {
    	 ListenerImplimentationclass.testLog.info("checking if filter icon displayed");
    	 awaitForElement(driver, filterIcon);
    	 return filterIcon.isDisplayed();
    	}catch (Exception e) {
            throw new Exception("Error in isFilterIconDisplayed()" + e.getMessage());
        }
    	
    }
	
	@StepInfo(info = "clicking on filter tab")
	public void clickOnFilterTab() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on filter tab");
		awaitForElement(driver, filterTab);
		clickOnElement(filterTab);
		}catch (Exception e) {
            throw new Exception("Error in clickOnFilterTab()" + e.getMessage());
        }
	}
	
	@StepInfo(info = "checking if filter bottom sheet displayed on clicking filter icon")
	public boolean isFilterBottomSheetDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if filter bottom sheet displayed on clicking filter icon");
		awaitForElement(driver, filterBottomSheet);
		return filterBottomSheet.isDisplayed();
		}catch (Exception e) {
            throw new Exception("Error in isFilterBottomSheetDisplayed()" + e.getMessage());
        }
	}
	
	@StepInfo(info = "clicking on close button from filter bottom sheet")
	public void clickOnCloseButtonFromFilterBottomSheet() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on close button from filter bottom sheet");
		awaitForElement(driver, closeFilterButton);
		clickOnElement(closeFilterButton);
		}catch (Exception e) {
            throw new Exception("Error in clickOnCloseButtonFromFilterBottomSheet()" + e.getMessage());
        }
	}
	
	@StepInfo(info = "checking if sevenDays filter icon displayed")
	public boolean isSevendaysFilterIconDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if sevenDays filter icon displayed");
		awaitForElement(driver, sevenDaysHistoryFilterOptions);
		return sevenDaysHistoryFilterOptions.isDisplayed();
		}catch (Exception e) {
            throw new Exception("Error in  isSevendaysFilterIconDisplayed()" + e.getMessage());
        }
	}
	
	@StepInfo(info = "clicking on thirty day filter options")
	public void clickOnThirtyDaysFilterOptions() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on thirty day filter options");
		awaitForElement(driver, monthHistoryFilterOptions);
		clickOnElement(monthHistoryFilterOptions);
		}catch (Exception e) {
            throw new Exception("Error in  clickOnThirtyDaysFilterOptions()" + e.getMessage());
        }
	}
	
	@StepInfo(info = "clicking on all history filter options")
	public void clickOnAllHistoryFilterOptions() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on all history filter options");
		awaitForElement(driver, allHistoryFilterOptions);
		clickOnElement(allHistoryFilterOptions);
		}catch (Exception e) {
            throw new Exception("Error in  clickOnAllHistoryFilterOptions()" + e.getMessage());
        }
	}
	
	@StepInfo(info = "checking if aane wala chadav text is displayed")
	public boolean isAaneWalaChadavTextDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if aane wala chadav text is displayed");
		awaitForElement(driver, aaneWalaChadavaText);
		return aaneWalaChadavaText.isDisplayed();
		}catch (Exception e) {
            throw new Exception("Error in  getAaneWalaChadavText()" + e.getMessage());
        }
	}
	
	@StepInfo(info = "checking if order date displayed in order card")
	public boolean isOrderDateDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if order date displayed in order card");
		awaitForElement(driver, orderDate);
		return orderDate.isDisplayed();
		}catch (Exception e) {
            throw new Exception("Error in  isOrderDateDisplayed()" + e.getMessage());
        }
	}
	
	@StepInfo(info = "checking if name on order card is displayed")
	public boolean isNameOnOrderCardDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if name on order card is displayed");
		awaitForElement(driver, nameOnOrderCard);
		return nameOnOrderCard.isDisplayed();
		}catch (Exception e) {
            throw new Exception("Error in  isNameOnOrderCardDisplayed()" + e.getMessage());
        }
	}
	
	@StepInfo(info = "checking if offering item image displayed in order card")
	public boolean isOfferingItemImageDisplayedOnOfferingCard() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if offering item image displayed in order card");
		awaitForElement(driver, offeringItemImageOnOrderCard);
		return offeringItemImageOnOrderCard.isDisplayed();
		}catch (Exception e) {
            throw new Exception("Error in  isOfferingItemImageDisplayedOnOfferingCard()" + e.getMessage());
        }
	}
	
	@StepInfo(info = "checking if empty title is displayed in unlogged state")
	public boolean isEmptyTitleDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if empty title is displayed in unlogged state");
		awaitForElement(driver, emptyTitle);
		return emptyTitle.isDisplayed();
		}catch (Exception e) {
            throw new Exception("Error in  isEmptyTitleDisplayed()" + e.getMessage());
        }
	}
	
	@StepInfo(info = "checking if login bottom sheet is displayed")
	public boolean isLoginBottomSheetDisplayed() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("checking if login bottom sheet is displayed");
		awaitForElement(driver, logInBottomSheet);
		return logInBottomSheet.isDisplayed();
		}catch (Exception e) {
            throw new Exception("Error in  isLoginBottomSheetDisplayed()" + e.getMessage());
        }
	}
	
	@StepInfo(info = "fetching chadav bookkare text ")
	public String getChadavBookKareText() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("fetching chadav bookkare text ");
		awaitForElement(driver, logInTab);
		return logInTab.getText();
		}catch (Exception e) {
            throw new Exception("Error in  getChadavBookKareText() " + e.getMessage());
        }
	}
	

	@StepInfo(info = "clicking on chadav book kare text")
	public void clickOnChadavBookKareTab() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on chadav book kare text");
			awaitForElement(driver, logInTab);
			clickOnElement(logInTab);
		}catch (Exception e) {
			throw new Exception("Error in clickOnChadavBookKareTab() " + e.getMessage());
		}
	}
	
	@StepInfo(info = "clikcing on login bottom sheet close button")
	public void clickOnLoginBottomSheetCloseBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clikcing on login bottom sheet close button");
			awaitForElement(driver, LoginBottomSheetCloseBtn);
			clickOnElement(LoginBottomSheetCloseBtn);
		}catch (Exception e) {
			throw new Exception("Error in clickOnLoginBottomSheetCloseBtn() " + e.getMessage());
		}
	}
	
	@StepInfo(info="Typing Mobile Number to Mobile Number Text feild")
	public LoginBottomSheetPage enterMobileNumberOfUserWithNoOfferingOrder() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("Typing Mobile Number to Mobile Number Text feild");
		awaitForElement(driver, mobileNumberTextFeild);
		type(mobileNumberTextFeild, TestData.phoneNumberOfNoOfferingOrder);
		return new LoginBottomSheetPage(driver);
		}catch (Exception e){
			throw new Exception("Error in EnteringmobileNumber()"+e.getMessage());
		}
	}
		
}
