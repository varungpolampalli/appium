package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ExcelUtility;
import base.ListenerImplimentationclass;
import io.appium.java_client.AppiumDriver;

public class SuvicharPage  extends AppGenericLib{

	ElementUtil elementUtil;
	public SuvicharPage(AppiumDriver driver)
	{
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}


	private WebElement SuvicharTab(String suvicharTabText  ) {
		String SuvicharTextxpathValue="//android.widget.TextView[@text='"+suvicharTabText+"']";
		return elementUtil.getElement("xpath", SuvicharTextxpathValue);
	}

	private WebElement AajKaSuvicharCarousel(String aajKaSuvicharCarousel)
	{
		String aajKaSuvicharCarouselXpathValue="//android.widget.TextView[@text='"+aajKaSuvicharCarousel+"']";
		return elementUtil.getElement("xpath",aajKaSuvicharCarouselXpathValue);
	}

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_container']/child::android.widget.FrameLayout[1]/android.widget.FrameLayout")
	private WebElement aajKaSuvicharCard;

	@FindBy(xpath="//android.widget.LinearLayout[@resource-id='com.mandir.debug:id/ll_header']/descendant::android.widget.TextView[2]")
	private WebElement suprabathCarousel;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_container']/descendant::android.widget.ImageView[@resource-id='com.mandir.debug:id/suvichar_image'][1]")
	private WebElement suprabathCard;

	@FindBy(xpath="//android.widget.LinearLayout[@resource-id='com.mandir.debug:id/ll_header']/descendant::android.widget.TextView[3]")
	private WebElement shubSandhyaCarousel;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_container']/descendant::android.widget.ImageView[@resource-id='com.mandir.debug:id/suvichar_image'][1]")
	private WebElement shubSandhyaCard;

	@FindBy(xpath="//android.widget.LinearLayout[@resource-id='com.mandir.debug:id/ll_header']/descendant::android.widget.TextView[4]")
	private WebElement shubRathriCarousel;

	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_container']/descendant::android.widget.ImageView[@resource-id='com.mandir.debug:id/suvichar_image'][1]")
	private WebElement shubRatriCard;
	
	@FindBy(xpath="//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_footer']/following-sibling::android.widget.ImageView")
	private WebElement aajKaSuvicharCardwatsappIcon;
	
	@FindBy(xpath="//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_container']/android.widget.FrameLayout[1]//android.widget.ImageView[2]")
	private WebElement watsAppIcon;

	@FindBy(id="android:id/profile_tabhost")
	private WebElement ShareBottomSheet;

	
	
	
	
	
	//Buisness Libraries
	@StepInfo(info="fetching the suvichaar sub text ")
	public String getSuvicharTabText() throws Exception {
		try { 
			ListenerImplimentationclass.testLog.info("fetching the suvichar sub text ");
			awaitForElement(driver, SuvicharTab(ExcelUtility.getExcelData("locators", "suvicharTabText", "value")));
			return SuvicharTab(ExcelUtility.getExcelData("locators", "suvicharTabText", "value")).getText();
		}
		catch (Exception e)
		{
			throw new Exception("Error in  getSuvicharTabText()"+e.getMessage());
		}
	}

	@StepInfo(info="Clicking on suvicharTab")

	public SuvicharPage clickOnSuvicharTab() throws Exception
	{
		ListenerImplimentationclass.testLog.info("fetching the suvichar sub text ");
		try {	

			awaitForElement(driver,SuvicharTab(ExcelUtility.getExcelData("locators", "suvicharTabText", "value")));
			clickOnElement(SuvicharTab(ExcelUtility.getExcelData("locators", "suvicharTabText", "value")));
			return this;
		}
		catch (Exception e)
		{
			throw new Exception("Error in Clicking on suvicharTab()"+e.getMessage());
		}
	}	
	@StepInfo(info="Scroll to suvicharTab from other tab")
	public SuvicharPage scrollToSuvicharTab() throws Exception
	{
		ListenerImplimentationclass.testLog.info("Scroll to suvicharTab from other tab");
		try { 
			Horizontalscroll(driver,1000, 430, 100, 430, 4);
			return this;

		} catch (Exception e) {
			throw new Exception("Error in scrollToSuvichar()"+e.getMessage());
		}
	}

	@StepInfo(info="Click on aaj ka suvichar carousel")
	public SuvicharPage clickOnAajKaSuvichar() throws Exception
	{
		ListenerImplimentationclass.testLog.info("Click on aaj ka suvichar carousel");
		try {
			awaitForElement(driver, AajKaSuvicharCarousel(ExcelUtility.getExcelData("locators", "aajKaSuvicharCarousel", "value")));
			clickOnElement(AajKaSuvicharCarousel(ExcelUtility.getExcelData("locators", "aajKaSuvicharCarousel", "value")));
			return this;
		} catch (Exception e) {
			throw new Exception("Error in clickOnAajKaSuvichar()"+e.getMessage());
		}
	}

	@StepInfo(info="Verify aaj ka suvichar card is displyed")
	public boolean IsAajKaSuvicharCardDisplayed() throws Exception
	{
		ListenerImplimentationclass.testLog.info("Verify aaj ka suvichar card");
		try {
			aajKaSuvicharCard.isDisplayed();
			scrollTillEnd();
			return aajKaSuvicharCard.isDisplayed();
		} catch (Exception e) {
			throw new Exception("Error in IsAajKaSuvicharCardIsDisplayed()"+e.getMessage());
		}


	}

	@StepInfo(info="Verify aaj ka suvichar carousel text")
	public String getAajKaSuvicharText() throws Exception
	{
		ListenerImplimentationclass.testLog.info("Verify aaj ka suvichar card");
		try {
			awaitForElement(driver,AajKaSuvicharCarousel(ExcelUtility.getExcelData("locators", "aajKaSuvicharCarousel", "value")));
			return AajKaSuvicharCarousel(ExcelUtility.getExcelData("locators", "aajKaSuvicharCarousel", "value")).getText();
		} catch (Exception e) {
			throw new Exception("Error in getAajKaSuvicharText()"+e.getMessage());
		}


	}

	@StepInfo(info="Scroll through Aaj ka suvichar cards")
	public boolean ScrollThroughAajKaSuvicharCards() throws Exception
	{
		ListenerImplimentationclass.testLog.info("Scroll through Aaj ka suvichar cards");
		try {
			scrollTillEnd();
			return AajKaSuvicharCarousel(ExcelUtility.getExcelData("locators", "aajKaSuvicharCarousel", "value")).isDisplayed();

		} catch (Exception e) {
			throw new Exception("Error in ScrollThroughAajKaSuvicharCards()"+e.getMessage());		}
	}

	@StepInfo(info="Click on Suprabath carousel")
	public String ClickOnSuprabathCarousel() throws Exception
	{
		ListenerImplimentationclass.testLog.info("Click on Suprabath carousel");
		try {
			suprabathCarousel.click();
			return suprabathCarousel.getText();

		} catch (Exception e) {

			throw new Exception("Error in ClickOnSuprabathCarousel()"+e.getMessage());	
		}

	}

	@StepInfo(info="Verify suprabath card is displayed")
	public boolean isSuprabathCardDisplayed() throws Exception
	{
		ListenerImplimentationclass.testLog.info("Verify suprabath card is displayed");
		try {

			suprabathCard.isDisplayed();
			scrollTillEnd();
			return suprabathCard.isDisplayed();
		} catch (Exception e) {

			throw new Exception("Error in IsSuprabathCardDisplayed()"+e.getMessage());	
		}

	}
	@StepInfo(info="Click on ShubSandhya carousel")
	public String clickOnShubSandhyaCarousel() throws Exception
	{
		ListenerImplimentationclass.testLog.info("Click on ShubSandhya carousel");
		try {
			shubSandhyaCarousel.click();
			return shubSandhyaCarousel.getText();
		} catch (Exception e) {
			throw new Exception("clickOnShubSandhyaCarousel()"+e.getMessage());	
		}

	}

	@StepInfo(info="Verify ShubSandhya card is displayed")
	public boolean isShubSandhyaCardDisplayed() throws Exception
	{
		ListenerImplimentationclass.testLog.info("Verify ShubSandhya card is displayed");
		try {
			shubSandhyaCard.isDisplayed();
			scrollTillEnd();
			return shubSandhyaCard.isDisplayed();
		} catch (Exception e) {
			throw new Exception("IsShubSandhyaCardDisplayed()"+e.getMessage());	
		}

	}


	@StepInfo(info="Click on ShubRatri carousel")
	public String clickOnShubRatriCarousel() throws Exception
	{
		ListenerImplimentationclass.testLog.info("Click on ShubRatri carousel");
		try {

			shubRathriCarousel.click();
			return shubRathriCarousel.getText();
		} catch (Exception e) {
			throw new Exception("clickOnShubRatriCarousel()"+e.getMessage());	
		}

	}

	@StepInfo(info="Verify ShubRatri card is displayed")
	public boolean isShubRatriCardDisplayed() throws Exception
	{
		ListenerImplimentationclass.testLog.info("Verify ShubRatri card is displayed");
		try {
			shubRatriCard.isDisplayed();
			scrollTillEnd();

			return shubRatriCard.isDisplayed();
		} catch (Exception e) {
			throw new Exception("IsShubRatriCardDisplayed()"+e.getMessage());	
		}

	}
	
	@StepInfo(info="checking suvichaar tab is displayed")
	public boolean isSuvichaarTabDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking suvichaar tab  is displayed");
			awaitForElement(driver, SuvicharTab(ExcelUtility.getExcelData("locators", "suvicharTabText", "value")));
			return SuvicharTab(ExcelUtility.getExcelData("locators", "suvicharTabText", "value")).isDisplayed();
		}
		catch (Exception e){
			throw new Exception("Error in isSuvichaarTabDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="Clicking on watsApp icon for AajKaSuvicharCard ")
	public boolean clickOnAajKaSuvicharCardWatsApppIcon() throws Exception
	{
		ListenerImplimentationclass.testLog.info("Clicking on watsApp icon for AajKaSuvicharCard ");
		try {
			aajKaSuvicharCardwatsappIcon.click();
			return ShareBottomSheet.isDisplayed();

			
		} catch (Exception e) {
			throw new Exception("clickOnWatsApppIcon()"+e.getMessage());	
		}	
}
	@StepInfo(info="Clicking on WatsApp icon on Cards")
	public boolean clickCardWatsApppIcon() throws Exception
	{
		ListenerImplimentationclass.testLog.info("Clicking on WatsApp icon on Cards");
		try {
			watsAppIcon.click();
			return ShareBottomSheet.isDisplayed();

			
		} catch (Exception e) {
			throw new Exception("clickCardWatsApppIcon()"+e.getMessage());	
		}	
}
	
	
}



