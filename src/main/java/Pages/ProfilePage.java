package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.AppGenericLib;
import base.ElementUtil;
import base.ExcelUtility;
import base.ListenerImplimentationclass;
import io.appium.java_client.AppiumDriver;

public class ProfilePage extends AppGenericLib {
	ElementUtil elementUtil;
	public ProfilePage(AppiumDriver driver) {
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id="com.mandir.debug:id/back_button_kundli_form")
	private WebElement backBtn;

	@FindBy(id="com.mandir.debug:id/share_kundli")
	private WebElement editBtn;

	@FindBy(id="com.mandir.debug:id/username")
	private WebElement userName;

	@FindBy(id="com.mandir.debug:id/punya_mudra_text")
	private WebElement punyaMudraTab;

	@FindBy(id="com.mandir.debug:id/bhakti_chakra_text")
	private WebElement bhaktiChakraTab;

	@FindBy(xpath="//android.widget.TextView[@resource-id='com.mandir.debug:id/punya_mudra_text']/following-sibling::android.widget.TextView")
	private WebElement punyaMudraCountText;

	@FindBy(id="com.mandir.debug:id/snackbar_text")
	private WebElement noInternetConnectionSnackBarText;

	@FindBy(id="com.mandir.debug:id/design_bottom_sheet")
	private WebElement alarmPermissionBottomSheet;
	
	@FindBy(id="com.mandir.debug:id/tv_allow")
	private WebElement alarmPermissionAllowBtn;
	
	@FindBy(id="android:id/button1")
	private WebElement appAllowBtn;
	
	@FindBy(xpath="//android.widget.ImageView[@content-desc='no_internet_image']/following-sibling::android.widget.TextView[1]")
	private WebElement noInternetConnectionMessage;

	private WebElement getprofilePageTitle(String profilePageText) {
		String profilePageTextxpathValue="//android.widget.TextView[@text='"+profilePageText+"']";
		return elementUtil.getElement("xpath", profilePageTextxpathValue);
	}

	private WebElement getKundliTab(String kundliReportText) {
		String kundliReportTextxpathValue="//android.widget.TextView[@text='"+kundliReportText+"']";
		return elementUtil.getElement("xpath", kundliReportTextxpathValue);
	}

	private WebElement getAapkeAlarmTab(String aapkeAlarmText) {
		String aapkeAlarmTextxpathValue="//android.widget.TextView[@text='"+aapkeAlarmText+"']";
		return elementUtil.getElement("xpath", aapkeAlarmTextxpathValue);
	}

	private WebElement getChadavSevaTab(String chadavSevaText) {
		String chadavSevaTextxpathValue="//android.widget.TextView[@text='"+chadavSevaText+"']";
		return elementUtil.getElement("xpath", chadavSevaTextxpathValue);
	}

	private WebElement getLoginTab(String loginKareText) {
		String loginKareTextxpathValue="//android.widget.TextView[@text='"+loginKareText+"']";
		return elementUtil.getElement("xpath", loginKareTextxpathValue);
	}

	@StepInfo(info="fetching profile page title")
	public String getprofilePageTitle() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("fetching profile page title");
			awaitForElement(driver, getprofilePageTitle(ExcelUtility.getExcelData("locators", "profilePageTitleText", "value")));
			return  getprofilePageTitle(ExcelUtility.getExcelData("locators", "profilePageTitleText", "value")).getText();
		}catch (Exception e){
			throw new Exception("Error in getprofilePageTitle()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking on back button from my profile page")
	public void clickOnBackBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking back button from my profile page");
			awaitForElement(driver, backBtn);
			clickOnElement(backBtn);
			waitOrPause(2);
		}catch (Exception e){
			throw new Exception("Error in clickOnBackBtn()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking on edit profile button")
	public EditProfilePage clickOnEditBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on edit profile button");
			awaitForElement(driver, editBtn);
			clickOnElement(editBtn);
			return new EditProfilePage(driver);
		}catch (Exception e){
			throw new Exception("Error in clickOnEditBtn()"+e.getMessage()); 
		}
	}

	@StepInfo(info="fetching username text")
	public String getUserNameText() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("fetching username text");
			awaitForElement(driver, userName);
			return userName.getText();
		}catch (Exception e){
			throw new Exception("Error in getUserNameText()"+e.getMessage()); 
		}
	}

	@StepInfo(info="clicking on punya mudra tab")
	public PunyaMudraPage clickOnPunyamudraTab() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on punya mudra tab");
			awaitForElement(driver, punyaMudraTab);
			clickOnElement(punyaMudraTab);
			return new PunyaMudraPage(driver);
		}catch (Exception e){
			throw new Exception("Error in clickOnPunyamudraTab()"+e.getMessage()); 
		}

	}

	@StepInfo(info="clicking on bhakti chakra tab")
	public ProfilePage clickOnBhaktiChakraTab() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on bhakti chakra tab");
			awaitForElement(driver, bhaktiChakraTab);
			clickOnElement(bhaktiChakraTab);
			return this;
		}catch (Exception e){
			throw new Exception("Error in clickOnBhaktiChakraTab()"+e.getMessage()); 
		}

	}

	@StepInfo(info="clicking on Kundli Report")
	public ProfilePage clickOnKundliReportTab() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on Kundli Report");
			awaitForElement(driver, getKundliTab(ExcelUtility.getExcelData("locators", "kundliReportText", "value")));
			clickOnElement(getKundliTab(ExcelUtility.getExcelData("locators","kundliReportText", "value")));
			return this;
		}catch (Exception e){
			throw new Exception("Error in clickOnKundliReportTab()"+e.getMessage()); 
		}

	}

	@StepInfo(info="clicking on Aapke Alarm")
	public ProfilePage clickOnAapkeAlarm() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on Aapke Alarm");
			awaitForElement(driver, getAapkeAlarmTab(ExcelUtility.getExcelData("locators", "alarmPageTitle", "value")));
			clickOnElement(getAapkeAlarmTab(ExcelUtility.getExcelData("locators","alarmPageTitle", "value")));
			return this;
		}catch (Exception e){
			throw new Exception("Error in clickOnAapkeAlarm()"+e.getMessage()); 
		}

	}

	@StepInfo(info="checking if alarm Permission bottom sheet displayed")
	public void handleAlarmPermissionBottomSheet() {
		try {
			ListenerImplimentationclass.testLog.info("checking if alarm Permission bottom sheet displayed");
			if (alarmPermissionBottomSheet.isDisplayed()) {
				clickOnElement(alarmPermissionAllowBtn);
				waitOrPause(3);
				clickOnElement(appAllowBtn);
			}
		}catch (Exception e) {
			logger.info("Alarm permission bottom sheet is not displayed");
		}

	}

	@StepInfo(info="clicking on chadav Seva Tab")
	public ProfilePage clickOnChadavSevaTab() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on chadav Seva Tab");
			awaitForElement(driver, getChadavSevaTab(ExcelUtility.getExcelData("locators", "chadavSevaTabTitle", "value")));
			clickOnElement(getChadavSevaTab(ExcelUtility.getExcelData("locators","chadavSevaTabTitle", "value")));
			return this;
		}catch (Exception e){
			throw new Exception("Error in  clickOnChadavSevaTab()"+e.getMessage()); 
		}

	}

	@StepInfo(info="checking if punya mudra count displayed in profile Page")
	public boolean isPunyaMudraCountDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if punya mudra count displayed in profile Page");
			awaitForElement(driver, punyaMudraCountText);
			return punyaMudraCountText.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in  isPunyaMudraCountDisplayed()"+e.getMessage()); 
		}
	}

	@StepInfo(info="checking if edit profile button is displayed")
	public boolean isEditProfileButtonDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if edit profile button is displayed");
			awaitForElement(driver, editBtn);
			return editBtn.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isEditProfileButtonDisplayed()"+e.getMessage()); 
		}

	}

	@StepInfo(info="checking if no internet connection message is displayed in snackbar")
	public boolean isNoInternetConnectionSnakBarMessageDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if no internet connection message is displayed ");
			awaitForElement(driver, noInternetConnectionSnackBarText);
			return noInternetConnectionSnackBarText.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isNoInternetConnectionSnakBarMessageDisplayed() "+e.getMessage()); 
		}
	}
	
	@StepInfo(info="checking if no internet connection message is displayed ")
	public boolean isNoInternetConnectionMessageDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if no internet connection message is displayed ");
			awaitForElement(driver, noInternetConnectionMessage);
			return noInternetConnectionMessage.isDisplayed();
		}catch (Exception e){
			throw new Exception("Error in isNoInternetConnectionMessageDisplayed()"+e.getMessage()); 
		}
	}

	@StepInfo(info="clicking on login tab from profile page")
	public LoginBottomSheetPage clickOnLoginTabFromProfilePage() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on login tab from profile page");
			awaitForElement(driver, punyaMudraTab);
			clickOnElement(getLoginTab(ExcelUtility.getExcelData("locators", "loginKareText", "value")));
			return new LoginBottomSheetPage(driver);
		}catch (Exception e){
			throw new Exception("Error in clickOnLoginTabFromProfilePage()"+e.getMessage()); 
		}
	}

}
