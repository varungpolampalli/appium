package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TestAnnotations.StepInfo;
import base.BaseTest;
import base.ElementUtil;
import base.ExcelUtility;
import base.ListenerImplimentationclass;
import io.appium.java_client.AppiumDriver;

public class PaymentPage extends BaseTest {

	ElementUtil elementUtil;
	public PaymentPage(AppiumDriver driver) {
		this.driver=driver;
		elementUtil=new ElementUtil(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_cancel']")
	private WebElement cancelBtn;

	@FindBy(xpath = "//android.widget.ImageView[@resource-id='com.mandir.debug:id/iv_payment_done']")
	private WebElement paymentSuccessIcon;

	@FindBy(xpath = "//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_payment_title']")
	private WebElement paymentSuccessFullTitle;

	@FindBy(xpath = "//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_price']")
	private WebElement totalPriceFromPaymentSuccessPage;

	@FindBy(xpath = "//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_info_steps']")
	private WebElement aageKyaKareInfoText;

	@FindBy(xpath = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mandir.debug:id/rv_offerings_steps']")
	private WebElement aageKyaKareInfoSteps;

	@FindBy(xpath = "//android.view.View[@resource-id='form-common']//android.widget.ListView")
	private WebElement listOfPaymentMethods;

	@FindBy(xpath = "//android.widget.Button[@resource-id='cta-view-details']/preceding-sibling::android.widget.TextView")
	private WebElement totalPriceFromListOfPaymentMethods;

	@FindBy(xpath = "//android.view.View[@resource-id='error-modal']")
	private WebElement paymentFailureBottomSheet;

	@FindBy(xpath = "//android.widget.Button[@text='Try Again']")
	private WebElement tryAgainBtn;

	@FindBy(xpath = "//android.view.View[@resource-id='netb-banks']")
	private WebElement listOfBanks;

	@FindBy(xpath = "//android.view.View[@resource-id='header-wrapper']/android.view.View[1]/android.view.View[1]")
	private WebElement backBtnFromAllListOfBanksPage;

	@FindBy(xpath = "//android.widget.TextView[@resource-id='com.mandir.debug:id/tv_payment_failed']")
	private WebElement paymentFailedMessage;

	@FindBy(xpath = "//android.widget.Button[@resource-id='com.mandir.debug:id/btn_okay']")
	private WebElement dobaraKoshishKareBtn;

	@FindBy(xpath = "//android.view.View[@resource-id='logo']/parent::android.view.View/android.view.View[4]/android.widget.Button")
	private WebElement cancelButtonFromListOfPaymentMethodsPage;

	//paytm locators

	@FindBy(xpath = "//android.view.View[@text='Select an option to pay']")
	private WebElement paytmPageOptionToPayText;

	@FindBy(xpath = "//android.view.View[@resource-id='ptm-login']/following-sibling::android.view.View")
	private WebElement listOfPaytmPaymentOptions;

	@FindBy(xpath = "//android.view.View[@resource-id='ptm-nb']")
	private WebElement paytmNetBankingBtn;

	@FindBy(xpath = "//android.widget.Button[contains(@text,'PAY')]")
	private WebElement paytmPayBtnFromListOfBanksPage;

	@FindBy(xpath = "//android.widget.Button[@text='clr icon Failure']")
	private WebElement failureBtnFromPaytmGateway;

	@FindBy(xpath = "//android.widget.Button[@text='clr icon Successful']")
	private WebElement successBtnFromPaytmGateWay;

	@FindBy(xpath = "//android.view.View[@text='Select an option to pay']/following-sibling::android.view.View[1]")
	private WebElement totalPriceFromPaytmListOfPaymentMethods;

	@FindBy(xpath = "//android.view.View[@text='View All Banks']/parent::android.view.View/parent::android.view.View")
	private WebElement listOfPaytmPaymentBanks;

	private WebElement getPaymentPageHeader(String paymentPageHeader)
	{
		String paymentPageHeaderxpathValue="//android.widget.TextView[@text='"+paymentPageHeader+"']";
		return elementUtil.getElement("xpath", paymentPageHeaderxpathValue);
	}


	private WebElement getNetBankingPaymentMethod(String netBankingPaymentMethodType)
	{
		String netbankingPaymentmethodxpathValue="//android.widget.TextView[@text='"+netBankingPaymentMethodType+"']";
		return elementUtil.getElement("xpath", netbankingPaymentmethodxpathValue);
	}

	private WebElement getNetBankName(String netBankNameText)
	{
		String netBankNamexpathValue="//android.widget.Button[@text='"+netBankNameText+"']";
		return elementUtil.getElement("xpath", netBankNamexpathValue);
	}


	private WebElement getPayNowBtn(String payNowBtnText)
	{
		String payNowBtnxpathValue="//android.widget.Button[@text='"+payNowBtnText+"']";
		return elementUtil.getElement("xpath", payNowBtnxpathValue);
	}


	private WebElement getSuccessBtn(String successBtnText)
	{
		String successBtnxpathValue="//android.widget.Button[@text='"+successBtnText+"']";
		return elementUtil.getElement("xpath", successBtnxpathValue);
	}

	private WebElement getFailureBtn(String failureBtnText)
	{
		String failureBtnTextxpathValue="//android.widget.Button[@text='"+failureBtnText+"']";
		return elementUtil.getElement("xpath", failureBtnTextxpathValue);
	}

	private WebElement getTikHaiBtn(String teekHaiBtnText)
	{
		String teekhaiBtnxpathValue="//android.widget.Button[@text='"+teekHaiBtnText+"']";
		return elementUtil.getElement("xpath", teekhaiBtnxpathValue);
	}



	@StepInfo(info="checking payment page is displayed")
	public boolean isPaymentPageDisplayed() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("checking payment page is displayed");
			awaitForElement(driver, getPaymentPageHeader(ExcelUtility.getExcelData("locators", "paymentPageHeader", "value")));
			return getPaymentPageHeader(ExcelUtility.getExcelData("locators", "paymentPageHeader", "value")).isDisplayed();
		}catch(Exception e)
		{
			throw new Exception("Error in getting isPaymentPageDisplayed()"+e.getMessage());

		}

	}


	@StepInfo(info="Clicking on NetBankingPaymentMethod in payment page ")
	public PaymentPage clickOnNetBankingBtn() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("Clicking on NetBankingPaymentMethod in payment page");
			awaitForElement(driver, getNetBankingPaymentMethod(ExcelUtility.getExcelData("locators", "netBankingPaymentMethodType", "value")));
			clickOnElement(getNetBankingPaymentMethod(ExcelUtility.getExcelData("locators", "netBankingPaymentMethodType", "value")));
		}catch(Exception e)
		{
			throw new Exception("Error in getting clickOnNetBankingBtn()"+e.getMessage());

		}
		return this;

	}


	@StepInfo(info="Clicking on net bank name btn in payment page ")
	public PaymentPage clickOnNetBankNameBtn() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("Clicking on net bank name btn in payment page");
			awaitForElement(driver, getNetBankName(ExcelUtility.getExcelData("locators", "BankNameText", "value")));
			clickOnElement(getNetBankName(ExcelUtility.getExcelData("locators", "BankNameText", "value")));
		}catch(Exception e)
		{
			throw new Exception("Error in getting clickOnNetBankNameBtn()"+e.getMessage());

		}
		return this;

	}


	@StepInfo(info="Clicking on Pay now btn ")
	public PaymentPage clickOnPayNowBtn() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("Clicking on Pay now btn");
			awaitForElement(driver, getPayNowBtn(ExcelUtility.getExcelData("locators", "payNowBtnText", "value")));
			clickOnElement(getPayNowBtn(ExcelUtility.getExcelData("locators", "payNowBtnText", "value")));
		}catch(Exception e){
			throw new Exception("Error in getting clickOnPayNowBtn()"+e.getMessage());
		}
		return this;

	}



	@StepInfo(info="Clicking on success  btn ")
	public PaymentPage clickOnSuccessBtn() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("Clicking on success  btn");
			awaitForElement(driver, getSuccessBtn(ExcelUtility.getExcelData("locators", "SuccessBtnText", "value")));
			clickOnElement(getSuccessBtn(ExcelUtility.getExcelData("locators", "SuccessBtnText", "value")));
		}catch(Exception e)
		{
			throw new Exception("Error in getting clickOnSuccessBtn()"+e.getMessage());

		}
		return this;
	}

	@StepInfo(info="Clicking on failure button")
	public PaymentPage clickOnFailureBtn() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("Clicking on failure button");
			awaitForElement(driver, getFailureBtn(ExcelUtility.getExcelData("locators", "FailureBtnText", "value")));
			clickOnElement(getFailureBtn(ExcelUtility.getExcelData("locators", "FailureBtnText", "value")));
			return this;
		}catch(Exception e)
		{
			throw new Exception("Error in getting clickOnFailureBtn()"+e.getMessage());

		}
	}


	@StepInfo(info="Clicking on teek hai  btn ")
	public PaymentPage clickOnTeekHaiBtn() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("Clicking on teek hai btn");
			awaitForElement(driver, getTikHaiBtn(ExcelUtility.getExcelData("locators", "teekHaiBtnText", "value")));
			clickOnElement(getTikHaiBtn(ExcelUtility.getExcelData("locators", "teekHaiBtnText", "value")));
		}catch(Exception e)
		{
			throw new Exception("Error in getting clickOnTikHaiBtn()"+e.getMessage());
		}
		return this;
	}

	@StepInfo(info="clicking on cancel button from payment success page")
	public void clickOnCancelBtnFromPaymentSuccessPage() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on cancel button from payment success page");
			awaitForElement(driver, cancelBtn);
			clickOnElement(cancelBtn);
		}catch(Exception e){
			throw new Exception("Error in getting clickOnCancelBtn()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if payment success icon displayed")
	public boolean isPaymentSuccessIconDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if payment success icon displayed");
			awaitForElement(driver, paymentSuccessIcon);
			return paymentSuccessIcon.isDisplayed();
		}catch(Exception e){
			throw new Exception("Error in getting isPaymentSuccessIconDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if payment successfull title displayed")
	public boolean isPaymentSuccessFullTitleDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if payment successfull title displayed");
			awaitForElement(driver, paymentSuccessFullTitle);
			return paymentSuccessFullTitle.isDisplayed();
		}catch(Exception e){
			throw new Exception("Error in getting isPaymentSuccessFullTitleDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if total price from payment sucesspage displayed")
	public boolean isToatalPriceFromPaymentSuccessPageDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if total price from payment sucesspage displayed");
			awaitForElement(driver, totalPriceFromPaymentSuccessPage);
			return totalPriceFromPaymentSuccessPage.isDisplayed();
		}catch(Exception e){
			throw new Exception("Error in getting isTotalPriceFromPaymentSuccessPageDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if teek hai button displayed")
	public boolean isTeekHaiBtnDisplayed() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("checking if teek hai button displayed");
			awaitForElement(driver, getTikHaiBtn(ExcelUtility.getExcelData("locators", "teekHaiBtnText", "value")));
			return getTikHaiBtn(ExcelUtility.getExcelData("locators", "teekHaiBtnText", "value")).isDisplayed();
		}catch(Exception e)
		{
			throw new Exception("Error in getting isTeekHaiBtnDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if list of payment methods displayed")
	public boolean isListOfPaymentMethods() throws Exception
	{
		try {
			ListenerImplimentationclass.testLog.info("checking if list of payment methods displayed");
			awaitForElement(driver, listOfPaymentMethods);
			return listOfPaymentMethods.isDisplayed();
		}catch(Exception e)
		{
			throw new Exception("Error in getting isListOfPaymentMethods()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if aage kya kare  text displayed")
	public boolean isAageKyaKareInfoTextDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if aage kya kare  text displayed");
			awaitForElement(driver, aageKyaKareInfoText);
			return aageKyaKareInfoText.isDisplayed();
		}catch(Exception e){
			throw new Exception("Error in getting isAageKyaKareInfoTextDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if aage kya kare info steps displayed")
	public boolean isAageKyaKareInfoStepsDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if aage kya kare info steps displayed");
			awaitForElement(driver, aageKyaKareInfoSteps);
			return aageKyaKareInfoSteps.isDisplayed();
		}catch(Exception e){
			throw new Exception("Error in getting isAageKyaKareInfoStepsDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if totalPrice is displayed in list of payment methods")
	public boolean isTotalPriceDisplayedInListOfPaymentMethods() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if totalPrice is displayed in list of payment methods");
			awaitForElement(driver, totalPriceFromListOfPaymentMethods);
			return totalPriceFromListOfPaymentMethods.isDisplayed();
		}catch(Exception e){
			throw new Exception("Error in getting isTotalPriceDisplayedInListOfPaymentMethods()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if payment failure error message displayed")
	public boolean isPaymentFailureErrorMessageDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if payment failure error message displayed");
			awaitForElement(driver, paymentFailureBottomSheet);
			return paymentFailureBottomSheet.isDisplayed();
		}catch(Exception e){
			throw new Exception("Error in getting isPaymentFailureErrorMessageDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking on try again button")
	public boolean clickOnTryAgainBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on try again button");
			awaitForElement(driver, tryAgainBtn);
			return tryAgainBtn.isDisplayed();
		}catch(Exception e){
			throw new Exception("Error in getting clickOnTryAgainBtn()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if list of banks displayed")
	public boolean isListOfBanksDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if list of banks displayed");
			awaitForElement(driver, listOfBanks);
			return listOfBanks.isDisplayed();
		}catch(Exception e){
			throw new Exception("Error in getting isListOfBanksDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking on back button from list of banks page")
	public PaymentPage clickOnBackBtnFromListOfBanksPage() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on back button from list of banks page");
			awaitForElement(driver, backBtnFromAllListOfBanksPage);
			clickOnElement(backBtnFromAllListOfBanksPage);
			return this;
		}catch(Exception e){
			throw new Exception("Error in clickOnBackBtnFromListOfBanksPage()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if payment failed message displayed")
	public boolean isPaymentFailedMessageDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if payment failed message displayed");
			awaitForElement(driver, paymentFailedMessage);
			return paymentFailedMessage.isDisplayed();
		}catch(Exception e){
			throw new Exception("Error in isPaymentFailedMessageDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking on dobara koshish kare tab")
	public void clickOnDobaraKoshishKareTab() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on dobara koshish kare tab");
			awaitForElement(driver, dobaraKoshishKareBtn);
			clickOnElement(dobaraKoshishKareBtn);
		}catch(Exception e){
			throw new Exception("Error in clickOnDobaraKoshishKareTab()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking on cancel button from list of banks page")
	public void clickOnCancelBtnFromListOfPaymentMethodsPage() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on cancel button from list of banks page");
		awaitForElement(driver, cancelButtonFromListOfPaymentMethodsPage);
		clickOnElement(cancelButtonFromListOfPaymentMethodsPage);
		waitOrPause(5);
		}catch(Exception e){
			throw new Exception("Error in clickOnCancelBtnFromListOfPaymentMethodsPage()"+e.getMessage());
		}
	}

	//Paytm payment gateway business libraries

	@StepInfo(info="checking if paytm page is displayed")
	public boolean isPaytmPageDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if paytm page is displayed");
			awaitForElement(driver, paytmPageOptionToPayText);
			return paytmPageOptionToPayText.isDisplayed();
		}catch(Exception e){
			throw new Exception("Error in isPaytmPageDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="cehcking if list of paytm payment options displayed")
	public boolean isListOfPayTmPaymentOptionsDisplayed() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if paytm page is displayed");
			awaitForElement(driver, listOfPaytmPaymentOptions);
			return listOfPaytmPaymentOptions.isDisplayed();
		}catch(Exception e){
			throw new Exception("Error in isListOfPayTmPaymentOptionsDisplayed()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking on paytm net banking ")
	public void clickOnPaytmNetBankingOption() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on paytm net banking ");
			awaitForElement(driver, paytmNetBankingBtn);
			clickOnElement(paytmNetBankingBtn);
		}catch(Exception e){
			throw new Exception("Error in clickOnPaytmNetBankingOption()"+e.getMessage());
		}

	}

	@StepInfo(info="clicking on pay button from all banks page")
	public void clickOnPaytmPayBtn() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on pay button from all banks page");
			awaitForElement(driver, paytmPayBtnFromListOfBanksPage);
			clickOnElement(paytmPayBtnFromListOfBanksPage);
		}catch(Exception e){
			throw new Exception("Error in clickOnPaytmPayBtn()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking on failure button from paytm gateway")
	public void clickOnFailureBtnFromPaytmGateway() throws Exception {
		try {
		ListenerImplimentationclass.testLog.info("clicking on failure button from paytm gateway");
		awaitForElement(driver, failureBtnFromPaytmGateway);
		clickOnElement(failureBtnFromPaytmGateway);
		waitOrPause(5);
		}catch(Exception e){
			throw new Exception("Error in clickOnFailureBtnFromPaytmGateway()"+e.getMessage());
		}
	}

	@StepInfo(info="clicking on success button from paytm gateway")
	public void clickOnSuccessBtnFromPaytmGateway() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("clicking on success button from paytm gateway");
			awaitForElement(driver, successBtnFromPaytmGateWay);
			clickOnElement(successBtnFromPaytmGateWay);
		}catch(Exception e){
			throw new Exception("Error in clickOnSuccessBtnFromPaytmGateway()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if total price displayed in list of paytm payment methods")
	public boolean isTotalPriceDisplayedInListOfPaytmPaymentMethods() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if total price displayed in list of paytm payment methods");
			awaitForElement(driver, totalPriceFromPaytmListOfPaymentMethods);
			return totalPriceFromPaytmListOfPaymentMethods.isDisplayed();
		}catch(Exception e){
			throw new Exception("Error in isTotalPriceDisplayedInListOfPaytmPaymentMethods()"+e.getMessage());
		}
	}

	@StepInfo(info="checking if list of paytm payment banks displayed")
	public boolean isListOfPaytmPaymentsBank() throws Exception {
		try {
			ListenerImplimentationclass.testLog.info("checking if list of paytm payment banks displayed");
			awaitForElement(driver, listOfPaytmPaymentBanks);
			return listOfPaytmPaymentBanks.isDisplayed();
		}catch(Exception e){
			throw new Exception("Error in isListOfPaytmPaymentsBank()"+e.getMessage());
		}
	}

}
