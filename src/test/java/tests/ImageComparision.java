package tests;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import Pages.MandirPage;
import base.BaseTest;

@Listeners(base.ListenerImplimentationclass.class)
public class ImageComparision extends BaseTest {
	@Test
	public void compareImageOfDifferentGodInMandirPage() throws Exception  {
		MandirPage mandirPage=new MandirPage(driver);
		File actualGodScreenshot = mandirPage.getScreenshotOfGod();
		double compPercentage = compareImage(actualGodScreenshot, new File("./src/test/Images/Hanuman.png"));
		System.err.println(compPercentage);
	}
	
	@Test
	public void compareImageOfSameGodInMandirPage() throws Exception {
		MandirPage mandirPage=new MandirPage(driver);
		File actualGodScreenshot = mandirPage.getScreenshotOfGod();
		double compPercentage = compareImage(actualGodScreenshot, new File("./src/test/Images/dasara.png"));
		System.err.println(compPercentage);
	}

	@Test
	public void compareaartiThaliImage() throws Exception {
		MandirPage mandirPage=new MandirPage(driver);
		File actualThaliScreenshot = mandirPage.getaarthiThaliImage();
		double compPercentage = compareImage(actualThaliScreenshot, new File("./src/test/Images/aarthiThali.png"));
		System.err.println(compPercentage);
	}

}
