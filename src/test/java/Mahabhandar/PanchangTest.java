package Mahabhandar;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import Pages.KundliPage;
import Pages.MahabhandarPage;
import Pages.MandirPage;
import Pages.MusicMiniPlayerPage;
import Pages.PanchangPage;
import Pages.TempleListingPage;
import TestAnnotations.TestInfo;
import base.BaseTest;
import base.TestData;

@Listeners(base.ListenerImplimentationclass.class)
public class PanchangTest extends BaseTest {


	@TestInfo(testcaseID ="SMPK-TC-3284,SMPK-TC-3285,SMPK-TC-3286,SMPK-TC-3287,SMPK-TC-3649,SMPK-TC-3365,SMPK-TC-3293,SMPK-TC-3363,SMPK-TC-3364,SMPK-TC-3362,"
			+ "SMPK-TC-3361,SMPK-TC-3359,SMPK-TC-3360,SMPK-TC-3290,SMPK-TC-3291,SMPK-TC-3292,SMPK-TC-3650,SMPK-TC-3652,SMPK-TC-5257,SMPK-TC-7083,SMPK-TC-3289,SMPK-TC-3288"
			+ "SMPK-TC-7088,SMPK-TC-7089,SMPK-TC-7090,SMPK-TC-7093 ")
	@Test(groups= {"regression","smoke"})
	public void verifyPanchangaFeature() throws Exception {
		MandirPage mandirPage=new MandirPage(driver);
		MahabhandarPage mahabhandarPage=new MahabhandarPage(driver);
		PanchangPage panchangPage=new PanchangPage(driver);
		TempleListingPage templeListingPage=new TempleListingPage(driver);
		MusicMiniPlayerPage musicMiniPlayerPage=new MusicMiniPlayerPage(driver);
		KundliPage kundliPage=new KundliPage(driver);
		
		
		mandirPage.clickOnMahabhandarTab()
		.swipeHorizontalBar();
		Assert.assertTrue(mahabhandarPage.isPanchangOptionDisplayed(), "Panchang Option is not displayed in mahabhandar Page");
		mahabhandarPage.clickOnPanchangTab()
		.clickOnDateForwardBtn();
		Assert.assertTrue(panchangPage.isPanchangCardTitleDisplayed(), "Panchang card is not displayed for the desired date");
		panchangPage.clickOnDateBackwardBtn();
		Assert.assertTrue(panchangPage.isPanchangCardTitleDisplayed(), "Panchnag card is not displayed for the desired date");
		panchangPage.clickOnDateSelectionTab();
		Assert.assertTrue(panchangPage.isDatePickerBottomSheetDisplayed(), "Date Picker bottom sheet is not displayed");
		panchangPage.clickOnCalendarCancelBtn();
		panchangPage.clickOnTomorrowTab();
		Assert.assertEquals(panchangPage.getDailyTabText(), TestData.dailyTabText, "Daily filter is not displayed in panchang page");
		Assert.assertEquals(panchangPage.getMontlyTabText(), TestData.monthlyTabText, "Monthly filter is not displayed in panchag page");
		Assert.assertTrue(panchangPage.isPanchangCardTitleDisplayed(), "Panchang Card  not displayed in Panchang Page");
		//		scrollToElement(driver, TestData.shareText);

		Assert.assertEquals(panchangPage.getAajBtnText(), TestData.aajBtnText, "Aaj Button in Panchang Page Not Validated");
		Assert.assertEquals(panchangPage.getKalBtnText(), TestData.kalBtnText, "Kal Button in Panchang Page Not Validated");
		panchangPage.clickOnLocationTab().clickOnBackBtnFromLocationSearchPage();
		Assert.assertTrue(mahabhandarPage.isPanchangOptionDisplayed(), "Panchang Option is not displayed in mahabhandar Page");
		panchangPage.clickOnLocationTab().typeLoactionToLoactionTextFeild().clickOnLocationName();
		Assert.assertTrue(panchangPage.isMoonCardDisplayed(), "Moon card is not displayed in daily Panchang section");

		panchangPage.scrollTillShubhaAshubhaSamay();
		panchangPage.clickOnHelpBtn();
		Assert.assertTrue(panchangPage.isHelpBannerDisplayed(), "Shubha-ashubha Samaya help Banner is not displayed in Panchang page");
		panchangPage.swipingTheHelpBottomSheet();
		panchangPage.clickOnOkBtn();
		scrollToElement(driver, TestData.shubhaAshubaTitle);
		Assert.assertTrue(panchangPage.isAuspiciousUnAuspiciousCardDisplayed(), "Auspicious/Unauspicious content is not displayed in Panchang page");
		
		panchangPage.clickOnAajKaShubhChadavTab();
		Assert.assertEquals(TestData.chadavSevaPageTitle,templeListingPage.getChadavSevaPageTitle(),"navigation to chadav seva page not validated");
		templeListingPage.clickOnBackBtn();
		panchangPage.clickOnDinkaShubhMantraTab();
		Assert.assertTrue(musicMiniPlayerPage.isMiniPlayerDisplayed(), "mini player is not displayed on clicking din ka shubh mantra");
		musicMiniPlayerPage.clickOnPlayPauseBtn().clickOnCloseBtn();
		panchangPage.clickOnKundliTab();
		Assert.assertEquals(kundliPage.getKundliPageTitle(), TestData.kundliPageTitle, "Kundli screen navigation not validated");
		kundliPage.clickOnBackBtn();
		
//		scrollToElement(driver, TestData.sampoornaPanchangText);
		Assert.assertTrue(panchangPage.isSampoornaPanchangCardDisplayed(), "Sampoorna Panchang Page is not displayed");
		String actualSampoornaPanchangHelpBannerTitle = panchangPage.clickOnSampoornaPanchangHelpBtn();
		Assert.assertEquals(actualSampoornaPanchangHelpBannerTitle, TestData.sampoornaPanchangText, "sampoorna Panchang Help Banner Not displayed");
		panchangPage.swipingTheHelpBottomSheet();
		panchangPage.clickOnOkBtn();
		scrollToElement(driver, TestData.horaAurChowgadiyaText);
		String actualHoraAurChowgadiyaHelpBannerTitle = panchangPage.clickOnHoraAurChowgadiyaHelpBtn();
		Assert.assertEquals(actualHoraAurChowgadiyaHelpBannerTitle, TestData.horaAurChowgadiyaHelpBannerTitle, "Hora Aur chowgadiya help banner Not displayed");
		panchangPage.swipingTheHelpBottomSheet();
		panchangPage.clickOnOkBtn();
		Assert.assertTrue(panchangPage.getDateHighLightBanner().contains(TestData.aajBtnText), "Date Highlight banner not displayed in Panchang Page");
		panchangPage.scrollTillEndOfThePage(4);
		panchangPage.clickOnShareIcon();
		Assert.assertTrue(panchangPage.isMessageDisplayedInShareBottomSheet(), "Share Bottom sheet is not displayed in Panchag Page");
		pressNavigationBack(driver);

		//scroll to beginning and verifying monthly panchang components
		panchangPage.scrollToBeginning(11);
		String actualMontlyPanchangText =panchangPage.clickOnMontlyTab().getMontlypanchangText();
		panchangPage.clickOnLocationTab().typeLoactionToLoactionTextFeild().clickOnLocationName();
		Assert.assertTrue(actualMontlyPanchangText.contains(TestData.monthlyTabText), "Montly panchang page validated");
		Assert.assertTrue(panchangPage.isCalenadarViewDisplayed(), "calender deatails  not displayed");
		panchangPage.scrollTillfestivalCards();
		Assert.assertTrue(panchangPage.isFestivalCardDisplayed(), "Festival details not displayed");
		panchangPage.scrollToBeginning(1);
		panchangPage.clickOnDateValue();
		Assert.assertTrue(panchangPage.isMoonCardDisplayed(), "Moon card is not displayed");
		panchangPage.clickOnMonthlyPanchangOkBtn();
		Assert.assertTrue(panchangPage.isPanchangCardTitleDisplayed(), "Panchang Card  not displayed in Panchang Page");

		//verify month selection Tab
		String reqYearValue=TestData.yearValue;
		String[] actualMonthAndYearArray = panchangPage.clickOnMontlyTab().getMonthSelectionValueText();

		int reqYearIntValue = Integer.parseInt(reqYearValue);
		int actualYearValue = Integer.parseInt(actualMonthAndYearArray[1]);

		try {
			while(reqYearIntValue>actualYearValue)
			{
				panchangPage.clickOnMonthForwardBtn();
				String  actualYear =panchangPage.getMonthSelectionValueText()[1];
				actualYearValue=Integer.parseInt(actualYear);
			}
			while(reqYearIntValue<actualYearValue)
			{
				panchangPage.clickOnMonthBackwardBtn();
				String  actualYear =panchangPage.getMonthSelectionValueText()[1];
				actualYearValue=Integer.parseInt(actualYear);

			}
			panchangPage.clickOnDateValue().clickOnMonthlyPanchangOkBtn();
			Assert.assertTrue(panchangPage.isPanchangCardTitleDisplayed(), "Panchang card is not displayed");

		}catch (Exception e) {
			System.out.println("please enter proper date");
		}

	}
}
