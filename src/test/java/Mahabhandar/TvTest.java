package Mahabhandar;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import Pages.MahabhandarPage;
import Pages.MandirPage;
import Pages.TvPage;
import TestAnnotations.TestInfo;
import base.BaseTest;
import base.TestData;
@Listeners(base.ListenerImplimentationclass.class)

public class TvTest extends BaseTest{

    @TestInfo(testcaseID="SMPK-TC-5265,SMPK-TC-3342,SMPK-TC-3343,SMPK-TC-3344,SMPK-TC-3345,SMPK-TC-3346,SMPK-TC-3347,SMPK-TC-3348,SMPK-TC-3351 ")
    @Test(groups= {"regression","smoke"})
    public void verifyTV() throws Exception{

        MandirPage mandirPage=new MandirPage(driver);
        MahabhandarPage mahabhandarPage=new MahabhandarPage(driver);
        TvPage tvPage=new TvPage(driver);
        mandirPage.clickOnMahabhandarTab();
        
        tvPage.scrollToTvTab();
        String actualTvtabTitle = tvPage.getTvTabText();
        Assert.assertEquals(actualTvtabTitle,TestData.tvTabTitle, "Tv Tab title  not validated");
        String[] episodeCount = tvPage.getTvEpisodesCount().split(" ");
        String actualEpisodeCount = episodeCount[0];
        Assert.assertEquals(actualEpisodeCount, TestData.tvEpisodesCount,"Episodes count not validated");
        String actualTvShowTitle=tvPage.getTvShowTitle();
        tvPage.clickOnTvShow();
        Assert.assertEquals(actualTvShowTitle, TestData.mahaBharath,"tv show not validated");
        String[] showLanguage = tvPage.getTvShowLanguage().split(" ");
        String actualShowLanguage=showLanguage[3];
        Assert.assertEquals(actualShowLanguage, TestData.tvShowLanguage,"Tv episodes not validated");
        tvPage.scrollEpisodes().clickOnTvBackButton();
        Assert.assertTrue(tvPage.isTvShowTabDisplayed(), "Tv show not displayed");
        tvPage.scrollTvShows().scrollTillEnd();
        tvPage.clickLastTvShow(); 
        String actualtvShowValue = tvPage.getTvShowText();
        Assert.assertEquals(actualtvShowValue, TestData.tvEpisode,"Tv episode not validated");


    }

}