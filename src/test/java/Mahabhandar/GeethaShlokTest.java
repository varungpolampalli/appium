package Mahabhandar;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import Pages.GeethaShlokPage;
import Pages.MandirPage;
import TestAnnotations.TestInfo;
import base.BaseTest;
import base.TestData;


@Listeners(base.ListenerImplimentationclass.class)

public class GeethaShlokTest extends BaseTest {


	@TestInfo(testcaseID="SMPK-TC-1921,SMPK-TC-1922,SMPK-TC-1923,SMPK-TC-1924,SMPK-TC-1925,SMPK-TC-3232")
	@Test(groups= {"regression","smoke"})
	public void verifyGeethaShlokPage() throws Exception
	{
		MandirPage mandirPage = new MandirPage(driver);
		GeethaShlokPage geethaShlokPage=new GeethaShlokPage(driver);
		mandirPage.clickOnMahabhandarTab();
		geethaShlokPage.clickOnGeethaShlokTab();
		String actualGeethaShlokTabText=geethaShlokPage.getGeethShlokaTabTitle();
		Assert.assertEquals(actualGeethaShlokTabText, TestData.geethaShlokaText,"geethaShlok screen not validated");
		
		String actualFirstAdyayTabText=geethaShlokPage.getFirstAdyayText();
		Assert.assertEquals(actualFirstAdyayTabText, TestData.firstAdyayText,"First Adyay screen not validated");
		geethaShlokPage.scrollAdyayCarousel();
		
		String actualLastAdyayTabText=geethaShlokPage.getLastAdyayText();
		Assert.assertEquals(actualLastAdyayTabText, TestData.lastAdyayText,"First Adyay screen not validated");
		
		geethaShlokPage.scrollAdyayCards();


		Assert.assertTrue(geethaShlokPage.isWatsApppIconDisplayed(), "WatsApp icon on adyay Card not validated ");
		geethaShlokPage.pressNavigationBack(driver);

	}

}
