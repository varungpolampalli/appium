package Mahabhandar;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import Pages.ChowgadiyaPage;
import Pages.GeethaShlokPage;
import Pages.HoraPage;
import Pages.KundliPage;
import Pages.MahabhandarPage;
import Pages.MandirPage;
import Pages.MukhyaPage;
import Pages.PanchangPage;
import Pages.RashifalPage;
import Pages.SahithyaPage;
import Pages.SujavPage;
import Pages.SuvicharPage;
import Pages.TvPage;
import TestAnnotations.TestInfo;
import base.BaseTest;
@Listeners(base.ListenerImplimentationclass.class)

public class MukhyaTest extends BaseTest

{
	@TestInfo(testcaseID="SMPK-TC-2085,SMPK-TC-2087,SMPK-TC-2088,SMPK-TC-2089,SMPK-TC-2091,SMPK-TC-2092,SMPK-TC-2093,SMPK-TC-2090,SMPK-TC-2094,SMPK-TC-3001")
	@Test(groups= {"regression","smoke"})
	public void VerifyMukhya() throws Exception
	{
		MandirPage mandirpage=new MandirPage(driver);
		MukhyaPage mukhyapage=new MukhyaPage(driver);
		MahabhandarPage mahabandarpage=new MahabhandarPage(driver);	
		
		mandirpage.clickOnMahabhandarTab();
		mahabandarpage.swipeHorizontalBar();
		mukhyapage.clickOnMukyatab().clickOnKundliCard();
		KundliPage kundlipage=new KundliPage(driver);
		Assert.assertTrue(kundlipage. isKundliTitleDisplayed(), "kundli page  Title is not displayed");
		kundlipage.clickOnBackBtn();

		mukhyapage.clickOnPanchangCard();
		PanchangPage panchangpage=new PanchangPage(driver);
		Assert.assertTrue(panchangpage. isPanchangCardTitleDisplayed(), "panchang Title is not displayed");
		
		mukhyapage.clickOnMukyatab().clickOnRashifalCard();
		RashifalPage rashifalpage=new RashifalPage(driver);
		Assert.assertTrue(rashifalpage. isRashifalTitleDisplayed(), "Rashifal Title is not displayed");


		mukhyapage.clickOnMukyatab().clickOnHoraCard();
		HoraPage horapage=new HoraPage(driver);
		Assert.assertTrue(horapage. isHoraTabDisplayed(), "Hora Title is not displayed");

		mukhyapage.clickOnMukyatab().clickOnChowgadiyaCard();
		ChowgadiyaPage chowgadiyapage= new ChowgadiyaPage(driver);
		Assert.assertTrue(chowgadiyapage. isChowgadiyaTabDisplayed(), "Chowgadiya Title is not displayed");

		mahabandarpage.swipeHorizontalBar();
		mukhyapage.clickOnMukyatab().clickOnSahithyaCard();
		SahithyaPage sahithyapage= new SahithyaPage(driver);
		Assert.assertTrue(sahithyapage. isSahithyaTabDisplayed(), "SahithyaPage Title is not displayed");


		mahabandarpage.swipeHorizontalBar();
		mukhyapage.clickOnMukyatab().clickOnSuvichaarCard();
		SuvicharPage suvicharpage= new SuvicharPage(driver);
		Assert.assertTrue(suvicharpage. isSuvichaarTabDisplayed(), "SuvichaarPage Title is not displayed");

		mahabandarpage.swipeHorizontalBar();
		mukhyapage.clickOnMukyatab().scrollTillEnd();
		mukhyapage.clickOnGeethashlokCard();
		GeethaShlokPage geethashlokpage=new GeethaShlokPage(driver);
		Assert.assertTrue(geethashlokpage. isGeethaShlokTabDisplayed(), "GeethaShlokPage Title is not displayed");


		mukhyapage.swipeHorizontalBarTillMukhya();
		mukhyapage.clickOnMukyatab().scrollTillEnd();
		mukhyapage.clickOnTvCard();
		TvPage tvpage=new TvPage(driver);
		Assert.assertTrue(tvpage. isTvTabDisplayed(), "Tvpage Title is not displayed");

		mukhyapage.swipeHorizontalBarTillMukhya();
		mukhyapage.clickOnMukyatab().scrollTillEnd();
		SujavPage sujavpage=new SujavPage(driver);
		Assert.assertTrue(sujavpage. isSujavTabDisplayed(), "Sujav page  Title is not displayed");
	}

}




