package Mahabhandar;

import java.util.Date;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.ibm.icu.text.SimpleDateFormat;

import Pages.MandirPage;
import Pages.RashifalPage;
import Pages.SahithyaPage;
import TestAnnotations.TestInfo;
import base.BaseTest;
import base.TestData;

@Listeners(base.ListenerImplimentationclass.class)

public class RashifalTest extends BaseTest{

	@TestInfo(testcaseID="SMPK-TC-1883,SMPK-TC-1884,SMPK-TC-1885,SMPK-TC-1886,SMPK-TC-1888,SMPK-TC-1889,SMPK-TC-1891,SMPK-TC-1893,SMPK-TC-1896,SMPK-TC-1899,SMPK-TC-1897,SMPK-TC-1905,SMPK-TC-1906,SMPK-TC-5239")
	@Test(groups= {"regression","smoke"})
	public void verifyingRashifal() throws Exception
	{
		MandirPage mandirPage = new MandirPage(driver);
		RashifalPage rashifalPage=new RashifalPage(driver);
		SahithyaPage sahithyapage=new SahithyaPage(driver);

		mandirPage.clickOnMahabhandarTab().swipeHorizontalBar();
		rashifalPage.clickOnRashifalTab();
		
		String actualRashifalTitle=rashifalPage.getRashifalTitle();
		Assert.assertEquals(actualRashifalTitle, TestData.rashifalTitle,"rashifal Screen Navigation Not Valiated");
		rashifalPage.clickOnRashiDropDown();
		rashifalPage.swipeRashifalBottomSheet();

		Assert.assertTrue(rashifalPage.getAllRashiCount()==12,"All rashi are not validated");
		rashifalPage.selectRashiOnDropDown();	
		String[] raashiVAlue = rashifalPage.getSelectedRashiValue().split(" ");
		String actualSelectedRashiValue = raashiVAlue[0];
		String actualRashiSelectedValue=rashifalPage.getSelectedRashiValue();

		String[] actualCurrentDateAndYearArray =rashifalPage.getDateAndMonthTitle();
		String actualCurrentDate = actualCurrentDateAndYearArray[1];
		String actualCurrentYear=actualCurrentDateAndYearArray[3];
		Date date=new Date();
		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
		String currentDate = sdf.format(date);
		String[] currentDateAndYearArray = currentDate.split("/");
		Assert.assertEquals(actualCurrentDate, currentDateAndYearArray[0], "Current Date is not validated in rashifal Page");
		Assert.assertEquals(actualCurrentYear, currentDateAndYearArray[2], "Current Year is not validated in rashifal page");

		Assert.assertEquals(actualRashiSelectedValue, TestData.rashi,"Selected Rashi is  not validated");
		rashifalPage.ClickOnWhatisRashifal();
		rashifalPage.ClickOnTeekHai();
		scrollToElement(driver, TestData.rashifalShubMantra);
		String expectedRashiValue = rashifalPage.getSelectedRashiInShubMantra();

		Assert.assertEquals(actualSelectedRashiValue, expectedRashiValue,"Selected rashi and content is different");

		rashifalPage.ClickOnShubManatraSongIcon();
		Assert.assertTrue(rashifalPage.isMiniPlayerDisplayed(), "Mini player is not displayed");


		rashifalPage.scrollToNextPage();
		Assert.assertTrue(rashifalPage.isMiniPlayerDisplayed(), "Mini player is not displayed");

		String actualSahithyaBhandarPageTitle=sahithyapage.getSahithyaTabText();
		Assert.assertEquals(actualSahithyaBhandarPageTitle, TestData.sahithyaText,"SahithyaMahabhandar Screen Navigation Not Valiated");
		rashifalPage.ScrollbackToRashifal()
		.clickOnRashifalTab();

		String actualRashifalTitleOnBacknavigation=rashifalPage.getRashifalTitle();
		Assert.assertEquals(actualRashifalTitleOnBacknavigation, TestData.rashifalTitle,"rashifal Screen Navigation Not Valiated");
		Assert.assertTrue(rashifalPage.isMiniPlayerDisplayed(), "Mini player is not displayed");

		rashifalPage.ClickPauseOrPlayButton()
		.CloseMiniPlayer();
		scrollTillEnd();
		rashifalPage.ClickWatsAppShareKareButton();
		Assert.assertTrue(rashifalPage.isMessageDisplayedInShareBottomSheet(), "share Bottom sheet not validated in rashifal Page");
		rashifalPage.pressNavigationBack(driver);
	}
}


