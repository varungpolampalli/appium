package Mahabhandar;

import java.util.Date;

import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.ibm.icu.text.SimpleDateFormat;

import Pages.ChowgadiyaPage;
import Pages.MandirPage;
import TestAnnotations.TestInfo;
import base.AIOAppium;
import base.BaseTest;
import base.TestData;


@Listeners(base.ListenerImplimentationclass.class)
public class ChowgadiyaTest extends BaseTest{


	@TestInfo(testcaseID = "SMPK-TC-2519, SMPK-TC-2520, SMPK-TC-2521,SMPK-TC-2522,SMPK-TC-2523,SMPK-TC-2526,SMPK-TC-2532,SMPK-TC-2525,SMPK-TC-2527,SMPK-TC-5260,\r\n"
			+ "SMPK-TC-7094")
	@Test(groups= {"regression","smoke"})
	public void verifyChowgadiyaPage() throws Exception {
		
		//validating aaj And Kal buttons in chowgadiya Page
		MandirPage mandirPage=new MandirPage(driver);
		ChowgadiyaPage chowgadiyaPage=new ChowgadiyaPage(driver);
		String actualAajBtnText = mandirPage.clickOnMahabhandarTab().clickOnChowgadiyaTab().getAajBtnText();
		String actualKalBtnText=chowgadiyaPage.getKalBtnText();
		Assert.assertEquals(actualAajBtnText, TestData.aajBtnText, "Aaj Button in chowgadiya Page Not Validated");
		Assert.assertEquals(actualKalBtnText, TestData.kalBtnText, "Kal Button in chowgadiya Page Not Validated");
		
		//validating if tomorrow's chowgadiya is displayed on clicking kal btn
		chowgadiyaPage.clickOnKalTab();
		Assert.assertTrue(chowgadiyaPage.isTomorrowChowgadiyaCardDisplayed(), "Tomorrow's chowgadiya not displayed on clicking kal btn");
		chowgadiyaPage.clickOnAajTab();
		
		//validating current date and year in chowgadiya page
		String[] actualCurrentDateAndYearArray = chowgadiyaPage.getdateAndMonthTitle();
		String actualCurrentDate = actualCurrentDateAndYearArray[1];
		String actualCurrentYear=actualCurrentDateAndYearArray[3];
		Date date=new Date();
		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
		String currentDate = sdf.format(date);
		String[] currentDateAndYearArray = currentDate.split("/");
		Assert.assertEquals(actualCurrentDate, currentDateAndYearArray[0], "Current Date is not validated in chowgadiya Page");
		Assert.assertEquals(actualCurrentYear, currentDateAndYearArray[2], "Current Year is not validated in chowgadiya page");
		
	
		//checking if location Tab Is displayed in chowgadiya Page and validating redirection from location page
		Assert.assertTrue(chowgadiyaPage.isLocationTabDisplayed(), "Location search modal is not displayed in chowgadiya page");
		chowgadiyaPage.clickOnLocationSearchModal().clickOnBackBtnFromLocationSearchPage();
		Assert.assertTrue(chowgadiyaPage.isLocationTabDisplayed(), "Location search modal is not displayed in chowgadiya page");
		chowgadiyaPage.clickOnLocationSearchModal().typeLoactionToLoactionTextFeild().clickOnLocation();
		
		
		//validating chowgadiya cards,help banner,Date Highlight banner and share icon
		boolean flag = chowgadiyaPage.isChowgadiyaDescriptionDisplayed();
		Assert.assertTrue(flag, "Chowgadiya Description is not displayed");
		String actualHelpBannerTitle = chowgadiyaPage.clickOnHelpBtn();
		Assert.assertEquals(actualHelpBannerTitle, TestData.helpBannerTitle, "chowgadiya kya hai bottom sheet title not validated");
		chowgadiyaPage.clickOnOkBtn();
		scrollTillEnd();
		String actualDateHighlightBanner=chowgadiyaPage.getDateHighLightBanner();
		Assert.assertTrue(actualDateHighlightBanner.contains(TestData.aajBtnText), "Date Highlight banner not displayed");
		chowgadiyaPage.clickOnShareIcon();
		Assert.assertTrue(chowgadiyaPage.isMessageDisplayedInShareBottomSheet(), "share Bottom sheet not validated in chowgadiya Page");
		pressNavigationBack(driver);
	}
}
