package Mahabhandar;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import Pages.MandirPage;
import Pages.MusicMiniPlayerPage;
import Pages.SujavPage;
import TestAnnotations.TestInfo;
import base.BaseTest;
import base.TestData;
@Listeners(base.ListenerImplimentationclass.class)


public class SujavTest extends BaseTest
{

	@TestInfo(testcaseID="SMPK-TC-1910,SMPK-TC-1913,SMPK-TC-1914,SMPK-TC-1915,SMPK-TC-1916,SMPK-TC-1917,SMPK-TC-1918,SMPK-TC-7084,SMPK-TC-1920,SMPK-TC-5249")
	@Test(groups= {"regression","smoke"})
	public void VerifySujav() throws Exception
	{
		
		MandirPage mandirpage=new MandirPage(driver);
		SujavPage sujav=new SujavPage(driver);
		mandirpage.clickOnMahabhandarTab();
		sujav.swipeuntilsujav();
		String actualSujavTabText =sujav.clickOnSujavtab().getSuajavTabTitle();
		Assert.assertEquals(actualSujavTabText, TestData.sujavPageTitle, "Suajav page title  not validated ");

		String actualRashifalTitle=sujav.clickOnRashifalCard().getRashifalTitle();
		Assert.assertEquals(actualRashifalTitle,TestData.rashifalTitle, "Rashiphal page title  not validated");

		sujav.swipeuntilsujav();
		sujav.clickOnSujavtab().scrollTillEnd();
		String actualHoraTitle=sujav.clickOnHoraCard().getHoraTitle();
		Assert.assertEquals(actualHoraTitle,TestData.horaTitle, "Hora page title  not validated");

		sujav.swipeuntilsujav();
		sujav.clickOnSujavtab().scrollTillEnd();
		String actualSahithyaTitle=sujav.clickOnSahithyaCard().getSahithyaTabText();
		Assert.assertEquals(actualSahithyaTitle,TestData.sahithyaText, "Sahithya page title  not validated");

		sujav.swipeuntilsujav();
		sujav.clickOnSujavtab().scrollTillEnd();
		String actualChowgadiyaTitle=sujav.clickOnChowgadiyaCard().getChowgadiyaTabText();
		Assert.assertEquals(actualChowgadiyaTitle,TestData.chowgadiyaPageTitle, "Chowgadiya page title  not validated");

		sujav.swipeuntilsujav();
		sujav.clickOnSujavtab();
		Assert.assertTrue(sujav. isSongCardDisplayed(), "Song card  is not displayed");
		
		sujav.clickOnSujavtab().clickOnAbhiSuneButton();
		MusicMiniPlayerPage musicminiplayer=new MusicMiniPlayerPage(driver);
		Assert.assertTrue(musicminiplayer. isMiniPlayerDisplayed(), "Mini Music player  not displayed");
		musicminiplayer.clickOnPlayPauseBtn();
		musicminiplayer.clickOnCloseBtn();
		
    	sujav.scrollTillEnd();
		String actualSuvichaarText=sujav.clickOnSuvicharCard().getSuvicharTabText();
		Assert.assertEquals(actualSuvichaarText,TestData.suvicharTabText, "Suvichaar page title  not validated");

		sujav.clickOnSujavtab().scrollTillEnd();
		String actualGeethaShlokTabText=sujav.clickOnGeethaShlokCard().getGeethShlokaTabTitle();
		Assert.assertEquals(actualGeethaShlokTabText,TestData.geethaShlokaText, "Geetha Shlok page title  not validated");

		sujav.clickOnSujavtab().scrollTillEnd();
		String actualTvtabTitle=sujav.clickOnTvCard().getTvTabText();
		Assert.assertEquals(actualTvtabTitle,TestData.tvTabTitle, "Tv Tab title  not validated");

		}	
}






