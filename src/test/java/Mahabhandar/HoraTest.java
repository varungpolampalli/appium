package Mahabhandar;

import java.util.Date;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.ibm.icu.text.SimpleDateFormat;

import Pages.HoraPage;
import Pages.MahabhandarPage;
import Pages.MandirPage;
import TestAnnotations.TestInfo;
import base.BaseTest;
import base.TestData;
@Listeners(base.ListenerImplimentationclass.class)


public class HoraTest extends BaseTest {

	@TestInfo(testcaseID = "SMPK-TC-3268,SMPK-TC-3270,SMPK-TC-3271,SMPK-TC-3279,SMPK-TC-3274,SMPK-TC-3269,SMPK-TC-5261,SMPK-TC-3272,SMPK-TC-3273,SMPK-TC-3276,SMPK-TC-3282,SMPK-TC-3278,SMPK-TC-7091")
	@Test(groups= {"regression","smoke"})

	public void VerifyHora() throws Exception
	{
		MandirPage mandirpage=new MandirPage(driver);
		mandirpage.clickOnMahabhandarTab();
		HoraPage horapage=new HoraPage(driver);
		MahabhandarPage mahabandarpage=new MahabhandarPage(driver);    
		mahabandarpage.swipeHorizontalBar();
		Assert.assertTrue(horapage.isHoraTabDisplayed(),"Hora Tab is  not displayed");

		//validating aaj And Kal buttons in hora Page
		String actualAajButtonTitle=horapage.clickOnHoraTab().getAajButtonText();
		Assert.assertEquals(actualAajButtonTitle, TestData.AajButtonTitle, "Aaj Button  not validated in hora page");
		String actualKalButtonTitle=horapage.getKalButtonText();
		Assert.assertEquals(actualKalButtonTitle, TestData.KalButtonTitle, "Kal Button  not validated in hora page");
		Assert.assertTrue(horapage.isLocationSearchModalDisplayed(),"Location search modal is not displayed in hora page");
		String actualHoraKyaBannerTitle=horapage.clickOnHelpButton().getHorakyaBannerTitle();
		Assert.assertEquals(actualHoraKyaBannerTitle, TestData.horaKyaBannerText, "Hora Kya Banner Text  not validated in hora page");
		horapage.pressNavigationBack(driver);

		//validating current date and year in hora page
		String[] actualCurrentDateAndYearArray = horapage.getDateAndMonthTitle();
		String actualCurrentDate = actualCurrentDateAndYearArray[1];
		String actualCurrentYear=actualCurrentDateAndYearArray[3];
		Date date=new Date();
		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
		String currentDate = sdf.format(date);
		String[] currentDateAndYearArray = currentDate.split("/");
		Assert.assertEquals(actualCurrentDate, currentDateAndYearArray[0], "Current Date is not validated in Hora Page");
		Assert.assertEquals(actualCurrentYear, currentDateAndYearArray[2], "Current Year is not validated in Hora page");

		//validating if tomorrow's hora is displayed on clicking kal btn
		horapage.clickOnKalBtn();
		Assert.assertTrue(horapage.iskalHoraDisplayed(),"kal hora is not validated" );
		horapage.clickOnAajBtn();		

		//checking if location Tab Is displayed in hora Page and validating redirection from location page
		horapage.clickOnLocationSearchModal().typeLoactionToLoactionTextFeild().clickOnLocationName();
		horapage.clickOnLocationSearchModal().clickOnBackBtnFromLocation();
		String actualAajButtonText=horapage.getAajButtonText();
		Assert.assertEquals(actualAajButtonText, TestData.AajButtonTitle, "location back btn is not validated in hora page");

		//Verify that user is displayed with "anukool, prathikool" information 
		String actualAnukulText=horapage.getAnukulText();
		Assert.assertEquals(actualAnukulText, TestData.anukulText,"Anukul text is not validated in hora page");
		horapage.swipedownUntilPratikul();
		String actualPratikulText=horapage.getPratikulText();
		Assert.assertEquals(actualPratikulText, TestData.pratikulText,"pratikul text is not validated in hora page");					

		//Verify that "aage aane wale hora" section with different timelines for the day
		horapage.verticalscrollTillAageAnewaleHora();
		Assert.assertTrue(horapage.isAageaneWalaCardDisplayed(),"age ane wale card is  not displayed");
		horapage.clickOnDownChevron();
		Assert.assertTrue(horapage.isHoraDetailsDisplayed(),"Hora detials is  not displayed");

		//Verify that user is able to share "aaj ka hora'' friends and family using apps such as Whatsapp
		scrollTillEnd();
		horapage.clickOnShareIcon();
		Assert.assertTrue(horapage.isMessageSharingBottomSheetDisplayed(), "share Bottom sheet not validated in hora Page");
		horapage.pressNavigationBack(driver);

	}
}

