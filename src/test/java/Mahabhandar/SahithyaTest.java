package Mahabhandar;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import Pages.MandirPage;
import Pages.SahithyaPage;
import TestAnnotations.TestInfo;
import base.BaseTest;
import base.PunyaMudraPopUpTest;
import base.TestData;

@Listeners(base.ListenerImplimentationclass.class)


public class SahithyaTest extends BaseTest{

	@TestInfo(testcaseID ="SMPK-TC-3241,SMPK-TC-3240 ,SMPK-3242,SMPK-TC-3243,SMPK-TC-3246,SMPK-TC-3247,SMPK-TC-7085,SMPK-TC-7086,SMPK-TC-7087,SMPK-TC-4073,SMPK-TC-5235,SMPK-TC-5236,SMPK-TC-5237,SMPK-TC-5238")
	@Test(groups= {"regression","smoke"})
	public void VerifySahithyaPage1() throws Exception
	{

		MandirPage mandirPage=new MandirPage(driver);
		SahithyaPage sahithyapage=new SahithyaPage(driver);
		PunyaMudraPopUpTest.handlePunyaMudraPopUp(driver);
		String actualMahaBhandarPageTitle = mandirPage.clickOnMahabhandarTab().getMahabhandarPageTitle();
		Assert.assertEquals(actualMahaBhandarPageTitle, TestData.mahabhandarPageTitle,"Mahabhandar Screen Navigation Not Valiated");
		String actualSahithyaBhandarPageTitle=sahithyapage.getSahithyaTabText();		
		Assert.assertEquals(actualSahithyaBhandarPageTitle, TestData.sahithyaText,"SahithyaMahabhandar Screen Navigation Not Valiated");
		Assert.assertTrue(sahithyapage.isMoreButtonDisplayed(), "more button is not validated");
		sahithyapage.clickOnLokPriyaMoreButton();
		String actualLokPriyaText= sahithyapage.getLokPriyaText();
		Assert.assertEquals(actualLokPriyaText, TestData.lokaPriyaText,"LokPriyaText not validated");
		Assert.assertTrue(sahithyapage.isFeaturePageDisplayed(), "feature page content is not displayed");
		sahithyapage.clickBackButton();
		Assert.assertTrue(sahithyapage.isArticleTagsDisplayed(),"feature tags not displayed");
		sahithyapage.clickVideoArticle();
		Assert.assertTrue(sahithyapage.isVideoPlayOrPauseButtonDisplayed(),"play or pause button displayed");
		sahithyapage.pressNavigationBack(driver);

		Assert.assertTrue(sahithyapage.verifyTextArticle(), "Text Article is not displayed");
		Assert.assertTrue(sahithyapage.verifyFontSize(), "Font Size seekbar is not displayed");
		sahithyapage.clickOnFontSizeCloseButton();
		Assert.assertTrue(sahithyapage.verifyWatsAppShareButton(), "Wats app share buttom sheet not displayed");
		sahithyapage.pressNavigationBack(driver);
		sahithyapage.isScrollable();

		sahithyapage.clickaudioArticleBackButton();
		sahithyapage.scrollToNextTag();
		sahithyapage.clickOnAudioArticle();
		try {
			sahithyapage.clickOnAudioButton();
			Assert.assertTrue(sahithyapage.isMiniPlayerDisplayed(), "Audio Mini Player is not displayed");
			sahithyapage.clickAudioPauseOrPlayButton().CloseMiniPlayer();
		} catch (Exception e) {
			System.err.println("Audio article not present");
		}
		sahithyapage.scrollTillEnd();
		sahithyapage.pressNavigationBack(driver);
		Assert.assertEquals(actualSahithyaBhandarPageTitle, TestData.sahithyaText,"SahithyaMahabhandar Screen Navigation Not Valiated");
		sahithyapage.isScrollable();	

	}
}
