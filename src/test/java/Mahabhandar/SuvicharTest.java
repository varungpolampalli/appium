package Mahabhandar;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import Pages.GeethaShlokPage;
import Pages.MahabhandarPage;
import Pages.MandirPage;
import Pages.RashifalPage;
import Pages.SuvicharPage;
import TestAnnotations.TestInfo;
import base.BaseTest;
import base.TestData;

@Listeners(base.ListenerImplimentationclass.class)


public class SuvicharTest extends BaseTest{

	@TestInfo(testcaseID="SMPK-TC-1890,SMPK-TC-1892,SMPK-TC-1894,SMPK-TC-1895,SMPK-TC-1898,SMPK-TC-1900,SMPK-TC-1901,SMPK-TC-1902,SMPK-TC-1903,SMPK-TC-1904,SMPK-TC-1907,SMPK-TC-1909,SMPK-TC-1911,SMPK-TC-1912,SMPK-TC-4809,SMPK-TC-4810,SMPK-TC-4811,,SMPK-TC-5259")
	@Test(groups= {"regression","smoke"})
	public void verifySuvicharPage() throws Exception
	{
		MandirPage mandirPage = new MandirPage(driver);
		SuvicharPage suvicharPage=new SuvicharPage(driver);
		RashifalPage rashifalPage=new RashifalPage(driver);
		GeethaShlokPage geethaShlokPage=new GeethaShlokPage(driver);
		MahabhandarPage mahaBhandarPage=new MahabhandarPage(driver);
		mandirPage.clickOnMahabhandarTab();
		String actualSuvicharText=suvicharPage.clickOnSuvicharTab().getSuvicharTabText();
		Assert.assertEquals(actualSuvicharText,TestData.suvicharTabText,"Suvichar Screen Navigation Not Valiated" );
		mahaBhandarPage.swipeHorizontalBar();
		String actualRashifalTitle=rashifalPage.clickOnRashifalTab().getRashifalTitle();
		Assert.assertEquals(actualRashifalTitle, TestData.rashifalTitle,"rashifal Screen Navigation Not Valiated");
		suvicharPage.scrollToSuvicharTab().clickOnSuvicharTab().clickOnAajKaSuvichar();
		Assert.assertEquals(actualSuvicharText,TestData.suvicharTabText,"Suvichar Screen Navigation Not Valiated" );
	    String actualAajKaSuvicharText=suvicharPage.getAajKaSuvicharText();
	    Assert.assertEquals(actualAajKaSuvicharText, TestData.aajKaSuvicharText,"Aaj ka suvichar not validated");
		Assert.assertTrue(suvicharPage.IsAajKaSuvicharCardDisplayed(),"aaj Ka Suvichar card is not displayed");
		Assert.assertTrue(suvicharPage.clickOnAajKaSuvicharCardWatsApppIcon(),"watsApp icon for AajKaSuvicharCard not displayed");
		suvicharPage.pressNavigationBack(driver);
		
		Assert.assertTrue(suvicharPage.ScrollThroughAajKaSuvicharCards(),"aaj ka suvichar cards not validated");
		String actualSuprabathText = suvicharPage.ClickOnSuprabathCarousel();
		Assert.assertEquals(actualSuprabathText, TestData.suprabathText,"suprabath carousel not validated");
		Assert.assertTrue(suvicharPage.clickCardWatsApppIcon(),"WatsApp icon on Suprabath Card not validated");
		suvicharPage.pressNavigationBack(driver);

		geethaShlokPage.clickOnGeethaShlokTab();
		String actualGeethaShlokTabText=geethaShlokPage.clickOnGeethaShlokTab().getGeethShlokaTabTitle();
		Assert.assertEquals(actualGeethaShlokTabText, TestData.geethaShlokaText,"geethaShlok screen not validated");
		
		suvicharPage.clickOnSuvicharTab();
		Assert.assertEquals(actualSuprabathText, TestData.suprabathText,"suprabath carousel not validated");
		
		Assert.assertTrue(suvicharPage.isSuprabathCardDisplayed(),"suprabath card is displayed");
		String actualShubSandhyaText=suvicharPage.clickOnShubSandhyaCarousel();
		Assert.assertEquals(actualShubSandhyaText, TestData.shubSandhyaText,"ShubSandhya is not validated");
		Assert.assertTrue(suvicharPage.clickCardWatsApppIcon(),"WatsApp icon on ShubSandhya Card not validated");
		suvicharPage.pressNavigationBack(driver);

		Assert.assertTrue(suvicharPage.isShubSandhyaCardDisplayed(),"ShubSandhya card is not displayed");
		String actualShubRatriText=suvicharPage.clickOnShubRatriCarousel();
		Assert.assertEquals(actualShubRatriText, TestData.shubRatriText,"shubRatri carousel not validated");
		Assert.assertTrue(suvicharPage.clickCardWatsApppIcon(),"WatsApp icon on ShubRatri Card not validated");
		
		suvicharPage.pressNavigationBack(driver);
		Assert.assertTrue(suvicharPage.isShubRatriCardDisplayed(),"shubRathri card is not displayed");
	}
}
