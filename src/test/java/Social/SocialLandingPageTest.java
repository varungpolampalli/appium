package Social;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import Pages.CommunityFeedPage;
import Pages.CommunityProfilePage;
import Pages.LoginBottomSheetPage;
import Pages.MandirPage;
import Pages.MenuPage;
import Pages.SocialLandingPage;
import TestAnnotations.TestInfo;
import base.BaseTest;
import base.TestData;

@Listeners(base.ListenerImplimentationclass.class)

public class SocialLandingPageTest extends BaseTest{

	@TestInfo(testcaseID = "SMPK-TC-14992,SMPK-TC-14993,SMPK-TC-14994,SMPK-TC-14995,SMPK-TC-14996,SMPK-TC-14997,SMPK-TC-14998,SMPK-TC-14999,SMPK-TC-15000,SMPK-TC-15001,SMPK-TC-15002,SMPK-TC-15003,SMPK-TC-15004,SMPK-TC-15005"
			+ "SMPK-TC-15006,SMPK-TC-15007,SMPK-TC-15008,SMPK-TC-15009,SMPK-TC-15010,SMPK-TC-15011,SMPK-TC-15012,SMPK-TC-15013,SMPK-TC-15014,SMPK-TC-15015,SMPK-TC-15016,SMPK-TC-15017")
	@Test(groups= {"regression","smoke"})
	public void verifySocialLandingPage() throws Exception {
		MandirPage mandirPage=new MandirPage(driver);
		MenuPage menuPage=new MenuPage(driver);
		SocialLandingPage socialLandingPage=new SocialLandingPage(driver);
		CommunityFeedPage communityFeedPage=new CommunityFeedPage(driver);
		LoginBottomSheetPage loginBottomSheetPage=new LoginBottomSheetPage(driver);
		CommunityProfilePage communityProfilePage=new CommunityProfilePage(driver);


		mandirPage.clickOnHamburgerMenu();
		scrollTillEnd();
		try {
			menuPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup();
			menuPage.clickOnCancelBtn();
		}catch (Exception e) {
			logger.info("user is already in logged out state");
			menuPage.clickOnCancelBtn();
		}
		mandirPage.clickOnSamudayaTab();
		Assert.assertEquals(socialLandingPage.getSamudayaPageTitle(), TestData.samudayaPageTitle, "Social Page title not validated");
		Assert.assertTrue(socialLandingPage.isAllCommunityCardsDisplayed(), "All community cards not displayed in social landing page");
		Assert.assertTrue(socialLandingPage.isCommunityCardProfileImageDisplayed(), "community card profile image not displayed in social landing page");
		Assert.assertTrue(socialLandingPage.isCommunityCardTitleDisplayed(), "community card title not displayed in social landing page");
		Assert.assertTrue(socialLandingPage.isUserCountDisplayed(), "user count not displayed in community card page in social landing page");
		Assert.assertTrue(socialLandingPage.isCommunityCardDescriptionDisplayed(), "community card description not displayed in social landing page");
		Assert.assertTrue(socialLandingPage.isAddButtonDisplayedInFirstCommunityCard(), "Jude button not displayed in community card in social landing page");
		socialLandingPage.clickOnAddBtnFromFirstCommunityCard();
		Assert.assertTrue(socialLandingPage.isLoginBottomSheetDisplayed(), "Log in bottom sheet not displayed on  clicking add button from social landing page");
		socialLandingPage.clickOnCloseBtnFromLogInBottomSheet();
		socialLandingPage.clickOnFirstCommunityCard();
		Assert.assertTrue(communityFeedPage.isCommunityNameDisplayed(), "Navigation to community feed page not validated on clicking community card in logged out state");
		communityFeedPage.clickOnBackBtn();

		//logging in from social landing page
		socialLandingPage.clickOnAddBtnFromFirstCommunityCard();
		loginBottomSheetPage.clickOnPhoneLogInBtn().enterMobileNumber().clickOnLogInBtn()
		.enterOtp().clickOnContinueBtn();
		
		socialLandingPage.clickOnAddBtnFromFirstCommunityCard();
		Assert.assertTrue(socialLandingPage.isApkaSamudayaTextDisplayed(), "Apka samuday text not displayed after login to community");
		Assert.assertTrue(socialLandingPage.isApkaSamudayaCardsDisplayed(), "Apka samuday cards not displayed after joining to community");
		Assert.assertTrue(socialLandingPage.isCommunityNameOfJoinedCommunityDisplayed(), "Community name of joined community not displayed");
		Assert.assertTrue(socialLandingPage.isProfileImageOfJoinedCommunityDisplayed(),"profile image of joined community not displayed");
		Assert.assertTrue(socialLandingPage.isAuthorImageDisplayed(), "Author image not displayed in joined community card");
		Assert.assertTrue(socialLandingPage.isLastMessageDisplayed(), "Last message is not displayed in joined community card");
		Assert.assertTrue(socialLandingPage.isUpdatesDhekeTabDisplayed(), "Updates dheke tab not displayed in joined community card");
		scrollToElement(driver, TestData.anyaSamudayText);
		Assert.assertTrue(socialLandingPage.isAnyaSamudayCardsDisplayed(), "Anya samuday cards not displayed");
		scrollTillEnd();
		try {
			Assert.assertTrue(socialLandingPage.isAdhikSamudayTabDisplayed(), "Adhik samuday tab not displayed");
		}catch (Exception e) {
			logger.info("Adhik samuday tab not displayed because user has joined all the communities");
		}
		mandirPage.clickOnMandirIcon().clickOnSamudayaTab();

		try {
			for (int i =socialLandingPage.getCountOfJoinedCommunityCard() ; i >0 ;i--  ) {
				socialLandingPage.clickOnFirstJoinedCommunityCard();
				communityFeedPage.clickOnCommunityTopBarTitle().clickOnHorizontalEllipsis().clickOnCommunityLeaveBtn()
				.clickOnYesbtnFromComfirmationPopup();
				communityProfilePage.clickOnBackBtn();
				communityFeedPage.clickOnBackBtn();
			}
		}catch (Exception e) {
			logger.info("User is removed from all the community");
		}
		socialLandingPage.clickOnHamburgerMenuFromSamuday();
		scrollTillEnd();
		menuPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup();
		menuPage.clickOnCancelBtn();

	}

}
