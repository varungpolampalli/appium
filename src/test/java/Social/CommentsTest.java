package Social;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import Pages.CommentsPage;
import Pages.CommunityFeedPage;
import Pages.CommunityProfilePage;
import Pages.LoginBottomSheetPage;
import Pages.MandirPage;
import Pages.MenuPage;
import Pages.SocialLandingPage;
import TestAnnotations.TestInfo;
import base.BaseTest;
@Listeners(base.ListenerImplimentationclass.class)


public class CommentsTest extends BaseTest {
	@TestInfo(testcaseID = "SMPK-TC-15019,SMPK-TC-15020,SMPK-TC-15026,SMPK-TC-15028,SMPK-TC-15029,SMPK-TC-15030,SMPK-TC-15022,SMPK-TC-15024,"
			+ "SMPK-TC-15032,SMPK-TC-15033,SMPK-TC-15036,SMPK-TC-15038,SMPK-TC-15039,SMPK-TC-15049,SMPK-TC-15048,SMPK-TC-15034,SMPK-TC-15037")
	@Test

	public void VerifyCommentsTest() throws Exception
	{
		SocialLandingPage socialLandingPage=new SocialLandingPage(driver);
		CommunityFeedPage communityFeedPage= new CommunityFeedPage(driver);
		CommunityProfilePage communityProfilePage=new CommunityProfilePage(driver);
		CommentsPage commentsPage=new CommentsPage(driver);
		MandirPage mandirPage=new MandirPage(driver);
		LoginBottomSheetPage loginBottomSheetPage=new LoginBottomSheetPage(driver);
		MenuPage menuPage= new MenuPage(driver);
		try {
			mandirPage.clickOnHamburgerMenu();
			scrollTillEnd();
			menuPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup();
			menuPage.clickOnCancelBtn();
		}catch(Exception e) {
			menuPage.clickOnCancelBtn();
		}		
		mandirPage.clickOnSamudayaTab();
		
		socialLandingPage.clickOnCommunityCard();
		communityFeedPage.scrollingTillCommentField();
		communityFeedPage.clickOnCommentTextField();

		//Verify that user is able to navigate to comments section on clicking the add comments field
		Assert.assertTrue(commentsPage.isCommentPageTitleDisplayed(), "comment page title not displayed");
		
		//Verify that user is getting the respective number of reaction and comments count in comments screen
		commentsPage.scrollingTillUp();
		Assert.assertTrue(commentsPage.isReactionbtnDisplayed(), "Reaction count not displayed");
		Assert.assertTrue(commentsPage.isCommentsDisplayed(), "comment count not displayed");   
				
		//Verify that unlogged user is getting login pupup on clicking reactions in comments page  
		commentsPage.clickOnReactionBtn();
		Assert.assertTrue(loginBottomSheetPage.isLoginBottomSheetDisplayed(), "login bottom sheet not displayed");	

		commentsPage.clickOnCloseBtn();	
		//Verify that user is getting login pupup on adding comments
		commentsPage.clickOnAddCommentField().enterComment();
				
		Assert.assertTrue(loginBottomSheetPage.isLoginBottomSheetDisplayed(), "login bottom sheet not displayed");	
		
		//Verify that user is able to successfully join the community and add comments after joining the community using join community bottomsheet in commets section
		loginBottomSheetPage.clickOnPhoneLogInBtn().enterMobileNumber()
		.clickOnLogInBtn()
		.enterOtp()
		.clickOnContinueBtn();	

		commentsPage.clickOnSendBtn();		
		try {
			Assert.assertTrue(commentsPage.isJoinBtnDisplayed(), "join btn nt displayed");			
			commentsPage.clickOnJoinBtn();
		}catch(Exception e) {
			logger.info("user didn't get join btn ");  
			
		}			
				
		//Verify that user is able to delete the added comments for the respective message
  		//adding comments again
		commentsPage.clickOnAddCommentField().enterComment();
		commentsPage.clickOnSendBtn();
		pressNavigationBack(driver);
		commentsPage.scrollingTillEndInCommentPage();
		commentsPage.clickOnLastEllipsisIcon().clickOnDeleteBtn().clickOnYesBtnInDeleteCommentPopup();
		       
		//Verify that user is able to successfully join the community and add comments after joining the community using join community bottomsheet in commets section	
		Assert.assertTrue(commentsPage.getCommentText().contains("test"), "comment is not added after logging in");

		String enteredComment=commentsPage.getCommentText();

		//Verify that user is getting ellipsis soon after adding the comment
		Assert.assertTrue(commentsPage.isEllipsisDisplayed(), "Ellipsis btn not displayed");

		//Verify that user is getting his profile image and name after comment get posted
		Assert.assertTrue(commentsPage.isProfileImageDisplayed(), "profile image not displayed");

		Assert.assertTrue(commentsPage.isProfileNameDisplayed(), "profile name not displayed");	
				
		//Verify that user is getting the respective preview of the messsage in comments section
		Assert.assertTrue(commentsPage.isPreviewMessageDisplayed(), "preview of the messsage in comments section  is not displayed");

		//Verify that user is getting delete icon on clicking the ellipsis of the respective added comments
		commentsPage.clickOnEllipsisIcon();
		Assert.assertTrue(commentsPage.isDeleteBtnDisplayed(), "delete btn not displayed");

		//Verify that user is getting the delete comment popup on clicking delete icon
		commentsPage.clickOnDeleteBtn();
		Assert.assertTrue(commentsPage.isDeleteCommentPopupDisplayed(), "delete confirmation popup not displayed");

		//Verify that user is able to cancel comment deletion
		commentsPage.clickOnCancelBtninDeleteCommentPopup();
		Assert.assertTrue(commentsPage.isCommentPageTitleDisplayed(), "comment page title not displayed");

		//Verify that the delete popup is getting disappeared for a user on device back
		commentsPage.clickOnEllipsisIcon().clickOnDeleteBtn();
		waitOrPause(3);
		pressNavigationBack(driver);
		waitOrPause(3);
		pressNavigationBack(driver);
		Assert.assertTrue(commentsPage.isCommentPageTitleDisplayed(), "comment page title not displayed");
			
		//Verify that the delete popup is getting dissappeared for a user on clicking anywhere on the screen	
		commentsPage.clickOnEllipsisIcon().clickOnDeleteBtn();
		commentsPage.tapByCoordinatesInCommentPage();
		waitOrPause(4);
		pressNavigationBack(driver);
		Assert.assertTrue(commentsPage.isCommentPageTitleDisplayed(), "comment page title not displayed");	
				
		//Verify that the latest comment is getting updated on back navigation from comment section to community exploration screen
		commentsPage.clickOnBackBtn();
		communityFeedPage.clickOnBackBtn();
		socialLandingPage.clickOnFirstJoinedCommunityCard();
	    Assert.assertEquals(enteredComment, communityFeedPage.getLastCommentText(), "comment not get updated");
					
		//Verify that community unjoined user can delete his own comments    
		communityFeedPage.clickOnCommunityTopBarTitle().clickOnHorizontalEllipsis().clickOnCommunityLeaveBtn().clickOnYesbtnFromComfirmationPopup();
		communityProfilePage.clickOnBackBtn();
		communityFeedPage.scrollingTillCommentField();
		communityFeedPage.clickOnCommentTextField();
		commentsPage.clickOnEllipsisIcon().clickOnDeleteBtn().clickOnYesBtnInDeleteCommentPopup();	
		       		
		//Verify that community unjoined user is getting community join popup on adding comments		
		commentsPage.clickOnAddCommentField().enterComment();
		commentsPage.clickOnSendBtn();
	
		Assert.assertTrue(commentsPage.isJoinBtnDisplayed(), "join btn nt displayed");
		pressNavigationBack(driver);
				
		//Verify that user is able to navigate back to community feed from comments section on back navigation
		commentsPage.clickOnBackBtn();
		communityFeedPage.scrollingThroughCommunityFeed();
		Assert.assertTrue(communityFeedPage.isCommunityTopbarTitleDisplayed(), "community top bar title is not validated after back navigation back");
				

	}
}
