package Social;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import Pages.CommunityFeedPage;
import Pages.CommunityProfilePage;
import Pages.LoginBottomSheetPage;
import Pages.MandirPage;
import Pages.MenuPage;
import Pages.MoreCommunitiesPage;
import Pages.SocialLandingPage;
import TestAnnotations.TestInfo;
import base.BaseTest;
import base.TestData;
@Listeners(base.ListenerImplimentationclass.class)

public class MoreCommunitiesTest extends BaseTest{

	@TestInfo(testcaseID = "SMPK-TC-15088,SMPK-TC-15089,SMPK-TC-15091,SMPK-TC-15092,SMPK-TC-15094,SMPK-TC-15096,SMPK-TC-15097,SMPK-TC-15098,SMPK-TC-15099,SMPK-TC-15100,SMPK-TC-15101,SMPK-TC-15093")
	@Test

	public void VerifyMoreCommunities() throws Exception
	{
		SocialLandingPage socialLandingPage=new SocialLandingPage(driver);
		MoreCommunitiesPage moreCommunities=new MoreCommunitiesPage(driver);
		CommunityFeedPage communityFeedPage= new CommunityFeedPage(driver);
		CommunityProfilePage communityProfilePage=new CommunityProfilePage(driver);
		MandirPage mandirPage=new MandirPage(driver);
		LoginBottomSheetPage loginBottomSheetPage=new LoginBottomSheetPage(driver);
		MenuPage menuPage= new MenuPage(driver);
		try {
			mandirPage.clickOnHamburgerMenu();
			scrollTillEnd();
			menuPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup();
			menuPage.clickOnCancelBtn();
		}catch(Exception e) {
			menuPage.clickOnCancelBtn();
		}		
		mandirPage.clickOnSamudayaTab();
		socialLandingPage.clickOnAddBtnFromFirstCommunityCard();

		loginBottomSheetPage.clickOnPhoneLogInBtn().enterMobileNumber()
		.clickOnLogInBtn()
		.enterOtp()
		.clickOnContinueBtn();	

		//Verify that user is able to get "Adhik samuday" button on joining a community
		socialLandingPage.scrollingTillEndInSocialpage();
		Assert.assertTrue(socialLandingPage.isAdhikSamudayTabDisplayed(), "more communities btn not dispalyed");
		socialLandingPage.clickOnMoreCommunitiesBtn();

		//Verify that user is able to navigate to view more communities page on clicking the "Adik samuday" card
        Assert.assertEquals(moreCommunities.getMoreCommunitiesPageTitle(), TestData.samudayaPageTitle, "more communities page title not validated");

		//Verify that user is getting respective list of communities in "lok Priya" communities in more communities section
		Assert.assertTrue(moreCommunities.isListOfCommunitiesInLokPriyaSectionDisplayed(), "List of communities in lokpriya section not displayed");

		//Verify that user is getting community description for community cards in "Lok priya" community section
		Assert.assertTrue(moreCommunities.isCommunityDescriptionInLokpriyaSectionDisplayed(), "community description not displayed in lokpriya section");

		//Verify that user is able to join a community on clicking "Jude button"
		moreCommunities.clickOnJoinBtnInMoreCommunities();
		pressNavigationBack(driver);
		Assert.assertTrue(socialLandingPage.isApkaSamudayaCardsDisplayed(), "apaka samudaya card not displayed after joining from more community");
				
		socialLandingPage.scrollingTillEndInSocialpage();
		socialLandingPage.clickOnMoreCommunitiesBtn();
		
		//Verify that user is getting respective community profile image and total member count on all the cards
        Assert.assertTrue(moreCommunities.isProfileImageDisplayed(), "profile image not displayed");      
        Assert.assertTrue(moreCommunities.isMemberCountDisplayed(), "member count not displayed");
	
		moreCommunities.clickOnJoinBtnInMoreCommunities();
		Assert.assertTrue(moreCommunities.isTickBtnDisplayed(), "tick btn not displayed after joining community");
				
		//Verify that user is able to navigate inside a community on clicking a community card in more communities page
		moreCommunities.clickOnFirstCardInMoreCommunities();
		//Assert.assertTrue(moreCommunities.isCommunityHeaderInCommunityFeedDisplayed(), "community header in community feed page not displayed");

		//Verify that user is landing to more communities page on back navigation from community exploration page
		pressNavigationBack(driver);
        Assert.assertEquals(moreCommunities.getMoreCommunitiesPageTitle(), TestData.samudayaPageTitle, "more communities page title not validated");
		
		//Verify that user is landing on social landing page on back navigation from more communites page
		pressNavigationBack(driver);
        Assert.assertEquals(socialLandingPage.getSamudayaPageTitle(), TestData.samudayaPageTitle, "samuday landing page title not validated");
                                
		socialLandingPage.scrollingTillEndInSocialpage();
		socialLandingPage.clickOnMoreCommunitiesBtn();
		
		//Verify that user is able to scroll through list of unjoined communities in view more community page
		moreCommunities.scrollTillAnyaSamudaya().scrollingTillAnyaSamudayCards();

		//Verify that user is getting respective list of communities in "Anya Samuday" Section
		Assert.assertTrue(moreCommunities.isListofCommunitiesInAnyaSamudayaSectionDisplayed(), "list of communities in anya samuday not displayed in lokpriya section");	
		pressNavigationBack(driver);
		
		//leaving group        
        try {
            for (int i =socialLandingPage.getCountOfJoinedCommunityCard() ; i >0 ;i--  ) {
                socialLandingPage.clickOnFirstJoinedCommunityCard();
                communityFeedPage.clickOnCommunityTopBarTitle().clickOnHorizontalEllipsis().clickOnCommunityLeaveBtn()
                .clickOnYesbtnFromComfirmationPopup();
                communityProfilePage.clickOnBackBtn();
                communityFeedPage.clickOnBackBtn();
            }
        }catch (Exception e) {
            logger.info("User is removed from all the community");
        }
	}		
}