package Social;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import Pages.CommentsPage;
import Pages.CommunityFeedPage;
import Pages.CommunityProfilePage;
import Pages.LoginBottomSheetPage;
import Pages.MandirPage;
import Pages.MenuPage;
import Pages.SocialLandingPage;
import TestAnnotations.TestInfo;
import base.BaseTest;
import base.TestData;

@Listeners(base.ListenerImplimentationclass.class)
public class CommunityFeedTest extends BaseTest {
	
	@TestInfo(testcaseID = "SMPK-TC-15054,SMPK-TC-15055,SMPK-TC-15056,SMPK-TC-15057,SMPK-TC-15058,SMPK-TC-15059,SMPK-TC-15060,SMPK-TC-15061,SMPK-TC-15062,SMPK-TC-15063,SMPK-TC-15064"
			+ "SMPK-TC-15065,SMPK-TC-15066,SMPK-TC-15067,SMPK-TC-15068,SMPK-TC-15069,SMPK-TC-15070,SMPK-TC-15071,SMPK-TC-15072,SMPK-TC-15073,SMPK-TC-15074,SMPK-TC-15075,SMPK-TC-15076,"
			+ "SMPK-TC-15077,SMPK-TC-15078,SMPK-TC-15079,SMPK-TC-15080,SMPK-TC-15081,SMPK-TC-15082,SMPK-TC-15083,SMPK-TC-15084,SMPK-TC-15085,SMPK-TC-15086,SMPK-TC-15087")
	@Test(groups= {"regression","smoke"})
	public void verifyCommunityFeedPage() throws Exception {
		MandirPage mandirPage=new MandirPage(driver);
		MenuPage menuPage=new MenuPage(driver);
		SocialLandingPage socialLandingPage=new SocialLandingPage(driver);
		CommunityFeedPage communityFeedPage=new CommunityFeedPage(driver);
		CommentsPage commentsPage=new CommentsPage(driver);
		CommunityProfilePage communityProfilePage=new CommunityProfilePage(driver);
		LoginBottomSheetPage loginBottomSheetPage=new LoginBottomSheetPage(driver);
		
		mandirPage.clickOnHamburgerMenu();
		scrollTillEnd();
		try {
			menuPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup();
			menuPage.clickOnCancelBtn();
		}catch (Exception e) {
			logger.info("user is already in logged out state");
			menuPage.clickOnCancelBtn();
		}
		mandirPage.clickOnSamudayaTab();
		socialLandingPage.clickOnFirstCommunityCard();
		Assert.assertTrue(communityFeedPage.isCommunityNameDisplayed(), "Navigation to community feed page not validated");
		
		//clicking on heart icon
		communityFeedPage.clickOnHeartReactionIcon();
		Assert.assertTrue(communityFeedPage.isLoginBottomSheetDisplayed(), "login bottom sheet not displayed on clicking heart reaction icon");
		communityFeedPage.clickOnCloseBtnFromLogInBottomSheet();
		
		communityFeedPage.clickOnHeartReactionIcon();
		pressNavigationBack(driver);
		
		communityFeedPage.clickOnHeartReactionIcon();
		communityFeedPage.tapAnyWhereOnTheScreenToCloseTheBottomSheet();
		
		//checking if community image and join button  displayed
		Assert.assertTrue(communityFeedPage.isCommunityImageDisplayed(), "Community image not displayed in community feed page");
		Assert.assertTrue(communityFeedPage.isCommunityProfileImageDisplayed(), "Community profile image not displayed in community feed page");
		Assert.assertTrue(communityFeedPage.isJoinBtnInCommunityFeedPageDisplayed(), "Join button not displayed in community feed page");
		
		Assert.assertTrue(communityFeedPage.isCommunityUserCountDisplayed(), "Community user count not  displayed in community feed page");
		Assert.assertTrue(communityFeedPage.isCommunityDescriptionDisplayed(), "Community description not displayed in community feed page");
		
		communityFeedPage.clickOnJoinBtn();
		Assert.assertTrue(communityFeedPage.isLoginBottomSheetDisplayed(), "login bottom sheet not displayed on clicking join button in community feed page");
		communityFeedPage.clickOnCloseBtnFromLogInBottomSheet();
		
		Assert.assertTrue(communityFeedPage.isMessagesDisplayed(), "Message not displayed in community feed page");
		
		communityFeedPage.clickOnShareIcon();
		Assert.assertTrue(communityFeedPage.isShareBottomSheetDisplayed(), "Share bottom sheet not displayed on clicking share message icon");
		communityFeedPage.clickOnCloseBtnFromShareBottomSheet();
		Assert.assertTrue(communityFeedPage.isAuthorImageDisplayed(), "Author image not displayed in community feed page");
		Assert.assertTrue(communityFeedPage.isAuthorNameDisplayed(), "Author name not displayed in community feed page");
		Assert.assertTrue(communityFeedPage.isMessagePostedDateDisplayed(), "Message posted date not displayed in community feed page");
		communityFeedPage.swipeCommunityFeedPage();
		Assert.assertTrue(communityFeedPage.isCommentFeildDisplayed(), "Comment feild not displayed in community feed page");
		Assert.assertTrue(communityFeedPage.isProfileIconDisplayedInCommentSection(), "profile icon not displayed in comment section");
		
		//checking comment feild and comment icon
		communityFeedPage.clickOnCommentIcon();
		Assert.assertTrue(commentsPage.isCommentPageTitleDisplayed(), "Comments Page title not displayed on clicking comment icon from community feed page");
		commentsPage.clickOnBackBtnFromCommentsPage();
		communityFeedPage.clickOnCommentFeild();
		Assert.assertTrue(commentsPage.isCommentPageTitleDisplayed(), "Comments Page title not displayed on clicking comment feild from community feed page");
		commentsPage.clickOnBackBtnFromCommentsPage();
		Assert.assertTrue(communityFeedPage.isBottomJoinBtnInCommunityFeedPageDisplayed(), "Join community button not displayed while scrolling through community feed page");
		Assert.assertTrue(communityFeedPage.isCommunityTopbarTitleDisplayed(), "Community top bar title not displayed while scrolling through community feed");
		Assert.assertTrue(communityFeedPage.isWhatsAppShareIconInTopBarDisplayedI(), "Whatsapp share icon in top bar not displayed");
		Assert.assertTrue(communityFeedPage.isProfileImageDisplayedInTopBar(), "Profile image not displayed in top bar of community feed page");
		communityFeedPage.clickOnWhatsAppShareIcon();
		Assert.assertTrue(communityFeedPage.isShareBottomSheetDisplayed(),"Share bottom sheet not displayed" );
		communityFeedPage.clickOnCloseBtnFromShareBottomSheet();
		communityFeedPage.clickOnCommunityTopBarTitle();
		Assert.assertTrue(communityProfilePage.isCommunityDetailHeaderDisplayed(), "Navigation to community profile page not validated on clicking top bar from community feed");
		communityProfilePage.clickOnBackBtn();
		
		//logging in through the community page
		communityFeedPage.clickOnBottomJoinBtn();
		loginBottomSheetPage.clickOnPhoneLogInBtn().enterMobileNumber().clickOnLogInBtn()
		.enterOtp().clickOnContinueBtn();
		
		communityFeedPage.clickOnBackBtn();
		socialLandingPage.clickOnFirstJoinedCommunityCard();
		
		//clicking on heart reaction count
		communityFeedPage.clickOnHeartReactionIconAfterLogIn();
		communityFeedPage.clickOnHeartReactionIconAfterLogIn();
		
		
		communityFeedPage.clickOnCommunityTopBarTitle();
		communityProfilePage.clickOnHorizontalEllipsis().clickOnCommunityLeaveBtn().clickOnYesbtnFromComfirmationPopup();
		communityProfilePage.clickOnBackBtn();
		
		communityFeedPage.clickOnHeartReactionIcon();
		Assert.assertTrue(communityFeedPage.isCommunityJoinBottomSheetDisplayed(), "Community join bottom sheet not displayed on clicking reaction in unjoined state");
		Assert.assertTrue(communityFeedPage.isCommunityNameDisplayed(), "Community name not displayed in community join bottom sheet");
		Assert.assertTrue(communityFeedPage.isCommunityProfileImageDisplayed(), "Community profile image not displayed in join bottom sheet");
		pressNavigationBack(driver);
		communityFeedPage.clickOnHeartReactionIcon();
		communityFeedPage.tapAnyWhereOnTheScreenToCloseCommunityJoinBottomSheet();
			
		//checking back navigation from community feed page
		communityFeedPage.clickOnBackBtn();
		Assert.assertEquals(socialLandingPage.getSamudayaPageTitle(), TestData.samudayaPageTitle,"back navigation not validated on clicking back button from community feed page");
		socialLandingPage.clickOnHamburgerMenuFromSamuday();
		scrollTillEnd();
		menuPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup();
		menuPage.clickOnCancelBtn();	
	}

}
