package Social;


import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import Pages.CommunityFeedPage;
import Pages.CommunityProfilePage;
import Pages.LoginBottomSheetPage;
import Pages.MandirPage;
import Pages.MenuPage;
import Pages.SocialLandingPage;
import TestAnnotations.TestInfo;
import base.BaseTest;

@Listeners(base.ListenerImplimentationclass.class)

public class CommunityProfileTest extends BaseTest{

	@TestInfo(testcaseID="SMPK-TC-14778,SMPK-TC-14781,SMPK-TC-14779,SMPK-TC-14938,SMPK-TC-14944,SMPK-TC-14945,SMPK-TC-14948,SMPK-TC-14949,SMPK-TC-14950,SMPK-TC-14951,SMPK-TC-14940,SMPK-TC-14953,SMPK-TC-14954,SMPK-TC-14957,SMPK-TC-14958,SMPK-TC-14946,SMPK-TC-14947,SMPK-TC-14941")
	@Test

	public void VerifyCommunityProfile() throws Exception
	{
		SocialLandingPage socialLandingPage=new SocialLandingPage(driver);
		CommunityFeedPage communityFeedPage= new CommunityFeedPage(driver);
		CommunityProfilePage communityProfilePage=new CommunityProfilePage(driver);
		MandirPage mandirPage=new MandirPage(driver);
		LoginBottomSheetPage loginBottomSheetPage=new LoginBottomSheetPage(driver);
		MenuPage menuPage= new MenuPage(driver);
		try {
			mandirPage.clickOnHamburgerMenu();
			scrollTillEnd();
			menuPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup();
			menuPage.clickOnCancelBtn();
		}catch(Exception e) {
			menuPage.clickOnCancelBtn();
		}		
		
		//Verify that user is able to navigate to community profie page on clicking the community top bar in community exploration page
		mandirPage.clickOnSamudayaTab();
		socialLandingPage.clickOnCommunityCard();
		communityFeedPage.scrollingThroughCommunityFeed();
		communityFeedPage.clickOnCommunityTopBarTitle();
		Assert.assertTrue(communityProfilePage.isCommunityDetailHeaderDisplayed(), "Community Header  is not displayed");

		//Verify that user is getting respective community description
		Assert.assertTrue(communityProfilePage.iscommunityDescriptionDisplayed(), "Community Description is not displayed");

		//Verify that user is getting respective community profile image and community image in profile section     
		Assert.assertTrue(communityProfilePage.isCommunityImageDisplayed(), "Community Image is not displayed");
		Assert.assertTrue(communityProfilePage.isCommunityProfileImageDisplayed(), "Community Profile Image is not displayed");

		//Verify that user is getting respective member count in community profile page
		Assert.assertTrue(communityProfilePage.isMemberCountDisplayed(), "Member count is not displayed");

		//Verify that on back navigation from profile page user is able to land on community exploration page
		communityProfilePage.clickOnBackBtn();		
		Assert.assertTrue(communityFeedPage.isCommunityNameDisplayed(), "Community name not displayed");
		communityFeedPage.scrollingThroughCommunityFeed();
		communityFeedPage.clickOnCommunityTopBarTitle();
		communityProfilePage.clickOnJoinBtn();    	
		//Verify that user is getting login popup on clicking join button in unlogged state
		Assert.assertTrue(loginBottomSheetPage.isLoginBottomSheetDisplayed(), "login bottom sheet not displayed");	
		
		//Verify that user is able to successfully login from community profile screen
		loginBottomSheetPage.clickOnPhoneLogInBtn().enterMobileNumber()
		.clickOnLogInBtn()
		.enterOtp()
		.clickOnContinueBtn();	

		//Verify that user is able to join the community on clicking the jon button in community profile page
		Assert.assertTrue(communityProfilePage.isHorizontalEllipsisDisplayed(), "Horizontal Ellipsis is not displayed after joining");

		//Verify that user is getting whatsapp share icon in bottom of the profile page
		Assert.assertTrue(communityProfilePage.isWhatsAppShareIconDisplayed(), "whats app icon is displayed");

		communityProfilePage.clickOnWhatsAppIconBtn();
		Assert.assertTrue(communityProfilePage.isShareBottomSheetDisplayed(), "share bottom sheet not displayed ");
		communityProfilePage.clickOnCloseIconOfShareBottomSheet();
			
		//Verify that user is getting community leave button on clicking the horizontal ellipsis in the right top screen of profile section
		communityProfilePage.clickOnHorizontalEllipsis();
		Assert.assertTrue(communityProfilePage.isCommunityLeaveBtnDisplayed(), "community leave btn not displayed");

		//Verify that user is getting community leave popup on clicking "Samuday chode" button
		communityProfilePage.clickOnCommunityLeaveBtn();
		Assert.assertTrue(communityProfilePage.isCommunityLeavePopupDisplayed(), "community leave popup is not displayed");

		//Verify that user is able to close the popup on clicking "nahi, rehne de" option in the popup
		communityProfilePage.clickOnPopupCancelBtn();
		Assert.assertTrue(communityProfilePage.isCommunityDetailHeaderDisplayed(), "Community Header is not displayed");

		//Verify that user is able to successfully leave the community
		communityProfilePage.clickOnHorizontalEllipsis().clickOnCommunityLeaveBtn().clickOnYesbtnFromComfirmationPopup();

		//Verify that user is getting community join button after successfully unjoining the community
		Assert.assertTrue(communityProfilePage.isJoinBtnDisplayed(), "Join btn is not displayed");

		//Verify that community feed page is updating when back navigation after leaving or joining a community from community profile page
		communityProfilePage.clickOnBackBtn();		
		Assert.assertTrue(communityFeedPage.isJoinBtnInCommunityFeedPageDisplayed(), "join btn is no displayed  after back navigation");	
		communityFeedPage.scrollingThroughCommunityFeed();
		communityFeedPage.clickOnCommunityTopBarTitle();

		//Verify that user is able to close the community leave popup on clicking device back button
		communityProfilePage.clickOnJoinBtn();    
		communityProfilePage.clickOnHorizontalEllipsis().clickOnCommunityLeaveBtn();
		waitOrPause(5);
		pressNavigationBack(driver);
		waitOrPause(5);
		Assert.assertTrue(communityProfilePage.isCommunityLeaveBtnDisplayed(), "Community leave is not displayed");
		
		//Verify that user is able to close the community leave popup on clicking anywhere on the screen other than the popup
		communityProfilePage.clickOnCommunityLeaveBtn();
		communityProfilePage.tapByCoordinatesInCommunityProfilePage();
		waitOrPause(5);
		Assert.assertTrue(communityProfilePage.isCommunityLeaveBtnDisplayed(), "Community leave is not displayed");		
	
		communityProfilePage.clickOnBackBtn();
		communityFeedPage.clickOnBackBtn();		
		
	}			
}
