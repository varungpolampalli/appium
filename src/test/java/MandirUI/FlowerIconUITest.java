package MandirUI;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import UIPages.FlowerBottomSheetUIPage;
import UIPages.MandirUIPage;
import base.BaseTest;
import base.UITestData;

@Listeners(base.ListenerImplimentationclass.class)

public class FlowerIconUITest extends BaseTest {
	@Test(groups= {"regression","smoke"})
	public void verifyFlowerIconUI() throws Exception
	{
		FlowerBottomSheetUIPage flowerBottomSheetUIPage=new FlowerBottomSheetUIPage(driver);
		MandirUIPage mandirUIPage =new MandirUIPage(driver);
		mandirUIPage.isFlowerIconDisplayed();
		mandirUIPage.clickOnFlowerIcon();
		
		//checking if Phol ki ladhi is displayed
		Assert.assertTrue(flowerBottomSheetUIPage.isPholkiLadhiDisplayed(), "checking if Phol ki ladhi is displayed");	
		//validating  Phol ki ladhi Flower text 
		String actualPholkiLaghiText=flowerBottomSheetUIPage.getPholkiLadhiFlowerText();
		Assert.assertEquals(actualPholkiLaghiText, UITestData.pholkiladhiFlowerText, "Phol ki ladhi Flower text not validated");

		//checking if Ghonda Flower is displayed
		Assert.assertTrue(flowerBottomSheetUIPage.isGhondaFlowerDisplayed(),"checking  Ghonda Flower is displayed");
		//validating  Ghonda Flower text 
		String actualGhondaFlowerText=flowerBottomSheetUIPage.getGhondaFlowerText();
		Assert.assertEquals(actualGhondaFlowerText, UITestData.ghondaFlowerText, "Ghonda Flower text not validated");

		//checking if BhelPatri Flower is displayed
		Assert.assertTrue(flowerBottomSheetUIPage.isBhelapatriFlowerDisplayed(),"checking  BhelPatri Flower is displayed");
		//validating  BhelPatri Flower text 
		String actualBhelpatriFlowerText=flowerBottomSheetUIPage.getBhelapatriFlowerText();
		Assert.assertEquals(actualBhelpatriFlowerText, UITestData.bhelapatriFlowerText, "BhelPatri Flower text not validated");

		//checking if mogra Flower is displayed
		Assert.assertTrue(flowerBottomSheetUIPage.isMograFlowerDisplayed(),"checking  Mogra Flower is displayed");
		//validating  mogra Flower text 
		Assert.assertTrue(flowerBottomSheetUIPage.getMograFlowerText().contains(UITestData.mograFlowerText), "Mogra Flower text not validated");

		flowerBottomSheetUIPage.slideOverFlowerCorousel();

		//checking if gulhal Flower is displayed
		Assert.assertTrue(flowerBottomSheetUIPage.isGulhalFlowerDisplayed(),"checking  Gulhal Flower is displayed");
		//validating  gulhal Flower text 
		String actualGulhalFlowerText=flowerBottomSheetUIPage.getGulhalFlowerText();
		Assert.assertEquals(actualGulhalFlowerText, UITestData.gulhalFlowerText, "Gulhal Flower text not validated");

		//checking if kamal Flower is displayed
		Assert.assertTrue(flowerBottomSheetUIPage.isKamalFlowerDisplayed(),"checking  Kamal Flower is displayed");
		//validating  kamal Flower text 
		String actualKamalFlowerText=flowerBottomSheetUIPage.getKamalFlowerText();
		Assert.assertEquals(actualKamalFlowerText, UITestData.kamalFlowerText, "Kamal Flower text not validated");

		//checking if Champa Flower is displayed
		Assert.assertTrue(flowerBottomSheetUIPage.isChampaFlowerDisplayed(),"checking  Champa Flower is displayed");
		//validating  Champa Flower text 
		String actualChampaFlowerText=flowerBottomSheetUIPage.getChampaFlowerText();
		Assert.assertEquals(actualChampaFlowerText, UITestData.champaFlowerText, "Champa Flower text not validated");

		flowerBottomSheetUIPage.slideOverFlowerCorousel();

		//checking if Gulabha Flower is displayed
		Assert.assertTrue(flowerBottomSheetUIPage.isGulabhaFlowerDisplayed(),"checking  Gulabha Flower is displayed");
		//validating  Gulabha Flower text 
		String actualGulabFlowerText=flowerBottomSheetUIPage.getGulabhaFlowerText();
		Assert.assertEquals(actualGulabFlowerText, UITestData.gulabhaFlowerText, "Gulabha Flower text not validated");

		//checking if Dhatura Flower is displayed
		Assert.assertTrue(flowerBottomSheetUIPage.isDhaturaFlowerDisplayed(),"checking  Dhatura Flower is displayed");
		//validating  Dhatura Flower text 
		String actualDhaturaFlowerText=flowerBottomSheetUIPage.getDhaturaFlowerText();
		Assert.assertEquals(actualDhaturaFlowerText, UITestData.dhaturaFlowerText, "Dhatura Flower text not validated");

		//checking if Palaash Flower is displayed
		Assert.assertTrue(flowerBottomSheetUIPage.isPalaashFlowerDisplayed(),"checking  Palaash Flower is displayed");
		//validating  Palaash Flower text 
		String actualPalaashFlowerText=flowerBottomSheetUIPage.getPalaashFlowerText();
		Assert.assertEquals(actualPalaashFlowerText, UITestData.PalaashFlowerText, "Palaash Flower text not validated");

		flowerBottomSheetUIPage.slideOverFlowerCorousel();

		//checking if Ashok Flower is displayed
		Assert.assertTrue(flowerBottomSheetUIPage.isAshokFlowerDisplayed(),"checking  Ashok Flower is displayed");
		//validating  Ashok Flower text 
		String actualAshokFlowerText=flowerBottomSheetUIPage.getAshokFlowerText();
		Assert.assertEquals(actualAshokFlowerText, UITestData.ashokFlowerText, "Ashok Flower text not validated");

		//checking if Paarijaat Flower is displayed
		Assert.assertTrue(flowerBottomSheetUIPage.isPaarijaatFlowerDisplayed(),"checking  Paarijaat Flower is displayed");
		//validating  Paarijaat Flower text 
		String actualPaarijaatFlowerText=flowerBottomSheetUIPage.getPaarijaatFlowerText();
		Assert.assertEquals(actualPaarijaatFlowerText, UITestData.paarijaatFlowerText, "Paarijaat Flower text not validated");

		//checking if Kovidaar Flower is displayed
		Assert.assertTrue(flowerBottomSheetUIPage.isKovidaarFlowerDisplayed(),"checking  Kovidaar Flower is displayed");
		//validating  Kovidaar Flower text 
		String actualKovidaarFlowerText=flowerBottomSheetUIPage.getKovidaarFlowerText();
		Assert.assertEquals(actualKovidaarFlowerText, UITestData.kovidaarFlowerText, "Kovidaar Flower text not validated");

		flowerBottomSheetUIPage.slideOverFlowerCorousel();	
		//checking if Nilakamal Flower is displayed
		Assert.assertTrue(flowerBottomSheetUIPage.isNilakamalFlowerDisplayed(),"checking  Nilakamal Flower is displayed");
		//validating  Nilakamal Flower text 
		String actualNilakamalFlowerText=flowerBottomSheetUIPage.getNilakamalFlowerText();
		Assert.assertEquals(actualNilakamalFlowerText, UITestData.NilakamalFlowerText, "Nilakamal Flower text not validated");

		//clicking on last flower to validate tutorial
		flowerBottomSheetUIPage.clickOnLockedFlower();
		Assert.assertTrue(flowerBottomSheetUIPage. isTutorialDisplayedForLockedFlower(), "flower tutorial is not displayed");

	}
}
