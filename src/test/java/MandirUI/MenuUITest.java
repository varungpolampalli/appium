package MandirUI;

import Pages.MandirPage;
import TestAnnotations.TestInfo;
import UIPages.MandirUIPage;
import UIPages.MenuUIPage;
import base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners(base.ListenerImplimentationclass.class)
public class MenuUITest extends BaseTest {

	@TestInfo(testcaseID = "")
	@Test(groups= {"regression","smoke"})
	public void verifyMenuPageUI() throws Exception {
		MandirUIPage mandirUIPage=new MandirUIPage(driver);
		MenuUIPage menuUIPage=new MenuUIPage(driver);

		try {
			mandirUIPage.clickOnHamburgerMenu().clickOnLoginBtn().clickOnPhoneLogInBtn()
			.enterMobileNumber()
			.clickOnLogInBtn()
			.enterOtp()
			.clickOnContinueBtn();
		}catch(Exception e) {
			logger.info("user is already logged in");
			pressNavigationBack(driver);
			scrollTillEnd();
			menuUIPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup();
			menuUIPage.scrollToBeginning();
			menuUIPage.clickOnLoginBtn().clickOnPhoneLogInBtn().enterMobileNumber().clickOnLogInBtn().enterOtp().clickOnContinueBtn();
		}
		Assert.assertTrue(menuUIPage.isCancelBtnDisplayed(),"Cancel button not displayed in menu page");
		Assert.assertTrue(menuUIPage.isUserNameDisplayed(),"Username not displayed in menu page");
		Assert.assertTrue(menuUIPage.isPhoneNumberDisplayed(),"phone number not displayed in menu page");
		Assert.assertTrue(menuUIPage.isNishulkKundliBanayeTabDisplayed(),"Nishulk kundli banaye tab not displayed in menu page");
		Assert.assertTrue(menuUIPage.isPanchangTabDisplayed(),"Panchang tab not displayed in Menu page");
		Assert.assertTrue(menuUIPage.isGeethShlokTabDisplayed(),"Geeth shloka  tab not displayed in Menu page");
		Assert.assertTrue(menuUIPage.iskundliTabDisplayed(),"Kundli  tab not displayed in Menu page");
		Assert.assertTrue(menuUIPage.isSahityaBhandarTabDisplayed(),"Sahitya bhandar tab not displayed in Menu page");
		Assert.assertTrue(menuUIPage.isWhatsAppparSamparkKareTabDisplayed(),"WhatsApp par sampark kare tab not displayed in menu page");
		Assert.assertTrue(menuUIPage.isAppUpdateKareTabDisplayed(),"App update kare  tab not displayed in Menu page");
		Assert.assertTrue(menuUIPage.isAppRateKareTabDisplayed(),"App rate kare  tab not displayed in Menu page");
		scrollTillEnd();
		Assert.assertTrue(menuUIPage.isAppShareKareTabDisplayed(),"App share kare  tab not displayed in Menu page");
		Assert.assertTrue(menuUIPage.isApneSujavBhejeTabDisplayed(),"apne sujav bheje tab not displayed in Menu page");
		Assert.assertTrue(menuUIPage.isTermsAndConditionsTabDisplayed(),"Terms and conditions  tab not displayed in Menu page");
		Assert.assertTrue(menuUIPage.isPrivacypolicyTabDisplayed(),"Privacy policy  tab not displayed in Menu page");
		Assert.assertTrue(menuUIPage.isSriMandirKeBareMeinTabDisplayed(),"Sri mandir ke bare mein tab not displayed in Menu page");
		Assert.assertTrue(menuUIPage.isLogoutTabDisplayed(),"logout tab not displayed in Menu page");

		//logging out from the application
		menuUIPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup();
	}

}
