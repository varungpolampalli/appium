package Profile;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import Pages.KundliPage;
import Pages.LoginBottomSheetPage;
import Pages.MandirPage;
import Pages.MenuPage;
import Pages.PunyaMudraPage;
import TestAnnotations.TestInfo;
import base.BaseTest;
import base.TestData;

//@Listeners(base.ListenerImplimentationclass.class)
public class KundliTest extends BaseTest{

	@TestInfo(testcaseID="SMPK-TC-3624,SMPK-TC-3626,SMPK-TC-3628,SMPK-TC-3629,SMPK-TC-3627,SMPK-TC-5272,SMPK-TC-3625,SMPK-TC-7061,SMPK-TC-3633,SMPK-TC-3634,"
			+ "SMPK-TC-3636,SMPK-TC-5273,SMPK-TC-5274, SMPK-TC-3631,SMPK-TC-3638, SMPK-TC-3635,SMPK-TC-7059,SMPK-TC-7060,SMPK-TC-3651,SMPK-TC-3671,SMPK-TC-3648")
	@Test(groups= {"regression","smoke"})
	public void verifyKundliSlot() throws Exception
	{
		//Resetting app to make sure that 3rd and 4th kundli to have  punya mudra eligibility coins
		MandirPage mandirpage= new MandirPage(driver);
		KundliPage kundlipage= new KundliPage(driver);
		MenuPage menuPage=new MenuPage(driver);
		LoginBottomSheetPage loginbottomsheetpage=new LoginBottomSheetPage(driver);
		driver.resetApp();
		driver.launchApp();
		kundlipage.gettingOnBoardingPage();
		mandirpage.clickOnHamburgerMenu().clickOnkundliTab();
		kundlipage.verticalScroll();

		//validating on clicking nishulk kundli button user is navigated to kundli report banavaye page
		String actualKundliTitle=kundlipage.getKundliPageTitle();
		Assert.assertEquals(actualKundliTitle,TestData.kundliPageTitle, "Kundli page title  not validated");

		//Verify that Kundli report page have 4 slots
		String actualFirstKundliSlotTabText =kundlipage.getFirstKundliSlotTabText();
		Assert.assertEquals(actualFirstKundliSlotTabText, TestData.firstKundliSlotTabText, "first kundli slot tab title  not validated");
		String actualSecondKundliSlotTabText =kundlipage.getSecondKundliSlotTabText();
		Assert.assertEquals(actualSecondKundliSlotTabText, TestData.secondKundliSlotTabText, "second kundli slot tab title  not validated");
		String actualThirdKundliSlotTabText =kundlipage.getThirdKundliSlotTabText();
		Assert.assertEquals(actualThirdKundliSlotTabText, TestData.thirdKundliSlotTabText, "third kundli slot tab title not validated");
		String actualFourthKundliSlotTabText =kundlipage.getFourthKundliSlotTabText();
		Assert.assertEquals(actualFourthKundliSlotTabText, TestData.fourthKundliSlotTabText, "fourth kundli slot tab title not validated");	

		//Verify that whatsapp share in the Kundli reports page
		kundlipage.clickOnShareIcon();
		Assert.assertTrue(kundlipage.isMessageSharingBottomSheetDisplayedInKundli(), "share Bottom sheet not validated in kundli Page");
		kundlipage.pressNavigationBack(driver);
		kundlipage.verticalScroll();

		//validating 3rd and 4th kundli punya mudra coins
		String actualThirdKundliPunyaMudraCoins =kundlipage.getThirdKundliPunyaMudraEligibilityCoins();
		Assert.assertEquals(actualThirdKundliPunyaMudraCoins, TestData.thirdKundliPunyaMudraCoins, " Third kundli PunyaMudra  coin is not validated");
		String actualFourthKundliPunyaMudraCoins=kundlipage.getFourthKundliPunyaMudraEligibilityCoins();
		Assert.assertEquals(actualFourthKundliPunyaMudraCoins, TestData.fourthKundliPunyamudraCoins, " Fourth kundli PunyaMudra  coin is not validated");

		//validating the bottom sheet displayed when user has less Punya mudra to be qualified to get Kundli at any slots
		kundlipage.clickOnFourthKundliSlotTabText();
		Assert.assertTrue(kundlipage.isPunyaMudraBottomSheetDisplayed(), "punya mudra Bottom sheet not validated in kundli Page");

		//validating the bottom sheet redirection to Punya mudra page to check eligibility
		String actualPunyaMudraPageTitle =kundlipage.clickOnPunyaMudraTutorialBtn().getPunyaMudraPageTitle();
		Assert.assertEquals(actualPunyaMudraPageTitle, TestData.punyaMudraTutorialPageTitle, "PunyaMudra Tutorial Button is not validated");


		//Verify that clicking on unlocked slot in the kundli page should ask user to login with bottom sheet displayed
		pressNavigationBack(driver);
		try {
			kundlipage.clickOnFirstKundliSlotTabText();
			Assert.assertTrue(loginbottomsheetpage.isLoginBottomSheetDisplayed(), "user is not displayed with login bottom sheet ");	
		}
		catch(Exception e) {

			logger.info("user didn't get login bottom sheet ");    
		}		

		pressNavigationBack(driver);		
		kundlipage.clickOnFirstKundliSlotTabText();
		try {
			if(loginbottomsheetpage.isLoginBottomSheetDisplayed());
			{
				loginbottomsheetpage.clickOnPhoneLogInBtn().enterMobileNumber()
				.clickOnLogInBtn()
				.enterOtp()
				.clickOnContinueBtn();
				kundlipage.clickOnFirstKundliSlotTabText();	
			}	
		}catch(Exception e) {
			logger.info("user is already logged in");    
		}		
		try {				    	 
			if(kundlipage.isPunyaMudraBottomSheetDisplayed());
			{
				kundlipage.clickOnPunyaMudraTutorialBtn();
				PunyaMudraPage punyamudraPage=new PunyaMudraPage(driver);
				punyamudraPage.clickOnLoginTabFromPunyamudraPage();				
				loginbottomsheetpage.clickOnPhoneLogInBtn().enterMobileNumber()
				.clickOnLogInBtn()
				.enterOtp()
				.clickOnContinueBtn();
				pressNavigationBack(driver);
				kundlipage.clickOnFirstKundliSlotTabText();	
			}	     
		}catch(Exception e) {			
			logger.info("user is already logged from punya mudra bottom sheet ");			
		}

		//Verifying  the slots by clicking on unlocked kundli slot when user have eligible Punya mudra's for any of the slots
		String actualKundlikeliyeJaankaridePageTitle=kundlipage.getKundliFormPageTitle();
		Assert.assertEquals(actualKundlikeliyeJaankaridePageTitle, TestData.kundliFormPageTitle, "kundlikeliyejaankari page title not validated by clicking unlocked kundli slot");

		//validating gender selection Tab
		Assert.assertTrue(kundlipage.isOtherGenderTabSelected(), "Other gender tab is not selectable");
		Assert.assertTrue(kundlipage.isMaleGenderTabSelected(), "Male gender tab is not selectable");
		Assert.assertTrue(kundlipage.isFemaleGenderTabSelected(), "Female gender tab is not selectable");

		//Typing UserName to username text feild
		kundlipage.clearNameTextFeild().enterName();
		Assert.assertEquals(kundlipage.getUserNameText(), TestData.userName, "Name not validated in kundli page");

		//validating birth selection tab
		kundlipage.clickOnBirthDateSelectionTab();
		Assert.assertTrue(kundlipage.isCalendarBottomSheetDisplayed(), "calendar bottom sheet is not displayed on clicking birth date selection tab");
		kundlipage.swipeDaySelectionTab().swipeMonthSelectionTab().swipeYearSelectionTab();
		int actualdayValue = kundlipage.getSelectedDayValue();
		int actualYearValue = kundlipage.getSelectedYearValue();
		kundlipage.clickOnSaveButtonFromCalenderBottomSheet();
		Assert.assertTrue(actualdayValue==kundlipage.getActualDayValue(), "selected DOB not validated");
		Assert.assertTrue(actualYearValue==kundlipage.getActualYearValue(), "selected year not validated");

		//validating Time selection tab
		kundlipage.clickOnBirthTimeSelectionTab();
		kundlipage.swipeHourSelectionTab().swipeMinuteSelectionTab();
		int actualHourValue = kundlipage.getSelectedHourValue();
		int actualMinuteValue = kundlipage.getSelectedMinuteValue();
		kundlipage.clickOnSaveButtonFromCalenderBottomSheet();
		Assert.assertTrue(actualHourValue==kundlipage.getActualHourValue(), "selected Hour not validated");
		Assert.assertTrue(actualMinuteValue==kundlipage.getActualMinuteValue(), "selected Minute not validated");

		//validating close button of time bottom sheet page
		kundlipage.clickOnBirthTimeSelectionTab();
		kundlipage.clickOnCloseButtonFromTimeBottomSheet();
		Assert.assertTrue(kundlipage.isAageBadeButtonDisplayed(), "Time bottom sheet is not closed on clicking cancel button");

		//validating location tab
		kundlipage.clickOnLocationTab();
		Assert.assertTrue(kundlipage.isLocationNameDisplayedBeforeSearching(), "user is not  displayed with Popular localities before search");
		kundlipage.typeLocationInLocationSearchFeild();
		Assert.assertTrue(kundlipage.getFirstLocationNameFromLocationTab().contains("Bangalore"), "search suggestions is not proper as per the search query");
		kundlipage.clickOnLocationFromLocationList();
		Assert.assertTrue(kundlipage.getLocationNameFromLocationTab().contains("Bangalore"), "user is not getting the respective selected location from location search  Page");

		//Validate that user can re-select the Location before submitting the Kundli form
		kundlipage.clickOnAgebadeBtn().clickOnVapasJaayeBtn().clickOnLocationTab();
		kundlipage.typeLocationInLocationSearchFeildAfterSeleting();
		kundlipage.clickOnLocationFromLocationList();
		Assert.assertTrue(kundlipage.getLocationNameFromLocationTab().contains("Delhi"), "user is not getting the respective selected location from location search  Page");

		//Verify that user is displayed with confirmation popup before submitting the kundli form
		kundlipage.clickOnAgebadeBtn();
		Assert.assertTrue(kundlipage.isConfirmationPopupDisplayed(),"confirmation pop up is not displayed");

		//Validate the details entered in the kundli form should be the same on confirmation Popup
		Assert.assertEquals(TestData.userName,kundlipage.getnameFromConfirmationPopup() , "Name not validated after saving");
		Assert.assertEquals(TestData.gender,kundlipage.getGenderFromConfirmationPopup() , "Gender not validated after saving");
		Assert.assertEquals(TestData.locationName,kundlipage.getLocationFromConfirmationPopup() , "location not validated after saving");	
		Assert.assertTrue(kundlipage.isDateTimeOnConfirmationPopupDisplayed(),"Date and time on confirmation pop up is not displayed");

		//Validate that user is displayed with a Save information popup upon clicking back navigation in Kundli form page
		kundlipage.clickOnCancelBtnInConfirmationPopup();
		kundlipage.clickOnBackBtnFromKundliFormPage();
		Assert.assertTrue(kundlipage.isSaveInformationPopupDisplayed(), "save information pop up is not displayed after clicking on back btn");
		kundlipage.clickOnCancelBtnOnSaveInformationpopup();

		//Verify that Kudli page have the introduction video and back button navigating back to respective page
		kundlipage.clickOnYoutubeBtn();
		Assert.assertTrue(kundlipage.isYouTubeBackBtnDisplayed(), "youtube back not validated in kundli Page");
	

		//logging out from kundli report page
		pressNavigationBack(driver);
		kundlipage.clickOnBackBtnfromKundliReportPage();
		scrollTillEnd();
		menuPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup();
		menuPage.clickOnCancelBtn();

	}
}


