package Profile;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import Pages.EditProfilePage;
import Pages.MandirPage;
import Pages.MenuPage;
import Pages.ProfilePage;
import TestAnnotations.TestInfo;
import base.BaseTest;
import base.FilePaths;
import base.FileUtility;
import base.TestData;

//@Listeners(base.ListenerImplimentationclass.class)
public class EditProfileTest extends BaseTest {

	@TestInfo(testcaseID = "SMPK-TC-4467,SMPK-TC-4468,SMPK-TC-4543,SMPK-TC-4544,SMPK-TC-4545,SMPK-TC-4557,SMPK-TC-4558,SMPK-TC-4559,SMPK-TC-4560,SMPK-TC-4561,SMPK-TC-4562,SMPK-TC-4563,SMPK-TC-4564,"
							+ "SMPK-TC-4565,SMPK-TC-4566,SMPK-TC-4568,SMPK-TC-4569,SMPK-TC-4570,SMPK-TC-4571,SMPK-TC-4572,SMPK-TC-4585,SMPK-TC-4586,SMPK-TC-4587,SMPK-TC-4589,SMPK-TC-4590")
	@Test(groups= {"regression","smoke"})
	public void verifyEditProfile() throws Exception {
		MandirPage mandirPage=new MandirPage(driver);
		MenuPage menuPage=new MenuPage(driver);
		EditProfilePage editProfilePage=new EditProfilePage(driver);
		ProfilePage profilePage=new ProfilePage(driver);
		try {
			mandirPage.clickOnHamburgerMenu()
			.clickOnLoginBtn().clickOnPhoneLogInBtn().enterMobileNumber()
			.clickOnLogInBtn()
			.enterOtp()
			.clickOnContinueBtn();
			menuPage.clickOnLoginBtn();
		}catch(Exception e) {
			logger.info("user is already logged in");
		}

		//pushing file to device for profile image upload
		pushFileToDevice(FilePaths.DEVICEFILEPATH, FilePaths.PROFILEIMAGEPATH);

		//validating on clicking edit kare button user is navigated to edit profile page
		profilePage.clickOnEditBtn();
		Assert.assertEquals(editProfilePage.getEditProfilePageTitle(), TestData.editProfilePageTitle, "Edit profile screen navigation not validated");

		//validating device back button from edit profile page
		pressNavigationBack(driver);
		Assert.assertEquals(profilePage.getprofilePageTitle(),TestData.profilePageTitle,"screen is not navigated to my profile on clicking device back navigation");

		//uploading profile image
		profilePage.clickOnEditBtn().clickOnChangePhotoBtn().clickOnImagesBtnFromDevice().clickOnPictureTabFromDevice();

		//validating gender selection Tab
		Assert.assertTrue(editProfilePage.isOtherGenderTabSelected(), "Other gender tab is not selectable");
		Assert.assertTrue(editProfilePage.isFemaleGenderTabSelected(), "Female gender tab is not selectable");
		Assert.assertTrue(editProfilePage.isMaleGenderTabSelected(), "Male gender tab is not selectable");

		//validating birth selection tab
		editProfilePage.clickOnBirthDateSelectionTab();
		Assert.assertTrue(editProfilePage.isCalendarBottomSheetDisplayed(), "calendar bottom sheet is not displayed on clicking birth date selection tab");
		editProfilePage.swipeDaySelectionTab().swipeMonthSelectionTab().swipeYearSelectionTab();
		int actualdayValue = editProfilePage.getSelectedDayValue();
		int actualYearValue = editProfilePage.getSelectedYearValue();
		editProfilePage.clickOnSaveButtonFromCalenderBottomSheet();
		Assert.assertTrue(actualdayValue==editProfilePage.getActualDayValue(), "selected DOB not validated");
		Assert.assertTrue(actualYearValue==editProfilePage.getActualYearValue(), "selected year not validated");


		//validating Time selection tab
		editProfilePage.clickOnBirthTimeSelectionTab();
		editProfilePage.swipeHourSelectionTab().swipeMinuteSelectionTab();
		int actualHourValue = editProfilePage.getSelectedHourValue();
		int actualMinuteValue = editProfilePage.getSelectedMinuteValue();
		editProfilePage.clickOnSaveButtonFromCalenderBottomSheet();
		Assert.assertTrue(actualHourValue==editProfilePage.getActualHourValue(), "selected Hour not validated");
		Assert.assertTrue(actualMinuteValue==editProfilePage.getActualMinuteValue(), "selected Minute not validated");

		//validating close and device back button of time bottom sheet page
		editProfilePage.clickOnBirthTimeSelectionTab();
		pressNavigationBack(driver);
		Assert.assertTrue(editProfilePage.isSaveButtonDisplayed(), "Time bottom sheet is not closed on clicking device back button");
		editProfilePage.clickOnBirthTimeSelectionTab();
		editProfilePage.clickOnCloseButtonFromTimeBottomSheet();
		Assert.assertTrue(editProfilePage.isSaveButtonDisplayed(), "Time bottom sheet is not closed on clicking device back button");

		//Typing UserName to username text feild
		String expectedUserName = editProfilePage.clearNameTextFeild().enterName();
		scrollTillEnd();
			
		//validating location tab
		editProfilePage.clickOnLocationTab();
		Assert.assertTrue(editProfilePage.isLocationListDisplayed(), "screen is not navigated to location search list on clicking location feild from edit profile section ");
		editProfilePage.typeLocationToLocationSearchFeild().getLocationNameFromLocationSearchList();
		editProfilePage.clickOnClearBtn();
		editProfilePage.typeLocationToLocationSearchFeild().clickOnLocationFromLocationList();
		Assert.assertTrue(editProfilePage.getLocationNameFromLocationTab().contains("Bangalore"), "user is not getting the respective selected location from location search  Page");
		editProfilePage.clickOnLocationTab().clickOnBackBtnFromLocationSearchPage();
		Assert.assertEquals(editProfilePage.getEditProfilePageTitle(), TestData.editProfilePageTitle, "Back navigation from location search page not validated");


		//validating occupation tab
		editProfilePage.clickOnOccupationTab();
		Assert.assertTrue(editProfilePage.isOccupationListDisplayed(), "screen is not navigated to occupation list on clicking occupation feild from edit profile section ");
		String occupationNameFromOccupationList = editProfilePage.getOccupationNameFromoccupationList();
		editProfilePage.clickOnOccupationNameFromOccupationList();
		Assert.assertEquals(occupationNameFromOccupationList, editProfilePage.getOccupationNameFromOccupationTab(), "user is not getting the respective selected occupation from occupation list");
		editProfilePage.clickOnOccupationTab().clickOnBackBtnFromOccupationListPage();
		Assert.assertEquals(editProfilePage.getEditProfilePageTitle(), TestData.editProfilePageTitle, "Back navigation from location search page not validated");


		//validating username,phonenumber and profile photo and also verifying the redirection from edit profile page
		editProfilePage.clickOnBackBtn().clickOnSaveBtnFromConfirmationPopUp();
		Assert.assertEquals(profilePage.getprofilePageTitle(),TestData.profilePageTitle,"screen is not navigated to my profile on clicking back button from edit profile section");
		Assert.assertEquals(profilePage.getUserNameText(), expectedUserName, "Name not validated in profile page");
		Assert.assertTrue(editProfilePage.isProfilePhotoDisplayed(), "Profile photo is not displayed");
		String actualPhoneNumberWithCountryCode = editProfilePage.getphoneNumber();
		String actualPhoneNumber = actualPhoneNumberWithCountryCode.substring(3);
		Assert.assertEquals(actualPhoneNumber, FileUtility.getPropertyValue("phoneNumber"), "phone number not validated from my profile page");

		//validating if all the details are updated after saving in edit profile page
		profilePage.clickOnEditBtn();
		Assert.assertTrue(actualdayValue==editProfilePage.getActualDayValue(), "Birth Date not validated after saving");
		Assert.assertTrue(actualYearValue==editProfilePage.getActualYearValue(), "Birth Year not updated after saving");
		Assert.assertTrue(actualHourValue==editProfilePage.getActualHourValue(), "Hour details of birth time not updated after saving");
		Assert.assertTrue(actualMinuteValue==editProfilePage.getActualMinuteValue(), "Minute details of birth time not updated after saving");
		Assert.assertTrue(editProfilePage.getLocationNameFromLocationTab().contains("Bangalore"), "user is not getting the respective selected location from location search  Page");
		Assert.assertEquals(occupationNameFromOccupationList, editProfilePage.getOccupationNameFromOccupationTab(), "user is not getting the respective selected occupation from occupation list");
		
		editProfilePage.clearNameTextFeild().enterDefaultName();
		editProfilePage.clickOnSaveButton();
		
		//logging out from the application
		profilePage.clickOnBackBtn();
		scrollTillEnd();
		menuPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup();

	}


}
