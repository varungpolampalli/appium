package Profile;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import Pages.KundliPage;
import Pages.LoginBottomSheetPage;
import Pages.MandirPage;
import Pages.MenuPage;
import Pages.MyAlarmPage;
import Pages.ProfilePage;
import Pages.PunyaMudraPage;
import Pages.TempleListingPage;
import TestAnnotations.TestInfo;
import base.BaseTest;
import base.TestData;

//@Listeners(base.ListenerImplimentationclass.class)
public class ProfileHomeTest extends BaseTest{
	@TestInfo(testcaseID = "SMPK-TC-4573,SMPK-TC-4574,SMPK-TC-4575,SMPK-TC-4576,SMPK-TC-4577,SMPK-TC-4578,SMPK-TC-4579,SMPK-TC-4580,SMPK-TC-4581,SMPK-TC-4582,SMPK-TC-4584")
	@Test(groups= {"regression","smoke"})
	public void verifyProfileHomePage() throws Exception {
		MandirPage mandirPage=new MandirPage(driver);
		MenuPage menuPage=new MenuPage(driver);
		ProfilePage profilePage=new ProfilePage(driver);
		PunyaMudraPage punyaMudraPage=new PunyaMudraPage(driver);
		KundliPage kundliPage=new KundliPage(driver);
		MyAlarmPage myAlarmPage=new MyAlarmPage(driver);
		TempleListingPage templeListingPage=new TempleListingPage(driver);
		LoginBottomSheetPage loginBottomSheetPage=new LoginBottomSheetPage(driver);
		try {
			mandirPage.clickOnHamburgerMenu()
			.clickOnLoginBtn().clickOnPhoneLogInBtn().enterMobileNumber()
			.clickOnLogInBtn()
			.enterOtp()
			.clickOnContinueBtn();
			menuPage.clickOnLoginBtn();	
		}catch(Exception e) {
			logger.info("user is already logged in");
		}
		//checking if edit profile button is displayed after logging in
		Assert.assertTrue(profilePage.isEditProfileButtonDisplayed(), "Edit Profile Button Is not displayed after logging in");
		
		//checking if punya mudra count is displayed After logging in
		Assert.assertTrue(profilePage.isPunyaMudraCountDisplayed(), "Punya Mudra Count is not displayed after logging in");
		
		//checking Navigation of punya mudra page and also checking the  back navigation
		profilePage.clickOnPunyamudraTab();
		Assert.assertEquals(punyaMudraPage.getPunyaMudraPageTitle(), TestData.punyaMudraTutorialPageTitle, "Screen Navigation To PunyaMudra Page from profile Page not validated");
		punyaMudraPage.clickOnBackButton();
		Assert.assertEquals(profilePage.getprofilePageTitle(), TestData.profilePageTitle, "Back Naviagtion From Punya Mudra Page to profile Page Not validated");
		scrollTillEnd();
		
		//checking Navigation of bhakti chakra Tab and also checking the back navigation
		profilePage.clickOnBhaktiChakraTab();
		Assert.assertTrue(punyaMudraPage.getSriMandirKulUpastithiTitle().contains(TestData.sriMandirHindiText), "Screen Navigation to Bhakthi chakra Tab from profile Page Not validated");
		punyaMudraPage.clickOnBackButton();
		Assert.assertEquals(profilePage.getprofilePageTitle(), TestData.profilePageTitle, "Back Naviagtion From Bhakti chakra Tab  to profile Page Not validated");
		
		//checking Navigation of Kundli report Tab and also checking the back navigation
		profilePage.clickOnKundliReportTab();
		Assert.assertEquals(kundliPage.getKundliPageTitle(), TestData.kundliPageTitle, "Screen Navigation to kundli Page Not validated From Profile Page");
		kundliPage.clickOnBackBtn();
		Assert.assertEquals(profilePage.getprofilePageTitle(), TestData.profilePageTitle, "Back Naviagtion From Kundli Page to profile Page Not validated");
		
		//checking Navigation of Alarm Page and also checking the back navigation
		profilePage.clickOnAapkeAlarm().handleAlarmPermissionBottomSheet();
		Assert.assertEquals(myAlarmPage.getAlarmPageTitle(), TestData.myAlarmPageTitle, "Screen Navigation To Alarm Page Not Validated from profile Page");
		myAlarmPage.clickOnBackBtn();
		Assert.assertEquals(profilePage.getprofilePageTitle(), TestData.profilePageTitle, "Back Naviagtion From My Alarm Page to profile Page Not validated");
		
		//checking Navigation of Chadav Seva Page and also checking the back navigation
		profilePage.clickOnChadavSevaTab();
		Assert.assertEquals(templeListingPage.getChadavSevaPageTitle(),TestData.chadavSevaPageTitle , "Screen Navigation To Chadav Seva Page not validated from Profile Page");
		templeListingPage.clickOnBackBtn();
		Assert.assertEquals(profilePage.getprofilePageTitle(), TestData.profilePageTitle, "Back Naviagtion From Chadav Seva Page to profile Page Not validated");
		
		//validating if login bottom sheet is displayed for unlogged user
		profilePage.clickOnBackBtn();
		scrollTillEnd();
		menuPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup();
		menuPage.scrollToBeginning().clickOnLoginBtn();
		Assert.assertTrue(loginBottomSheetPage.isLoginBottomSheetDisplayed(), "Log in bottom sheet is not displayed");
	}

}
