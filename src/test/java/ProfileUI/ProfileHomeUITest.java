package ProfileUI;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import Pages.MandirPage;
import Pages.MenuPage;
import TestAnnotations.TestInfo;
import UIPages.MandirUIPage;
import UIPages.MenuUIPage;
import UIPages.ProfileHomeUIPage;
import base.BaseTest;

@Listeners(base.ListenerImplimentationclass.class)
public class ProfileHomeUITest extends BaseTest {
	
	@TestInfo(testcaseID = "")
	@Test(groups= {"regression","smoke"})
	public void verifyProfileHome() throws Exception {
		ProfileHomeUIPage profileHomeUIPage=new ProfileHomeUIPage(driver);
		MandirUIPage mandirUIPage=new MandirUIPage(driver);
		MenuUIPage menuUIPage=new MenuUIPage(driver);
		
		mandirUIPage.clickOnHamburgerMenu();
		scrollTillEnd();
		try {
			menuUIPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup();
			menuUIPage.clickOnCancelBtn();
		}catch (Exception e) {
			menuUIPage.clickOnCancelBtn();
		}
		mandirUIPage.clickOnPunyamudraProfileIcon();
		Assert.assertTrue(profileHomeUIPage.isProfilePageTitleDisplayed(), "Profile page title not displayed");
		Assert.assertTrue(profileHomeUIPage.isBackBtnDisplayed(), "Back button not displayed in proifle home page");
		Assert.assertTrue(profileHomeUIPage.isLoginTabFromProfilePageDisplayed(), "Login tab not displayed in profile home page");
		Assert.assertTrue(profileHomeUIPage.isSnakBarMessageDisplayed(), "Snack bar message not displayed in profile page");
		Assert.assertTrue(profileHomeUIPage.isPunyamudraTabDisplayed(), "Punya mudra tab not displayed in profile page");
		Assert.assertTrue(profileHomeUIPage.isBhaktiChakraTabDisplayed(),"Bhakti chakra tab not displayed in profile home page");
		
		profileHomeUIPage.clickOnLoginTabFromProfilePage().clickOnPhoneLogInBtn().
		enterMobileNumber().clickOnLogInBtn().enterOtp().clickOnContinueBtn();
		
		Assert.assertTrue(profileHomeUIPage.isUserNameDisplayed(), "user name not displayed in profile home page");
		Assert.assertTrue(profileHomeUIPage.isPhoneNumberDisplayed(),"phone number not displayed in profile page");
		Assert.assertTrue(profileHomeUIPage.isProfilePhotoDisplayed(),"profile photo not displayed in profile page");
		Assert.assertTrue(profileHomeUIPage.isPunyaMudraCountDisplayed(),"punya mudra count not displayed in profile home page");
		Assert.assertTrue(profileHomeUIPage.isBhaktiChakraTabDisplayed(), "Chakra progress bar not displayed in profile page");
		scrollTillEnd();
		Assert.assertTrue(profileHomeUIPage.isChadavSevaTabDisplayed(), "Chadav seva tab not displayed in profile page");
		Assert.assertTrue(profileHomeUIPage.isKundliReportTabDisplayed(), "Kundli report tab not displayed in profile page");
		Assert.assertTrue(profileHomeUIPage.isAapkeAlarmTabDisplayed(), "alarm tab not displayed in profile page");
		Assert.assertTrue(profileHomeUIPage.isEditProfileButtonDisplayed(), "edit kare button not displayed in profile page");
		
		//logging out from the application
		profileHomeUIPage.clickOnBackBtn();
		mandirUIPage.clickOnHamburgerMenu();
		scrollTillEnd();
		menuUIPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup();
		menuUIPage.clickOnCancelBtn();
	}
	
	
	
}
