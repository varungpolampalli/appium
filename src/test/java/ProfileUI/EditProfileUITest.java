package ProfileUI;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import TestAnnotations.TestInfo;
import UIPages.EditProfileUIPage;
import UIPages.MandirUIPage;
import UIPages.MenuUIPage;
import UIPages.ProfileHomeUIPage;
import base.BaseTest;

@Listeners(base.ListenerImplimentationclass.class)

public class EditProfileUITest extends BaseTest {
	
	@TestInfo(testcaseID = "")
	@Test(groups= {"regression","smoke"})
	public void verifyEditProfileUIPage() throws Exception {
		MandirUIPage mandirUIPage=new MandirUIPage(driver);
		MenuUIPage menuUIPage=new MenuUIPage(driver);
		ProfileHomeUIPage profileHomeUIPage=new ProfileHomeUIPage(driver);
		EditProfileUIPage editProfileUIPage=new EditProfileUIPage(driver);				
		try {
			mandirUIPage.clickOnHamburgerMenu()
			.clickOnLoginBtn().clickOnPhoneLogInBtn().enterMobileNumber()
			.clickOnLogInBtn()
			.enterOtp()
			.clickOnContinueBtn();
			menuUIPage.clickOnLoginBtn();
		}catch(Exception e) {
			logger.info("user is already logged in");
		}
		profileHomeUIPage.clickOnEditBtn();
		
		//checking if all the elements present in Edit Profile page
		Assert.assertTrue(editProfileUIPage.isBackBtnDisplayed(), "Back button not displayed in edit profile page");
		Assert.assertTrue(editProfileUIPage.isEditProfilePageTitle(), "Edit profile page title not displayed in edit profile page");
		Assert.assertTrue(editProfileUIPage.isProfilePhotDisplayed(), "Profile photo not displayed in edit profile page");
		Assert.assertTrue(editProfileUIPage.isChangePhotoBtnDisplayed(), "Photo badle button not displayed in edit profile page");
		Assert.assertTrue(editProfileUIPage.isNameTextFeildDisplayed(), "Name text feild not displayed in edit profile page");
		Assert.assertTrue(editProfileUIPage.isOtherGenderTabDisplayedAndSelected(), "Other gender tab is not displayed or not selected in edit profile page");
		Assert.assertTrue(editProfileUIPage.isFemaleGenderTabDisplayedAndSelected(), "female gender tab is not displayed or not selected in edit profile page");
		Assert.assertTrue(editProfileUIPage.isMaleGenderTabDisplayedAndSelected(), "Male gender tab is not displayed or not selected in edit profile page");
		Assert.assertTrue(editProfileUIPage.isBirthDateSelectionTabDisplayed(), "Birth date selection tab not displayed in Edit profile page");
		Assert.assertTrue(editProfileUIPage.isBirthTimeSelectionTabDisplayed(), "Birth time selection tab not displayed in edit profile page");
		Assert.assertTrue(editProfileUIPage.isLocationTabDisplayed(), "Location tab not in edit profile page");
		scrollTillEnd();
		Assert.assertTrue(editProfileUIPage.isOccupationTabDisplayed(), "Occupation Tab not displayed in edit profile page");
		Assert.assertTrue(editProfileUIPage.isSaveButtonDisplayed(), "Save button not displayed in edit profile page");
		editProfileUIPage.clickOnSaveButton().clickOnBackBtn();
		scrollTillEnd();
		menuUIPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup();
		menuUIPage.clickOnCancelBtn();
		
	}
	
	
	
}
