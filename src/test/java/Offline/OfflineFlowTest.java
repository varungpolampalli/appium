package Offline;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import Pages.ChowgadiyaPage;
import Pages.GeethaShlokPage;
import Pages.GodSelectionPage;
import Pages.HoraPage;
import Pages.MahabhandarPage;
import Pages.MandirPage;
import Pages.MukhyaPage;
import Pages.MusicBottomSheetPage;
import Pages.PanchangPage;
import Pages.ProfilePage;
import Pages.RashifalPage;
import Pages.SujavPage;
import Pages.SuvicharPage;
import Pages.TvPage;
import TestAnnotations.TestInfo;
import base.BaseTest;
import base.TestData;


//@Listeners(base.ListenerImplimentationclass.class)
public class OfflineFlowTest extends BaseTest {

	@TestInfo(testcaseID = "SMPK-TC-3300,SMPK-TC-3301,SMPK-TC-3302,SMPK-TC-7095,SMPK-TC-7096,SMPK-TC-7097,SMPK-TC-7098,SMPK-TC-7099,SMPK-TC-7100,SMPK-TC-7101,"
			+ "SMPK-TC-7102,SMPK-TC-3299")
	@Test(groups= {"regression","smoke"})
	public void verifyOfflineFlow() throws Exception {
		try {
			MandirPage mandirPage=new MandirPage(driver);
			ProfilePage profilePage=new ProfilePage(driver);
			MahabhandarPage mahaBhandarPage=new MahabhandarPage(driver);

			//turning off mobile data and wifi
			turnOffMobileDataAndWifi(driver);
			
			// resetting the app and validating the tutorials in offline mode
			driver.resetApp();
			driver.launchApp();


			GodSelectionPage godSelectionPage=new GodSelectionPage(driver);
			godSelectionPage.clickOnGodSelectionCancelBtn();
			mandirPage.handleOnBoardingTutorial();

			//validating the build temple CTA
			mandirPage.clickOnEditGodBtn();
			godSelectionPage.clickOnGodSelectionSaveBtn();

			//validating profile in offline mode
			mandirPage.clickOnPunyamudraProfileIcon();
			Assert.assertTrue(profilePage.isNoInternetConnectionSnakBarMessageDisplayed(), "No internet connection snack bar message is not displayed in mandir page");
			pressNavigationBack(driver);
			
			//validating toast message in mahabhandar page
			mandirPage.clickOnMahabhandarTab();
			Assert.assertTrue(mahaBhandarPage.isNoInternetConnectionSnakBarMessageDisplayed(), "No internet connection snack bar message is not displayed in mahabhandar page");
			
			//validating suvichar page in offline mode
			SuvicharPage suvicharPage=new SuvicharPage(driver);
			suvicharPage.clickOnSuvicharTab();
			Assert.assertTrue(mahaBhandarPage.isNoInternetConnectionMessageDisplayed(), "No internet connection message is not displayed on clicking suvichar tab");

			//validating geetha shlok page in offline mode
			GeethaShlokPage geethaShlokPage=new GeethaShlokPage(driver);
			geethaShlokPage.clickOnGeethaShlokTab();
			Assert.assertTrue(mahaBhandarPage.isNoInternetConnectionMessageDisplayed(), "No internet connection message is not displayed on clicking Geetha shlok tab");

			//validating Tv page in offline mode
			TvPage tvPage=new TvPage(driver);
			tvPage.scrollToTvTab();
			Assert.assertTrue(mahaBhandarPage.isNoInternetConnectionMessageDisplayed(), "No internet connection message is not displayed on clicking TV tab");

			//validating sujav page in offline mode
			SujavPage sujavPage=new SujavPage(driver);
			sujavPage.clickOnSujavtab();
			Assert.assertTrue(mahaBhandarPage.isNoInternetConnectionMessageDisplayed(), "No internet connection message is not displayed on clicking sujav tab");

			mandirPage.clickOnMandirIcon();
			mandirPage.clickOnMahabhandarTab();
			mahaBhandarPage.swipeHorizontalBar();

			//validating Panchang page in offline mode
			PanchangPage panchangPage=new PanchangPage(driver);
			mahaBhandarPage.clickOnPanchangTab();
			Assert.assertTrue(mahaBhandarPage.isNoInternetConnectionSnakBarMessageDisplayed(), "No internet snack bar message is not displayed in panchang page");
			
			//validating Mukhya page in offline mode
			MukhyaPage mukhyaPage=new MukhyaPage(driver);
			mukhyaPage.clickOnMukyatab();
			Assert.assertTrue(mukhyaPage.isMukhyaCardsDisplayed(), "cards not displayed in mukhya screen");
			Assert.assertTrue(mahaBhandarPage.isNoInternetConnectionSnakBarMessageDisplayed(), "No internet connection snack bar message is not displayed in mukhya page");
			
			//validating Rashifal page in offline mode
			RashifalPage rashifalPage=new RashifalPage(driver);
			rashifalPage.clickOnRashifalTab();
			Assert.assertTrue(mahaBhandarPage.isNoInternetConnectionSnakBarMessageDisplayed(), "No internet connection snack bar message is not displayed in Rashifal page");
			
			//validating hora page in offline mode
			HoraPage horaPage=new HoraPage(driver);
			horaPage.clickOnHoraTab();
			Assert.assertTrue(mahaBhandarPage.isNoInternetConnectionSnakBarMessageDisplayed(), "No internet connection snack bar message is not displayed in mahabhandar page");
			
			//validating chowgadiya page in offline mode
			ChowgadiyaPage chowgadiyaPage=new ChowgadiyaPage(driver);
			mahaBhandarPage.clickOnChowgadiyaTab();
			Assert.assertTrue(mahaBhandarPage.isNoInternetConnectionSnakBarMessageDisplayed(), "No internet connection snack bar message is not displayed in chowgadiya page");
			
			mandirPage.clickOnMandirIcon();
			MandirPage mandirpage=new MandirPage(driver);
			MusicBottomSheetPage musicBottomSheetPage=new  MusicBottomSheetPage(driver);
			Assert.assertTrue(mandirpage. isNoInternetConnectionSnakBarMessageDisplayed(), "No internet connection snack bar message is not displayed");
			mandirpage.clickOnFlowerIcon();
			Assert.assertTrue(mandirpage. isNoInternetConnectionSnakBarMessageDisplayed(), "No internet connection snack bar message is not displayed");
			mandirpage.clickOnSongIcon();
			Assert.assertTrue(musicBottomSheetPage. isNoInternetConnectionMessageDisplayed(), "message in share bottom sheet not displayed in music bottom sheet screen");
			pressNavigationBack(driver);
			mandirpage.clickOnSangeethTab();
			Assert.assertTrue(mandirpage. isNoInternetConnectionSnakBarMessageDisplayed(), "No internet connection snack bar message is not displayed");

			//turning off mobile data and wifi
			turnOnDataAndWifi(driver);

		}catch (Exception e) {
			throw new Exception("Error in offline flow test "+e.getMessage());
		}finally {
			turnOnDataAndWifi(driver);
		}
	}

}
