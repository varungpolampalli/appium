package Mandir;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import Pages.MandirPage;
import TestAnnotations.TestInfo;
import base.BaseTest;

@Listeners(base.ListenerImplimentationclass.class)
/**
 * 
 * @author TestYantra
 *
 */
public class AarthiTest extends BaseTest {
	
	@TestInfo(testcaseID = "SMPK-TC-1833,SMPK-TC-1835,SMPK-TC-1836,SMPK-TC-1838")
	@Test(groups= {"regression","smoke"})
	public void verifyingTheAarthiMode() throws Exception {
		MandirPage mandirPage=new MandirPage(driver);
		mandirPage.clickOnAarthiCTAicon();
		waitOrPause(5);
		mandirPage.clickOnAarthiCTAicon();
		waitOrPause(5);
		mandirPage.dragAarthiThali();
		Assert.assertTrue(mandirPage.isAarthiThaliDisplayed(), "Cannot validate the presence of Aarthi Thali On Mandir Page");

	}
	
	

}
