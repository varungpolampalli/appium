package Mandir;

import Pages.*;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import TestAnnotations.TestInfo;
import base.BaseTest;
import base.ExcelUtility;
import base.FileUtility;
import base.TestData;

@Listeners(base.ListenerImplimentationclass.class)
public class MenuTest extends BaseTest {
	
	@TestInfo(testcaseID = "SMPK-TC-2097,SMPK-TC-2099,SMPK-TC-3035,SMPK-TC-3036,SMPK-TC-3038,SMPK-TC-3062,SMPK-TC-3064,SMPK-TC-3037,SMPK-TC-3055,SMPK-TC-3056,SMPK-TC-3057,SMPK-TC-3058,SMPK-TC-3059,SMPK-TC-3060,SMPK-TC-3061,SMPK-TC-3065,SMPK-TC-3368")
	@Test(groups= {"regression","smoke"})
	public void verifyMenupageOptions() throws Exception {
//		driver.resetApp();
//		driver.launchApp();
		MandirPage mandirPage=new MandirPage(driver);
		MenuPage menuPage=new MenuPage(driver);
		PanchangPage panchangPage=new PanchangPage(driver);
		KundliPage kundliPage=new KundliPage(driver);
		ProfilePage profilePage=new ProfilePage(driver);
		SahithyaPage sahithyaPage=new SahithyaPage(driver);
		GeethaShlokPage geethaShlokPage=new GeethaShlokPage(driver);
		MahabhandarPage mahabhandarPage=new MahabhandarPage(driver);
		
		mandirPage.clickOnMahabhandarTab();
		panchangPage.clickOnHamburgerMenuBtn();
		pressNavigationBack(driver);
		Assert.assertEquals(mahabhandarPage.getMahabhandarPageTitle(), TestData.mahabhandarPageTitle, "Back button navigation in menu page not validated");
		panchangPage.clickOnHamburgerMenuBtn();
		Assert.assertTrue(menuPage.isMenuOptionsDisplayed(), "Menu Page Navigation not validated");
		menuPage.clickOnCancelBtn();
		Assert.assertTrue(mandirPage.isBottomNavigationDisplayed(), "Previous Page navigation Not Validated");
		try {
		mandirPage.clickOnHamburgerMenu().clickOnLoginBtn().clickOnPhoneLogInBtn()
		.enterMobileNumber()
		.clickOnLogInBtn()
		.enterOtp()
		.clickOnContinueBtn();
		}catch(Exception e) {
			logger.info("user is already logged in");
			pressNavigationBack(driver);
			scrollTillEnd();
			menuPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup();
			menuPage.scrollToBeginning();
			menuPage.clickOnLoginBtn().clickOnPhoneLogInBtn().enterMobileNumber().clickOnLogInBtn().enterOtp().clickOnContinueBtn();
		}
		driver.hideKeyboard();
		String actualPhoneNumberWithCountryCode = menuPage.getLogInSubTitle();
		String actualPhoneNumber = actualPhoneNumberWithCountryCode.substring(3);
		Assert.assertEquals(actualPhoneNumber, FileUtility.getPropertyValue("phoneNumber"), "phone number not validated");
		Assert.assertTrue(menuPage.isProfileImageDisplayed(), "Profile Image is not displayed in menu page");
		Assert.assertTrue(menuPage.isProfileNameDisplayed(), "Profile Name is not displayed in menu page");
		menuPage.clickOnLoginBtn();
		Assert.assertEquals(profilePage.getprofilePageTitle(),TestData.profilePageTitle,"screen is not navigated to my profile on clicking user details slot");
		profilePage.clickOnBackBtn();
		scrollToElement(driver, ExcelUtility.getExcelData("locators", "panchangText", "value"));
		menuPage.clickOnPanchangTab();
		Assert.assertEquals(panchangPage.getDailyTabText(), TestData.dailyTabText, "Panchang screen Navigation validated");
		panchangPage.clickOnHamburgerMenuBtn();
		scrollToElement(driver, ExcelUtility.getExcelData("locators", "kundliText", "value"));
		menuPage.clickOnkundliTab();
		Assert.assertEquals(kundliPage.getKundliPageTitle(), TestData.kundliPageTitle, "Kundli screen navigation not validated");
		kundliPage.clickOnBackBtn();
		menuPage.clickOnSahityaBhandarTab();
		Assert.assertEquals(sahithyaPage.getSahithyaTabText(),TestData.sahithyaText ,"Sahithya bhandar screen navigation not validated" );
		sahithyaPage.clickOnHamburgerMenuBtn();
		menuPage.clickOnGeethShlokTab();
		Assert.assertEquals(geethaShlokPage.getGeethShlokaTabTitle(),TestData.geethaShlokaText ,"GeethaShloka screen navigation not validated" );
		geethaShlokPage.clickOnHamburgerMenuBtn();
		menuPage.clickOnAppUpdateKareTab();
		Assert.assertTrue(menuPage.isPlayStoreSignInBtnDisplayed(), "Playstore screen navigation not validated on clicking app update kare link");
		pressNavigationBack(driver);
		menuPage.clickOnAppRateKareTab();
		Assert.assertTrue(menuPage.isPlayStoreSignInBtnDisplayed(), "Playstore screen navigation not validated on clicking app Rate kare link");
		pressNavigationBack(driver);
		scrollTillEnd();
		menuPage.clickOnAppShareKareTab();
		Assert.assertTrue(menuPage.isMessageDisplayedInShareBottomSheet(), "App share kare link not validated");
		pressNavigationBack(driver);
		menuPage.clickOnTermsAndConditionsTab();
		Assert.assertEquals(menuPage.getTermsAndConditionsPageTitle(), TestData.termsAndConditionsText, "Terms and Conditions screen navigation not validated");
		pressNavigationBack(driver);
		menuPage.clickOnPrivacypolicyTab();
		Assert.assertEquals(menuPage.getPrivacyPolicyPageTitle(), TestData.privacyPolicyText, "Privacy Policy  screen navigation not validated");
		pressNavigationBack(driver);
		menuPage.clickOnApneSujavBhejeTab().clickOnHameSandeshBhejeTab();
		Assert.assertTrue(menuPage.isChatContentDisplayed(), "Apne Sujaav bheje screen navigation not validated");
		menuPage.clickOnNavigateBackBtn().clickOnNavigateBackBtn();
		scrollTillEnd();
		menuPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup();
	}

}
