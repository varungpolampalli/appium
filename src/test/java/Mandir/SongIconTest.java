package Mandir;


import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import Pages.AlarmSettingPage;
import Pages.LoginBottomSheetPage;
import Pages.MandirPage;
import Pages.MenuPage;
import Pages.MusicBottomSheetPage;
import Pages.MusicMiniPlayerPage;
import Pages.MyAlarmPage;
import Pages.ProfilePage;
import Pages.SangeethPage;
import TestAnnotations.TestInfo;
import base.BaseTest;
import base.TestData;

@Listeners(base.ListenerImplimentationclass.class)
public class SongIconTest extends BaseTest {
	@TestInfo(testcaseID = "")
	@Test(groups= {"regression","smoke"})
	public void verifySongIconFeature() throws Exception {
		MandirPage mandirPage=new MandirPage(driver);
		MusicBottomSheetPage musicBottomSheetPage=new MusicBottomSheetPage(driver);
		MusicMiniPlayerPage musicMiniPlayerPage=new MusicMiniPlayerPage(driver);
		MyAlarmPage alarmpage=new MyAlarmPage(driver);
		LoginBottomSheetPage logInBottomSheet=new LoginBottomSheetPage(driver);
		SangeethPage sangeethpage=new SangeethPage(driver);
		AlarmSettingPage alarmSettingPage=new AlarmSettingPage(driver);
		MenuPage menuPage=new MenuPage(driver);
		
		try {
			mandirPage.clickOnHamburgerMenu().
			clickOnLoginBtn()
			.clickOnPhoneLogInBtn()
			.enterMobileNumber()
			.clickOnLogInBtn()
			.enterOtp()
			.clickOnContinueBtn();
		}catch (Exception e) {
			logger.info("user is already logged in");
			new ProfilePage(driver).clickOnBackBtn();
		}
		
		new MenuPage(driver).clickOnCancelBtn();
		mandirPage.clickOnSangeethTab().swipeToPasandidaTab().clickOnPasandidaTab();
		try {
			System.err.println(sangeethpage.getFavouriteSongCount());
			for (int i =sangeethpage.getFavouriteSongCount() ; i >0 ;i-- ) {
				sangeethpage.clickOnEllipseIcon().clickOnRemoveFromFavouritesTab();
			}	
		}catch (Exception e) {
			logger.info("Fav song list is empty");
		}
		sangeethpage.clickOnMenuBtn();
		scrollTillEnd();
		menuPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup();
		menuPage.clickOnCancelBtn();
		mandirPage.clickOnMandirIcon();
		
		//validating music bottom sheet
		mandirPage.clickOnSongIcon();
		String songTitleFromMusicList = musicBottomSheetPage.getSongNameFromMusicList();
		Assert.assertTrue(musicBottomSheetPage.isMusicBottomsheetDisplayed(), "Music bottom sheet is not displayed ");
		Assert.assertTrue(musicBottomSheetPage.isMusicCategoriesDisplayed(), "Music Categories is not displayed");
		musicBottomSheetPage.clickOnSong();
		Assert.assertEquals(songTitleFromMusicList, mandirPage.getsongTitleBelowSongIcon(), "Song Title below Music Icon not validated and aslo navigation to mandir page not validated");
		mandirPage.clickOnSongIcon();
		Assert.assertTrue(musicBottomSheetPage.isMainPlayerDisplayed(), "Main Player is Not Displayed On Playing song from the msuic bototm sheet");
		Assert.assertTrue(musicBottomSheetPage.isSongTitleDisplayed(), "Song Title Is Not Displayed");
		Assert.assertTrue(musicBottomSheetPage.isAudioLengthDisplayed(), "Audio Length is Not Displayed in The Main Player");

		//clicking on play or pause button and checking 
		musicBottomSheetPage.clickOnPlayorPauseBtn();
		musicBottomSheetPage.clickOnRepeatModeIcon();
		Assert.assertTrue(musicBottomSheetPage.isRepeatModeOptionsDisplayed(), "control modal options not displayed on clicking repeat mode icon");
		musicBottomSheetPage.clickOnSaveButtonFromRepeatModeOptionsPopUp();

		//validating the lyrics 
		musicBottomSheetPage.clickOnSangeethKeBol();
		Assert.assertTrue(musicBottomSheetPage.isSongLyricsDisplayed(), "Song lyrics  not displayed");
		musicBottomSheetPage.clickOnSangeethKaKramTab().clickOnWhatsappShareIconFromMainPlayer();
		Assert.assertTrue(musicBottomSheetPage.isMessageDisplayedInShareBottomSheet(), "Share bottom sheet not validated");
		pressNavigationBack(driver);
		musicBottomSheetPage.clickOnPlayorPauseBtn();

		//validating manidr mein suno tab
		musicBottomSheetPage.clickOnMandirMeinSunoTab();
		Assert.assertTrue(mandirPage.isBottomNavigationDisplayed(), "mandir page is displayed on clicking mandir mein suno tab");

		//validating mini player
		mandirPage.clickOnMahabhandarTab();
		Assert.assertTrue(musicMiniPlayerPage.isMiniPlayerDisplayed(), "mini player is not displayed");
		musicMiniPlayerPage.clickOnExpandBtn();
		Assert.assertTrue(musicBottomSheetPage.isMainPlayerDisplayed(), "main player is not displayed on clicking expand button from the mini player");
		pressNavigationBack(driver);
		musicMiniPlayerPage.clickOnPlayPauseBtn();
		musicMiniPlayerPage.clickOnCloseBtn();

		//validating ellipse icon in music bottom sheet
		mandirPage.clickOnMandirIcon().clickOnSongIcon();
		musicBottomSheetPage.clickOnEllipse();
		Assert.assertTrue(musicBottomSheetPage.isShareIconAndAlarmBottomSheetDisplayed(), "Share and alarm bottom sheet not displayed on clicking ellipse icon");

		//validating share icon from the ellipse bottom sheet 
		musicBottomSheetPage.clickOnShareIcon();
		Assert.assertTrue(musicBottomSheetPage.isMessageDisplayedInShareBottomSheet(), "Share bottom sheet not validated");
		pressNavigationBack(driver);
		
		//validating alarm icon from the bottom sheet
		//		musicBottomSheetPage.clickOnAlarmIcon();
		//		String actualMyAlarmPageTitle =musicBottomSheetPage.clickOnAlarmIcon().clickOnAlarmSaveBtn().clickOnDenyBtn().getAlarmPageTitle();
		//		Assert.assertEquals(actualMyAlarmPageTitle, TestData.myAlarmPageTitle, "User Is not able set the Alarm");
		//		alarmpage.clickOnBackBtn();

//		mandirPage.clickOnMandirIcon().clickOnSongIcon();
//		musicBottomSheetPage.clickOnEllipse();

		//validating favourites icon
		musicBottomSheetPage.clickOnFavoriteicon();
		try {
			Assert.assertTrue(musicBottomSheetPage.isLogInBottomSheetDisplayed(), "Login bottom sheet not displayed on clicking favourite icon");
			logInBottomSheet.clickOnPhoneLogInBtn()
			.enterMobileNumber()
			.clickOnLogInBtn()
			.enterOtp()
			.clickOnContinueBtn();
			musicBottomSheetPage.clickOnOkBtnFromConfirmationPopup();
		}catch (Exception e) {
			logger.info("user is already logged in /tutorial not displayed ");
		}
		pressNavigationBack(driver);

		//validating if the respective song is added to favourites
		mandirPage.clickOnSangeethTab().swipeToPasandidaTab().clickOnPasandidaTab();
		Assert.assertEquals(songTitleFromMusicList,sangeethpage.getSongTitleFromFavourites() ,"Respective song is not added to favourites");
		sangeethpage.clickOnWhatsAppShareIcon();
		Assert.assertTrue(musicBottomSheetPage.isMessageDisplayedInShareBottomSheet(), "Share bottom sheet not validated");
		pressNavigationBack(driver);

		//validating ellipse button under favourites tab
		sangeethpage.clickOnEllipseIcon();
		Assert.assertTrue(sangeethpage.isEllipseBottmSheetDisplayed(), "bottom sheet not validated on clicking ellipse button");
		sangeethpage.clickOnCloseBottomSheetBtn();

		//validating alarm and remove button from the ellipse bottom sheet
		sangeethpage.clickOnEllipseIcon().clickOnAlarmIcon();
		String actualAlarmpageTitle = alarmSettingPage.clickOnAlarmSaveBtn().clickOnDenyBtn().getAlarmPageTitle();
		Assert.assertEquals(actualAlarmpageTitle, TestData.myAlarmPageTitle, "User Is not able set the Alarm");
		alarmpage.clickOnBackBtn();
		try {
			for (int i =sangeethpage.getFavouriteSongCount() ; i >0 ;i--  ) {
				sangeethpage.clickOnEllipseIcon().clickOnRemoveFromFavouritesTab();
			}	
		}catch (Exception e) {
			logger.info("Fav song list is empty");
		}
		Assert.assertTrue(sangeethpage.isEmptySongListDisplayed(), "Empty song list not validated");
		Assert.assertTrue(sangeethpage.isTrendingListDisplayed(), "Trending playlist is not displayed below the favourite list");
		sangeethpage.clickOnFavIconFromTrendingPlaylist();
		Assert.assertTrue(sangeethpage.isFavSongListIsDisplayed(), "Song not validated in fav list after adding from trending playlist");
		sangeethpage.clickOnEllipseIcon().clickOnRemoveFromFavouritesTab();
		sangeethpage.clickOnAddSongToFavouriteListBtn();
		Assert.assertTrue(sangeethpage.getSangeethSangrahaPageSubTitle().contains(TestData.sangeethPageTitle),"Navigation after clicking on add song to favourite not validated");
		String actualSongTitleFromThePlaylist = sangeethpage.clickOnFavIconFromThePlayist().getSongTitleFromThePlaylist();
		sangeethpage.clickOnPasandidaTab();
		Assert.assertEquals(actualSongTitleFromThePlaylist, sangeethpage.getSongTitleFromFavourites(), "Song not validated after adding from the playlist");
		try {
			for (int i =0 ; i <sangeethpage.getFavouriteSongCount() ;i++ ) {
				sangeethpage.clickOnEllipseIcon().clickOnRemoveFromFavouritesTab();
			}	
		}catch (Exception e) {
			logger.info("Fav song list is empty");
		}
	}

}	
