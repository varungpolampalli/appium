package Mandir;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import Pages.GodSelectionPage;
import Pages.MandirPage;
import TestAnnotations.TestInfo;
import base.BaseTest;
import base.PunyaMudraPopUpTest;
import base.TestData;
@Listeners(base.ListenerImplimentationclass.class)
public class OnBoardingTest extends BaseTest {

	@TestInfo(testcaseID = "SMPK-TC-3669,SMPK-TC-3672,SMPK-TC-3673,SMPK-TC-7092"
			+ "SMPK-TC-1832(aarti senario),SMPK-TC-1834 (aarti senario)")
	@Test(groups= {"regression","smoke"})
	public void verifyOnBoardingTutorial() throws Exception 
	{
		driver.resetApp();
		driver.launchApp();
		GodSelectionPage godSelectionPage=new GodSelectionPage(driver);
		godSelectionPage.clickOnGodSelectionSaveBtn();
		MandirPage mandirPage=new MandirPage(driver);
		Assert.assertEquals(mandirPage.getOnboardingTutorialTitle(), TestData.sangeethOnBoardingTutorialTitle, "sangeeth onboarding tutorial not displayed");
		//		mandirPage.clickOnNextBtn();
		mandirPage.tapAnywhereOnScreen();
		Assert.assertEquals(mandirPage.getOnboardingTutorialTitle(), TestData.punyamudraOnBoardingTutorial, "punya mudra onboarding tutorial not displayed");
		//		mandirPage.clickOnNextBtn();
		mandirPage.tapAnywhereOnScreen();
		Assert.assertEquals(mandirPage.getOnboardingTutorialTitle(), TestData.diaOnBoardingTutorial, "aarthi onboarding tutorial not displayed");
		//		mandirPage.clickOnNextBtn();
		mandirPage.tapAnywhereOnScreen();
		PunyaMudraPopUpTest.handlePunyaMudraPopUp(driver);
		driver.closeApp();
		driver.launchApp();
		PunyaMudraPopUpTest.handlePunyaMudraPopUp(driver);
		Assert.assertTrue(mandirPage.isSecondTimeOnboardingTutorialDisplayed(), "Second time on boarding tutorial not displayed");
		mandirPage.tapAnywhereOnScreen();
		Assert.assertTrue(mandirPage.isSecondTimeOnboardingTutorialDisplayed(), "Second time on boarding tutorial not displayed");
		mandirPage.tapAnywhereOnScreen();
		
		//validating edit button
		mandirPage.clickOnEditGodBtn();
		Assert.assertTrue(godSelectionPage.isMandirStapithKareBtnDisplayed(), "God selection screen is not displayed on clicking god edit flow button");

		//validating cancel button in god selection page
		godSelectionPage.clickOnGodSelectionCancelBtn();
		Assert.assertTrue(mandirPage.isGodDisplayed(), "Cancel  button in god selection page not validated");

	}
}
