package Mandir;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import Pages.GodSelectionPage;
import Pages.LoginBottomSheetPage;
import Pages.MandirPage;
import Pages.MenuPage;
import Pages.UYOGPage;
import TestAnnotations.TestInfo;
import base.BaseTest;
import base.FilePaths;
import base.PunyaMudraPopUpTest;
import base.TestData;

@Listeners(base.ListenerImplimentationclass.class)
public class UYOGTest extends BaseTest {

	@TestInfo(testcaseID ="SMPK-TC-3193,SMPK-TC-3194,SMPK-TC-3195,SMPK-TC-3196,SMPK-TC-3197,SMPK-TC-3198,SMPK-TC-3199,SMPK-TC-3200,SMPK-TC-3201,SMPK-TC-3202,SMPK-TC-3203,SMPK-TC-3204,SMPK-TC-3205,SMPK-TC-3207"
			+ "SMPK-TC-3208,SMPK-TC-3209,SMPK-TC-3210,SMPK-TC-3211,SMPK-TC-3212,SMPK-TC-3215,SMPK-TC-3217,SMPK-TC-3660,SMPK-TC-5264,SMPK-TC-7065,SMPK-TC-7066,SMPK-TC-7067,SMPK-TC-7068,SMPK-TC-7069"
			+ "SMPK-TC-7070,SMPK-TC-7071,SMPK-TC-7072,SMPK-TC-7073,SMPK-TC-7074,SMPK-TC-7075,SMPK-TC-7076,SMPK-TC-7077,SMPK-TC-7078,SMPK-TC-7079,SMPK-TC-7080")
	@Test(groups= {"regression","smoke"})
	public void verifyUYOGfeatureAndEditFlow() throws Exception {
		MandirPage mandirPage=new MandirPage(driver);
		UYOGPage uyogPage=new UYOGPage(driver);
		LoginBottomSheetPage loginBottomSheet=new LoginBottomSheetPage(driver);
		GodSelectionPage godSelectionPage=new GodSelectionPage(driver);
		MenuPage menuPage=new MenuPage(driver);

		//pushing file to device for upload god
		pushFileToDevice(FilePaths.DEVICEFILEPATH, FilePaths.PROFILEIMAGEPATH);

		mandirPage.clickOnHamburgerMenu();
		if(menuPage.getLoginTitle().equals(TestData.loginKareText)) {
			menuPage.clickOnCancelBtn();
		}else {
			scrollTillEnd();
			menuPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup().clickOnCancelBtn();
			PunyaMudraPopUpTest.handlePunyaMudraPopUp(driver);
		}

		mandirPage.swipingTillUYOGIconAndClick();
		Assert.assertTrue(uyogPage.isFirstUYOGTutorialDisplayed(), "user is not getting onboarding tutorial");
		uyogPage.clickOnCloseBtnFromUYOGTutorial();
		Assert.assertTrue(loginBottomSheet.isLoginBottomSheetDisplayed(), "loginBottom sheet is not displayed on clicking close button from login tutorial");
		pressNavigationBack(driver);
		uyogPage.clickOnUYOGFrame().clickOnNextBtnFromUYOGTutorial().clickOnNextBtnFromUYOGTutorial();
		Assert.assertTrue(uyogPage.isApnemandirBanayeCTABtnDisplayed(),"Apne mandir Banaye CTA button is not displayed on the last tutorial");
		uyogPage.clickOnApneMandirBanayeCTABtn();
		Assert.assertTrue(loginBottomSheet.isLoginBottomSheetDisplayed(), "loginBottom sheet is not displayed on clicking Apne Mandir Banaye CTA icon");
		try {
			loginBottomSheet.clickOnPhoneLogInBtn()
			.enterMobileNumber()
			.clickOnLogInBtn()
			.enterOtp()
			.clickOnContinueBtn();
		}catch(Exception e) {
			logger.info("user is already logged in");	
		}
		try {
			uyogPage.clickOnAllowOnceBtnFromBottomSheet();
		}catch (Exception e) {
			logger.info("device permission already granted");
		}
		try {
			uyogPage.clickOnAllowFromBottomSheet();
		}catch (Exception e) {
			logger.info("permission to access camera already granted");
		}

		//checking if photo chune bottom sheet is displayed
		Assert.assertTrue(uyogPage.isPhotoChuneBottomSheetDisplayed(), "choose photo from gallery and camera options bottom sheet not displayed ");

		//uploading photo from gallery tab
		uyogPage.clickOnChoosePhotoFromGalleryTab();

		try {
			uyogPage.clickOnImageForUYOG();
		}catch (Exception e) {
			mandirPage.clickOnEditGodBtn();
			scrollTillEnd();
			uyogPage.clickOnEditOwnGodBtn().clickOnDeleteOwnGodTab().clickOnRemoveOwnGodButton();
			uyogPage.clickOnUYOGFrame().clickOnNextBtnFromUYOGTutorial().clickOnNextBtnFromUYOGTutorial().clickOnApneMandirBanayeCTABtn()
			.clickOnChoosePhotoFromGalleryTab().clickOnImageForUYOG();

		}
		Assert.assertTrue(uyogPage.isCropImageTabDisplayed(), "crop image  page not validated");

		//rotating the image
		uyogPage.clickOnRotateBtn();

		//clicking on back button from crop image screen and also checking the pop up on pressing back button
		uyogPage.clickOnBackBtnFromCropImagePage();
		Assert.assertTrue(uyogPage.isWarningPopupDisplayed(), "warning pop up not validated on clicking back button from image resize screen");
		uyogPage.clickOnNahiJaariRakeBtn();
		Assert.assertTrue(uyogPage.isCropImageTabDisplayed(), "screen not validated on clicking nahi jari kare from the warning pop up");

		uyogPage.clickOnBackBtnFromCropImagePage().clickOnRadhKareBtn();
		Assert.assertTrue(uyogPage.isUYOGframeDisplayedInMandirPage(),"Navigation to respective screen after clicking radh kare is not validated");

		//navigating to frame selection tab and 
		String actualChooseFramePageTitle = uyogPage.clickOnUYOGFrame().clickOnNextBtnFromUYOGTutorial().clickOnNextBtnFromUYOGTutorial().clickOnApneMandirBanayeCTABtn()
				.clickOnChoosePhotoFromGalleryTab().clickOnImageForUYOG().clickOnAageBadeBtn().getChooseFrameTitle();

		//validating frame selection page and checking if frame selection corousel tab is displayed and scrollable
		Assert.assertEquals(actualChooseFramePageTitle, TestData.chooseFramePageTitle, "Choose frame Page title not validated and navigation not validated");
		Assert.assertTrue(uyogPage.isGodFrameTabDisplayed(), "Frames corousel is not displayed");
		uyogPage.swipeFrameCorousel().swipeFrameCorouselBack();

		//validating back button in frame selection page and also validating warning pop up(jaari rake and radh kare )
		uyogPage.clickOnBackBtnFromFrameSelectionScreen();
		Assert.assertTrue(uyogPage.isWarningPopupDisplayed(), "warning pop up not validated on clicking back button from frame selection screen");
		uyogPage.clickOnNahiJaariRakeBtn();
		Assert.assertTrue(uyogPage.isGodFrameTabDisplayed(), "screen not validated on clicking nahi jaari kare from the warning pop up");
		uyogPage.clickOnBackBtnFromFrameSelectionScreen().clickOnRadhKareBtn();
		Assert.assertTrue(uyogPage.isUYOGframeDisplayedInMandirPage(),"Navigation to respective screen after clicking radh kare is not validated");

		//navigating and fetching name selection Page title
		String actualChooseNamePageTitle = uyogPage.clickOnUYOGFrame().clickOnNextBtnFromUYOGTutorial().clickOnNextBtnFromUYOGTutorial().clickOnApneMandirBanayeCTABtn()
				.clickOnChoosePhotoFromGalleryTab().clickOnImageForUYOG().clickOnAageBadeBtn().clickOnAageBadeBtn().getChooseNamePageTitle();

		//validating Name selection Page and also validating back button in the same page and handling warning pop up
		Assert.assertEquals(actualChooseNamePageTitle,TestData.chooseNamePageTitle , "Choose Name Page title not validated");
		uyogPage.clickOnBackBtnFromNameSelectionScreen();
		Assert.assertEquals(actualChooseFramePageTitle, TestData.chooseFramePageTitle, "back navigation from name selection page to respective screen not validated");
		uyogPage.clickOnAageBadeBtn()
		.clearOwnGodTextFeild().typeNameToOwnGodTextFeild();
		Assert.assertTrue(uyogPage.isMandirStapithKareButtonDisplayed(), "Mandir stapith kare button is not displayed in choose name page");
		uyogPage.clickOnMandirStapithKareButton();
		Assert.assertTrue(uyogPage.isBadaiHoPopup(), "badai ho pop up or bottom sheet is not displayed");
		uyogPage.clickOnMandirDhekeButton();
		Assert.assertTrue(uyogPage.isOwnGodDisplayed(), "own god is not displayed in mandir page");

		//Edit flow test
		//navigating to edit own god page and validating the edit own god page
		mandirPage.clickOnEditGodBtn();
		scrollToElement(driver, TestData.ownGodName);
		uyogPage.clickOnEditOwnGodBtn();
		Assert.assertEquals(uyogPage.getEditOwnGodPageTitle(), TestData.editOwnGodPageTitle,"Edit own god page title not validated");
		uyogPage.clickOnbackBtnfromEditOwnGod();
		Assert.assertTrue(godSelectionPage.isMandirStapithKareBtnDisplayed(), "Navigation to prevoius page not validated on clicking back button from edit owmn god page");
		uyogPage.clickOnEditOwnGodBtn().clickOnShareBtn();
		Assert.assertTrue(uyogPage.isShareBottomSheetDisplayed(), "share bottom sheet is not displayed on clicking share button");
		pressNavigationBack(driver);

		//deleting own god  from the edit own god page
		uyogPage.clickOnDeleteOwnGodTab();
		Assert.assertTrue(uyogPage.isWarningPopupDisplayed(), "Warning pop up is not displayed on clicking delete own god button");
		uyogPage.clickOnNahiJaariRakeBtn();
		Assert.assertEquals(uyogPage.getEditOwnGodPageTitle(), TestData.editOwnGodPageTitle,"screen navigation on clicking nahi jaari rake is not validated");
		uyogPage.clickOnDeleteOwnGodTab().clickOnRemoveOwnGodButton();
		Assert.assertTrue(uyogPage.isUYOGframeDisplayedInMandirPage(),"empty UYOG frame is not displayed on deleting own god");
		uyogPage.swipeBackFromUYOGFrame();
	
		//checking if add own god button is displayed and is able to click on that from god listing page
		mandirPage.clickOnEditGodBtn();
		scrollTillEnd();
		Assert.assertTrue(uyogPage.isAddOwnGodIconDisplayedInGodListingPage(), "Add own god icon not displayed in god listing page");
		uyogPage.clickOnAddOwnGodIconFromGodListingPage();
		Assert.assertTrue(uyogPage.isPhotoChuneBottomSheetDisplayed(), "choose photo from gallery and camera options bottom sheet not displayed ");
		pressNavigationBack(driver);

		//logging out from the application
		uyogPage.swipeBackFromUYOGFrame();
		mandirPage.clickOnHamburgerMenu();
		scrollTillEnd();
		menuPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup();
	}

}
