package Mandir;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import Pages.GodSelectionPage;
import Pages.LoginBottomSheetPage;
import Pages.MandirPage;
import Pages.MenuPage;
import Pages.ProfilePage;
import Pages.PunyaMudraPage;
import Pages.UYOGPage;
import TestAnnotations.TestInfo;
import base.BaseTest;
import base.PunyaMudraPopUpTest;
import base.TestData;
@Listeners(base.ListenerImplimentationclass.class)
public class LoginTest extends BaseTest {

	@TestInfo(testcaseID = "SMPK-TC-3370,SMPK-TC-3659,SMPK-TC-3189,SMPK-TC-3321,SMPK-TC-3372,SMPK-TC-3664")
	@Test(groups= {"regression","smoke"})
	public void verifyLogInFromDifferentAvenues() throws Exception {
		MandirPage mandirPage=new MandirPage(driver);
		MenuPage menuPage=new MenuPage(driver);
		ProfilePage profilePage=new ProfilePage(driver);
		LoginBottomSheetPage loginBottomSheetPage=new LoginBottomSheetPage(driver);
		PunyaMudraPage punyamudraPage=new PunyaMudraPage(driver);
		UYOGPage uyogPage=new UYOGPage(driver);


		//login from menu page and also verifying without truecaller app installed
		try {
			mandirPage.clickOnHamburgerMenu().
			clickOnLoginBtn()
			.clickOnPhoneLogInBtn()
			.enterMobileNumber()
			.clickOnLogInBtn()
			.enterOtp()
			.clickOnContinueBtn();
		}catch(Exception e) {
			logger.info("user is already logged in");
			profilePage.clickOnBackBtn();
			scrollTillEnd();
			menuPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup();
			menuPage.scrollToBeginning().clickOnLoginBtn()
			.clickOnPhoneLogInBtn()
			.enterMobileNumber()
			.clickOnLogInBtn()
			.enterOtp()
			.clickOnContinueBtn();
		}
		Assert.assertTrue(menuPage.isProfileNameDisplayed(), "log in not validated from menu page ");

		scrollTillEnd();
		menuPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup();
		menuPage.scrollToBeginning().clickOnLoginBtn();

		//validating not logged in user
		Assert.assertTrue(loginBottomSheetPage.isLoginBottomSheetDisplayed(), "Not logged in state not validated");
		pressNavigationBack(driver);
		menuPage.clickOnCancelBtn();
		PunyaMudraPopUpTest.handlePunyaMudraPopUp(driver);

		//log in from my profile Page
		mandirPage.clickOnPunyamudraProfileIcon().clickOnLoginTabFromProfilePage()
		.clickOnPhoneLogInBtn()
		.enterMobileNumber()
		.clickOnLogInBtn()
		.enterOtp()
		.clickOnContinueBtn();
		Assert.assertEquals(profilePage.getUserNameText(), TestData.userName, "Log in not validated from my profile page");
		profilePage.clickOnBackBtn();
		mandirPage.clickOnHamburgerMenu();
		scrollTillEnd();
		menuPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup();
		menuPage.clickOnCancelBtn();
		PunyaMudraPopUpTest.handlePunyaMudraPopUp(driver);

		//login from punya mudra page
		mandirPage.clickOnPunyamudraProfileIcon();
		profilePage.clickOnPunyamudraTab().clickOnLoginTabFromPunyamudraPage()
		.clickOnPhoneLogInBtn()
		.enterMobileNumber()
		.clickOnLogInBtn()
		.enterOtp()
		.clickOnContinueBtn();
		punyamudraPage.clickOnBackButton();
		Assert.assertEquals(profilePage.getUserNameText(), TestData.userName, "Log in not validated from my profile page");
		profilePage.clickOnBackBtn();
		mandirPage.clickOnHamburgerMenu();
		scrollTillEnd();
		menuPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup();
		menuPage.clickOnCancelBtn();
		PunyaMudraPopUpTest.handlePunyaMudraPopUp(driver);
		
		//log in from UYOG
//		mandirPage.clickOnEditGodBtn();
//		scrollTillEnd();
//		uyogPage.clickOnAddOwnGodIconFromGodListingPage();
//		loginBottomSheetPage.clickOnPhoneLogInBtn()
//		.enterMobileNumber()
//		.clickOnLogInBtn()
//		.enterOtp()
//		.clickOnContinueBtn();
//		
//		try {
//			uyogPage.clickOnAllowOnceBtnFromBottomSheet();
//		}catch (Exception e) {
//			logger.info("device permission already granted");
//		}
//		try {
//			uyogPage.clickOnAllowFromBottomSheet();
//		}catch (Exception e) {
//			logger.info("permission to access camera already granted");
//		}
//		
//		Assert.assertTrue(uyogPage.isPhotoChuneBottomSheetDisplayed(), "choose photo from gallery and camera options bottom sheet not displayed ");
//		pressNavigationBack(driver);
//		uyogPage.swipeBackFromUYOGFrame();
		mandirPage.clickOnHamburgerMenu();
		scrollTillEnd();
		menuPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup();
		menuPage.clickOnCancelBtn();
		PunyaMudraPopUpTest.handlePunyaMudraPopUp(driver);
	}
}

