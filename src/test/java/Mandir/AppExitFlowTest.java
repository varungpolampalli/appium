package Mandir;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import Pages.KundliPage;
import Pages.MandirPage;
import Pages.MenuPage;
import Pages.SahithyaPage;
import TestAnnotations.TestInfo;
import base.BaseTest;
import base.PunyaMudraPopUpTest;
import base.TestData;

@Listeners(base.ListenerImplimentationclass.class)


public class AppExitFlowTest extends BaseTest{
	@TestInfo(testcaseID = "SMPK-TC-4611,SMPK-TC-4612,SMPK-TC-4613,SMPK-TC-4614,SMPK-TC-4615,SMPK-TC-4616")

	@Test(groups= {"regression","smoke"})
	public void VerifyAppExitFlow() throws Exception
	{
		MandirPage mandirPage=new MandirPage(driver);
		SahithyaPage sahithyaPage=new SahithyaPage(driver);
		KundliPage kundlipage= new KundliPage(driver);
		MenuPage menuPage=new MenuPage(driver);
		String actualMahaBhandarPageTitle = mandirPage.clickOnMahabhandarTab().getMahabhandarPageTitle();
		Assert.assertEquals(actualMahaBhandarPageTitle, TestData.mahabhandarPageTitle,"Mahabhandar Screen Navigation Not Valiated");
		String actualSahithyaBhandarPageTitle=sahithyaPage.getSahithyaTabText();
		Assert.assertEquals(actualSahithyaBhandarPageTitle, TestData.sahithyaText,"SahithyaMahabhandar Screen Navigation Not Valiated");
		sahithyaPage.pressNavigationBack(driver);
		Assert.assertTrue(mandirPage.isGodDisplayed(), "god selection screen not validated");
		sahithyaPage.pressNavigationBack(driver);
		Assert.assertTrue(mandirPage.isAppExitDialogBoxDisplayed(), "App exit dialog box not validated");
		mandirPage.clickOnNahiButton();
		Assert.assertTrue(mandirPage.isGodDisplayed(), "god selection screen not validated");
		mandirPage.clickOnHamburgerMenu().clickOnkundliTab();
		kundlipage.pressNavigationBack(driver);
		menuPage.clickOnCancelBtn();
		sahithyaPage.pressNavigationBack(driver);
		mandirPage.clickOnOkButton();
	}
}
