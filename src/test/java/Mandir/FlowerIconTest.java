package Mandir;

import java.io.File;


import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import Pages.FlowerBottomSheetPage;
import Pages.GodSelectionPage;
import Pages.MandirPage;
import Pages.PunyaMudraPage;
import TestAnnotations.TestInfo;
import base.BaseTest;
import base.TestData;

@Listeners(base.ListenerImplimentationclass.class)

public class FlowerIconTest extends BaseTest {

	@TestInfo(testcaseID = "SMPK-TC-1882,SMPK-TC-1871,SMPK-TC-1865,SMPK-TC-1845,SMPK-TC-1843,SMPK-TC-1844, SMPK-TC-1842,SMPK-TC-1857,SMPK-TC-1874")
	@Test(groups= {"regression","smoke"})
	public void verifyFlowerIcon() throws Exception
	{
		FlowerBottomSheetPage flowerBottomSheetpage=new FlowerBottomSheetPage(driver);
		MandirPage mandirPage =new MandirPage(driver);
		mandirPage.clickOnFlowerIcon();
		flowerBottomSheetpage.clickOnGhondaFlowerIcon().slideOverFlowerCorousel();
		Assert.assertTrue(flowerBottomSheetpage. isFlowerBottomSheetDisplayed(), "flower bottom sheet is not displayed");
		Assert.assertTrue(flowerBottomSheetpage.isListOfFlowersDisplayed(), "List of Flowers is not displayed");
		try {
			flowerBottomSheetpage.isPholkiLadhiDisplayed();
			Assert.assertTrue(flowerBottomSheetpage.isPhoolKiLadhiClickable(), "Phool ki ladhi is not clickable");
			flowerBottomSheetpage.clickOnPholkiLadhiFlowerIcon();
			Assert.assertTrue(flowerBottomSheetpage.isGreentickDisplayed(),"Green tick is not displayed");
		  }catch(Exception e) {
			logger.info("if phol ki ladhi is clicked already");
			flowerBottomSheetpage.swipeTillPhoolkiLadhi();
			Assert.assertTrue(flowerBottomSheetpage.isGreentickDisplayed(),"Green tick is not displayed");
		}
		flowerBottomSheetpage.horizontalScrollTillLastLockedFlower();
		flowerBottomSheetpage.clickOnLockedFlower();
		Assert.assertTrue(flowerBottomSheetpage. isTutorialDisplayedForLockedFlower(), "flower tutorial is not displayed");
		Assert.assertTrue(flowerBottomSheetpage.getPurchaseCoinNumber()==flowerBottomSheetpage.getLockedCoinNumber(), "coin number  not compared");
		flowerBottomSheetpage.clickOutsideFlowerBottomSheet();	
		flowerBottomSheetpage.pressNavigationBack(driver);
		Assert.assertTrue(flowerBottomSheetpage.isBottomNavigationDisplayed(),"Bottom Navigation Bar  not displayed");
	}

	@TestInfo(testcaseID = "SMPK-TC-1860,SMPK-TC-1879,SMPK-TC-1876,SMPK-TC-1877")
	@Test(groups= {"regression","smoke"})
	public void verifyGodTitleandCompareImageOfFlowerShowerandpunyamudrasection() throws Exception
	{
		MandirPage mandirPage =new MandirPage(driver);
		FlowerBottomSheetPage flowerBottomSheetpage=new FlowerBottomSheetPage(driver);
		GodSelectionPage godselectionpage= new GodSelectionPage(driver);	
		mandirPage.clickOnEditGodBtn();
		String godNameFromEditPage = godselectionpage.getGodNamefromGodEditPage();
		godselectionpage.clickOnGodSelectionSaveBtn();
		mandirPage.clickOnFlowerIcon();
		String godNameFromFlowerBottomSheet=flowerBottomSheetpage.getGodNameFromFlowerBottomSheet();
		Assert.assertEquals(godNameFromEditPage, godNameFromFlowerBottomSheet, "God title not validated on flower bottom sheet");
		pressNavigationBack(driver);
		mandirPage.clickOnSongIcon().clickOnSong();
		mandirPage.clickOnFlowerIcon();
		File actualFlowerShowerScreenshot = flowerBottomSheetpage.getScreenshotOFlowerShower();
		double compPercentage = compareImage(actualFlowerShowerScreenshot, new File("./src/test/Images/Flowershower.png"));
		System.err.println(compPercentage);
		flowerBottomSheetpage.horizontalScrollTillLastLockedFlower();
		String actualPunyaMudraPageTitle = flowerBottomSheetpage.clickOnLockedFlower().clickOnPunyaMudraTutorialButton().getPunyaMudraPageTitle();
		Assert.assertEquals(actualPunyaMudraPageTitle, TestData.punyaMudraTutorialPageTitle, "PunyaMudra Tutorial Button is not validated");
		PunyaMudraPage punyamudrapage=new PunyaMudraPage(driver);
		punyamudrapage.clickOnBackButton();
		Assert.assertTrue(flowerBottomSheetpage.isBottomNavigationDisplayed(),"Bottom Navigation Bar is not displayed");

	}	
}		
