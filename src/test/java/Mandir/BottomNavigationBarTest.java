package Mandir;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import Pages.MandirPage;
import TestAnnotations.TestInfo;
import base.BaseTest;
import base.TestData;

/**
 * 
 * @author TestYantra
 *
 */

@Listeners(base.ListenerImplimentationclass.class)
public class BottomNavigationBarTest extends BaseTest {


	@TestInfo(testcaseID = "SMPK-TC-2067,SMPK-TC-2068,SMPK-TC-2069,SMPK-TC-2070")
	@Test(groups= {"regression","smoke"})
	public void verifyBottomNavigationBar1() throws Exception
	{
		MandirPage mandirPage=new MandirPage(driver);
		String actualAajPageTitle=mandirPage.clickOnAajTab().getaajPageTitle();
		Assert.assertEquals(actualAajPageTitle, TestData.aajPageTitle, "Aaj screen Navigation not validated");
		mandirPage.clickOnMandirIcon();
		String actualSangeethPageTitle=mandirPage.clickOnSangeethTab().getSangeethPageTitle();
		Assert.assertEquals(actualSangeethPageTitle, TestData.sangeethPageTitle, "Sangeeth Screen Navigation Not Valiated");
		mandirPage.clickOnMandirIcon();
		String actualMahaBhandarPageTitle=mandirPage.clickOnMahabhandarTab().getMahabhandarPageTitle();
		Assert.assertEquals(actualMahaBhandarPageTitle, TestData.mahabhandarPageTitle, "Mahabhandar Screen Navigation Not Valiated");
		Assert.fail();
		mandirPage.clickOnMandirIcon();
//		String actualSamudayPageTitle=mandirPage.clickOnSamudayaTab().getSamudayaPageTitle();
//		Assert.assertEquals(actualSamudayPageTitle, TestData.samudayaPageTitle, "Samudaya Page Title Not validated");
		
	}
           
}
