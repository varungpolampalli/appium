package Mandir;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import Pages.MandirPage;
import Pages.ProfilePage;
import Pages.PunyaMudraPage;
import TestAnnotations.TestInfo;
import base.BaseTest;
import base.TestData;

@Listeners(base.ListenerImplimentationclass.class)

public class PunyaMudraTest extends BaseTest {

	@TestInfo(testcaseID="")
	@Test(groups= {"regression","smoke"})
	public void verifyPunyaMudraPage() throws Exception {

		MandirPage mandirPage = new MandirPage(driver);
		PunyaMudraPage punyaMudraPage=new PunyaMudraPage(driver);
		ProfilePage profilePage = new ProfilePage(driver);
		mandirPage.clickOnPunyamudraProfileIcon().clickOnPunyamudraTab();
		//			String actualPunyaMudra = punyaMudraPage.getCoinsAvailableOnProfile();
		Assert.assertEquals(punyaMudraPage.getPunyaMudraPageTitle(), TestData.punyaMudraTutorialPageTitle, "Screen Navigation To PunyaMudra Page not validated");
		Assert.assertEquals(punyaMudraPage.getpunyaKaiseKamayeTitle(),TestData.punyaKaiseKamayeTitle,"Punya Kaise kamaye not validated");
		punyaMudraPage.clickOnPassBookTab();
		try {
			if(punyaMudraPage.IsLoginKareInPassBookTabDisplayed())
				punyaMudraPage.clickOnLoginInPassBookTab()
				.clickOnPhoneLogInBtn()
				.enterMobileNumber()
				.clickOnLogInBtn()
				.enterOtp()
				.clickOnContinueBtn();	
		}catch (Exception e) {
			logger.info("Already logged into app");		
		}
		try {
			punyaMudraPage.clickTeekHaiButton();
			mandirPage.clickOnPunyamudraProfileIcon().clickOnPunyamudraTab();
		} catch (Exception e) {
			logger.info("enabled Teek Hai Button not ");		
		}	
		punyaMudraPage.scrollTillEnd();
		try {
			if(punyaMudraPage.isAbhiSuneButtonDisplayed())
			{
				punyaMudraPage.clickAbhiSuneButton();

				Assert.assertTrue(punyaMudraPage.isMiniPlayerDispalyed(),"MiniPlayer not displayed");
				punyaMudraPage.clickPauseOnMiniPlayer();
				punyaMudraPage.clickCloseMiniPlayer();
			}
		} catch (Exception e) {
			logger.info("Coin streak is enabled");		
		}
		punyaMudraPage.clickOnDhekeApnaBhaktiChakra(); 
		punyaMudraPage.scrollTillEnd();
		punyaMudraPage.clickDhekeAapkiPassBookTab();
		String actualPassBookTitle = punyaMudraPage.aapkiPassBookTitle();
		Assert.assertEquals(actualPassBookTitle, TestData.passBookTitle,"PassBook title not validated");
		punyaMudraPage.scrollTillEnd();
}
	}





