package OfferingService;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import Pages.ChadavRedirectionPage;
import Pages.MahabhandarPage;
import Pages.MandirPage;
import Pages.MenuPage;
import Pages.ProfilePage;
import Pages.TempleListingPage;
import TestAnnotations.TestInfo;
import base.BaseTest;
import base.TestData;

@Listeners(base.ListenerImplimentationclass.class)
public class ChadavRedirectionTest extends BaseTest{
	
	@TestInfo(testcaseID="SMPK-TC-9372,SMPK-TC-9374")
	@Test(groups= {"regression","smoke"})
	public void VerifyChadavRedirection() throws Exception
	{
		MandirPage mandirPage=new MandirPage(driver); 
		MenuPage menuPage=new MenuPage(driver);
		
		ChadavRedirectionPage chadavRedirectionPage=new ChadavRedirectionPage(driver);
		try {
			mandirPage.clickOnHamburgerMenu().
			clickOnLoginBtn()
			.clickOnPhoneLogInBtn()
			.enterMobileNumber()
			.clickOnLogInBtn()
			.enterOtp()
			.clickOnContinueBtn();
		}catch(Exception e)
		{
			logger.info("user is already logged in");
			new ProfilePage(driver).clickOnBackBtn();
			scrollTillEnd();
			menuPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup();
			menuPage.clickOnCancelBtn();
			mandirPage.clickOnHamburgerMenu().
			clickOnLoginBtn()
			.clickOnPhoneLogInBtn()
			.enterMobileNumber()
			.clickOnLogInBtn()
			.enterOtp()
			.clickOnContinueBtn();
			menuPage.clickOnCancelBtn();
	    }		
		//redirecting to home page
		menuPage.clickOnCancelBtn();		
		
		//Verify that Chadava icon is visible for a user on navigating from other tabs back to mandir
		mandirPage.clickOnMahabhandarTab();
		mandirPage.clickOnMandirIcon();
		Assert.assertTrue(chadavRedirectionPage.isChadavIconDisplayed(), "Chadav Icon  is not displayed");
		
		//Verify that user is able to land to the respective section on clicking the chadava icon
		chadavRedirectionPage.clickOnChadavIcon();
		String actualRedirectionPageTitle=chadavRedirectionPage.getChadavTabTitle();
		Assert.assertEquals(actualRedirectionPageTitle, TestData.chadavTabTitle, "chadav feed tab title is not validated");
		
		new TempleListingPage(driver).clickOnBackBtn();
		mandirPage.clickOnHamburgerMenu();
		scrollTillEnd();
		menuPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup();
	
	}
}



