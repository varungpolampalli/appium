package OfferingService;


import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import Pages.ChadavFeedPage;
import Pages.MandirPage;
import Pages.MenuPage;
import Pages.ProfilePage;
import Pages.TempleListingPage;
import Pages.TutorialCardPage;
import TestAnnotations.TestInfo;
import base.BaseTest;

@Listeners(base.ListenerImplimentationclass.class)

public class TutorialCardTest extends BaseTest{
	@TestInfo(testcaseID = " SMPK-TC-9469,SMPK-TC-9480,SMPK-TC-9471")
	@Test(groups= {"regression","smoke"})
	
	public void VerifyTutorialCard() throws Exception
	{
		MandirPage mandirPage=new MandirPage(driver);
		MenuPage menuPage=new MenuPage(driver);
		TutorialCardPage tutorialCardPage=new TutorialCardPage(driver);
		ChadavFeedPage chadavFeedPage= new ChadavFeedPage(driver);
		TempleListingPage templeListingPage=new TempleListingPage(driver);
		ProfilePage profilePage=new ProfilePage(driver);
	
		try {
			mandirPage.clickOnHamburgerMenu();
			scrollTillEnd();
			menuPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup();
			menuPage.clickOnCancelBtn();
		}catch(Exception e) {
			menuPage.clickOnCancelBtn();
			logger.info("user is already logged out");    
		}
		
		//Verify that user is able to see the tutorial card on the top of the 'Aaj ke Liye' tab in Chadhava section on logged out state
		tutorialCardPage.isTutorialCardDisplayed();
		
		//Verify that on back navigation from tutorial video user is landing on the respective tab
		tutorialCardPage.clickOnTutorialCard();
		pressNavigationBack(driver);
		Assert.assertTrue(chadavFeedPage.isHeroCardDisplayed(), "Hero card is not displayed in Aaj ke liye tab");
		
		//Verify that user is getting the tutorial card after login, if he has not made any offerings orders
		templeListingPage.clickOnBackBtn();
		profilePage.clickOnBackBtn();
		mandirPage.clickOnHamburgerMenu().clickOnLoginBtn().clickOnPhoneLogInBtn();
		tutorialCardPage.enterMobileNumberOfUserWithNoOfferingOrder().clickOnLogInBtn().enterOtp().clickOnContinueBtn();
		menuPage.clickOnCancelBtn();
		mandirPage.clickOnPunyamudraProfileIcon().clickOnChadavSevaTab();
		chadavFeedPage.clickOnChadavShevaye();
		
		
	}
}
