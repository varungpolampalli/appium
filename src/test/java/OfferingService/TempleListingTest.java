package OfferingService;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import Pages.ItemListingPage;
import Pages.MandirPage;
import Pages.ProfilePage;
import Pages.TempleListingPage;
import TestAnnotations.TestInfo;
import base.BaseTest;
import base.TestData;

@Listeners(base.ListenerImplimentationclass.class)
public class TempleListingTest extends BaseTest {
	
	@TestInfo(testcaseID = "SMPK-TC-9441,SMPK-TC-9442,SMPK-TC-9443,SMPK-TC-9445,SMPK-TC-9447,SMPK-TC-9450,SMPK-TC-9453,SMPK-TC-9458,SMPK-TC-9459")
	@Test(groups= {"regression","smoke"})
	public void verifyTempleListingFeature() throws Exception {
		MandirPage mandirPage=new MandirPage(driver);
		TempleListingPage templeListingPage=new TempleListingPage(driver);
		ItemListingPage itemListingPage=new ItemListingPage(driver);
		ProfilePage profilePage=new ProfilePage(driver);
		
		mandirPage.clickOnPunyamudraProfileIcon().clickOnChadavSevaTab();
		templeListingPage.clickOnMandirKaSuchiTab();
		scrollToElement(driver, TestData.openTempleName);
		templeListingPage.scrollToTempleCard();
		Assert.assertTrue(templeListingPage.isTempleListDisplayed(), "Temple List on Temple card not validated");
		Assert.assertTrue(templeListingPage.isTempleTitleDisplayed(),"Temple Title on Temple card not validated");
		Assert.assertTrue(templeListingPage.isTempleImageDisplayed(),"Temple Image on Temple card not validated");
		Assert.assertTrue(templeListingPage.isSubTitleDisplayed(),"Temple Sub-Title on Temple card not validated");
		Assert.assertTrue(templeListingPage.isTempleDescriptionDisplayed(),"Temple Description on Temple card not validated");
		Assert.assertTrue(templeListingPage.isChadavaOfferingsImageDisplayed(), "Chadava Offerings Image on Temple card not validated");
		Assert.assertTrue(templeListingPage.isOfferingsButtonDisplayed(), "Chadava Offerings Button on Temple card not validated");
		Assert.assertTrue(templeListingPage.isOfferingsAvailablityTabDisplayed(), "Offerings Availablity Tab on Temple Card not validated");
		Assert.assertTrue(templeListingPage.isOfferingsAvailablityImageDisplayed(), "Offerings Availablity Image on Temple Card not validated");
		templeListingPage.clickOnChandiDeviTempleCard();
		Assert.assertEquals(templeListingPage.getTempleTitle(),itemListingPage.getTempleNameFromItemListingPage() , "Navigation to item lisiting page not validated on clicking temple card from temple listing page");
		itemListingPage.clickOnBackBtnFromItemListingPage();
		scrollTillEnd();
		templeListingPage.clickOnWhatsAppShareButton();
		Assert.assertTrue(templeListingPage.isWhatsAppShareBottomSheetDisplayed(), "Whatsapp share bottom sheet not displayed on clicking whatsapp  icon from temple listing page");
		templeListingPage.isWhatsAppShareBottomSheetDisplayed();
		pressNavigationBack(driver);
		templeListingPage.clickOnBackBtn();
		Assert.assertEquals(profilePage.getprofilePageTitle(), TestData.profilePageTitle, "Back navigation not validated from temple lisitng page");
	}

}
