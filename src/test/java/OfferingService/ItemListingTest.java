package OfferingService;


import Pages.ItemListingPage;
import Pages.MandirPage;
import Pages.TempleListingPage;
import TestAnnotations.TestInfo;
import base.BaseTest;
import base.TestData;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;


@Listeners(base.ListenerImplimentationclass.class)
public class ItemListingTest extends BaseTest {

    @TestInfo(testcaseID = "SMPK-TC-7104,SMPK-TC-7105,SMPK-TC-7106,SMPK-TC-7107,SMPK-TC-7108,SMPK-TC-7109,SMPK-TC-7111,SMPK-TC-7114,SMPK-TC-7115,SMPK-TC-7116,SMPK-TC-7117,SMPK-TC-7118,SMPK-TC-7119," +
            "SMPK-TC-7120,SMPK-TC-7121,SMPK-TC-7122,SMPK-TC-7123,SMPK-TC-7124,SMPK-TC-7125,SMPK-TC-7126,SMPK-TC-7127,SMPK-TC-7128,SMPK-TC-7129,SMPK-TC-7130,SMPK-TC-7132,SMPK-TC-7133,SMPK-TC-7134,SMPK-TC-7135,SMPK-TC-7136," +
            "SMPK-TC-7137,SMPK-TC-7138,SMPK-TC-7139,SMPK-TC-7140,SMPK-TC-7141,SMPK-TC-7163,SMPK-TC-7164")
    @Test(groups= {"regression","smoke"})
    public void verifyItemListingFeature() throws Exception {
        MandirPage mandirPage=new MandirPage(driver);
        TempleListingPage templeListingPage=new TempleListingPage(driver);
        ItemListingPage itemListingPage=new ItemListingPage(driver);

        mandirPage.clickOnPunyamudraProfileIcon().clickOnChadavSevaTab();
        templeListingPage.clickOnMandirKaSuchiTab();
        scrollToElement(driver,TestData.openTempleName);
        templeListingPage.clickOnChandiDeviTempleCard();
        //validating navigation to item lisiting page by validating temple title
        Assert.assertTrue(itemListingPage.isItemListingPageTitleDisplayed(),"Item listing page title not displayed");

        //checking if product is already in cart and removing those items
        try{
            if(itemListingPage.isBottomCartCTAIconDisplayed()){
                itemListingPage.scrollToOfferingItem();
                if(itemListingPage.getAddToCartTextFromFirstProduct().equals(TestData.hatayeText)) {
                    itemListingPage.clickOnFirstProductAddToCartBtn();
                }
                if(itemListingPage.getAddToCartTextFromSecondProduct().equals(TestData.hatayeText)){
                    itemListingPage.clickOnSecondItemAddToCartBtn();
                }
            }
        }catch (Exception e){
            logger.info("product not found in cart");
        }
        //validating the back button from item listing page
        itemListingPage.clickOnBackBtnFromItemListingPage();
        Assert.assertEquals(templeListingPage.getChadavSevaPageTitle(), TestData.chadavSevaPageTitle , "Screen Navigation To Chadav Seva Page not validated from item listing page");
        templeListingPage.clickOnChandiDeviTempleCard();

        //verifying the temple location
        Assert.assertTrue(itemListingPage.isTempleLocationDisplayed(),"temple location not displayed below the temple title in item listing page");
        //verifying whatsapp share icon
        Assert.assertTrue(itemListingPage.isWhatsAppShareIconDisplayed(),"Whatsapp share icon not displayed in item listing page");
        itemListingPage.clickOnWhatsAppShareIcon();
        Assert.assertTrue(itemListingPage.isShareBottomSheetDisplayed(),"share bottom sheet not displayed on clicking whatsapp icon");
        pressNavigationBack(driver);

        //verifying the video and image of the temple
        itemListingPage.clickOnTempleVideo();
        itemListingPage.tapOnScreen();
        pressNavigationBack(driver);

        //validating scrolling of temple info images and checking if image is displayed and also validating description
        itemListingPage.scrollTempleInfoImages();
        Assert.assertTrue(itemListingPage.isProductInfoImageDisplayed(),"Product info image not displayed");
        Assert.assertTrue(itemListingPage.isTempleDescriptionDisplayed(),"Temple description not displayed in item listing page");

        //checking if count of offering displayed below the temple description
        Assert.assertTrue(itemListingPage.isCountOfOfferingsDisplayed(),"Respective count of offering not displayed below the description");
        itemListingPage.scrollToOfferingItem();

        //checking if offering item meta data displayed
        Assert.assertTrue(itemListingPage.isOfferingItemImageDisplayed(),"offering item image not displayed in item listing page");
        Assert.assertTrue(itemListingPage.isOfferingItemNameDisplayed(),"offering item  name not displayed in item listing page");
        Assert.assertTrue(itemListingPage.isOfferingItemDescriptionDisplayed(),"offering item description not displayed in item lsiting page");
        Assert.assertTrue(itemListingPage.isOfferingItemPriceDisplayed(),"offering item price details not displayed in item listing page");
        Assert.assertTrue(itemListingPage.isOfferingItemBalanceDetailsDisplayed(),"offering item balance details not displayed");

        //clicking on aaur pade from item description and clicking on that
        itemListingPage.clickOnAaurPadeInProductDescription();
        Assert.assertTrue(itemListingPage.isProductDescriptionBottomSheetDisplayed(),"Bottom sheet not displayed on clicking aaur pade from product description");
        Assert.assertTrue(itemListingPage.isProductNameDisplayedInProductBottomSheet(),"Product name not displayed in product description bottom sheet");
        Assert.assertTrue(itemListingPage.isProductImageIsDisplayedInBottomSheet(),"Product image not displayed in product description bottom sheet");
        Assert.assertTrue(itemListingPage.isProductPriceDisplayedInBottomSheet(),"product price not displayed in product description bottom sheet");
        Assert.assertTrue(itemListingPage.isProductDescriptionDisplayedInBottomSheet(),"product description not displayed in bottom sheet");

        //validating the cancel button and device back button from bottom sheet
        itemListingPage.clickOnProductDescriptionBottomSheetCancelBtn();
        Assert.assertTrue(itemListingPage.isOfferingItemDescriptionDisplayed(),"cancel button not validated on bottom sheet");
        itemListingPage.clickOnAaurPadeInProductDescription();
        waitOrPause(3);
        pressNavigationBack(driver);
        Assert.assertTrue(itemListingPage.isOfferingItemDescriptionDisplayed(),"bottom sheet not closed on clicking device back button");
        itemListingPage.clickOnAaurPadeInProductDescription();

        //validating add to cart button
        itemListingPage.clickOnAddToCartBtnFromProductBottomSheet();
        Assert.assertTrue(itemListingPage.isBottomCartCTAIconDisplayed(),"bottom cart CTA icon not displayed in item listing page");
        //adding item to cart by clicking on chune button
        itemListingPage.clickOnSecondItemAddToCartBtn();

        //validating the respective count and price displayed in bottom cart CTA
        int firstOfferingItemPrice = itemListingPage.getOfferingItemPrice();
        System.err.println(firstOfferingItemPrice);
        int secondItemProductPrice = itemListingPage.getSecondItemProductPrice();

        int totalActualProductPrice = firstOfferingItemPrice + secondItemProductPrice;

        Assert.assertTrue(itemListingPage.getProductCountFromBottomCartCTA()==2,"product count not validated from bottom cart CTA");
        Assert.assertTrue(itemListingPage.getProductPriceFromBottomCartCTA()==totalActualProductPrice,"product price not validated from bottom cart CTA");

        //removing item on clicking hataye button
        itemListingPage.clickOnSecondItemAddToCartBtn();
        Assert.assertTrue(itemListingPage.getProductCountFromBottomCartCTA()==1,"product count not validated from bottom cart CTA");

        //checking if aage bade button is displayed in item listing page
        Assert.assertTrue(itemListingPage.isAageBtnDisplayed(),"Aage bade button is not displayed in item listing page");

        //remove items from cart
        itemListingPage.clickOnFirstProductAddToCartBtn();

        scrollTillEnd();
        itemListingPage.scrollToEndOfPage();
        itemListingPage.scrollTillUserReviewCard();

        //validating user review section
        Assert.assertTrue(itemListingPage.isUserReviewCardDisplayed(),"user review card not displayed in item listing page");
        Assert.assertTrue(itemListingPage.isUserNameDisplayedInRatingCard(),"user name not displayed in rating card");
        Assert.assertTrue(itemListingPage.isRatingsDisplayedInReviewCard(),"Ratings not displayed in review card");

        //validating if user is able to scroll user review card in item listing page
        itemListingPage.scrollUserReviewCardCorousel();

        scrollTillEnd();

        //validating FAQ section
        Assert.assertTrue(itemListingPage.isFAQSectionDisplayed(),"FAQ section not displayed in item listing page");
        Assert.assertTrue(itemListingPage.isRespectiveFAQListDisplayed(),"Respective FAQ is not displayed in item listing page");
        itemListingPage.clickOnDownCheveron();
        Assert.assertTrue(itemListingPage.isFAQAnswerDisplayed(),"FAQ answer not displayed in item listing page on clicking down cheveron");
        itemListingPage.clickOnDownCheveron();

        //validating device back button from item listing page
        pressNavigationBack(driver);
        Assert.assertEquals(templeListingPage.getChadavSevaPageTitle(), TestData.chadavSevaPageTitle , "Screen Navigation To Chadav Seva Page not validated from item listing page");
    }
}
