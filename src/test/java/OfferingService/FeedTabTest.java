package OfferingService;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import Pages.ChadavFeedPage;
import Pages.ItemListingPage;
import Pages.MandirPage;
import Pages.ProfilePage;
import TestAnnotations.TestInfo;
import base.BaseTest;
import base.TestData;

@Listeners(base.ListenerImplimentationclass.class)

public class FeedTabTest extends BaseTest{
	@TestInfo(testcaseID="SMPK-TC-9386,SMPK-TC-9388,SMPK-TC-9402,SMPK-TC-9403,SMPK-TC-9411")	
	@Test(groups= {"regression","smoke"})
	public void VerifyHeroCard1() throws Exception{
		MandirPage mandirPage=new MandirPage(driver);
		ChadavFeedPage chadavFeedPage=new ChadavFeedPage(driver);
		ProfilePage profilePage=new ProfilePage(driver);
		ItemListingPage itemListingPage=new ItemListingPage(driver);

		mandirPage.clickOnPunyamudraProfileIcon().clickOnChadavSevaTab();
		chadavFeedPage.clickOnChadavShevaye();

		//Verify that on back navigation user landing the profile if entry is done through profile card
		chadavFeedPage.clickOnBackBtn();
		Assert.assertEquals(profilePage.getprofilePageTitle(), TestData.profilePageTitle, "Navigation not validated on clicking back btn from chadav seva page");

		//Verify that user is getting respective hero card/feature card at the top of the "Aaj ke liye" tab
		profilePage.clickOnChadavSevaTab();
		chadavFeedPage.clickOnChadavShevaye();
        scrollToElement(driver,TestData.sriMandirParLokpriyaChdaveText);
        chadavFeedPage.scrollToFullHeroCard();

		Assert.assertTrue(chadavFeedPage.isHeroCardDisplayed(), "Hero card is not displayed in Aaj ke liye tab");

		//Verify that user is getting respective title and description for a store card without category
		Assert.assertTrue(chadavFeedPage.isHeroCardDescriptionDisplayed(), "Hero card description is not displayed ");

		//Verify that user is getting respective title  for a store card without category
		Assert.assertTrue(chadavFeedPage.isHeroCardTitleDisplayed(), "Hero card title is not displayed ");

		//Verify that item is getting selected for a user on landing to a respective temple by clicking on a SKU*Temple card
		chadavFeedPage.clickOnHeroCard();
		Assert.assertTrue(itemListingPage.isBottomCartCTAIconDisplayed(), "AgeBade bottom cart  is not displayed after clicking on temple card");

		//Verify that user is getting navigated to the respective temple on clicking any card in the "Aaj ke liye" tab
		Assert.assertTrue(itemListingPage.isTempleTitleDisplayed(), "Temple title  is not displayed after clicking on temple card");
	}
		
	
	@TestInfo(testcaseID="SMPK-TC-9412,SMPK-TC-9393,SMPK-TC-9394,SMPK-TC-9404,SMPK-TC-9377")	

		@Test
		public void VerifyFeedTab() throws Exception
		{
		ChadavFeedPage chadavFeedPage=new ChadavFeedPage(driver);
		MandirPage mandirPage=new MandirPage(driver);

		//pressNavigationBack(driver);
		mandirPage.clickOnPunyamudraProfileIcon().clickOnChadavSevaTab();
		chadavFeedPage.clickOnChadavShevaye();
		
		//chadavFeedPage.scrollingTillCategoryListTags();
        scrollToElement(driver,TestData.categoryListName);

		//Verify that user is getting respective title and description for a sku card without category
		Assert.assertTrue(chadavFeedPage.isSKUCardTitleDisplayed(), "SKU card title is not displayed ");
		Assert.assertTrue(chadavFeedPage.isSKUCardDescriptionDisplayed(), "SKU card description is not displayed ");

		//Verify that the category list is updating for a user on changing or clicking the category tags
		chadavFeedPage.clickOnThirdCategoryTag();
		Assert.assertTrue(chadavFeedPage.isCategoryHeaderDisplayed(), "category list is not updating for a user on changing or clicking the category tags");

		//Verify that user is able to scroll the SKU cards horizontally in a respective category
    	chadavFeedPage.scrollingTillSKUCards();
		chadavFeedPage.scrollingThroughSKUCards();

		//Verify that user is getting respective reviews below the "Aaj ke liye" tab section
        scrollToElement(driver,TestData.reviewHeader);
        chadavFeedPage.scrollingTillReviewSection();
		Assert.assertTrue(chadavFeedPage.isReviewCardDisplayed(), "Verify that user is not getting respective reviews below the Aaj ke liye tab section");

		//Verify that user is able to scroll through the tab list
		chadavFeedPage.scrollThroughTab();

	}
 }
