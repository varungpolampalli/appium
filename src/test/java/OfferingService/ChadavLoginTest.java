package OfferingService;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import Pages.CartPage;
import Pages.ChadavFeedPage;
import Pages.ChadavLoginPage;
import Pages.ItemListingPage;
import Pages.LoginBottomSheetPage;
import Pages.MandirPage;
import Pages.MenuPage;
import TestAnnotations.TestInfo;
import base.BaseTest;
import base.TestData;

@Listeners(base.ListenerImplimentationclass.class)

public class ChadavLoginTest extends BaseTest {	 

	@TestInfo(testcaseID="SMPK-TC-7172,SMPK-TC-7174,SMPK-TC-7175,SMPK-TC-7176,SMPK-TC-7177")	
	@Test(groups= {"regression","smoke"})
	public void ChadavLogin() throws Exception
	{
		MandirPage mandirPage=new MandirPage(driver);
		MenuPage menuPage=new MenuPage(driver);
		ChadavFeedPage chadavFeedPage=new ChadavFeedPage(driver);
		ItemListingPage itemListingPage=new ItemListingPage(driver);
		LoginBottomSheetPage loginbottomsheetpage=new LoginBottomSheetPage(driver);
		ChadavLoginPage chadavLoginPage=new ChadavLoginPage(driver);
		CartPage cartPage=new CartPage(driver);
		try {
			mandirPage.clickOnHamburgerMenu();
			scrollTillEnd();
			menuPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup();
			menuPage.clickOnCancelBtn();
		}catch(Exception e) {
			menuPage.clickOnCancelBtn();
		}
					
		mandirPage.clickOnPunyamudraProfileIcon().clickOnChadavSevaTab();

		//Verify that unlogged user is getting login bottom-sheet on clicking the "Aage bade CTA" in item listing page
		chadavFeedPage.clickOnChadavShevaye();
        scrollToElement(driver,TestData.sriMandirParLokpriyaChdaveText);
        chadavFeedPage.clickOnHeroCard();
		itemListingPage.clickOnAgeBadeBtn();	
		Assert.assertTrue(chadavLoginPage.isLoginBottomSheetDisplayed(), "Login bottom sheet  is not displayed");
		chadavLoginPage.clickOnLoginBottomSheetCloseBtn();
  
		//Verify that user is able to close the login bottom-sheet on clicking device back button
		itemListingPage.clickOnAgeBadeBtn();	
		waitOrPause(5);
		pressNavigationBack(driver);	
		Assert.assertTrue(itemListingPage.isAageBtnDisplayed(), "AgeBade button  is not displayed");
		itemListingPage.clickOnAgeBadeBtn();	
		try {
			if(loginbottomsheetpage.isLoginBottomSheetDisplayed());			
				loginbottomsheetpage.clickOnPhoneLogInBtn().enterMobileNumber()
				.clickOnLogInBtn()
				.enterOtp()
				.clickOnContinueBtn();				
		}catch(Exception e) {
			logger.info("user is already logged in");    
		}		
		
		//Verify that user is able to close the login bottom-sheet on clicking anywhere on the screen
		itemListingPage.tapByCoordinatesInItemListingPage();
		Assert.assertTrue(itemListingPage.isAageBtnDisplayed(), "AgeBade botton  is not displayed");
		
		itemListingPage.clickOnAgeBadeBtn();
		itemListingPage.clearNameTextFeild().enterName();
		itemListingPage.clickOnageBadeOrBhogatanKareBtn();
				
		//Verify that user is getting navigated to cart screen on clicking the Cart Bottom CTA "Aage bade"
		String actualCartPageTitle=cartPage.getCartPageTitle();
		Assert.assertEquals(actualCartPageTitle, TestData.cartPageTitle, "Cart page title is not validated");	
		
		//Verify that user is getting the respective items added to the cart after successful login
		pressNavigationBack(driver);
			
		itemListingPage.swipeupTillItem();
		String beforeAddingItemNameInListingPage=itemListingPage.getItemBhajarangbaliText();
		itemListingPage.clickOnSecondItemAddToCartBtn();	
		
		itemListingPage.clickOnAgeBadeBtn();		
		itemListingPage.clearNameTextFeild().enterName();
		itemListingPage.clickOnageBadeOrBhogatanKareBtn();
		
		String afterAddingItemNameInCartPage=cartPage.getItemBhajarangbaliTextInCart();
		Assert.assertEquals(beforeAddingItemNameInListingPage,afterAddingItemNameInCartPage, "user is not getting the respective list of added items");
		
		cartPage.clickOnSecondRemoveBtnInCartList();

		//navigating back to home page
		for(int i=0; i<4; i++)
		{
	   pressNavigationBack(driver);
		}
		mandirPage.clickOnHamburgerMenu();
		scrollTillEnd();
		menuPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup();
				
	}
}
	