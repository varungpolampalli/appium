package OfferingService;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import Pages.CartPage;
import Pages.ChadavFeedPage;
import Pages.ItemListingPage;
import Pages.LoginBottomSheetPage;
import Pages.MandirPage;
import Pages.MenuPage;
import Pages.OrderHistoryPage;
import Pages.PaymentPage;
import Pages.ProfilePage;
import Pages.TempleListingPage;
import TestAnnotations.TestInfo;
import base.BaseTest;
import base.TestData;

@Listeners(base.ListenerImplimentationclass.class)
public class PaymentFlowTest extends BaseTest {

	@TestInfo(testcaseID = "SMPK-TC-9415,SMPK-TC-9416,SMPK-TC-9417,SMPK-TC-9418,SMPK-TC-9419,SMPK-TC-9420,SMPK-TC-9421,SMPK-TC-9422,SMPK-TC-9423"
			+ "SMPK-TC-9424,SMPK-TC-9425,SMPK-TC-9426,SMPK-TC-9427,SMPK-TC-9429,SMPK-TC-9430,SMPK-TC-9431,SMPK-TC-9432,SMPK-TC-9433,SMPK-TC-9434")
	@Test(groups= {"regression","smoke"})
	public void verifyOfferingServiceEndToEndFlow() throws Exception {
		MandirPage mandirPage=new MandirPage(driver);
		ItemListingPage itemListingPage=new ItemListingPage(driver);
		CartPage cartPage=new CartPage(driver);
		ChadavFeedPage chadavFeedPage=new ChadavFeedPage(driver);
		LoginBottomSheetPage loginbottomsheetpage=new LoginBottomSheetPage(driver);
		PaymentPage paymentPage=new PaymentPage(driver);
		OrderHistoryPage orderHistoryPage=new OrderHistoryPage(driver);
		TempleListingPage templeListingPage=new TempleListingPage(driver);
		ProfilePage profilePage=new ProfilePage(driver);
		MenuPage menuPage=new MenuPage(driver);

		mandirPage.clickOnPunyamudraProfileIcon().clickOnChadavSevaTab();
		chadavFeedPage.clickOnChadavShevaye();
		scrollToElement(driver, TestData.sriMandirParLokpriyaChdaveText);
		chadavFeedPage.scrollToHeroCard();
		chadavFeedPage.clickOnHeroCard();
		itemListingPage.clickOnAgeBadeBtn();
		try {
			loginbottomsheetpage.clickOnPhoneLogInBtn().enterMobileNumber()
			.clickOnLogInBtn()
			.enterOtp()
			.clickOnContinueBtn();
			driver.hideKeyboard();
		}catch (Exception e) {
			logger.info("user is already logged in"); 
			driver.hideKeyboard();
		}
		itemListingPage.clickOnageBadeOrBhogatanKareBtn();
		cartPage.clickOnBhugadaankareBtn();

		if(paymentPage.isPaytmPageDisplayed()) {
			driver.hideKeyboard();
			Assert.assertTrue(paymentPage.isListOfPayTmPaymentOptionsDisplayed(), "List of paytm payment options not displayed");
			Assert.assertTrue(paymentPage.isTotalPriceDisplayedInListOfPaytmPaymentMethods(), "total price not displayed in list of paytm payment methods");
			scrollTillEnd();
			paymentPage.clickOnPaytmNetBankingOption();
			Assert.assertTrue(paymentPage.isListOfPaytmPaymentsBank(), "List of paytm payments bank otpion not displayed");
			paymentPage.clickOnPaytmPayBtn();
			paymentPage.clickOnFailureBtnFromPaytmGateway();
			Assert.assertTrue(paymentPage.isPaymentFailedMessageDisplayed(), "Payment failed message not displayed on clicking failure button");
			paymentPage.clickOnDobaraKoshishKareTab();
			cartPage.clickOnBhugadaankareBtn();
			driver.hideKeyboard();
			scrollTillEnd();
			paymentPage.clickOnPaytmNetBankingOption();
			paymentPage.clickOnPaytmPayBtn();
			paymentPage.clickOnSuccessBtnFromPaytmGateway();

			Assert.assertTrue(paymentPage.isPaymentSuccessIconDisplayed(), "Payment success icon not displayed");
			Assert.assertTrue(paymentPage.isToatalPriceFromPaymentSuccessPageDisplayed(), "Total price from payment succes page not validated");
			Assert.assertTrue(paymentPage.isPaymentSuccessFullTitleDisplayed(), "payment sucessfull title not displayed");
			Assert.assertTrue(paymentPage.isAageKyaKareInfoTextDisplayed(), "Aage kya kare info text not displayed");
			Assert.assertTrue(paymentPage.isAageKyaKareInfoStepsDisplayed(), "Aage kya kare info steps not displayed");
			Assert.assertTrue(paymentPage.isTeekHaiBtnDisplayed(), "Teek hai button not displayed in payment succesfull page");
			paymentPage.clickOnTeekHaiBtn();
			Assert.assertTrue(orderHistoryPage.isFirstOrderCardDisplayed(), "Redirecion not validated on clicking teek hai button from payment succes page");
			templeListingPage.clickOnBackBtn();
			profilePage.clickOnBackBtn();
			mandirPage.clickOnHamburgerMenu();
			scrollTillEnd();
			menuPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup();
			menuPage.clickOnCancelBtn();	
		}else {
			paymentPage.clickOnNetBankingBtn().clickOnNetBankNameBtn().clickOnPayNowBtn().clickOnSuccessBtn();
			Assert.assertTrue(paymentPage.isPaymentSuccessIconDisplayed(), "Payment success icon not displayed");
			Assert.assertTrue(paymentPage.isToatalPriceFromPaymentSuccessPageDisplayed(), "Total price from payment succes page not validated");
			Assert.assertTrue(paymentPage.isPaymentSuccessFullTitleDisplayed(), "payment sucessfull title not displayed");
			Assert.assertTrue(paymentPage.isAageKyaKareInfoTextDisplayed(), "Aage kya kare info text not displayed");
			Assert.assertTrue(paymentPage.isAageKyaKareInfoStepsDisplayed(), "Aage kya kare info steps not displayed");
			Assert.assertTrue(paymentPage.isTeekHaiBtnDisplayed(), "Teek hai button not displayed in payment succesfull page");
			paymentPage.clickOnTeekHaiBtn();
			Assert.assertTrue(orderHistoryPage.isFirstOrderCardDisplayed(), "Redirecion not validated on clicking teek hai button from payment succes page");

			chadavFeedPage.clickOnChadavShevaye();
			scrollToElement(driver, TestData.sriMandirParLokpriyaChdaveText);
			chadavFeedPage.scrollToHeroCard();
			chadavFeedPage.clickOnHeroCard();
			itemListingPage.clickOnAgeBadeBtn();
			itemListingPage.clickOnageBadeOrBhogatanKareBtn();
			cartPage.clickOnBhugadaankareBtn();
			Assert.assertTrue(paymentPage.isListOfPaymentMethods(), "List of payment methods not displayed");
			Assert.assertTrue(paymentPage.isTotalPriceDisplayedInListOfPaymentMethods(), "Total price not displayed in list of payment methods page");
			Assert.assertTrue(paymentPage.isListOfBanksDisplayed(), "List of banks not displayed");
			paymentPage.clickOnBackBtnFromListOfBanksPage();
			paymentPage.clickOnCancelBtnFromListOfPaymentMethodsPage();
			Assert.assertTrue(paymentPage.isPaymentFailedMessageDisplayed(), "Payment failed message not displayed on clicking cancel button from list of payment methods page");
			paymentPage.clickOnDobaraKoshishKareTab();
			Assert.assertTrue(cartPage.isBhugadaankareBtnDisplayed(), "navigation not validated on clicking dobara koshish kare btn");
			cartPage.clickOnBhugadaankareBtn();
			Assert.assertTrue(paymentPage.isPaymentPageDisplayed(), "Payment page navigation not validated on clicking bhugathan kare tab");
			paymentPage.clickOnNetBankingBtn().clickOnNetBankNameBtn().clickOnPayNowBtn().clickOnFailureBtn();
			Assert.assertTrue(paymentPage.isPaymentFailureErrorMessageDisplayed(), "Payment failure error message not displayed");
			paymentPage.clickOnTryAgainBtn();
			paymentPage.clickOnBackBtnFromListOfBanksPage().clickOnCancelBtnFromListOfPaymentMethodsPage();
			paymentPage.clickOnDobaraKoshishKareTab();
			cartPage.clickOnBackBtnFromCartpage();
			itemListingPage.clickOnBackBtnFromItemListingPage();
			templeListingPage.clickOnBackBtn();
			profilePage.clickOnBackBtn();
			mandirPage.clickOnHamburgerMenu();
			scrollTillEnd();
			menuPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup();
			menuPage.clickOnCancelBtn();
		}
	}

}
