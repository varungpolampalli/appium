package OfferingService;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import Pages.ChadavFeedPage;
import Pages.MandirPage;
import Pages.MenuPage;
import Pages.OrderHistoryPage;
import Pages.ProfilePage;
import Pages.TempleListingPage;
import TestAnnotations.TestInfo;
import base.BaseTest;
import base.TestData;

@Listeners(base.ListenerImplimentationclass.class)
public class OrderHistoryTest extends BaseTest {

	@TestInfo(testcaseID="SMPK-TC-9287,SMPK-TC-9288,SMPK-TC-9289,SMPK-TC-9290,SMPK-TC-9296,SMPK-TC-9297,SMPK-TC-9298,SMPK-TC-9299,SMPK-TC-9300,"
			+ "SMPK-TC-9301,SMPK-TC-9302,SMPK-TC-9303,SMPK-TC-9305,SMPK-TC-9306,SMPK-TC-9307,SMPK-TC-9309,SMPK-TC-9310,SMPK-TC-9311,SMPK-TC-9312,SMPK-TC-9313"
			+ "SMPK-TC-9315,SMPK-TC-9316,SMPK-TC-9317,SMPK-TC-9318,SMPK-TC-9322,SMPK-TC-9324,SMPK-TC-9325")
	@Test(groups= {"regression","smoke"})
	public void verifyOrderHistory() throws Exception
	{
		MandirPage mandirPage=new MandirPage(driver);
		TempleListingPage templeListingPage=new TempleListingPage(driver);
		OrderHistoryPage orderHistoryPage=new OrderHistoryPage(driver);
		MenuPage menuPage=new MenuPage(driver);
		ProfilePage profilePage=new ProfilePage(driver);
		ChadavFeedPage chadavFeedPage=new ChadavFeedPage(driver);

		try {
			mandirPage.clickOnHamburgerMenu();
			scrollTillEnd();
			menuPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup();
			menuPage.clickOnCancelBtn();
		}catch (Exception e) {
			logger.info("user is already in logged out state");
			menuPage.clickOnCancelBtn();
		}

		mandirPage.clickOnPunyamudraProfileIcon().clickOnChadavSevaTab();
		templeListingPage.clickOnSampannaSevayeTab();
		
		//validating order history page in unlogged stage
		Assert.assertTrue(orderHistoryPage.isLogInTabDisplayed(), "Login tab not displayed in order history page");
		Assert.assertTrue(orderHistoryPage.isEmptyTitleDisplayed(), "message not validated in order history page in unlogged state");
		
		orderHistoryPage.clickOnLogInTab();
		Assert.assertTrue(orderHistoryPage.isLoginBottomSheetDisplayed(), "Login bottom sheet not displayed in order history page");
		orderHistoryPage.clickOnLoginBottomSheetCloseBtn();
		
		orderHistoryPage.clickOnLogInTab().clickOnPhoneLogInBtn();
		orderHistoryPage.enterMobileNumberOfUserWithNoOfferingOrder().clickOnLogInBtn().enterOtp().clickOnContinueBtn();
		
		Assert.assertTrue(orderHistoryPage.isEmptyTitleDisplayed(), "message not validated in order history page with no orders with user logged in state ");
		Assert.assertEquals(orderHistoryPage.getChadavBookKareText(), TestData.chadavBookKareText, "Chadav book kare tab not validated");
		orderHistoryPage.clickOnChadavBookKareTab();
		scrollToElement(driver, TestData.sriMandirParLokpriyaChdaveText);
		chadavFeedPage.scrollToHeroCard();
		Assert.assertTrue(chadavFeedPage.isHeroCardDisplayed(), "feed tab navigation not validated");
		templeListingPage.clickOnBackBtn();
		profilePage.clickOnBackBtn();
		mandirPage.clickOnHamburgerMenu();
		scrollTillEnd();
		menuPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup();
		menuPage.clickOnCancelBtn();
		
		mandirPage.clickOnPunyamudraProfileIcon().clickOnChadavSevaTab();
		templeListingPage.clickOnSampannaSevayeTab();
		try {
			orderHistoryPage.clickOnLogInTab().clickOnPhoneLogInBtn().enterMobileNumber()
			.clickOnLogInBtn().enterOtp().clickOnContinueBtn();
		}catch (Exception e) {
			logger.info("user is already logged in");
		}
		orderHistoryPage.clickOnFilterTab();
		orderHistoryPage.clickOnAllHistoryFilterOptions();
		Assert.assertTrue(orderHistoryPage.isFirstOrderCardDisplayed(), "order details not validated for a user who is having past orders after succesfull login");
		Assert.assertTrue(orderHistoryPage.isNameOnOrderCardDisplayed(), "Name on order card not validated or displayed");
		Assert.assertTrue(orderHistoryPage.isOrderDateDisplayed(), "order date not validated in order card");
		Assert.assertTrue(orderHistoryPage.isOfferingItemImageDisplayedOnOfferingCard(), "offering item image not displayed in order card");
		try {
		Assert.assertTrue(orderHistoryPage.isAaneWalaChadavTextDisplayed(), "Aane wala chadav text not displayed");
		}catch (Exception e) {
			logger.info("user don't have any processing orders");
		}
		Assert.assertTrue(orderHistoryPage.isFilterIconDisplayed(), "filter icon not displayed in order details page");
		orderHistoryPage.clickOnFilterTab();
		Assert.assertTrue(orderHistoryPage.isFilterBottomSheetDisplayed(), "Filter bottom sheet not displayed on clicking filter tab from order details page");
		orderHistoryPage.clickOnCloseButtonFromFilterBottomSheet();
		Assert.assertTrue(orderHistoryPage.isFirstOrderCardDisplayed(), "close button not validated from filter bottom sheet");
		orderHistoryPage.clickOnFilterTab();
		Assert.assertTrue(orderHistoryPage.isSevendaysFilterIconDisplayed(),"Seven days filter is not displayed in order history page");
		orderHistoryPage.clickOnThirtyDaysFilterOptions();
		try {
			Assert.assertTrue(orderHistoryPage.isFirstOrderCardDisplayed(), "order card not displayed on clicking thirty day filter icon");
		}catch (Exception e) {
			logger.info("no ordered items in past thirty days");
		}
		orderHistoryPage.clickOnFilterTab();
		orderHistoryPage.clickOnAllHistoryFilterOptions();
		Assert.assertTrue(orderHistoryPage.isFirstOrderCardDisplayed(), "order card  not displayed on clicking all history filter option");
		orderHistoryPage.clickOnFilterTab();
		orderHistoryPage.clickOnCloseButtonFromFilterBottomSheet();
		Assert.assertTrue(orderHistoryPage.isFirstOrderCardDisplayed(), "Filter bottom sheet not closed on clicking device back button");
		waitOrPause(3);
		scrollTillEnd();
		Assert.assertTrue(orderHistoryPage.isCompletedOfferingCardDisplayed(), "Completed offering card not displayed in order details page");
		Assert.assertTrue(orderHistoryPage.isGreenTickDisplayed(), "Green tick not displayed in order details page");
		templeListingPage.clickOnBackBtn();
		profilePage.clickOnBackBtn();
		mandirPage.clickOnHamburgerMenu();
		scrollTillEnd();
		menuPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup();
		menuPage.clickOnCancelBtn();
	}

}
