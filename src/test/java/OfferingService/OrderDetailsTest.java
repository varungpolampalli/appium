
package OfferingService;

import Pages.*;
import TestAnnotations.TestInfo;
import base.BaseTest;
import base.TestData;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
@Listeners(base.ListenerImplimentationclass.class)

public class OrderDetailsTest extends BaseTest {
	@TestInfo(testcaseID = "SMPK-TC-9326,SMPK-TC-9327,SMPK-TC-9328,SMPK-TC-9329,SMPK-TC-9330,SMPK-TC-9331,SMPK-TC-9332,SMPK-TC-9333,SMPK-TC-9335,SMPK-TC-9336,SMPK-TC-9337," +
			"SMPK-TC-9338,SMPK-TC-9339,SMPK-TC-9340,SMPK-TC-9341,SMPK-TC-9342,SMPK-TC-9343,SMPK-TC-9344,SMPK-TC-9346,SMPK-TC-9347,SMPK-TC-9348,SMPK-TC-9355,SMPK-TC-9365,SMPK-TC-9367,SMPK-TC-9368,SMPK-TC-9369,SMPK-TC-9370,"
			+ "SMPK-TC-9812")
	@Test(groups= {"regression","smoke"})
	public void VerifyOrderDetailsPage() throws Exception {
		MandirPage mandirPage=new MandirPage(driver);
		TempleListingPage templeListingPage=new TempleListingPage(driver);
		OrderDetailsPage orderDetailsPage=new OrderDetailsPage(driver);
		OrderHistoryPage orderHistoryPage=new OrderHistoryPage(driver);
		ProfilePage profilePage=new ProfilePage(driver);
		MenuPage menuPage=new MenuPage(driver);

		mandirPage.clickOnPunyamudraProfileIcon().clickOnChadavSevaTab();
		templeListingPage.clickOnSampannaSevayeTab();

		try{
			if(orderHistoryPage.isLogInTabDisplayed()) {
				orderHistoryPage.clickOnLogInTab().clickOnPhoneLogInBtn()
				.enterMobileNumber()
				.clickOnLogInBtn()
				.enterOtp()
				.clickOnContinueBtn();
			}
		}catch (Exception e){
			logger.info("user is already logged in");
			templeListingPage.clickOnBackBtn();
			profilePage.clickOnBackBtn();
			mandirPage.clickOnHamburgerMenu();
			scrollTillEnd();
			menuPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup();
			menuPage.clickOnCancelBtn();
			mandirPage.clickOnPunyamudraProfileIcon().clickOnChadavSevaTab();
			templeListingPage.clickOnSampannaSevayeTab();
			orderHistoryPage.clickOnLogInTab().clickOnPhoneLogInBtn()
			.enterMobileNumber()
			.clickOnLogInBtn()
			.enterOtp()
			.clickOnContinueBtn();
		}
		orderHistoryPage.clickOnFilterTab();
		orderHistoryPage.clickOnAllHistoryFilterOptions();
		orderHistoryPage.clickOnFirstOrderCard();

		//validating if booking id displayed in order details page
		Assert.assertTrue(orderDetailsPage.isBookingIdDisplayed(),"Booking id not displayed in order details page");
		Assert.assertTrue(orderDetailsPage.isCountAndPriceOfOfferingItemDisplayed(),"count and price of offering item not displayed in order details page");
		Assert.assertTrue(orderDetailsPage.isOrderStatusDisplayed(),"order status not displayed in order details page");
		Assert.assertTrue(orderDetailsPage.isStatusIconDisplayed(),"order status icon not displayed in order details page");
		Assert.assertTrue(orderDetailsPage.isOfferingIconDisplayed(),"offering icon not displayed in order details page");
		Assert.assertTrue(orderDetailsPage.isOrderDateDisplayed(),"order date not displayed in order details page");
		Assert.assertTrue(orderDetailsPage.isDownloadKareBtnDisplayed(),"download kare button not displayed in order details page");

		//clicking on whatsapp share icon
		orderDetailsPage.clickingOnWhatsappShareIcon();
		Assert.assertTrue(orderDetailsPage.isShareBottomSheetDisplayed(),"share bottom sheet not displayed on clicking whatsapp share icon");
		pressNavigationBack(driver);

		//checking if bottom toast bar message displayed
		Assert.assertTrue(orderDetailsPage.isBottomToastBarMessageDisplayed(),"Bottom toast bar message not displayed");

		//validating certificate image
		Assert.assertTrue(orderDetailsPage.isCertificateImageDisplayed(),"certificate image not displayed in order details page");
		orderDetailsPage.clickOnCertificateImage();
		Assert.assertTrue(orderDetailsPage.isDetailedImageViewDisplayed(),"Detailed image view not displayed o clicking certificate image");
		Assert.assertTrue(orderDetailsPage.isDownloadButtonDisplayedInCertificateImage(),"Download button not displayed in certificate image");
		orderDetailsPage.clickingOnWhatsappShareIconFromDetailedImageView();
		Assert.assertTrue(orderDetailsPage.isShareBottomSheetDisplayed(),"Share bottom sheet not displayed on clicking whatsapp share button from detailed image view page");
		pressNavigationBack(driver);
		orderDetailsPage.clickOnBackBtnFromDetailedImageViewPage();
		scrollTillEnd();

		Assert.assertTrue(orderDetailsPage.isBillSummaryCardDisplayed(),"Bill summary card not displayed in order details page");
		Assert.assertTrue(orderDetailsPage.isOfferingCountAndPriceDisplayedInBillSummaryCard(),"Offering count and price not displyed in order details page");
		orderDetailsPage.clickOnDownCheveronFromBillSummaryCard();
		scrollTillEnd();
		Assert.assertTrue(orderDetailsPage.isPriceBreakUpDetailsDisplayed(),"Price break up details not displayed in order details page");
		Assert.assertTrue(orderDetailsPage.isTotalPriceDisplayedInBillSummaryCard(),"Total price not displayed in bill summary card in order details page");
		Assert.assertTrue(orderDetailsPage.isDownloadButtonDisplayedInBillSummaryCard(),"Download button not displayed in bill summary card");
		orderDetailsPage.clickOnDownCheveronFromBillSummaryCard();

		orderDetailsPage.clickOnBackBtn();
		Assert.assertTrue(orderHistoryPage.isFirstOrderCardDisplayed(),"back navigation from order details page not validated");

		//validating the completed offering details
		scrollTillEnd();
		orderHistoryPage.clickOnCompletedOfferingCard();
		scrollTillEnd();
		Assert.assertTrue(orderDetailsPage.isCompletedOrderTitleDisplayed(), "completed offering video title not displayed");
		Assert.assertTrue(orderDetailsPage.isCompletedOfferingVideoDisplayed(), "Completed offering video not displayed");
		Assert.assertTrue(orderDetailsPage.isDownloadBtnDisplayedBelowVideo(), "Download button not displayed below the offering complted video");
		Assert.assertTrue(orderDetailsPage.isShareBtnDisplayedBelowVideo(), "Share button not displayed below the offering completed video");
		//      orderDetailsPage.clickOnShareBtnFromVideo();
		//      Assert.assertTrue(orderDetailsPage.isShareBottomSheetDisplayed(),"share bottom sheet not displayed on clicking share button from completed offering video");
		//      pressNavigationBack(driver);
		orderDetailsPage.clickOnCompletedOfferingVideo();
		pressNavigationBack(driver);
		orderDetailsPage.clickOnBackBtn();
		templeListingPage.clickOnBackBtn();
		profilePage.clickOnBackBtn();
		mandirPage.clickOnHamburgerMenu();
		scrollTillEnd();
		menuPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup();
	}
}
