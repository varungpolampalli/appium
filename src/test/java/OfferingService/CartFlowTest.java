package OfferingService;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import Pages.CartPage;
import Pages.ChadavFeedPage;
import Pages.ItemListingPage;
import Pages.LoginBottomSheetPage;
import Pages.MandirPage;
import Pages.MenuPage;
import Pages.ProfilePage;
import TestAnnotations.TestInfo;
import base.BaseTest;
import base.TestData;

@Listeners(base.ListenerImplimentationclass.class)

public class CartFlowTest extends BaseTest{
	
	@TestInfo(testcaseID="SMPK-TC-9247,SMPK-TC-9248,SMPK-TC-9249,SMPK-TC-9250,SMPK-TC-9252,SMPK-TC-9253,SMPK-TC-9254,SMPK-TC-9255,SMPK-TC-9266,SMPK-TC-9267,SMPK-TC-9268,SMPK-TC-9269,SMPK-TC-9270,SMPK-TC-9272,SMPK-TC-9273,SMPK-TC-9274,SMPK-TC-9275,SMPK-TC-9276,SMPK-TC-9277,SMPK-TC-9278,SMPK-TC-9279,SMPK-TC-9281,SMPK-TC-9286")
	@Test(groups= {"regression","smoke"})
	public void verifyCartFlow() throws Exception {
		MandirPage mandirPage=new MandirPage(driver);
		ProfilePage profilePage=new ProfilePage(driver);
		ChadavFeedPage chadavFeedPage=new ChadavFeedPage(driver);
		ItemListingPage itemListingPage=new ItemListingPage(driver);
		CartPage cartPage=new CartPage(driver);
		LoginBottomSheetPage loginbottomsheetpage=new LoginBottomSheetPage(driver);
		MenuPage menuPage=new MenuPage(driver);
		mandirPage.clickOnPunyamudraProfileIcon();
		profilePage.clickOnChadavSevaTab();
		
		//Verify that first item got auto selected after clicking on temple card
		chadavFeedPage.clickOnChadavShevaye();
        scrollToElement(driver,TestData.sriMandirParLokpriyaChdaveText);
        chadavFeedPage.scrollToHeroCard();
        chadavFeedPage.clickOnHeroCard();
		Assert.assertTrue(itemListingPage.isBottomCartCTAIconDisplayed(), "AgeBade bottom cart  is not displayed");
		itemListingPage.clickOnAgeBadeBtn();		
		try {
			if(loginbottomsheetpage.isLoginBottomSheetDisplayed());
			{
				loginbottomsheetpage.clickOnPhoneLogInBtn().enterMobileNumber()
				.clickOnLogInBtn()
				.enterOtp()
				.clickOnContinueBtn();
			}	
		}catch(Exception e) {
			logger.info("user is already logged in");    
		}		
		
		//Verify that the username bottom sheet is getting closed on clicking anywhere on screen
		itemListingPage.tapByCoordinatesInItemListingPage();
		Assert.assertTrue(itemListingPage.isAageBtnDisplayed(), "AgeBade botton  is not displayed");
		
		//Verify that user is able to slide over the offering item suggestion
		itemListingPage.clickOnAgeBadeBtn();
		itemListingPage.clearNameTextFeild().enterName();
		itemListingPage.clickOnageBadeOrBhogatanKareBtn().slideOverOfferingItem();
		
		//Verify that user is landing on the respective screen on back navigation from the cart screen
		cartPage.clickOnBackBtnFromCartpage();
		Assert.assertTrue(itemListingPage.isAageBtnDisplayed(), "AgeBade bade button not displayed");		

		//Verify that the user is getting respective list of added items
		itemListingPage.swipeupTillItem();
		String beforeAddingItemNameInListingPage=itemListingPage.getItemBhajarangbaliText();		
		try {
			if(itemListingPage.getAddToCartTextFromSecondProduct().equals(TestData.hatayeText))
			{
                itemListingPage.clickOnSecondItemAddToCartBtn();
            }
		}      
      catch (Exception e){
          itemListingPage.clickOnSecondItemAddToCartBtn();
      }
				
		//String beforeAddingItemNameInListingPage=itemListingPage.getItemBhajarangbaliText();
		itemListingPage.clickOnSecondItemAddToCartBtn();	
		
		itemListingPage.clickOnAgeBadeBtn();
		//Verify that user is getting user name bottom sheet before making the payment
		Assert.assertTrue(itemListingPage.isNameBottomSheetDisplayed(), "Name bottom sheet is not displayed");
		
        //Verify that username bottom sheet is getting closed on clicking device back
		for(int i=0; i<2;i++)
		{
			pressNavigationBack(driver);
		}
		Assert.assertTrue(itemListingPage.isAageBtnDisplayed(), "AgeBade bade button not displayed");

		
		//Verify that user is able to enter his/her name in the user name bottom sheet
		itemListingPage.clickOnAgeBadeBtn();
		itemListingPage.clearNameTextFeild().enterName();
		itemListingPage.clickOnageBadeOrBhogatanKareBtn();

		//Verify that the user is getting respective list of added items
		String afterAddingItemNameInCartPage=cartPage.getItemBhajarangbaliTextInCart();
		Assert.assertEquals(beforeAddingItemNameInListingPage,afterAddingItemNameInCartPage, "user is not getting the respective list of added items");

		//Verify that user is getting navigated to cart screen on clicking the Cart Bottom CTA "Aage bade"
		String actualCartPageTitle=cartPage.getCartPageTitle();
		Assert.assertEquals(actualCartPageTitle, TestData.cartPageTitle, "Cart page title is not validated");

		//Verify that user is getting offering item sugessions below the added items
		Assert.assertTrue(cartPage.isOfferingItemSuggestionDisplayed(), "Offering Item suggestion is not displayed");

		//Verify that user is getting respective offering item metadata on the offering item suggestion
		Assert.assertTrue(cartPage.isOfferingItemSuggestionNameDisplayed(), "Offering Item suggestion name is not displayed");
		Assert.assertTrue(cartPage.isOfferingItemSuggestionDescriptionDisplayed(), "Offering Item suggestion description is not displayed");
		Assert.assertTrue(cartPage.isOfferingItemSuggestionPrizeDisplayed(), "Offering Item suggestion prize is not displayed");
		Assert.assertTrue(cartPage.isOfferingItemSuggestionImageDisplayed(), "Offering Item suggestion image is not displayed");
		Assert.assertTrue(cartPage.isOfferingItemSuggestionChuneBtnDisplayed(), "Offering Item suggestion chune is not displayed");

		//Verify that user is getting respective metadata of the added items
		Assert.assertTrue(cartPage.isRemoveBtnOfFirstItemDisplayed(), "Remove btn of first item is not displayed");
		Assert.assertTrue(cartPage.isRemoveBtnOfSecondItemDisplayed(), "Remove btn of second item is not displayed");
		Assert.assertTrue(cartPage.isFirstItemNameOfAddeditemsDisplayed(), "First Item Name of Added items is not displayed");
		Assert.assertTrue(cartPage.isSecondItemNameOfAddeditemsDisplayed(), "Second Item Name of Added items is not displayed");
		Assert.assertTrue(cartPage.isFirstItemPrizeOfAddeditemsDisplayed(), "First Item prize of Added items is not displayed");
		Assert.assertTrue(cartPage.isSecondItemPrizeOfAddeditemsDisplayed(), "Second Item prize of Added items is not displayed");

		//Verify that user is getting "Bhoogdaan Kare" text instead of "aage bade" in bottom cart CTA
		Assert.assertTrue(cartPage.isBhugadaankareBtnDisplayed(), "bhugadan kare btn is not displayed");

		//Verify that user is getting bill summary for the added items
		Assert.assertTrue(cartPage.isBillSummaryDisplayed(), "Bill summary is not displayed");

		//Verify that user is getting the respective total price in the bill summary section
		int FirstItemPrizeFromCartList=cartPage.getFirstItemPrizeFromCartList();
		int SecondItemPrizeFromCartList=cartPage.getSecondItemPrizeFromCartList();
		int totalItemPrizeFromCartList=FirstItemPrizeFromCartList+SecondItemPrizeFromCartList;
		Assert.assertTrue(totalItemPrizeFromCartList==cartPage.getTotalCartValueFromCartList(), "total price is not validated in the bill summary section");

		//Verify that for a user the bill summary is getting updated on adding and removing items in the cart
		cartPage.clickOnSecondRemoveBtnInCartList();
		Assert.assertEquals(cartPage.getItemPrizeAfterRemovingItem(), cartPage.getItemPrizeInBillSummary(), "bill summary is not getting updated on  removing items in the cart");

		//swiping till FAQ
		cartPage.swipeupTillFAQ();

		//Validate that user is getting respective metadata in the bill summary
		Assert.assertTrue(cartPage.isVastuRashiTextInBillSummaryDisplayed(), "vastuRashi text in Bill summary is not displayed");
		Assert.assertTrue(cartPage.isVastuRashiAmountInBillSummaryDisplayed(), "vastuRashi amount in Bill summary is not displayed");
		Assert.assertTrue(cartPage.isSriMandirSuvidashlokTextInBillSummaryDisplayed(), "sri mandir suvidashlok text in Bill summary is not displayed");
		Assert.assertTrue(cartPage.isSriMandirSuvidashlokPrizeInBillSummaryDisplayed(), "sri mandir suvidashlok prize in Bill summary is not displayed");
		Assert.assertTrue(cartPage.isTotalAmountTextDisplayed(), "total amount text in Bill summary is not displayed");
		Assert.assertTrue(cartPage.isTotalAmountPrizeDisplayed(), "total amount prize in Bill summary is not displayed");

		//Verify that user is getting respective FAQ's in cart section.
		Assert.assertTrue(cartPage.isFaqSectionDisplayed(), "faq section in cart section is not displayed");

		//Verify that user is able to expand/ collapse FAQ's in cart section
		cartPage.clickOnDownChevaron();
		Assert.assertTrue(cartPage.isAnswerOfFaqSectionDisplayed(), "answer of faq section by clicking on down chevaron in cart section is not displayed");

		//Verify that user is able to scroll through the cart screen
		cartPage.scrollingThroughCart();

		//Verify that user is getting respective list of items in the cart on adding items from the suggestions
		cartPage.scrollToBeginning();
		String beforeAddingItemNamefromSuggestion=cartPage.getItemNameFromItemsuggestion();
		cartPage.clickOnChuneBtnFromSuggestion();
		String afterAddingItemNamefromSuggestion=cartPage.getItemSuggestionTextInCart();
		Assert.assertEquals(beforeAddingItemNamefromSuggestion,afterAddingItemNamefromSuggestion, "user is not getting the respective list of added item from suggestion");

		//Verify that user can remove items from the cart list after adding items through item suggestions
		String beforeRemovingItemNamefromCart=cartPage.getItemSuggestionTextInCart();
		cartPage.clickOnSecondRemoveBtnInCartList();
		cartPage.swipingThroughFirstCart();
		String afterRemovingItemNamefromSuggestion=cartPage.getItemNameFromItemsuggestion();
		Assert.assertEquals(beforeRemovingItemNamefromCart,afterRemovingItemNamefromSuggestion, "user is not able to remove the respective list of added item from cart");

		//Verify that user is able to remove items from the cart
		cartPage.clickOnFirstRemoveBtnInCartList();
		int actualCartValue=cartPage.getCartValue();		
		Assert.assertTrue(actualCartValue==0, "cart value is not validated after removing item");
	
		//Verify that user is getting the respective list of suggestions on removing all items in cart review page
		Assert.assertTrue(cartPage.isOfferingItemSuggestionDisplayed(), "Offering Item suggestion is not displayed");
	
		//logging out from app
		for(int i=0;i<4;i++)
		{
			pressNavigationBack(driver);
		}
		waitOrPause(4);
		mandirPage.clickOnHamburgerMenu();
		scrollTillEnd();
		menuPage.clickOnLogoutTab().clickOnYesBtnFromLogoutPopup();
		menuPage.clickOnCancelBtn();
	}

}