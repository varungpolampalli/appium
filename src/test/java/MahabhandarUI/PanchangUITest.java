package MahabhandarUI;

import TestAnnotations.TestInfo;
import UIPages.MandirUIPage;
import UIPages.PanchangUIPage;
import base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners(base.ListenerImplimentationclass.class)
public class PanchangUITest extends BaseTest {

    @TestInfo(testcaseID = "SMPK-TC-9700,SMPK-TC-9703,SMPK-TC-9704,SMPK-TC-9705,SMPK-TC-9706,SMPK-TC-9707,SMPK-TC-9708,SMPK-TC-9709," +
            "SMPK-TC-9710,SMPK-TC-9711,SMPK-TC-9712,SMPK-TC-9713,SMPK-TC-9714,SMPK-TC-9715,SMPK-TC-9716,SMPK-TC-9717,SMPK-TC-9718,SMPK-TC-9719,SMPK-TC-9720,SMPK-TC-9721,SMPK-TC-9722,SMPK-TC-9723")
    @Test(groups= {"regression","smoke"})
    public void verifyPanchangPageUI() throws Exception {
        MandirUIPage mandirUIPage=new MandirUIPage(driver);
        PanchangUIPage panchangUIPage=new PanchangUIPage(driver);
        mandirUIPage.clickOnMahabhandarTab();
        panchangUIPage.swipeHorizontalBar().clickOnPanchangTab();

        Assert.assertTrue(panchangUIPage.isDailyPanchangTitleDisplayed(),"daily panchang page title is not displayed");
        Assert.assertTrue(panchangUIPage.isdailyPanchangTabDisplayed(),"daily panchang tab not displayed in panchang page");
        Assert.assertTrue(panchangUIPage.isMonthlyPanchangTabDisplayed(),"Monthly panchang tab not displayed in panchang tab");
        Assert.assertTrue(panchangUIPage.isLocationTabDisplayed(),"location tab not displayed in panchang page");
        Assert.assertTrue(panchangUIPage.isDayInfoDisplayed(),"Day info is not displayed in panchang page");
        Assert.assertTrue(panchangUIPage.isMoonImageDisplayed(),"Moon image is not displayed in moon card");
        Assert.assertTrue(panchangUIPage.isMoonCardTitleDisplayed(),"Moon card title not displayed in Moon card");
        Assert.assertTrue(panchangUIPage.isMoonCardTimeSlotTimeDetailsDisplayed(),"Moon card Time details not displayed ");
        Assert.assertTrue(panchangUIPage.isMonthDetailsDisplayedInMoonCard(),"Month details not displayed in moon card");
        Assert.assertTrue(panchangUIPage.isSeasonDetailsDisplayedInMoonCard(),"Season details not displayed in moon card");
        Assert.assertTrue(panchangUIPage.isFloatingShareIconIsDisplayed(),"Floating share icon is not displayed in panchang page");
        panchangUIPage.scrollTillShubhaAshbhaSamay();
        Assert.assertTrue(panchangUIPage.isShubhaAshubhaSamayTitleDisplayed(),"Shubha-Ashubha samay title  not displayed");
        Assert.assertTrue(panchangUIPage.isShubhaAshubhaSamayCardsDisplayed(),"Shubha ashubha samay cards not displayed in panchang page");
        Assert.assertTrue(panchangUIPage.isShubhaAshubhaSamayCardTitleDisplayed(),"Shubha ashubha samay card title is not displayed");
        Assert.assertTrue(panchangUIPage.isShubhaAshubhaSamayCardDescriptionDisplayed(),"Shubha ashubha samay card description not displayed");
        panchangUIPage.scrollTillSuryodaySuryastha();
        Assert.assertTrue(panchangUIPage.isSuryodaySuryasthaTitleDisplayed(),"Suryoday suryastha title not displayed in panchang page");
        Assert.assertTrue(panchangUIPage.isSuryodaySuryasthaCardsDisplayed(),"Suryoday suryastha cards not displayed in panchang page");
        Assert.assertTrue(panchangUIPage.isSuryodaySuryasthaCardDescriptionDisplayed(),"suryoday suryaastha card description not displayed in panchang page");
        Assert.assertTrue(panchangUIPage.isSuryodaySuryasthaCardIconIsDisplayed(),"Suryoday suryaastha card icon not displayed in panchang page");
        Assert.assertTrue(panchangUIPage.isSuryodaySuryasthaCardTitleDisplayed(),"Suryoday suryastha card title not displayed in panchag page");
        panchangUIPage.scrollTillSampoornaPanchangCard();
        Assert.assertTrue(panchangUIPage.isSampoornaPanchangCardDisplayed(),"Sampoorna Panchang  card not displayed in panchang page");
        Assert.assertTrue(panchangUIPage.isShubhaAshubhaSamayCardTitleDisplayed(),"Sampoorna panchang card title not displayed");
        Assert.assertTrue(panchangUIPage.isTakurPrasiddhPanchangTabDisplayed(),"Takur prasiddh panchang tab not displayed in panchang page");
        Assert.assertTrue(panchangUIPage.isMasikPanchnagTabDisplayed(),"Masik panchang tab not displayed in panchang page");
    }

}
