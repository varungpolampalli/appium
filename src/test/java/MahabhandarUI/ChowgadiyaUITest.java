package MahabhandarUI;

import Pages.MahabhandarPage;
import TestAnnotations.TestInfo;
import UIPages.ChowgadiyaUIPage;
import UIPages.MahabhandarUIPage;
import UIPages.MandirUIPage;
import base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners(base.ListenerImplimentationclass.class)
public class ChowgadiyaUITest extends BaseTest {
    @TestInfo(testcaseID = "SMPK-TC-9724,SMPK-TC-9725,SMPK-TC-9726,SMPK-TC-9727,SMPK-TC-9728,SMPK-TC-9729,SMPK-TC-9730,SMPK-TC-9731,SMPK-TC-9732,SMPK-TC-9733" +
            "SMPK-TC-9734,SMPK-TC-9735,SMPK-TC-9736,SMPK-TC-9737,SMPK-TC-9738,SMPK-TC-9739,SMPK-TC-9740,SMPK-TC-9741,SMPK-TC-9742,SMPK-TC-9743,SMPK-TC-9744," +
            "SMPK-TC-9745,SMPK-TC-9746,SMPK-TC-9747")
    @Test(groups= {"regression","smoke"})
    public void verifyChowgadiyaPageUI() throws Exception {
        MandirUIPage mandirUIPage=new MandirUIPage(driver);
        MahabhandarUIPage mahabhandarUIPage=new MahabhandarUIPage(driver);
        ChowgadiyaUIPage chowgadiyaUIPage=new ChowgadiyaUIPage(driver);

        mandirUIPage.clickOnMahabhandarTab();
        mahabhandarUIPage.clickOnChowgadiyaTab();
        Assert.assertTrue(chowgadiyaUIPage.isChowgadiyaTitleDisplayed(), "Chowgadiya title is not displayed in chowgadiya Page");
        Assert.assertTrue(chowgadiyaUIPage.isLocationTabDisplayed(),"chowgadiya location tab is not displayed in chowgadiya page");
        Assert.assertTrue(chowgadiyaUIPage.isDateHighLightTabDisplayed(),"Date highlight tab is not displayed in chowgadiya page");
        Assert.assertTrue(chowgadiyaUIPage.isHelpIconDisplayed(),"Help icon is not displayed in chowgadiya page");
        Assert.assertTrue(chowgadiyaUIPage.isAajBtnDisplayed(),"Aaj button is not displayed in chowgadiya page");
        Assert.assertTrue(chowgadiyaUIPage.isKalBtnDisplayed(),"Kal button is not displayed in chowgadiya page");

        chowgadiyaUIPage.clickOnKalTab();
        Assert.assertTrue(chowgadiyaUIPage.isDateHighlightTabisDisplayedInKalTab(),"Date highlight tab is not displayed in kal tab");
        Assert.assertTrue(chowgadiyaUIPage.isChowgadiyaCardDisplayedInKalTab(),"chowgadiya card is not displayed in kal tab");
        Assert.assertTrue(chowgadiyaUIPage.isChowgadiyaCardTitleDisplayedInKalTab(),"chowgadiya card title is not displayed in kal tab");
        Assert.assertTrue(chowgadiyaUIPage.isChowgadiyaCardTimeDetailsDisplayedInKalTab(),"chowgadiya time details is not displayed in kal tab");
        Assert.assertTrue(chowgadiyaUIPage.isDownCheveronDisplayedInChowgadiyaCardInKalTab(),"Down cheveron is not displayed in kal tab");
        chowgadiyaUIPage.clickOnDownCheveron();
        Assert.assertTrue(chowgadiyaUIPage.isChowgadiyaCardDescriptionDisplayed(),"chowgadiya card description is not displayed in kal tab");
        Assert.assertTrue(chowgadiyaUIPage.isFloatingShareIconIsDisplayed(),"floating share icon not displayed in kal tab");

        chowgadiyaUIPage.clickOnAajTab();
        Assert.assertTrue(chowgadiyaUIPage.isTodaysChowgadiyaCardIsDisplayed(),"chowgadiya card is not displayed in aaj tab");
        Assert.assertTrue(chowgadiyaUIPage.isChowgadiyaCardTitleDisplayed(),"chowgadiya card title is not displayed in aaj tab");
        Assert.assertTrue(chowgadiyaUIPage.isChowgadiyaCardTimeDetailsDisplayed(),"chowgadiya time details  not displayed in aaj tab");
        Assert.assertTrue(chowgadiyaUIPage.isDownCheveronDisplayed(),"down cheveron is not displayed in aaj tab");
        Assert.assertTrue(chowgadiyaUIPage.isChowgadiyaCardDescriptionDisplayed(),"chowgadiya card description not displayed in aaj tab");
        Assert.assertTrue(chowgadiyaUIPage.isFloatingShareIconIsDisplayed(),"floating share icon not displayed in aaj tab");
        chowgadiyaUIPage.swipeTillAageAaneWalaChowgadiya();
        Assert.assertTrue(chowgadiyaUIPage.isAageAaneWaleChowgadiyaTitleDisplayed(),"Aage aane wala chowgadiya is not displayed in aaj tab");
        Assert.assertTrue(chowgadiyaUIPage.isAageAaneWaleChowgadiyaCardIsDisplayed(),"Aage aane wala chowgadiya card is not displayed");
        Assert.assertTrue(chowgadiyaUIPage.isAageAanewaleChowgadiyaCardTitleIsDisplayed(),"Aage aane wala chowgadiya card title not displayed in chowgadiya page");
        Assert.assertTrue(chowgadiyaUIPage.isAageAanewaleChowgadiyaCardTimeDetailsDisplayed(),"Aage aane wala chowgadiya card time details is not displayed in chowgadiya page");
        Assert.assertTrue(chowgadiyaUIPage.isAageAanewaleChowgadiyaCardDownCheveronDisplayed(),"Aage aane wala chowgadiya card down cheveron is not displayed in chowgadiya page");
        chowgadiyaUIPage.clickOnAageAanewalaChowgadiyaCardDownCheveron();
        Assert.assertTrue(chowgadiyaUIPage.isAageAaneWalaChowgadiyaCardDescriptionIsDisplayed(),"aage aane wala chowgadiya card description is not displayed in chowgadoya page");
        scrollTillEnd();
        Assert.assertTrue(chowgadiyaUIPage.isDateHighlightBannerDisplayed(),"Date highlight banner is not displayed wile scrolling in chowgadiya page");
        Assert.assertTrue(chowgadiyaUIPage.isWhatsAppShareIconDisplayed(),"Whastapp share icon is not displayed in chowgadiya page");
    }


}
