package MahabhandarUI;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import UIPages.HoraUIPage;
import UIPages.MandirUIPage;
import base.BaseTest;

@Listeners(base.ListenerImplimentationclass.class)

public class HoraUITest extends BaseTest {
	@Test(groups= {"regression","smoke"})
	public void HoraUI() throws Exception
	{
		MandirUIPage mandirUIPage =new MandirUIPage(driver);
		mandirUIPage.clickOnMahabhandarTab();
		HoraUIPage horaUIPage=new HoraUIPage(driver);
		horaUIPage.clickOnHoraTab();
		//checking if Hora title is displayed in hora Page
		Assert.assertTrue(horaUIPage.isHoraTitleDisplayed(),"Hora title is not displayed in hora page");
		//checking if  location search modal is displayed in hora page
		Assert.assertTrue(horaUIPage.isLocationSearchModalDisplayed(),"Hora title is not displayed in hora page");
		//checking floating whatsapp icon is displayed
	    Assert.assertTrue(horaUIPage.isFloatingWhatsAppIconDisplayed(), " Floating whatsapp icon is not displayed");	    
	   // checking help icon is displayed
	    Assert.assertTrue(horaUIPage.isHelpIconDisplayed(), " Help icon is not displayed");
		//checking if Date information tab is displayed
		Assert.assertTrue(horaUIPage.isDateInformationTabDisplayed(),"Date information tab is not displayed in hora page");
        // checking if planet image is displayed
		Assert.assertTrue(horaUIPage.isPlanetImageDisplayed(), "planet image is not displayed");
		//checking if Planet Name is Displayed
		Assert.assertTrue(horaUIPage.isPlanetNameDisplayed(), "planet name is not displayed");
		//checking if planet timing slot is Displayed
		Assert.assertTrue(horaUIPage.isPlanetTimingSlotDisplayed(), "planet timing slot is not Displayed");
		//checking if planet Card Details is displayed
		Assert.assertTrue(horaUIPage.isPlanetCardDetailsDisplayed(), "planet Card Details is displayed");
		//checking if Hora description is displayed
		Assert.assertTrue(horaUIPage.isHoraDescriptionDisplayed(), "Hora description is not displayed");
		//checking if Anukul text is displayed
		Assert.assertTrue(horaUIPage.isPlanetAnukulDisplayed(), "Anukul text is not displayed");
		horaUIPage.swipedownUntilPratikul();
		//checking if Pratikul text is displayed
		Assert.assertTrue(horaUIPage.isPlanetPratikulDisplayed(), "Pratikul text is not displayed");    
		horaUIPage.verticalscrollTillAageAnewaleHora();
       //checking Aage anewala hora is displayed
		Assert.assertTrue(horaUIPage.isAageanewalaHoraDisplayed(), "Aageanewala hora is not displayed");
		horaUIPage.clickOnDownChevaron();
		//checking Aageanewala PlanetImage is displayed
		Assert.assertTrue(horaUIPage.isAageanewalaDPlanetImageDisplayed(), "Aageanewala PlanetImage is not displayed");
		//checking Aageanewala PlanetName is displayed
		Assert.assertTrue(horaUIPage.isAageanewalaPlanetNameDisplayed(), "Aageanewala PlanetName is not displayed");
		//checking Aageanewala PlanetTimingSlot is displayed
		Assert.assertTrue(horaUIPage.isAageanewalaDPlanetTimingSlotDisplayed(), "Aageanewala PlanetTimingSlot is not displayed");
		//checking Aageanewala PlanetCardDetails is displayed
		Assert.assertTrue(horaUIPage.isAageanewalaPlanetCardDetailstDisplayed(), "Aageanewala planet card details is not displayed");
		//checking Aageanewala HoraDescription is displayed
		Assert.assertTrue(horaUIPage.isAageanewalaHoraDescriptionDisplayed(), "Aageanewala HoraDescription is not displayed");		
		horaUIPage.swipeupTillKal();
		horaUIPage.clickOnKalBtn();
		//clicking on kal planet down chevron
		horaUIPage.clickOnKalPlanetDownChevaron();
		//checking kal planet image is displayed
		Assert.assertTrue(horaUIPage.isKalPlanetImageDisplayed(), "kal planet image is not displayed");
		//checking kal planet Name is displayed
		Assert.assertTrue(horaUIPage.isKalPlanetNameDisplayed(), "kal planet Name is not displayed");
		//checking kal planet timing slot is displayed
		Assert.assertTrue(horaUIPage.isKalPlanetTimingSlotDisplayed(), "kal planet timing slot is not displayed");
		//checking kal planet card details is displayed
		Assert.assertTrue(horaUIPage.isKalPlanetCardDetailsDisplayed(), "kal planet card details is not displayed");
		//checking kal Hora description is displayed
		Assert.assertTrue(horaUIPage.isKalHoraDescriptionDisplayed(), "kal planet Hora description is not displayed");

}
}